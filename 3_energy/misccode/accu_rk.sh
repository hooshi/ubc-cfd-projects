#!/bin/bash 

## -------------------------------------------------------------------
## Run the accuracy checks
## -------------------------------------------------------------------

II="0 1 2 3 4"
NX=(25 50 100 200 400)
TIME_STEP=(0.05 0.025 0.008 0.002 0.0005)

rm -f sol/accu_rk.log sol/accu_rk.norm
for ii in $II
do
	echo "Running NX="${NX[ii]} "TIME_STEP="${TIME_STEP[ii]}
	./driver 0 0 .1 ${TIME_STEP[ii]} 5 1 ${NX[ii]} sol/accu_rk_${NX[ii]} \
		&>> sol/accu_rk.log 1>> sol/accu_rk.norm
done

