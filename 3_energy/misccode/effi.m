## ----------------------------------------------------------------------------
##
## effi.m 
## 
## Efficiency test plots.
## ---------------------------------------------------------------------------

## The data
nx = [25 50 100 200 400];
trk = [1.23e-02 1.28e-02 7.06e-02 9.83e-01 1.54e+01];
tim = [7.61e-03 1.40e-02 4.78e-02 4.78e-01 5.65e+00];
tef = [2.95e+00 2.58e+01 2.49e+01 2.80e+01 1.67e+02];

nrk = [823 253 431 1669 6463];
nim = [76 60 62 164 484];
nef = [63605 140447 34920 9695 14124];

## ----------------------------------------------------------------------------
##
## The run time
## ---------------------------------------------------------------------------
#plot
clf, hold on;
loglog(nx, trk , "r-o;RK2;", "linewidth", 2);
loglog(nx, tim , "b-^;Implicit, dt=0.1;", "linewidth", 2);
loglog(nx, tef , "g-s;Implicit, Huge dt;", "linewidth", 2);

##axis label and range
hx=xlabel("NX");
hy=ylabel("Run time (s)");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);

##axis label
xt = get(gca,"XTick");
set(gca,"XTickLabel", sprintf("%.0f|",xt) );
xt = get(gca,"YTick");
set(gca,"YTickLabel", sprintf("%.3f|",xt) );

##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southeast", "linewidth",2);

##save the plot
print("../plot/effi-t.eps","-deps","-FArial", "-color");


## ----------------------------------------------------------------------------
##
## The run time
## ---------------------------------------------------------------------------
#plot
clf, hold on;
loglog(nx, nrk , "r-o;RK2;", "linewidth", 2);
loglog(nx, nim , "b-^;Implicit, dt=0.1;", "linewidth", 2);
loglog(nx, nef , "g-s;Implicit, Huge dt;", "linewidth", 2);

##axis label and range
hx=xlabel("NX");
hy=ylabel("Number of iterations");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);

##axis label
xt = get(gca,"XTick");
set(gca,"XTickLabel", sprintf("%.0f|",xt) );
xt = get(gca,"YTick");
set(gca,"YTickLabel", sprintf("%.0f|",xt) );

##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "northeast", "linewidth",2);

##save the plot
print("../plot/effi-n.eps","-deps","-FArial", "-color");
