#!/bin/bash 

## -------------------------------------------------------------------
## Run the implicit wit maximum time step
## -------------------------------------------------------------------

II="0 1 2 3 4"
NX=(25 50 100 200 400)
TIME_STEP=(1000 1000 100 10 5 5)

rm -f sol/effi_im.log sol/effi_im.norm
for ii in $II
do
	echo "Running NX="${NX[ii]} "TIME_STEP="${TIME_STEP[ii]}
	./driver 0 1 .1 ${TIME_STEP[ii]} 5 1 ${NX[ii]} sol/effi_rk_${NX[ii]} \
		&>> sol/effi_im.log 1>> sol/effi_im.norm
done
