#!/bin/bash 

## -------------------------------------------------------------------
## Run the real problem
## -------------------------------------------------------------------


rm -f sol/real.log

echo "Running Real case 1"
./driver 1 1 .1 .1  120 1 4800 sol/real1 &>> sol/real.log 

echo "Running Real case 2"
./driver 1 1 .1 .1  130 2 5000 sol/real2 &>> sol/real.log 

echo "Running Real case 3"
./driver 1 1 .2 .1  120 1 4800 sol/real3 2 &>> sol/real.log 

echo "Running Real case 4 ( 1 coarse grid)"
./driver 1 1 .2 .1  120 1 2400 sol/real4 2 &>> sol/real.log 

echo "Running Real case 5 ( 1 coarser grid)"
./driver 1 1 .2 .1  120 1 1200 sol/real5 2 &>> sol/real.log 
