## ----------------------------------------------------------------------------
##
## Real.m 
## 
## Plots for the real problem.
## ---------------------------------------------------------------------------

## ----------------------------------------------------------------------------
##
## Case 1
##
## ---------------------------------------------------------------------------

## load the data
sol = load("-ascii", "../sol/real1.down");
X = sol(:,1);
dT = sol(:,2);
clear sol;

## find the length
ii = 1;
while (ii <= length(X)-1 ) 

  if ( ( dT(ii+1) - dT(ii) ) < 1e-3 *  dT(ii) * (X(2) - X(1)) ) 
	break;
  endif

  ii = ii +1;
endwhile
printf("Case one, X=%f\n", X(ii));

## plot the solution
clf, hold on;
plot(X, dT , "k", "linewidth", 2);
plot([X(ii) X(ii)], [0 dT(ii)] , "k--", "linewidth", 2);


##axis label and range
hx=xlabel("X");
hy=ylabel("Wall Temperature Gradient");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
title("H=1, T*=1, Ec=0.1");

##save the plot
print("../plot/real1.eps","-deps","-FArial");

## ----------------------------------------------------------------------------
##
## Case 2
##
## ---------------------------------------------------------------------------
## load the data
sol = load("-ascii", "../sol/real2.down");
X = sol(:,1);
dT = sol(:,2);
clear sol;

## find the length
ii = 1;
while (ii <= length(X)-1 ) 

  #if ( ( dT(ii+1) - dT(ii) ) < 2e-5 *  dT(ii)) 
  if ( ( dT(ii+1) - dT(ii) ) < 1e-3 *  dT(ii) * (X(2) - X(1)) ) 

	break;
  endif

  ii = ii +1;
endwhile
printf("Case one, X=%f\n", X(ii)/2);

## plot the solution
clf, hold on;
plot(X, dT , "k", "linewidth", 2);
plot([X(ii) X(ii)], [0 dT(ii)] , "k--", "linewidth", 2);


##axis label and range
hx=xlabel("X");
hy=ylabel("Wall Temperature Gradient");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
title("H=2, T*=1, Ec=0.1");

##save the plot
print("../plot/real2.eps","-deps","-FArial");


## ----------------------------------------------------------------------------
##
## Case 3
##
## ---------------------------------------------------------------------------
## load the data
sol = load("-ascii", "../sol/real3.down");
X = sol(:,1);
dT = sol(:,2);
clear sol;

## find the length
ii = 1;
while (ii <= length(X)-1 ) 

# if ( ( dT(ii+1) - dT(ii) ) < 2e-5 *  dT(ii)) 
  if ( ( dT(ii+1) - dT(ii) ) < 1e-3 *  dT(ii) * (X(2) - X(1)) ) 
	break;
  endif

  ii = ii +1;
endwhile
printf("Case one, X=%f\n", X(ii));

## plot the solution
clf, hold on;
plot(X, dT , "k", "linewidth", 2);
plot([X(ii) X(ii)], [0 dT(ii)] , "k--", "linewidth", 2);


##axis label and range
hx=xlabel("X");
hy=ylabel("Wall Temperature Gradient");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
title("H=1, T*=2, Ec=0.2");

##save the plot
print("../plot/real3.eps","-deps","-FArial");

