
N = [620 310 155];

l1= [1.847545e-04, 9.219908e-04 , 3.861929e-03,];
l2=[2.196007e-04,9.981565e-04, 4.118596e-03];
l3=[4.338274e-04, 1.709442e-03, 6.610723e-03];

p1 = polyfit (log10(N),  log10(l1),1);
p2 = polyfit (log10(N),  log10(l2),1);
p3 = polyfit (log10(N),  log10(l3),1);

clf, hold on;
loglog(N, l1 , ["k-;L1, slope =" num2str(p1(1)) ";"], "linewidth", 2);
loglog(N, l2 , ["k--;L2, slope =" num2str(p2(1)) ";"], "linewidth", 2);
loglog(N, l3 , ["k-.;Linf, slope =" num2str(p3(1)) ";"], "linewidth", 2);

##axis label and range
hx=xlabel("N");
hy=ylabel("Error Norm");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
title("Error norms for the exit temperature profile (X=120)");

##axis label
xt = get(gca,"XTick");
set(gca,"XTickLabel", sprintf("%.0f|",xt) );


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);

##save the plot
print("../plot/grid-conv.eps","-deps","-FArial");

##--------------------------------------------------------------------
## interpolation stuff
##--------------------------------------------------------------------
p1 = 4.770261;
p2 = 4.777353;
p3 = 4.779126;

p = log2((p1-p2)/(p2-p3));
pext = (2^p * p3 - p2 ) / (2^p -1);
eext = (pext -p3) / pext *100;
e3 = (p3-p2)/p3*100;
printf("Order is %.3f, pext: %.3f, errext: %f , err3: %f \n",...
	   p, pext, eext, e3);
