C = 'color'; c = {'b','r','g','m','c','y'};
x = [0 0]; y = [-8 8]; K = 'k'; LW = 'linewidth'; FS = 'fontsize';

plot(y,x,K,LW,1), hold on, plot(x,y,K)
t = 0:.01:2*pi;
z = exp(1i*t); r = z-1;

## plot eigenvalues of second order upwind
RE=50; PR=0.7; kappa = 1/RE/PR
xx = 0:0.01:2*pi; 
f = @(x,v,dx) v*(-1i*sin(x)) - 2*v*kappa/dx * (1-cos(x));  

## ----------------------------------------------------------------------------
##
## when forcing real axis stabilty
##
## ---------------------------------------------------------------------------

##plot stability region for rk2
clf, plot(y,x,K,LW,1), hold on, plot(x,y,K)
w = z-1;
for i = 1:3
  w = w-(1+w+.5*w.^2-z.^2)./(1+w);
end
plot(w,";RK2 Stability Region;",C,K,LW,2)              

##plot the z lines              
plot(f(xx,0.2/2/kappa,0.2),";dx=0.2;",C,c{1},LW,2);  
plot(f(xx,0.1/2/kappa,0.1),";dx=0.1;",C,c{2},LW,2);              
plot(f(xx,0.08/2/kappa,0.08),";dx=0.08;",C,c{3},LW,2);              
plot(f(xx,0.05/2/kappa,0.05),";dx=0.05;",C,c{4},LW,2);              
plot(f(xx,0.02/2/kappa,0.02),";dx=0.02;",C,c{5},LW,2);              

##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);

##title
title("Finding CFL(MAX), dx<0.1","fontsize",15);

##axis label and range
hx=xlabel("Re");
hy=ylabel("Im");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
axis([-2.5 0.5 -1.75 1.75]), axis square, grid on


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);


##save the plot
print("../plot/stability2.eps","-deps","-FArial" , "-color");



## ----------------------------------------------------------------------------
##
## RK 2 when dx = .2
##
## ---------------------------------------------------------------------------
##plot stability region for rk2
figure;
clf, plot(y,x,K,LW,1), hold on, plot(x,y,K)
w = z-1;
for i = 1:3
  w = w-(1+w+.5*w.^2-z.^2)./(1+w);
end
plot(w,";RK2 Stability Region;",C,K,LW,2)              

##plot the z lines              
plot(f(xx,1.5,0.2),";CFL=1.5;",C,c{1},LW,2);              
plot(f(xx,1.44,0.2),";CFL=1.44;",C,c{2},LW,2);              
plot(f(xx,1.3,0.2),";CFL=1.3;",C,c{3},LW,2);              


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "northwest", "linewidth",2);

##title
title("Finding CFL(MAX), dx=0.2","fontsize",15);

##axis label and range
hx=xlabel("Re");
hy=ylabel("Im");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
axis([-1 0.1 0 1.75]),  grid on


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);


##save the plot
print("../plot/stability1.eps","-deps","-FArial" , "-color");
