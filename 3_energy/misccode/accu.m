## ----------------------------------------------------------------------------
##
## Verif.m 
## 
## Creates the accuracy check cases plots.
## ---------------------------------------------------------------------------

## ----------------------------------------------------------------------------
##
## Runge kutta
##
## ---------------------------------------------------------------------------

# load the data
sol = load("-ascii", "../sol/accu_rk.norm");
N = sol(:,1);
l1 = sol(:,2);
l2 = sol(:,3);
l3 = sol(:,4);
clear sol;

## the slopes
p1 = polyfit (log10(N),  log10(l1),1);
p2 = polyfit (log10(N),  log10(l2),1);
p3 = polyfit (log10(N),  log10(l3),1);

## plot the solution
clf, hold on;
loglog(N, l1 , ["k-;L1, slope =" num2str(p1(1)) ";"], "linewidth", 2);
loglog(N, l2 , ["k--;L2, slope =" num2str(p2(1)) ";"], "linewidth", 2);
loglog(N, l3 , ["k-.;Linf, slope =" num2str(p3(1)) ";"], "linewidth", 2);

##axis label and range
hx=xlabel("N");
hy=ylabel("RK2 SS Solution Error Norms");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
axis([10 500]);

##axis label
xt = get(gca,"XTick");
set(gca,"XTickLabel", sprintf("%.0f|",xt) );


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);

##save the plot
print("../plot/accu-rk.eps","-deps","-FArial");

## ----------------------------------------------------------------------------
##
## Implicit
##
## ---------------------------------------------------------------------------

# load the data
sol = load("-ascii", "../sol/accu_im.norm");
N = sol(:,1);
l1 = sol(:,2);
l2 = sol(:,3);
l3 = sol(:,4);
clear sol;

## the slopes
p1 = polyfit (log10(N),  log10(l1),1);
p2 = polyfit (log10(N),  log10(l2),1);
p3 = polyfit (log10(N),  log10(l3),1);

## plot the solution
figure;
clf, hold on;
loglog(N, l1 , ["k-;L1, slope =" num2str(p1(1)) ";"], "linewidth", 2);
loglog(N, l2 , ["k--;L2, slope =" num2str(p2(1)) ";"], "linewidth", 2);
loglog(N, l3 , ["k-.;Linf, slope =" num2str(p3(1)) ";"], "linewidth", 2);

##axis label and range
hx=xlabel("N");
hy=ylabel("Implicit SS Solution Error Norms");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
axis([10 500]);

##axis label
xt = get(gca,"XTick");
set(gca,"XTickLabel", sprintf("%.0f|",xt) );


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);

##save the plot
print("../plot/accu-im.eps","-deps","-FArial");
