#!/bin/bash 

## -------------------------------------------------------------------
## Run the accuracy checks
## -------------------------------------------------------------------

II="0 1 2 3 4"
NX=(25 50 100 200 400)
##maximum 1000 1000 100 10
TIME_STEP=(.1 .1 .1 .1 .1 .1 .1)

rm -f sol/accu_im.log sol/accu_im.norm
for ii in $II
do
	echo "Running NX="${NX[ii]} "TIME_STEP="${TIME_STEP[ii]}
	./driver 0 1 .1 ${TIME_STEP[ii]} 5 1 ${NX[ii]} sol/accu_rk_${NX[ii]} \
		&>> sol/accu_im.log 1>> sol/accu_im.norm
done
