## ----------------------------------------------------------------------------
##
## Verif.m 
## 
## Creates the plots for the verification cases.
## ---------------------------------------------------------------------------

# load the data
sol = load("-ascii", "../sol/verif.norm");

# find the error norms and the mesh size
N = sol(:,1);
Fl1 = sol(:,2);
Fl2 = sol(:,3);
Fl3 = sol(:,4);
Sl1 = sol(:,5);
Sl2 = sol(:,6);
Sl3 = sol(:,7);

## the slopes
Fp1 = polyfit (log10(N),  log10(Fl1),1);
Fp2 = polyfit (log10(N),  log10(Fl2),1);
Fpinf = polyfit (log10(N), log10(Fl3),1);
Sp1 = polyfit (log10(N),  log10(Sl1),1);
Sp2 = polyfit (log10(N),  log10(Sl2),1);
Spinf = polyfit (log10(N), log10(Sl3),1);

clear sol;

## ----------------------------------------------------------------------------
##
## Flux 
##
## ---------------------------------------------------------------------------

clf, hold on;
loglog(N, Fl1 ,  ["k-;L1, slope =" num2str(Fp1(1)) ";"], "linewidth", 2);
loglog(N, Fl2 ,  ["k--;L2, slope =" num2str(Fp2(1)) ";"], "linewidth", 2);
loglog(N, Fl3 , ["k-.;Linf, slope =" num2str(Fpinf(1)) ";"], "linewidth", 2);

##axis label and range
hx=xlabel("N");
hy=ylabel("Flux Error Norms");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);

##axis label
xt = get(gca,"XTick");
set(gca,"XTickLabel", sprintf("%.0f|",xt) );
##axis([10 2000 ])


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);

##save the plot
print("../plot/verif-flux.eps","-deps","-FArial");

## ----------------------------------------------------------------------------
##
## Source term
##
## ---------------------------------------------------------------------------
figure;
clf, hold on;
loglog(N, Sl1 ,  ["k-;L1, slope =" num2str(Sp1(1)) ";"], "linewidth", 2);
loglog(N, Sl2 ,  ["k--;L2, slope =" num2str(Sp2(1)) ";"], "linewidth", 2);
loglog(N, Sl3 , ["k-.;Linf, slope =" num2str(Spinf(1)) ";"], "linewidth", 2);

##axis label and range
hx=xlabel("N");
hy=ylabel("Source term Error Norms");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);

##axis label
xt = get(gca,"XTick");
set(gca,"XTickLabel", sprintf("%.0f|",xt) );
##axis([10 2000 ])


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);

##save the plot
print("../plot/verif-source.eps","-deps","-FArial");
