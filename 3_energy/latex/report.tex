%% -------------------------------------------------------
% Preamable ---------------------------------------------
%--------------------------------------------------------
\documentclass[letter,12pt]{article}
%-------------------------------------------------------
 \usepackage [colorlinks, linkcolor=blue,
 citecolor=magenta, urlcolor=cyan]{hyperref}   % pdf links
% \usepackage {hyperref}
\usepackage{graphicx} % inserting image
\usepackage{setspace} % double/single spacing
\usepackage[top=2cm,right=2cm,bottom=2cm,left=2cm]{geometry} % margins 
\usepackage{amsmath}  %math formulas
\usepackage{array}    %beautiful tables
\usepackage[export]{adjustbox} %aligning images
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{color,soul} %highlight
\usepackage{subcaption}        %figures with subfigures
\usepackage{amssymb}         %fonts for N, R, ...

\graphicspath{{img/}} %where the images are

\newenvironment{tight_itemize}{
\begin{itemize}
  \setlength{\itemsep}{0pt}
  \setlength{\parskip}{0pt}
}{\end{itemize}}

\newenvironment{tight_enumerate}{
\begin{enumerate}
  \setlength{\itemsep}{0pt}
  \setlength{\parskip}{0pt}
}{\end{enumerate}}


\newcommand*\rfrac[2]{{}^{#1}\!/_{#2}}
\newcommand*{\hvec}[1]{\mathbf{#1}}
\newcommand*{\hmat}[1]{\mathbf{#1}}

% -------------------------------------------------------
% Author and title---------------------------------------
%--------------------------------------------------------
\title{Third Programming Assignment, MECH510}
\date{November 2015}
\author{Shayan Hoshyari\\ Student \#: 81382153}

% -------------------------------------------------------
% Begin document-----------------------------------------
%--------------------------------------------------------
\begin{document}
\pagenumbering{arabic}
\maketitle

% -------------------------------------------------------
% Tables--------------------------------------------------
%--------------------------------------------------------
\tableofcontents

% -------------------------------------------------------
% ch1-Problem--------------------------------------------
%--------------------------------------------------------
 \section{The Problem}
 \label{ch:problem}

In this assignment the incompressible, laminar, 2D energy equation:
\[
 T_t + \nabla\cdot(uT) = \frac{1}{\text{RePr}} \nabla^2 T +
\frac{\text{Ec}}{\text{Re}} (2u_x^2 + 2v_y^2 + (v_x+u_y)^2)
\]
Will be solved for a given velocity field. The computational domain is
a rectangular box and the velocity field is given by the
fully-developed profile:
\begin{equation*}
  \begin{aligned} 
    &u(x,y) = 18\frac{y}{H}(1-\frac{y}{H}) \\
    &v(x,y) =0 \\
  \end{aligned}
\end{equation*} Where $H$ is the channel height. The temperature will
be fixed at the inflow (different for each problem) and the walls
(lower wall: $T=0$, upper wall: $T=1$), and the temperature will be
considered fully developed at the outflow. Finally the governing
dimensionless numbers are given as: Re=$50$, Pr=$0.7$, Ec=$0.1$. To
ease the notation, two additional numbers will be defined as:
$\chi=\frac{\text{Ec}}{\text{Re}}$ and
$\kappa=\frac{1}{\text{Re}\cdot\text{Pr}}$.


% -------------------------------------------------------
% ch2-Numerical Method-----------------------------------
% --------------------------------------------------------
\section{Numerical Method}
\label{ch:numeric}

Solving the energy equation is divided into two main parts: space
discretization and time discretization. To discretize the equation in
space we use the finite volume method and the second order central
interpolation scheme to transform the original PDE into a set of
algebraic differential equations:
\[ \frac{d \hvec{T}}{d t} = \hvec{FI}(\hvec{T}) \]

%-----------------
% RK 2
%----------------
To solve this system we will use two different schemes. The first
scheme is the explicit second order Runge Kutta method (subscript $n$
means at time step $n$, and $FI$ is the flux integral plus the source
term):
\begin{equation*}
  \begin{aligned} &\hvec{T}_* = \hvec{T}_n + \frac{\Delta t}{2}
\hvec{FI}(\hvec{T}_n) \\ &\hvec{T}_{n+1} = \hvec{T}_n + \Delta t
\hvec{FI}(\hvec{T}_*)
  \end{aligned}
\end{equation*}

To find the stability bound of this method we first recall the
eigenvalues of the energy equation:
\[
 \lambda \Delta t = \nu(-I \sin \phi ) + 
\frac{2 \nu \kappa}{\Delta x} (1-\cos \phi)
\] 
Where $\nu$ is the CFL=$u_{max}\Delta t / \Delta x$ number and
$u_{max}$ is found from the velocity profile to be $4.5$. Now with the
given value of $\kappa$ we propose the following maximum stable CFL
number for the RK2 scheme:
\[
\nu_{max} = 
\begin{cases}
\frac{\Delta x}{2 \kappa} &\Delta x \leq 0.1  \\
1.44                      &\Delta x = 0.2     \\
\text{we do not care}     &\text{otherwise} \\
\end{cases}
\]
In this assignment we will consider mesh sizes $\Delta x = 0.2$,
$\Delta x = 0.1$, $\Delta x = 0.05$, \ldots, so we are not concerned
with $ 0.1 < \Delta x < 0.2$.  Figure \ref{fig:numeric-stab} shows the
RK2 stability region and the $\lambda \Delta t$ curves for different
values of $\Delta x$ while the CFL number is found using the proposed
method.  We can clearly see that all cases are stable.

\begin{figure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{stability1.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{stability2.eps}
  \end{subfigure}
  % 
  \caption{The RK2 stability curve and the $\lambda \Delta t$ curves
    for the energy equation. The left figure shows the eigenvalue curves
    when $\Delta x = 0.2$ for different CFL numbers. It is clear that
    CFL=$1.44$ is stable in this case. The right figure shows the
    eigenvalue curves for different $\Delta x$ values when the CFL number
    is set to $\frac{\Delta x}{2\kappa}$, suggesting that
    CFL=$\frac{\Delta x}{2 \kappa}$ is stable for $\Delta x \leq 0.1$}
  \label{fig:numeric-stab}
\end{figure}

%-----------------
% IMPLICIT EULER
%-----------------
The second method that we will consider is the Implicit Euler method:
\[
   \hvec{T}_{n+1} = \hvec{T}_n + \Delta t \hvec{F}(\hvec{T}_{n+1})
\]
After linearization the this method reduces to the solution of the 
following system of equations:
\[
(\frac{\hmat{I}} { \Delta t} -
\frac{\partial \hvec{FI}_n}{\partial\hvec{T}_n}) \hvec{ \delta T} =
\hvec{FI}_n
\]
We will solve this equation using the approximate factorization method.
Theoretically, this scheme should be stable for any CFL number.

To find the steady state solution we continue to advance the solution
in time until the maximum change in the solution drops below
$10^{-10}$. The ratio $\frac{\Delta x}{\Delta y}$ is always kept equal
to 2 in this assignment.
% -------------------------------------------------------
% ch3-results--------------------------------------------
% -------------------------------------------------------
\section{Results}
\label{ch:result} 

In this section the results for each part of the assignment are
presented.

%----------------------------------------------------------
% RESULT: Verification
%----------------------------------------------------------
\subsection{Flux Integral and Source Term Verification}
\label{ch:res-verif}

In this section, the method of manufactured solutions will be used to
verify the correctness of the flux integral and source term codes. The
computational domain is defined as the region: $[0 \quad 1]\times[0
\quad 1]$. The average control volume values (including ghost cells)
of temperature and velocities will be set using the following
equations:
%
\begin{equation*}
  \begin{aligned} 
    &T(x,y) = \cos(\pi x) sin(\pi y) \\
    &u(x,y) = y \sin(\pi x) \\
    &v(x,y) = x \cos(\pi y) \\
  \end{aligned}
\end{equation*}
%
Then the values $S_{ij}$ and $FI_{ij}$ will be calculated and compared
with their exact values which are found using the midpoint integration
rule and the exact values given below:
%
\[
FI = -\frac{1}{A} \int_A \left[ 
\pi y \cos(2\pi x) \sin(\pi y)
+ \pi x \cos(\pi x) \cos(2 \pi y) 
+ 2 \kappa \pi^2 \cos(\pi x) \sin(\pi y) 
\right] dA
\]
%
\[
S = -\frac{\chi}{A} \int_A \left[
2(\pi y \cos(\pi x))^2 + 
2(\pi x \sin(\pi y))^2 +
(\sin(\pi x) + \cos(\pi y) )^2
\right]dA
\]

The error norms are shown in Figure \ref{fig:verif}. With their
slopes close to two, the correctness of the code is verified.

\begin{figure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{verif-flux.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{verif-source.eps}
  \end{subfigure}
  % 
  \caption{The norms of the source term and flux integral error. The
    horizontal axis shows the number of control volumes in each direction
    and the slopes are found using least square curve fitting.}
  \label{fig:verif}
\end{figure}

%----------------------------------------------------------
% RESULT: Stability and accuracy check
%----------------------------------------------------------
\subsection{Stability and Accuracy Check}
\label{ch:res-accu}

In this section the accuracy of the explicit RK2 and the implicit
solver will be verified.

The energy equation is solved on a $5\times1$ channel, with the
initial condition of $T_0(x,y)=y$. The boundary conditions, and the
velocity field have their default values mentioned in Section
\ref{ch:problem}, while the input temperature profile is set to its
fully developed value:
%
\[
T(0,y,t) = y + \frac{27}{4}\text{Pr}\cdot\text{Ec} 
\left[ 1 - (1-2y)^ 4 \right]
\]

The PDE is solved to the steady state on a series of refined meshes
($N_x=25,50,100,200,400$) and the error norms of the steady state
solution are shown in Figure \ref{fig:accu}. $N_x$ and $N_y$ are the
number of CV's in the $x$ and $y$ directions respectively. It is
clearly seen that the error curves have slopes close to two, so the
solutions are truly second order.

\begin{figure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{accu-rk.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{accu-im.eps}
  \end{subfigure}
  % 
  \caption{The almost identical error norms of the steady state
    solution when the steady state temperature profile is used as the
    inlet boundary condition. As the domain is not a square, the
    horizontal axis shows the average value of $\bar{N}=\sqrt{N_x
      N_y}$. The implicit solutions are obtained by using a time step of
    $0.1$, while the explicit solver used the maximum stable time step
    obtained from the stability analysis and a safety factor of $80\%$.}
  \label{fig:accu}
\end{figure}

%----------------------------------------------------------
% RESULT: Efficiency Test
%----------------------------------------------------------
\subsection{Efficiency Test}
\label{ch:res-effi}

In this section the efficiency of the explicit and implicit methods
and the effect of time step selection for the implicit method are
studied. We will provide further information about the run time,
time step and the number of iterations of the test cases solved in
Section \ref{ch:res-accu}.
 
For the implicit solver, I considered two cases. In the first case I
just used the well performing time step of $\Delta t = 0.1$ for all
the meshes. In the second case I tried to use the largest time step
possible.  I realized that when the time step is increased too much
the run time gets ridiculously long. I tried to choose as large time
steps as my patience to wait for the solution allowed. It was 
interesting that the solution did not get unbounded in any case, it
just did not converge (or converged really slowly).

For the explicit solver, the time step dictated by the stability
analysis with a safety factor of $80\%$ performed well in all cases
except $N_x=50$. In this case I had to use a safety factor of $62\%$.
The time steps used for each case are shown in Table \ref{tab:effi}
and Figure \ref{fig:effi} shows the run time and number of iterations
for each case. Three interesting remarks can be made about these
results:


%itemize ----------------------------------------------------
\begin{itemize}

\item Increasing the time step of the Implicit Euler scheme more than
a certain limit makes the performance even worse than the explicit RK2
scheme, while the stability analysis guarantees stability for any
$\Delta t$. This inconsistency can be explained by the fact that the
stability analysis assumes no source terms, periodic boundary
conditions, a one-dimensional problem and constant speed. Violating
these conditions might be the reason for this
inconsistency. Alternatively, the approximate factorization method for
the solution of the equation \\ $(\frac{\hmat{I}} { \Delta t} -
\frac{\partial \hvec{FI}}{\partial\hvec{T}}) \hvec{ \delta T} =
\hvec{FI}$ with a too big time step reverts to a Jacobi like
ADI method.  The ADI method is known to perform well for diagonally
dominant matrices. The presence of the $\frac{\hmat{I}} { \Delta
t}$ term is in fact making the equation diagonally dominant.  When we
increase  $\Delta t$ too much we will loose the diagonal dominancy,
which results in poor convergence.

\item RK2 time step limit increases from $N_x=25$ to $N_x=50$. This
can be explained by Figure \ref{fig:numeric-stab}. When $N_x=25$,
i.e., $\Delta x=0.1$ the upper part of the $\lambda \Delta t$ ellipse
causes the instability while for $\Delta x\geq0.2$ the left side of
the $\lambda \Delta t$ ellipse causes the instability. This change in
the behavior of the $\lambda \Delta t$ curve is the reason for the
reverse behavior of maximum stable time step.

\item The superiority of the implicit method becomes more evident for
fine meshes.

\end{itemize}

\begin{figure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{effi-t.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{effi-n.eps}
  \end{subfigure}
  % 
  \caption{The number of iterations and the run time versus mesh
    size. Although the explicit and implicit methods perform almost the
    same for small meshes, the former performs significantly more
    efficiently than the latter for fine meshes, when a reasonable time
    step is chosen. For huge time steps the performance of the implicit
    method degrades immensely.}
 \label{fig:effi}
\end{figure}

\begin{table} \centering
  \caption{The time steps for the efficiency test cases}
  \label{tab:effi}
  \begin{tabular}{c c c c c c} 
    \hline 
    Mesh Size &$25\times10$
    &$50\times20$ &$100\times40$ &$200\times80$ &$400\times160$\\ 
    \hline
    RK2 &0.050 &0.0250 &0.008 &0.002 &0.0005 \\ 
    Implicit with huge $\Delta t$&1000 &1000 &100 &10 &5 \\ 
    \hline
  \end{tabular}
\end{table}


%----------------------------------------------------------
% RESULT: The real problem
%----------------------------------------------------------
\subsection{The Real Problem}
\label{ch:res-real}

In this section the inflow boundary condition is changed to $T(0,y,t)
= y/H$, where $H$ is the height of the channel. Our goal here is to
find (a) the fully-developed temperature gradient and (b) the thermal
development length of the channel. To achieve this goal I will use a
rather brute force method: I will increase the length of the channel
to the value of $120$!, so that I am sure that my outflow boundary
conditions are correct. Then I will study the flow inside this long
channel.  The reason to choose this length is that the outlet
temperature profile for a length of $L=60$ looked similar to the
analytic fully developed profile to me, so I chose a safety factor
of two to come up with $L=120$.

Firstly, I will plot the derivative of the temperature at the bottom
wall as a function of $x$. Using this derivative I will define the fully
developed length as the length at which the change in the derivative
becomes relatively small. Mathematically:
\[ 
T_x(x_{i+1},0)-T_x(x_i,0) < 10^{-3}T_x(x_i,0) \Delta x \quad x_i>L_{d}
\] 
Where $L_d$ is the development length.

I will use this definition to find the ratio of development length to
channel height for three cases (a) Ec=$0.1$, $H=1$, $T^*=1$ (b)
Ec=$0.1$, $H=2$, $T^*=1$ (c) Ec=$0.2$, $H=1$, $T^*=2$. Where $H$ is
the channel height and $T^*$ is the upper wall temperature. It is
expected that all three cases result in the same $\rfrac{L_d}{H}$.

The value of temperature gradient at the bottom wall is shown in
Figure \ref{fig:real-der} for each case. My definition results in the
following values for $\rfrac{L_d}{H}$ for cases (a), (b) and (c)
respectively: $44$, $51$, $50$ (a mesh size of $4800\times80$ is used
for  all cases). These values are close to each other although they are
not identical. This difference may be due to the fact that the
definition of development length is somehow approximate and we should
consider some tolerance for it.

\begin{figure}
\center
  \begin{subfigure}{0.7\textwidth}
    \includegraphics[width=.7\linewidth,center]{real1.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.7\textwidth}
    \includegraphics[width=.7\linewidth,center]{real2.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.7\textwidth}
    \includegraphics[width=.7\linewidth,center]{real3.eps}
  \end{subfigure}
  % 
  \caption{The bottom wall temperature gradient versus $x$, for the real
    problem cases (a), (b) and (c). The dashes show the location of
    development length.}
 \label{fig:real-der}
\end{figure}

Now I return to case (a) to evaluate the steady state wall temperature
gradient (Note that we know  the analytic value of this property
is 4.78). As the development length was found to be less than the
length of the channel, the value of this gradient at the end of the
channel ($x=120$) is definitely the fully developed value, so I will
consider the wall temperature gradient at the last cell as the fully
developed temperature gradient and use Richardson Extrapolation to
investigate grid convergence.

I will show the fully developed wall temperature gradient by $p$ and
the order of convergence by $r$. The values $p_1$, $p_2$ and $p_3$ are
found for mesh sizes of $4800\times80$, $2400\times40$ and
$1200\times20$ respectively:
%
\[p_1 = 4.770261, \quad p_2 = 4.777353, \quad p_3 = 4.779126\]
%
Now the order of convergence will be:
\[r = \log_2 \left( \frac{p_1-p_2}{p_2-p_3} \right) = 2.00\] 
And the extrapolated value for wall temperature gradient is:
\[p_{ext} = \frac{2^r p_3 - p_2}{2^r - 1} = 4.780\] 
We can find both the approximate and exact error for this value:
\[e_{approximate} = \frac{p-p_{ext}}{p_{ext}} \times 100 = 0.012 \% \]
\[e_{exact} = \frac{p-4.78}{4.78} \times 100 = 0.000 \% \]

Alternatively, the norm of the difference between the outlet temperature
profile and the exact fully developed profile can be calculated and plotted 
versus the average mesh size $\sqrt{N_xN_y}$. This plot is shown in Figure
\ref{fig:real-norm}. It is clear that the outlet temperature profile is second order accurate.

\begin{figure}
\center
  \begin{subfigure}{0.7\textwidth}
    \includegraphics[width=.7\linewidth,center]{grid-conv.eps}
  \end{subfigure}
  \caption{The error norms for the temperature profile at outlet
    ($x=120$) for the real problem, case (a). It is clearly seen that the
    results are second order accurate.}
 \label{fig:real-norm}
\end{figure}


% -------------------------------------------------------
% ch4-Conclusion-----------------------------------------
%--------------------------------------------------------
\section{Conclusion}
\label{ch:conclusion}

In this assignment the incompressible energy equation was solved using
the finite volume method. The fluxes were evaluated using the central
approximation scheme while the temporal discretization was done using
the RK2 and the Implicit Euler methods.

Firstly, the numerical method was verified to be second order
accurate. Then the performance of the explicit and implicit schemes
were compared. It was shown that the implicit scheme performs more
efficiently than the RK2 explicit scheme provided that a reasonable
time step is chosen. Choosing a huge time step for the implicit scheme
might not cause instability, but it increases the run time immensely.

Finally, the development of the temperature profile in a channel was
studied and the temperature gradient at the bottom wall was
approximated.  The approximation was found to be second order
accurate. The ratio of the development length to channel height was
also found to be around 50.

\end{document}