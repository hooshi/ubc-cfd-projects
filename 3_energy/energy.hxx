/* energy.hxx
 *
 * header file for functions in energy project.
 */

#ifndef ENERGY_HXX
#define ENERGY_HXX

#include <cmath>
#include <cassert>
#include <cstring>
#include <cstdio>
#include <cmath>

#define PI 3.141592653589793238462643383
#define ABS(x) ( ((x)>0) ? (x) : (-(x)) )
#define MAX(x,y) ( ((x)>(y)) ? (x) : (y) )

#define dirX 0
#define dirY 1

#define Ax 0
#define Bx 1
#define Cx 2

#define Ay 0
#define By 1
#define Cy 2

#define At 0
#define Bt 1
#define Ct 2
#define Dt 3
#define Et 4


/****************************************************************************
 *                                    Grid                                  *
 ****************************************************************************/
/* Grid
 *
 *  Saves a two-dimensional grid.
 */
class Grid{
private:
	int inx_, iny_;         //number of cells (including ghosts)
	double dxmax_, dymax_;  //domain sizes
	double ddx_, ddy_; //mesh sizes in each direction
	Grid() {};        //prohibited constructor

public:
	// Creates the grid
	// param inx: number of cells in x direction excluding ghosts
	// param iny: number of cells in y direction excluding ghosts
	// param dxmax: domain size in x direction
	// param dymax: domain size in y direction
	Grid(const int inx, const double dxmax,
		 const int iny, const double dymax);

	// Refines the mesh
	// param inx: new number of cells in x direction excluding ghosts
	// param iny: new number of cells in y direction excluding ghosts
	void vRefine(const int inx, const int iny);
	
	// Returns number of cells including ghosts in the x direction
	int iNCX() const;

	// Returns number of cells including ghosts in the y direction
	int iNCY() const;

	// Returns mesh size in x direction
	double dDx() const;

	// Returns mesh size in y direction
	double dDy() const;

	// Finds the cell center coordinates
	// param ii: column numer of the cell
	// param jj: row number of the cell
	// param adcoord: the coordinates to be returned
	void vCellCent(const int ii, const int jj, double* adcoord) const;

	// Finds the lower left and upper right vertex coordinates of a cell
	// param ii: column numer of the cell
	// param jj: row number of the cell
	// param adcoord: the coordinates to be returned the format is
	//          [xlowerleft] [ylowerleft] [xupperright] [yupperright]
	void vCellVerts(const int ii, const int jj, double* adcoord) const;
	
	// Writes a vtk file
	// param num: the values of solution
	// param err: the values of error (can be given as NULL)
	// param namevtk: name of the vtk file
	void  vWriteVtk(double *num, double *err, const char *namevtk) const;

	/* New for the navier stokes */
	int iNFX() const { return inx_ + 1;}
	int iNFY() const { return iny_ + 1;}

	void vCLeft(const int iface, const int jj, int &ii) const{
		assert( (iface > 0) && (iface < iNFX()) );
		ii = iface-1;
	}
	void vCRight(const int iface, const int jj, int &ii) const{
		assert( (iface >= 0) && (iface < iNFX()-1) );
		ii = iface;
	}
	void vCTop(const int ii, const int jface, int &jj) const{
		assert( (jface >= 0) && (jface < iNFY()-1) );
		jj = jface;
	}
	void vCBott(const int ii, const int jface, int &jj) const {
		assert( (jface > 0) && (jface < iNFY()) );
		jj = jface-1;
	}
	
};

/****************************************************************************
 *                            Velocity Field                                *
 ****************************************************************************/

/* Velocity Field
 *
 * Defines a velocity field in the computational domain.
 * Using polymorphism different velocity fields can be used.
 */
class VelField{
protected:
	double du0, dv0;
	VelField():du0(0), dv0(0) {}
public:
	virtual ~VelField(){}

	// Returns the velocity at a certain point
	// param adcoord: coordinates of the point
	// param du: the u velocity
	// param dv: the v velocity
	virtual void vVels (const double* adcoord, double& du, double& dv) const = 0;

	// Returns a name for the Object that can be printed on the screen
	virtual void vName (char *) const = 0;
};

/* Test Velocity Field
 *
 * This field corresponds to:
 *  u = u0 y sin(pi x)
 *  v = v0 x cos(pi y) 
 */
class VelFieldTest:public VelField{
protected:
	VelFieldTest(){}
public:
	friend VelField* factory_VelField(char const *);
    void vVels(const double* dcoord, double& du, double& dv) const{
		du = du0 * dcoord[dirY] * sin(PI * dcoord[dirX]);
		dv = dv0 * dcoord[dirX] * cos(PI * dcoord[dirY]);
	}
	void vName (char * str) const {
		sprintf(str, "test u0=%lf v0=%lf", du0, dv0);
	}
};

/* Channel Velocity Field
 *
 * This field corresponds to:
 *  u = 6 ubar y/H (1- y/H)
 *  v = 0
 */
class VelFieldChannel: public VelField{
protected:
	double dheight; //height of the channel
	VelFieldChannel(){}
public:
	friend VelField* factory_VelField(char const *);
	void vVels(const double* dcoord, double& du, double& dv) const{
		du = 6 * du0 * (dcoord[dirY]/dheight) * (1-dcoord[dirY]/dheight);
		dv = 0;
	}
	void vName (char * str) const {
		sprintf(str, "channel u = 6*(%lf)*(y/%.3lf)*(1-(y/%.3lf)) v=0", du0, dheight, dheight);
	}
};


/* Factory that creats a velocity field
 * The input is a string, and the format should be:
 *
 * t<u0>,<v0>: creates a VelFieldTest with u0 and v0 values assigned accordingly.
 * c<u0>,<H>: creates a VelFieldChannel with u0 and Height assigned accordingly.
 *
 * Output: a VelField pointer, pointing to the correct object.
 */
VelField* factory_VelField(char const *);


/****************************************************************************
 *                            Boundary Patch                                *
 ****************************************************************************/

/* Boundary Patch
 *
 * This class stores the boundary conditions for a boundary. This class in
 * polymorphic so different boundary conditions can be used.
 *
 * The BPatch is not a pure virtual class. It corresponds to a boundary with
 * constant value of derivative or solution on it.
 */
class BPatch{
protected:
	double dval;
	BPatch():dval(0), ctype(dirich) {}
public:
	// This enum is used to identify the 4 boundaries of the domain.
	enum whichWall {right=0, up=1, left=2, down=3};
	// This enum distinguishes Neumann and Dirichlet Boundary conditions.
	enum conType {neumann=0, dirich};

	// Stores the type of the boundary (Neumann or Dirichlet).
    conType ctype;

	// The factory class has to be a friend.
	friend BPatch* factory_BPatch(char const *);

	// Returns the value of solution or the derivative on the boundary
	// param the location on the boundary
	virtual double dVal (const double) const{
		return dval;
	}

	// Returns a string as the name of the object so it can be printed on the screen.
	virtual void vName (char *str) const {
		sprintf(str, "bpatch, type=%s, f(x)=(%lf)", (ctype ? "DIRICH" : "NEUMANN"), dval);
	}
	
	//destructor
    virtual ~BPatch(){}

};


/* BpatchSine
 *
 * Corresponds to the value of boundary condition:
 * f(z)=(val)sin(pi*z)
 */
class BPatchSine:public BPatch{
protected:
	BPatchSine(){}
public:
	friend BPatch* factory_BPatch(char const *);

	double dVal (const double dloc) const{
		return dval*sin(PI*dloc);
	}
	void vName (char *str) const {
		sprintf(str, "bpatch, type=%s, f(z)=(%lf)sin(pi*z)",  (ctype ? "DIRICH" : "NEUMANN"), dval);
	}
};

/* BpatchQuad
 *
 * Corresponds to the value of boundary condition:
 *  f(z)=z + (val)(1-(1-2z)^4)
 */
class BPatchQuad:public BPatch{
protected:
	BPatchQuad(){}
public:
	friend BPatch* factory_BPatch(char const *);

    double dVal (const double dloc) const{
		return dloc + dval * (1 - pow(1-2*dloc,4) );
	}
	virtual void vName (char * str) const {
		sprintf(str, "bpatch, type=%s, f(z)=z + (%lf)(1-(1-2z)^4)", (ctype ? "DIRICH" : "NEUMANN"), dval);
	}
};

/* BpatchLin
 *
 * Corresponds to the value of boundary condition:
 *  f(z)= val(z)
 */
class BPatchLin:public BPatch{
protected:
	BPatchLin(){}
public:
	friend BPatch* factory_BPatch(char const *);

    double dVal (const double dloc) const{
		return dval* dloc;
	}
	virtual void vName (char * str) const {
		sprintf(str, "bpatch, type=%s, f(z)=(%lf)z", (ctype ? "DIRICH" : "NEUMANN"), dval);
	}
};


/* Factory that creats a boundary patch.
 * The input is a string, and the format should be:
 *
 * [n,d]c<val>: creates a neumann or dirichlet boundary condition with a
 *              constant value for the derivative or the solution on the
 *              whole boundary.
 * [n,d]s<val>: creates a neumann or dirichlet boundary condition with
 *              the value of derivative or solution equal to val*sin(pi*z)
 *              where z is either x or y.
 * [n,d]r<val>: creates a neumann or dirichlet boundary condition with
 *              the value of derivative or solution equal to z + (val)(1-(1-2z)^4)
 *              where z is either x or y.
 * [n,d]l<val>: creates a neumann or dirichlet boundary condition with
 *              the value of derivative or solution equal to val * z
 *              where z is either x or y.
 *
 * Output: a BPatch pointer, pointing to the correct object.
 */
BPatch* factory_BPatch(char const *);

/****************************************************************************
 *                                  Physics                                 *
 ****************************************************************************/
struct FITS{
	int M,N;
	int idir;
	int ii, iil, iir;
	int jj, jjt, jjb;
	double dsoll, dsolr, dsolt, dsolb;
	double ddx, ddy;
};

/* Physics
 *
 * Defines the model that has to be solved.
 */
class Physics{
private:
	Physics();
public:

	// Boundary conditions
	BPatch *bnd[4];

	// The velocity field.
	VelField *vfield;

	// Arrays to store the velocities and the source term.
	double *adu, *adv, *ads;

	// Dimensionless numbers
	double dec, //Eckert
		dpr,    //Prandtl
		dre,    //Reynolds
		dk,     //Kappa = 1 / Re / Pr
		dc;     //Chi = Ec/Re

	/* Constructor.
	 *
	 * input strvfield: a string containing the appropriate command to create the
	 *       the velocity field.
	 * input astrbnd: an array of strings containing the commands to create
	 *       each boundary condition.
	 * input dec_, dpr_, dre_: the dimensionless numbers.
	 * input gr: the grid object.
	 */ 
	Physics(const char * strvfield, char const * const * astrbnd,
			const double dec_, const double dpr_, const double dre_, const Grid &gr);

	//Destructor
	~Physics();
	
	/* Stores the velocity values and stores them in adu and adv.
	 * input gr: the grid object.
	 */
	void vInitVelocity(const Grid &gr);
	
	/* Stores the source term in the ads
	 * input gr: the grid object.
	 */
	void vInitSource(const Grid &gr);

	/* Sets the ghost for any solution.
	 * input dsol: an array containing the solution for which we want to set the ghost cells.
	 * input gr: the grid object.
	 */
	void vSetGhosts(double *dsol, const Grid &gr) const;

	/* Finds the flux integral.
	 * input adsol: an array containing the solution.
	 * input gr: the grid object.
	 * output adflux: will store the flux integral.
	 */
	void vFluxInteg(const double *adsol, const Grid &gr, double *adflux) const;

	/* Finds the matrix [Dx].
	 * input jj: the row number for which we want the jacobian.
	 * input gr: the grid object.
	 * input ddtime: the time step.
	 * returns a2djac: a two dimensional array storing the jacobian.
	 */
	void vJacX(const int jj, const Grid &gr, const double ddtime, double *a2djac[3]) const;

	/* Finds the matrix [Dy].
	 * input ii: the column number for which we want the jacobian.
	 * input gr: the grid object.
	 * input ddtime: the time step.
	 * returns a2djac: a two dimensional array storing the jacobian.
	 */
	void vJacY(const int ii, const Grid &gr, const double ddtime, double *a2djac[3]) const;

	/* new for navier stokes */
	void vLocJacX(FITS *q, double &djl, double &djr) const{
		double dul = adu[q->jj * q->N + q->iil];
		double dur = adu[q->jj * q->N + q->iir];

		djl = -dul / 2. - dk / q->ddx;
		djr = -dur / 2. + dk / q->ddx;

	}
	
	void vLocJacY(FITS *q, double &djb, double &djt) const{
		double dvt = adv[q->jjt * q->N + q->ii];
		double dvb = adv[q->jjb * q->N + q->ii];

		djb = -dvb / 2. - dk / q->ddy;
		djt = -dvt / 2. + dk / q->ddy;;
 	}
	
	void vFluxX(FITS *q, double &dflux) const{
		double dul = adu[q->jj * q->N + q->iil];
		double dur = adu[q->jj * q->N + q->iir];
		
		dflux = -(dul * q->dsoll + dur * q->dsolr) / 2. +
			dk * (q->dsolr - q->dsoll) / q->ddx ;

	}
	void vFluxY(FITS *q, double &dflux) const{
		double dvt = adv[q->jjt * q->N + q->ii];
		double dvb = adv[q->jjb * q->N + q->ii];
		
		dflux = -(dvb * q->dsolb + dvt * q->dsolt) / 2. +
			dk * (q->dsolt - q->dsolb) / q->ddy ;

	}
};

/****************************************************************************
 *                                  Integrators                             *
 ****************************************************************************/
void vFluxAssem(const Physics &ph, const Grid &gr,
				const double *adsol, double *adflux);

void vJacAssemX(const int jj, const Physics &ph, const Grid &gr,
				const double ddt, const double *adsol, double a2djac[][3]);

void vJacAssemY(const int ii, const Physics &ph, const Grid &gr,
				const double ddt, const double *adsol, double a2djac[][3]);

/****************************************************************************
 *                                  Memory                                  *
 ****************************************************************************/
/* Memory
 *
 * This struct stores the solution and the additional data that
 * the time advance mathod might require, e.g. the flux integral
 * and an array to store the jacobians. Using this method
 * instead of creating the data in the time advance functions
 * instead has the advantage that the memory is created only once.
 */
struct Memory{
	
	double *adsol, /* solution */
		*adflux,   /* flux integral */
		*adsolcol, /* an array to extract a column of the solution */
		*adsolrk,  /* intermediate solution for RK2 */ 
		*a2djac[3], /* an array to store the jacobian */
		(*a2dNEWjac)[3] ; /* the new jacobian */

	Memory() {vNullify();}

	// Set all the array to NULL
	void vNullify(){
		adsol = adflux = adsolcol = adsolrk = (double*)NULL;
		//a2djac[0] = a2djac[1] = a2djac[2] = (double*) NULL;
		a2dNEWjac = (double(*)[3]) NULL;
	}

	// Frees the memory
	// This function always has to be called before the end of program.
	void vDestroy(){
		if(adsol) delete[] adsol;
		if(adflux) delete[] adflux;
		if(adsolcol) delete[] adsolcol;
		if(adsolrk) delete[] adsolrk;

		if(a2djac[0]) delete[] a2djac[0];
		if(a2djac[1]) delete[] a2djac[1];
		if(a2djac[2]) delete[] a2djac[2];

		if (a2dNEWjac) delete[] a2dNEWjac;
	}
};

/****************************************************************************
 *                                  TimeAdvance                             *
 ****************************************************************************/
/* Memory initializers.
 *
 * Before calling a time advance function a memory has to be created.
 * These functions take care of that. The memory always has to be
 * destroyed after creation using Memory::vDestroy().
 *
 * param Grid the computational grid
 * param Memory the memory to be initialized
 */
void vInitMemRK2(const Grid&, Memory *);
void vInitMemImplict(const Grid&, Memory *);

/* Time advance functions.
 *
 * These functions advance the solution in time for one time step.
 *
 * param Grid
 * param Physics 
 * param ddtime the time step
 * param mem a memory object that stores the required data for the
 *       time advance method. Note that the memory should be initialized
 *       before it can be used.
 * returns The maximum change in the solution
 */
double vTimeAdvRK2(const Grid&, const Physics& ph, const double ddtime, Memory * mem);
double vTimeAdvImplicit(const Grid& gr, const Physics& ph, const double ddtime, Memory * mem);

/****************************************************************************
 *                                  Thomas                                  *
 ****************************************************************************/

/* Solve a tri-diagonal system Ax = b.
 * 
 * Uses the Thomas algorithm, which is Gauss elimination and back
 * substitution specialized for a tri-diagonal matrix.
 *
 * Input:
 *  LHS: The matrix A.  The three columns are the three non-zero diagonals.
 *       In LHS[j][i] i and j mean:
 *       i = row number
 *       j = 0 : left to diag, 1 : on diag, 2: right to diag
 *  RHS: The vector b.
 *  iSize: Number of equations.  For situations with BC's, those are included
 *     in the count.
 *
 * Output:
 *  LHS: Garbled.
 *  RHS: The solution x.
 */
void vSolveThomas(double *LHS[3], double *RHS, const int iSize);

// In this version LHS[i][j] is reversed
void vSolveThomasBlock(double LHS[][3], double RHS[], const int iSize);
	
#endif

