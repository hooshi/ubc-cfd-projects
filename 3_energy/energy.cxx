/* energy.hxx
 *
 * Definitions for functions in energy project.
 */

#include "energy.hxx"
#include "visit_writer.hxx"
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstring>

/****************************************************************************
 *                                    Grid                                  *
 ****************************************************************************/

Grid::Grid(const int inx, const double dxmax,
		   const int iny, const double dymax){
	inx_ = inx + 2;
	iny_ = iny + 2;
	dxmax_ = dxmax;
	dymax_ = dymax;
	ddx_ = dxmax_ / inx;
	ddy_ = dymax_ / iny;

	fprintf(stderr, "Creating Grid NX=%d NY=%d Nbar=%lf \n\t Xmax=%lf Ymax=%lf dx=%lf dy=%lf dx/dy=%lf\n",
			inx, iny, sqrt(inx*iny), dxmax_, dymax_, ddx_, ddy_, ddx_/ddy_);
}

void Grid::vRefine(const int inx, const int iny){
	inx_ = inx + 2;
	iny_ = iny + 2;
	ddx_ = dxmax_ / inx;
	ddy_ = dymax_ / iny;

}
	
int Grid::iNCX() const {return inx_; }

int Grid::iNCY() const {return iny_; }

double Grid::dDx() const {return ddx_;}
double Grid::dDy() const {return ddy_;}

// coordinates
void Grid::vCellCent(const int ii, const int jj, double* adcoord) const{
	assert( (ii < inx_) && (jj < iny_) );
	adcoord[dirX] = (ii - 0.5) * ddx_;
	adcoord[dirY] = (jj - 0.5) * ddy_;
}
void Grid::vCellVerts(const int ii, const int jj, double* adcoord) const{
	assert( (ii < inx_) && (jj < iny_) );
	adcoord[0*2 + dirX] = (ii - 1) * ddx_;
	adcoord[0*2 + dirY] = (jj - 1) * ddy_;
	adcoord[1*2 + dirX] = ii  * ddx_ ;
	adcoord[1*2 + dirY] = jj  * ddy_ ;	
}

// write a vtk file
void  Grid::vWriteVtk(double *num, double *err, const char *namevtk) const{
	
	int dims[] = {inx_-1, iny_-1, 1}, k;
	float *x, *y, z[] = {0};
	int nvars = (err ? 2 : 1);
	int vardim[] = {1,1};
	int centering[] = {0,0};
	float *vars[2];
	const char varname1[] = "Numerical";
	const char varname2[] = "Error";
	const char *varnames[] = {varname1, varname2};

	//allocate memory
	x = new float[inx_-1];
	y = new float[iny_-1];
	vars[0] = new float[ (inx_-2) * (iny_-2)];
	if (err) vars[1] = new float[(inx_-2) * (iny_-2)];

	//copy data

	for (int i = 0 ; i < inx_-1 ; i++)
		x[i] = (float) dxmax_ * i / (inx_-2);
	for (int j = 0 ; j < iny_-1 ; j++)
		y[j] = (float) dymax_ * j / (iny_-2);

	k =0;
	for (int j = 1 ; j < iny_-1 ; j++){
		for (int i = 1 ; i < inx_-1 ; i++){
			vars[0][k] = (float)num[j*inx_+i];
			if (err) vars[1][k] = (float)err[j*inx_+i];
			k++;
		}
	}

	//write to a vtk file
	write_rectilinear_mesh(namevtk, 0, dims, x, y, z, nvars, vardim, centering, varnames, vars);

	//free memory
	delete[] x;
	delete[] y;
	delete[] vars[0];
	if (err) delete[] vars[1];
}


/****************************************************************************
 *                                    Physics                               *
 ****************************************************************************/

Physics::Physics(const char * strvfield, char const * const * astrbnd,
				 const double dec_, const double dpr_, const double dre_, const Grid &gr)
	:dec(dec_), dpr(dpr_), dre(dre_)
{

	fprintf(stderr, "Creating physics: Ec=%lf, Re=%lf, Pr=%lf\n", dec_, dre_, dpr_);
	//create boundary patches
	bnd[BPatch::right] = factory_BPatch(astrbnd[BPatch::right]);
	bnd[BPatch::up] = factory_BPatch(astrbnd[BPatch::up]);
	bnd[BPatch::left] = factory_BPatch(astrbnd[BPatch::left]);
	bnd[BPatch::down] = factory_BPatch(astrbnd[BPatch::down]);

	//create the velocity field
	vfield = factory_VelField(strvfield);

	//set \kappa and \chi dimensionless numbers
	dk = 1 / dre / dpr;
	dc = dec / dre ;

	//create the source and velocity averages
	adv = new double [gr.iNCX() * gr.iNCY()];
	adu = new double [gr.iNCX() * gr.iNCY()];
	ads = new double [gr.iNCX() * gr.iNCY()];		
	vInitVelocity(gr);
	vInitSource(gr);
}

Physics::~Physics(){

	//delete velocity field
	delete vfield;

	//delete boundary patches
	delete bnd[0];
	delete bnd[1];
	delete bnd[2];
	delete bnd[3];

	//delete velocity and source terms
	delete[] adv;
	delete[] adu;
	delete[] ads;
}

void Physics::vInitVelocity(const Grid &gr){
	int ii, jj, N=gr.iNCX(), M=gr.iNCY();
	double dcent[2];
	
	// find the average velocity values for all cells including ghosts
	// using the midpoint integration rule
	for (jj = 0 ; jj < M ; jj++){
		for (ii=0 ; ii < N ; ii++){
			gr.vCellCent(ii, jj, dcent);
			vfield->vVels(dcent, adu[jj*N + ii], adv[jj*N + ii]);
		}
	}
}

void Physics::vInitSource(const Grid &gr){
	int ii, jj, N=gr.iNCX(), M=gr.iNCY();
	double dux, duy, dvx, dvy;
	
	// find the source term. Do not include ghost cells
	for (jj = 1 ; jj < M-1 ; jj++){
		for (ii=1 ; ii < N-1 ; ii++){
		    dux = (adu[jj*N + ii + 1] - adu[jj*N + ii - 1]) / 2. / gr.dDx();
		    dvx = (adv[jj*N + ii + 1] - adv[jj*N + ii - 1]) / 2. / gr.dDx();

			duy = (adu[(jj+1)*N + ii] - adu[(jj-1)*N + ii]) / 2. / gr.dDy();
			dvy = (adv[(jj+1)*N + ii] - adv[(jj-1)*N + ii]) / 2. / gr.dDy();

			ads[jj*N + ii] = dc * ( 2*dux*dux + 2*dvy*dvy + (dvx+duy)*(dvx+duy) );
		}
	}	
}

static double dGhost(const BPatch::conType etype, const BPatch::whichWall edir,
					 const double dsoladj, const double dvalbnd, const double ddelta){

	if ( etype == BPatch::neumann){
		
		double ddir;
		//find the direction
		if ( (edir == BPatch::left) || (edir == BPatch::down) )
			ddir = -1;
		else
			ddir = 1;
		//return the ghost value
		return dsoladj + ddir * dvalbnd * ddelta;
		
	}else{
		return 2*dvalbnd - dsoladj;
	}
}
void Physics::vSetGhosts(double *dsol, const Grid &gr) const{
	int ii, jj, N=gr.iNCX(), M=gr.iNCY();
	double  dvalbnd, adcoord[2];

	//find the ghost values for left and right walls
	for (jj = 1 ; jj < M-1 ; jj++){

		//find the y value that we are in
		gr.vCellCent(0, jj, adcoord);
		
		//left wall
		dvalbnd = bnd[BPatch::left]->dVal(adcoord[dirY]);		
		dsol[jj*N + 0] = dGhost(bnd[BPatch::left]->ctype, BPatch::left, dsol[jj*N+1], dvalbnd, gr.dDx());
		
		//right wall
		dvalbnd = bnd[BPatch::right]->dVal(adcoord[dirY]);		
		dsol[jj*N + (N-1)] = dGhost(bnd[BPatch::right]->ctype, BPatch::right, dsol[jj*N+(N-2)], dvalbnd, gr.dDx());
	}

	//find the ghost values for the up and down walls
	for (ii = 1 ; ii < N-1 ; ii++){
		
		//find the x value that we are in
		gr.vCellCent(ii, 0, adcoord);
		
		//down wall
		dvalbnd = bnd[BPatch::down]->dVal(adcoord[dirX]);		
		dsol[0*N + ii] = dGhost(bnd[BPatch::down]->ctype, BPatch::down, dsol[1*N + ii], dvalbnd, gr.dDy());

		//up wall
		dvalbnd = bnd[BPatch::up]->dVal(adcoord[dirX]);		
		dsol[(M-1)*N + ii] = dGhost(bnd[BPatch::up]->ctype, BPatch::up, dsol[(M-2)*N + ii], dvalbnd, gr.dDy());
	}

	//remove some valgrind warnings (four corners)
	dsol[0] = dsol[N*M-1] = dsol[(M-1)*N] = dsol[(N-1)] = 0;
}


void Physics::vFluxInteg(const double *adsol, const Grid &gr, double *adflux) const{

    vFluxAssem(*this, gr, adsol, adflux);
	/*
	int ii, jj, N=gr.iNCX(), M=gr.iNCY();
	double dsoll, dsolr, dsolu, dsold, dsolcent;
	double dul, dur, dvu, dvd;
	double ddx=gr.dDx(), ddy=gr.dDy();

	// find the flux integral. do not include ghost cells
	for (jj = 1 ; jj < M-1 ; jj++){
		for (ii=1 ; ii < N-1 ; ii++){
			dsoll = adsol[jj*N + ii - 1];
			dsolr = adsol[jj*N + ii + 1];
			dsolu = adsol[(jj+1)*N + ii];
			dsold = adsol[(jj-1)*N + ii];
			dsolcent = adsol[jj*N + ii];
			
			dvu = adv[(jj+1)*N + ii];
			dvd= adv[(jj-1)*N + ii];
			
			dul = adu[jj*N + ii - 1];
			dur = adu[jj*N + ii + 1];

			adflux[jj*N + ii] =
				-( (dur*dsolr - dul*dsoll)/2. - dk * (dsolr-2.*dsolcent+dsoll)/ddx  ) / ddx
				-( (dvu*dsolu - dvd*dsold)/2. - dk * (dsolu-2.*dsolcent+dsold)/ddy  ) / ddy;
    	}
	}
   */
}

static double bnd2jac(const BPatch* bpa){
	if ( bpa->ctype == BPatch::neumann )
		return -1;
	else if (bpa->ctype == BPatch::dirich )
		return 1;
	else{
		return 0;
		assert(0);
	}
}
//J[jj][ii] -> column j row i
void Physics::vJacX(const int jj, const Grid &gr, const double ddtime, double *a2djac[3]) const{

	int N=gr.iNCX();
	double ddx = gr.dDx(), dul, dur;
	
	//first row of jacobian
	a2djac[0][0] = 0 ; a2djac[1][0] = 1 ; 
	a2djac[2][0] = bnd2jac(bnd[BPatch::left]) ;
	
	//last row
	a2djac[0][N-1] = bnd2jac(bnd[BPatch::right]) ;
	a2djac[1][N-1] = 1 ; a2djac[2][N-1] = 0 ;

	//other rows
	for ( int ii = 1 ; ii < N-1 ; ii++ ){
		dul = adu[jj*N + ii - 1];
		dur = adu[jj*N + ii + 1];
		 
		a2djac[0][ii] = (-dul/2./ddx - dk / ddx / ddx) * ddtime;
		a2djac[1][ii] = 1 + 2 * dk / ddx / ddx * ddtime;
		a2djac[2][ii] = (+dur/2./ddx - dk / ddx / ddx) * ddtime;
	}
		
	
}
void Physics::vJacY(const int ii, const Grid &gr, const double ddtime, double *a2djac[3]) const{
	
	int N=gr.iNCX(), M=gr.iNCY();
	double ddy = gr.dDy(), dvu, dvd;
	
	//first row of jacobian
	a2djac[0][0] = 0 ; a2djac[1][0] = 1 ; 
	a2djac[2][0] = bnd2jac(bnd[BPatch::down]) ;
	
	//last row
	a2djac[0][M-1] = bnd2jac(bnd[BPatch::up]) ;
	a2djac[1][M-1] = 1 ; a2djac[2][M-1] = 0 ;

	//other rows
	for ( int jj = 1 ; jj < M-1 ; jj++ ){
		dvu = adv[(jj+1)*N + ii];
		dvd = adv[(jj-1)*N + ii];
		 
		a2djac[0][jj] = (-dvd/2./ddy - dk / ddy / ddy) * ddtime;
		a2djac[1][jj] = 1 + 2 * dk / ddy / ddy * ddtime;
		a2djac[2][jj] = (+dvu/2./ddy - dk / ddy / ddy) * ddtime;
	}
}

/****************************************************************************
 *                                  Integrators                             *
 ****************************************************************************/
void vFluxAssem(const Physics &ph, const Grid &gr,
				const double *adsol, double *adflux){

	int M,N;
	N = gr.iNCX();
	M = gr.iNCY();
	
	FITS inp;
	inp.N = gr.iNCX();
	inp.M = gr.iNCY();
	inp.ddx = gr.dDx();
	inp.ddy = gr.dDy();


	/*
	  set the flux to zero
	*/
	memset(adflux, 0, (size_t)(sizeof(double)*N*M) );
    
	/*
	  Assemble the flux integral for the faces oriented in
	  the x direction.
	*/
	for (inp.jj = 1 ; inp.jj < M-1 ; inp.jj++){
		int iface;
		double dflux;

		// loop over all faces (no ghost)
		for (iface = 1 ; iface < gr.iNFX()-1 ; iface++){
			
			//find adjacent cells
			gr.vCLeft(iface, inp.jj, inp.iil);
			gr.vCRight(iface, inp.jj, inp.iir);

			// find the value of solution
			inp.dsoll = adsol[inp.jj*N + inp.iil];
			inp.dsolr = adsol[inp.jj*N + inp.iir];

			// find the flux
			ph.vFluxX(&inp, dflux);
			dflux /= inp.ddx;

			// assemble
			adflux[inp.jj * N + inp.iir] -= dflux;
			adflux[inp.jj * N + inp.iil] += dflux; 
		}
	}

	/*
	  Assemble the flux integral for the faces oriented in
	  the Y direction.
	*/
	for (inp.ii = 1 ; inp.ii < N-1 ; inp.ii++){
		int jface;
		double dflux;

		// loop over all faces (no ghost)
		for (jface = 1 ; jface < gr.iNFY()-1 ; jface++){
			
			//find adjacent cells
			gr.vCTop(inp.ii, jface, inp.jjt);
			gr.vCBott(inp.ii, jface, inp.jjb);

			// find the value of solution
			inp.dsolt = adsol[inp.jjt*N + inp.ii];
			inp.dsolb = adsol[inp.jjb*N + inp.ii];

			// find the flux
			ph.vFluxY(&inp, dflux);
			dflux /= inp.ddy;

			// assemble
			adflux[inp.jjt * N + inp.ii] -= dflux;
			adflux[inp.jjb * N + inp.ii] += dflux; 
		}
	}


}

void vJacAssemX(const int jj, const Physics &ph, const Grid &gr,
				const double ddt, const double *adsol, double a2djac[][3]){

	double ddt_dx = ddt / gr.dDx();
	int M,N;
	N = gr.iNCX();
	M = gr.iNCY();
	
	FITS inp;
	inp.N = N;
	inp.M = M;
	inp.ddx = gr.dDx();
	inp.ddy = gr.dDy();


	/* set the jac to I */
	for (int ii = 0; ii < N ; ii++){
		a2djac[ii][Cx] = a2djac[ii][Ax] = 0;
		a2djac[ii][Bx] = 1;	
	}

	/* start assembling */
	inp.jj = jj;
	for (int iface = 1 ; iface < gr.iNFX()-1 ; iface++){
		double djl, djr;
		
		//find adjacent cells
		gr.vCLeft(iface, jj, inp.iil);
		gr.vCRight(iface, jj, inp.iir);

		// find the value of solution
		inp.dsoll = adsol[inp.jj*N + inp.iil];
		inp.dsolr = adsol[inp.jj*N + inp.iir];

		// find the jac
		ph.vLocJacX(&inp, djl, djr);
		djl *= (ddt_dx);
		djr *= (ddt_dx);

		// assemble
		a2djac[inp.iil][Bx] -= djl;
		a2djac[inp.iil][Cx] -= djr;

		a2djac[inp.iir][Ax]	+= djl;
		a2djac[inp.iir][Bx] += djr; 
	}

	/* set the ghost cells */
	a2djac[0][Cx] = bnd2jac(ph.bnd[BPatch::left]) ;
    a2djac[0][Bx] = 1;
	a2djac[N-1][Ax] = bnd2jac(ph.bnd[BPatch::right]) ;
	a2djac[N-1][Bx] = 1;
	
}

void vJacAssemY(const int ii, const Physics &ph, const Grid &gr,
				const double ddt, const double *adsol, double a2djac[][3]){
	
	double ddt_dy = ddt / gr.dDy();
	int M,N;
	N = gr.iNCX();
	M = gr.iNCY();
	
	FITS inp;
	inp.N = N;
	inp.M = M;
	inp.ddx = gr.dDx();
	inp.ddy = gr.dDy();


	/* set the jac to I */
	for (int jj = 0; jj < M ; jj++){
		a2djac[jj][Cy] = a2djac[jj][Ay] = 0;
		a2djac[jj][By] = 1;	
	}

	/* start assembling */
	inp.ii = ii;
	for (int jface = 1 ; jface < gr.iNFY()-1 ; jface++){
		double djb, djt;
		
		//find adjacent cells
		gr.vCBott(ii, jface, inp.jjb);
		gr.vCTop(ii, jface, inp.jjt);

		// find the value of solution
		inp.dsolb = adsol[inp.jjb*N + inp.ii];
		inp.dsolt = adsol[inp.jjt*N + inp.ii];

		// find the jac
		ph.vLocJacY(&inp, djb, djt);
		djt *= (ddt_dy);
		djb *= (ddt_dy);

		// assemble
		a2djac[inp.jjb][By] -= djb;
		a2djac[inp.jjb][Cy] -= djt;

		a2djac[inp.jjt][Ay]	+= djb;
		a2djac[inp.jjt][By] += djt; 
	}

	/* set the ghost cells */
	a2djac[0][Cx] = bnd2jac(ph.bnd[BPatch::down]) ;
    a2djac[0][Bx] = 1;
	a2djac[M-1][Ay] = bnd2jac(ph.bnd[BPatch::up]) ;
	a2djac[M-1][By] = 1;
}

/****************************************************************************
 *                                  TimeAdvance                             *
 ****************************************************************************/
void vInitMemRK2(const Grid& gr, Memory *mem){
	int N=gr.iNCX(), M=gr.iNCY();

	mem->adsol = new double[M*N];
	mem->adflux = new double[M*N];
	mem->adsolrk = new double[M*N];

	fprintf(stderr, "Setting up time advance: RK2\n"); 
}

void vInitMemImplict(const Grid& gr, Memory *mem){
	int N=gr.iNCX(), M=gr.iNCY();

	mem->adsol = new double[M*N];
	mem->adflux = new double[M*N];
	mem->adsolcol = new double [M];
	mem->a2djac[0] = new double [MAX(M,N)];
	mem->a2djac[1] = new double [MAX(M,N)];
	mem->a2djac[2] = new double [MAX(M,N)];
	mem->a2dNEWjac = new double [MAX(M,N)][3];

	fprintf(stderr, "Setting up time advance: Implicit Euler + App Fac\n"); 
}

double vTimeAdvRK2(const Grid& gr, const Physics& ph, const double ddtime, Memory * mem){

	int N=gr.iNCX(), M=gr.iNCY();
	double dlinf, ddelta;
		
	//find solrk and set its ghosts
	ph.vFluxInteg(mem->adsol, gr, mem->adflux);
	for (int jj = 1 ; jj < M-1 ; jj++){
		for (int ii = 1 ; ii < N-1 ; ii++){
			mem->adsolrk[jj*N+ii] =
				mem->adsol[jj*N+ii] + ddtime/2. * (mem->adflux[jj*N+ii] + ph.ads[jj*N+ii]);
		}
	}
	ph.vSetGhosts(mem->adsolrk, gr);

	// find the final change in the solution and put in in mem->adfluxiteg
	ph.vFluxInteg(mem->adsolrk, gr, mem->adflux);
	dlinf = 0;
	for (int jj = 1 ; jj < M-1 ; jj++){
		for (int ii = 1 ; ii < N-1 ; ii++){
			ddelta = mem->adflux[jj*N+ii] + ph.ads[jj*N+ii];
			ddelta *= ddtime;
			mem->adsol[jj*N+ii] += ddelta;
			dlinf = MAX(dlinf, ABS(ddelta) );
		}
	}	
	ph.vSetGhosts(mem->adsol, gr);

    return dlinf;
}

static void vPrintSol(const int N, const int M, const double *adx){
	for (int ii=1 ; ii < N-1 ; ii++){
		for (int jj=1; jj < M-1; jj++){
			fprintf(stderr, "%-8d %-8d %-20.10lf\n", ii, jj, adx[jj*N+ ii]);
		}
		fprintf(stderr, "\n");				
	}
}
double vTimeAdvImplicit(const Grid& gr, const Physics& ph, const double ddtime, Memory * mem){
		int N=gr.iNCX(), M=gr.iNCY();
		double dlinf;

		//fprintf(stderr, "Solution before: \n");
		// vPrintSol(N, M, mem->adsol);

		/*
		  Flux integral
		*/
		ph.vFluxInteg(mem->adsol, gr, mem->adflux);
		
		
	    /*
		  solve along rows
		*/
		for (int jj = 1 ; jj < M -1 ; jj++){
			
			for (int ii=1 ; ii < N-1 ; ii++){
				mem->adflux[jj*N+ii] += ph.ads[jj*N+ii];
				mem->adflux[jj*N+ii] *= ddtime;
			}
			mem->adflux[jj*N + 0] = mem->adflux[jj*N + (N-1)] = 0;
			
			//ph.vJacX(jj, gr, ddtime, mem->a2djac);
			//vSolveThomas(mem->a2djac, mem->adflux+jj*N , N);			

			vJacAssemX(jj, ph, gr, ddtime, mem->adsol, mem->a2dNEWjac);
			vSolveThomasBlock(mem->a2dNEWjac, mem->adflux+jj*N, N);			

		}

		// printf("JACX: \n");
		// for (int ii = 0 ; ii < N ; ii++){
		// 	printf("LHS[i][*]: %10.5lf %10.5lf %10.5lf | %10.5lf %10.5lf %10.5lf \n",
		// 		   mem->a2djac[0][ii], mem->a2djac[1][ii], mem->a2djac[2][ii],
		// 		   mem->a2dNEWjac[ii][0], mem->a2dNEWjac[ii][1], mem->a2dNEWjac[ii][2]);
		// }	    
		// fprintf(stderr, "Solution after row solve: \n");
		// vPrintSol(N, M, mem->adflux);

		/*
		  solve along columns
		*/
		for (int ii = 1 ; ii < N -1 ; ii++){
			
			//ph.vJacY(ii, gr, ddtime, mem->a2djac);
			vJacAssemY(ii, ph, gr, ddtime, mem->adsol, mem->a2dNEWjac);
			
			//extract the iith column
		   for (int jj = 1 ; jj < M -1 ; jj++)
			   mem->adsolcol[jj] = mem->adflux[jj*N + ii];
		   mem->adsolcol[0] = mem->adsolcol[M-1] = 0;
				   
		   //vSolveThomas(mem->a2djac, mem->adsolcol, M);
		   vSolveThomasBlock(mem->a2dNEWjac, mem->adsolcol, M);			


		   //put the solution back
		   for (int jj = 1 ; jj < M -1 ; jj++)
			   mem->adflux[jj*N + ii] = mem->adsolcol[jj];
			
		}

		// fprintf(stderr, "Solution after col solve: \n");
		// vPrintSol(N, M, mem->adflux);


		/*
		  update solution and find dlinf(\delta T)
		*/
		dlinf = 0;
		for (int ii=1 ; ii < N-1 ; ii++){
			for (int jj=1; jj < M-1; jj++){
				int kk = jj*N+ii;
				mem->adsol[kk] += mem->adflux[kk];
				dlinf = MAX(dlinf, ABS(mem->adflux[kk]));
			}
		}

		/*
		  update the ghosts
		*/
		ph.vSetGhosts(mem->adsol, gr);
		  
		return dlinf;
}

/****************************************************************************
 *                                    Factory                               *
 ****************************************************************************/
VelField* factory_VelField(char const * str){
	char cmet, strname[200];
	int inread;

	inread = sscanf(str, "%c", &cmet);
	assert(inread);

	if (cmet == 't'){
		
		VelFieldTest *ans = new VelFieldTest;
		inread = sscanf(str, "%c%lf%c%lf", &cmet, &ans->du0, &cmet, &ans->dv0);
		assert(inread == 4);
		ans->vName(strname);
		fprintf(stderr, "Creating a: %s\n", strname);
		return ans;
		
	}else if (cmet == 'c'){

		VelFieldChannel *ans = new VelFieldChannel;
		inread = sscanf(str, "%c%lf%*c%lf", &cmet, &ans->du0, &ans->dheight);
		assert(inread == 3);
		ans->dv0 = 0;
		ans->vName(strname);
		fprintf(stderr, "Creating a: %s\n", strname);
		return ans;
		
	} else{
		assert(0);
		return NULL;
	}
}

// first char is type: n->neumann d->dirich
static BPatch::conType c2conType(const char c){
	switch(c){
	case 'n':
		return BPatch::neumann;
		break;
	case 'd':
		return BPatch::dirich;
		break;
	default:
		assert(0);
		return BPatch::dirich;
	}
}

// second char is func: c->const s->sine r->real(channel) l->linear
// third is the value to be inserted in the function
BPatch* factory_BPatch(char const *str){
	
	char ctype, cfunc, strname[200];
	double dval;
	int inread;

	inread = sscanf(str, "%c%c%lf", &ctype, &cfunc, &dval);
	assert(inread == 3);

	if (cfunc == 'c'){
		
		BPatch *ans = new BPatch;
		ans->ctype = c2conType(ctype);
		ans->dval = dval;
		ans->vName(strname);
		fprintf(stderr, "Creating a: %s\n", strname);
		return ans;
		
	}else if (cfunc == 's'){

		BPatchSine *ans = new BPatchSine;
		ans->ctype = c2conType(ctype);
		ans->dval = dval;
		ans->vName(strname);
		fprintf(stderr, "Creating a: %s\n", strname);
		return ans;
		
	} else if (cfunc == 'r'){
		
		BPatchQuad *ans = new BPatchQuad;
		ans->ctype = c2conType(ctype);
		ans->dval = dval;
		ans->vName(strname);
		fprintf(stderr, "Creating a: %s\n", strname);
		return ans;

	} else if (cfunc == 'l'){
		
		BPatchLin *ans = new BPatchLin;
		ans->ctype = c2conType(ctype);
		ans->dval = dval;
		ans->vName(strname);
		fprintf(stderr, "Creating a: %s\n", strname);
		return ans;

	} else{
		assert(0);
		return NULL;
	}
}

/****************************************************************************
 *                                    Thomas                                *
 ****************************************************************************/
void vSolveThomas(double *LHS[3], double *RHS, const int iSize)
{
  int i;
  /* This next line actually has no effect, but it -does- make clear that
     the values in those locations have no impact. */
  LHS[0][0] = LHS[2][iSize-1] = 0;
  /* Forward elimination */
  for (i = 0; i < iSize-1; i++) {
    LHS[2][i] /= LHS[1][i];
    RHS[i] /= LHS[1][i];
    LHS[1][i+1] -= LHS[2][i]*LHS[0][i+1];
    RHS[i+1] -= LHS[0][i+1]*RHS[i];
  }
  /* Last line of elimination */
  RHS[iSize-1] /= LHS[1][iSize-1];

  /* Back-substitution */
  for (i = iSize-2; i >= 0; i--) {
    RHS[i] -= RHS[i+1]*LHS[2][i];
  }
}



void vSolveThomasBlock(double LHS[][3], double RHS[], const int iSize)
{
  int i;
  /* This next line actually has no effect, but it -does- make clear that
     the values in those locations have no impact. */
  LHS[0][0] = LHS[iSize-1][2] = 0;
  /* Forward elimination */
  for (i = 0; i < iSize-1; i++) {
    LHS[i][2] /= LHS[i][1];
    RHS[i] /= LHS[i][1];
    LHS[i+1][1] -= LHS[i][2]*LHS[i+1][0];
    RHS[i+1] -= LHS[i+1][0]*RHS[i];
  }
  /* Last line of elimination */
  RHS[iSize-1] /= LHS[iSize-1][1];

  /* Back-substitution */
  for (i = iSize-2; i >= 0; i--) {
    RHS[i] -= RHS[i+1]*LHS[i][2];
  }
}
