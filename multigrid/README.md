# Multigrid Solver Playground

Some code for playing around with the multigrid method for solving PDEs. 

* poisson_finite_difference/  
C code that solved the Poisson equation in a rectangle. The discretization is the 
usual cell-centered finite difference method involving the immediate four neighbours.
* poisson_finite_element/    
This is slightly fancier. It has a half edge data structure, and functionality for
 non-nested grid intepolation. I am working on it.

* An example problem:

 +  Domain, equation, solution and boundary conditions:  [Same as this problem](https://h00shi.github.io/FILES/math521_boundary_curvature.pdf)

 +  8 grids are used. Coarsest has 182 triangles. Finest 3,263,881 triangles. The four coarsest grids:

   <img src="./demo/mesh.0.png" width="200"/> <img src="./demo/mesh.1.png" width="200"/>  
   <img src="./demo/mesh.2.png" width="200"/> <img src="./demo/mesh.3.png" width="200"/>

 + Effect of RCM reordering on the left hand side non-zero fill structure (shown for the second coarsest mesh).
   <img src="./demo/spy_natural.png" width="250"/> <img src="./demo/spy_rcm.png" width="250"/>


 +  Convergence history for finest mesh (should add per-time too).

   <img src="./demo/convergence_history.png" width="400"/>


 +  Final solution and error field for finest mesh.

   <img src="./demo/solution.png" width="250"/> <img src="./demo/error.png" width="250"/>


TODO: 
+ Look into the eigenvalues. 
+ Find out why in the unstructured case P=R^T, while in the structured case P=1/2 R^T,
and any other configuration fails. This is indeed strange. Use a 1-D  case first. Try
both the simples Galerkin and finite difference discretizations. Found it! It is due to the scaling in 
finite different. The LHS for fdm is (1/h^2)A, while for Galerking it is (1/h)A (A=tridiag(-1,2,-1)). 
Now P and R must satisfy LHS_coarse=R LHS_fine P. The 1/2 factor in FD comes from (2/h)^2= *2* * (2/h) * 1/h. (I have to workout the details).
+ Write a small report.

Possible extensions: 
+ Adaptivity.
+ Advection-diffusion, Euler equations.
+ Poisson surface reconstruction.
+ Eulerian solid simulation.
+ Inviscid incompressible fluid simulator with free boundaries (the de facto computer graphics version).
