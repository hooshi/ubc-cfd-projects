#!/usr/bin/env python3

import math


# First line: <# of vertices> <dimension (must be 2)> <# of attributes> <# of boundary markers (0 or 1)>
# Following lines: <vertex #> <x> <y> [attributes] [boundary marker]
# One line: <# of segments> <# of boundary markers (0 or 1)>
# Following lines: <segment #> <endpoint> <endpoint> [boundary marker]
# One line: <# of holes>
# Following lines: <hole #> <x> <y>
# Optional line: <# of regional attributes and/or area constraints>
# Optional following lines: <region #> <x> <y> <attribute> <maximum area>

ROUND_PHYSICAL_ID = 101
SIDES_PHYSICAL_ID = 102
HAS_BOUNDARY_IDS = 1

def write_annulus(fname, n_level):
    
    n_division = 2**n_level * 4

    points = []
    points.append([0.25, 0.00])
    points.append([1.00,  0.00])
    points.append([0.00,   1.00])
    points.append([0.00,   0.25])

    for i in range(1,n_division+1):
        angle = 0.5 * math.pi * float(i) / float(n_division+1)
        rr = 1
        points.append([rr*math.cos(angle), rr*math.sin(angle)])
    for i in range(1,n_division+1):
        j = n_division+1-i
        angle = 0.5 * math.pi * float(j) / float(n_division+1) 
        rr = 0.25
        points.append([rr*math.cos(angle), rr*math.sin(angle)])

    assert len(points) == 4+2*n_division

    with open(fname, "w") as ff:

        ## POINTS
        ff.write("# POINTS \n")
        ff.write( "%d 2 0 0 \n "%len(points) )
        for i, v in enumerate(points):
            ff.write( "%d %.16e %.16e \n "%(i, v[0], v[1]) )

        ## POINTS
        ff.write("\n")
        ff.write("# SEGMENTS \n")
        ff.write( "%d %d  \n"%(len(points), HAS_BOUNDARY_IDS))
        ff.write( "0 0 1 %d \n"%SIDES_PHYSICAL_ID)
        ff.write( "1 2 3 %d\n"%SIDES_PHYSICAL_ID)
        ff.write( "2 %d %d %d\n"%(1, 4+0, ROUND_PHYSICAL_ID))
        ff.write( "3 %d %d %d\n"%(4+n_division-1, 2,ROUND_PHYSICAL_ID))
        ff.write( "4 %d %d %d\n"%(3, 4+n_division,ROUND_PHYSICAL_ID))
        ff.write( "5 %d %d %d\n"%(4+2*n_division-1, 0,ROUND_PHYSICAL_ID))
        j = 6
        for i in range(n_division-1):
            ff.write( "%d %d %d %d\n"%(j,4+i,4+i+1,ROUND_PHYSICAL_ID))
            j=j+1
            ff.write( "%d %d %d %d\n"%(j,4+n_division+i,4+n_division+i+1,ROUND_PHYSICAL_ID))
            j = j+1
            
        assert j ==  4+2*n_division, "%d != %d"%(j, 4+2*n_division)

        ## HOLES
        ff.write("\n")
        ff.write("# HOLES\n")
        ff.write("0\n")

write_annulus("dirt/annulus_1.poly", 1)
write_annulus("dirt/annulus_2.poly", 2)
write_annulus("dirt/annulus_3.poly", 3)
write_annulus("dirt/annulus_4.poly", 4)
write_annulus("dirt/annulus_5.poly", 5)
write_annulus("dirt/annulus_6.poly", 6)
write_annulus("dirt/annulus_7.poly", 7)
write_annulus("dirt/annulus_8.poly", 8)
