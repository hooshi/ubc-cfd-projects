#!/usr/bin/env bash

mkdir -p dirt

## =====================================================
## Square, point locator tests
## =====================================================

A=0.4096;triangle -pqa${A}Djz square.poly
A=0.1024;triangle -prqa${A}Djz square.1
A=0.0512;triangle -prqa${A}Djz square.2
## Agressive
A=0.0016;triangle -prqa${A}Djz square.3
A=0.0001;triangle -prqa${A}Djz square.4
A=0.00000625; triangle -prqa${A}Djz square.5
A=0.000000390625; triangle -pqa${A}Djz square.6

mv square.[1567].* dirt/
#
cp precious/square.[234].* dirt/
#
rm -f square.[234].*


cp precious/square.msh dirt/

## =====================================================
## Square solving poisson equation
## =====================================================

## For mg
cp square.poly squaremg.poly
A=0.00640000;triangle -pqa${A}Djz squaremg.poly
A=0.00160000;triangle -prqa${A}Djz squaremg.1
A=0.00040000;triangle -prqa${A}Djz squaremg.2
A=0.00010000;triangle -prqa${A}Djz squaremg.3
A=0.00002500;triangle -prqa${A}Djz squaremg.4
A=0.00000600;triangle -prqa${A}Djz squaremg.5
A=0.00000150;triangle -prqa${A}Djz squaremg.6
A=0.00000035;triangle -prqa${A}Djz squaremg.7

mv squaremg.[12345678].* dirt/
rm squaremg.poly

## =====================================================
## Annulus
## =====================================================
python generate_annulus_boundary.py

function run_annulus
{
    A=${1}
    FNO=${2}
    triangle -pqa${A}Djz dirt/annulus_${FNO}
    mv dirt/annulus_${FNO}.1.poly dirt/annulus.${FNO}.poly
    mv dirt/annulus_${FNO}.1.ele dirt/annulus.${FNO}.ele
    mv dirt/annulus_${FNO}.1.node dirt/annulus.${FNO}.node
}
run_annulus 0.00640000 1
run_annulus 0.00160000 2
run_annulus 0.00040000 3
run_annulus 0.00010000 4
run_annulus 0.00002500 5
run_annulus 0.00000600 6
run_annulus 0.00000150 7
run_annulus 0.00000035 8
