paraview_tools.py

  Some paraview scripts for processing the output vtk files.

test_mg.cxx
test_poisson.cxx
test_numerics.cxx
timer_locator.cxx
timer_transfer.cxx
test_connectivity.cxx
tests.cxx

  Are all linked into a single test executable.

timer_locator.cxx

  An executable that locates the vertices of one mesh on another and
  times the process.

timer_mg.cxx

  Solves the Poisson equation on an unstructured triangular mesh,
  using either

  - Multigrid (MG) smoothed with Gauss-Seidel (GS) alone.
  - Conjugate gradient (CG) preconditioned with MG smoothed by GS.
  - CG  preconditioned by GS.
  - CG preconditioned by incomplete Cholesky factorization (with or
    without reverse Cuthill-Mckee reordering).

  The boundary conditions and the source term have to be hard-coded by
  inheriting from the ``Poisson_problem_data'' class.
