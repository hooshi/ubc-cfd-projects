CMakeLists.txt

  The parent CMAKE file for building the project.

src/

  The reusable code.

exe/

  Tests and the executables.

mesh/

  Mesh files. On a UNIX like system CMAKE will attempt to create
  symlinks to the meshes folder inside the cmake build directory.

FINDCBLAS.cmake

  This is not used for now.
