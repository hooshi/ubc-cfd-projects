#ifndef DOF_ORDERING_IS_INCLUDED
#define DOF_ORDERING_IS_INCLUDED

#include "grid_connectivity_context.hxx"


namespace ordering
{

void cm_ordering_for_vertices(const Grid_connectivity_context& grid, std::vector<int>& new2old);
void rcm_ordering_for_vertices(const Grid_connectivity_context& grid,  std::vector<int>& new2old);

} // End of namespace ordering


#endif /* DOF_ORDERING_IS_INCLUDED */