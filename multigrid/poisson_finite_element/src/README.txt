- bridson/

  Modified code from Robert Bridson's website. Contains a preconditioned
  conjugate gradient solver, a small vector class, and a sparse matrix
  class.

- anslib/ and not_ideal/

  These are not used.

- adepth/

  A basic timer class from the Adept project.

cell_type.hxx

  Some geometric information on each cell type (e.g., tet, hex, quad).

dof_ordering.hxx
dof_ordering.cxx

  Reorder the unknowns based on a mesh. For now just reorders the vertices.

fake_printf.hxx

  A fake fprintf class, and a stream class that writes to a FILE*.

fe.cxx
fe.hxx

  Basis functions and quadrature (in physical space) information for
  triangle and segment elements.

grid_connectivity_context.cxx
grid_connectivity_context.hxx

  Storage of triangular mesh connectivity. Stores vertices and
  neighbours of a triangle.

grid_itertors.cxx
grid_itertors.hxx

  Half-edge iterators for grid_connectivity.

grid_io.cxx
grid_io.hxx

  Code for reading and writing grid data.


point_locator.cxx
point_locator.hxx

  Locate the vertices of one mesh on another one. Both O(N) and O(N^2)
  complexity algorithms are present. The O(N) algorithm uses BFS
  search.

quadrature.cxx
quadrature.hxx

  Quadrature information (on reference elements). Adapted from Libmesh
  for the ANSLib library by myself.

poisson_equation.cxx
poisson_equation.hxx

  A very simple finite-element discretization of the Poisson equation
  on a triangular mesh. Details can be found here
  https://h00shi.github.io/FILES/math521_proj_report.pdf

mg_tools.cxx
mg_tools.hxx

  Solve Poisson equation using the multigrid method (or precondition
  CG a solver).
