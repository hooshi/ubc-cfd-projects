#include <algorithm>
#include <queue>

#include "dof_ordering.hxx"
#include "grid_iterators.hxx"


namespace ordering
{

void
cm_ordering_for_vertices(const Grid_connectivity_context & grid, std::vector<int> & new2old)
{
  Grid_iterators giter(grid);
  //
  std::vector<bool> is_vertex_visited(giter.n_vertices(), false);
  std::queue<int> to_visit;
  //
  std::vector<int> & visit_order = new2old;
  visit_order.resize(0);
  visit_order.reserve(giter.n_vertices());

  // Seed the queue
  to_visit.push(giter.n_vertices() / 2);

  // Start visiting
  while(!to_visit.empty())
    {
      const int visitee_id = to_visit.front();
      to_visit.pop();

      //
      // If the vertex is not visited, mark it as visited and propose visiting his neighbours
      //
      if(!is_vertex_visited[visitee_id])
        {
          // record
          is_vertex_visited[visitee_id] = true;
          visit_order.push_back(visitee_id);
          // neighbours
          Grid_iterators::Vertex_umbrella umbrella = giter.vertex_umbrella_iterator(visitee_id);
          do
            {
              to_visit.push(umbrella.half_edge().origin().id());
            }
          while(umbrella.advance());
        }
    }

    // assert
    assert((int)visit_order.size() == giter.n_vertices());
}

void
rcm_ordering_for_vertices(const Grid_connectivity_context & grid, std::vector<int> & new2old)
{
  cm_ordering_for_vertices(grid, new2old);
  std::reverse(new2old.begin(), new2old.end());
}

} // End of namespace ordering
