#ifndef FAST_MARCHING_HXX_IS_INCLUDED
#define FAST_MARCHING_HXX_IS_INCLUDED

#include "grid_connectivity_context.hxx"

class Fast_marching_method
{

  void seed_points_from_boundary( const std::vector<int> &bdry_tags, std::vector<double>& distance);
  void march(std::vector<double>& distance);

private:
  const Grid_connectivity * _grid;
};

#endif /* FAST_MARCHING_HXX_IS_INCLUDED */