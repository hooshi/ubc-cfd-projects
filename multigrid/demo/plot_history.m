__THIS_IS_NOT_A_FUNCTION__=1


history_mg; h_mg=history;
history_gs; h_gs=history;
history_icc; h_icc=history;
history_icc_rcm; h_icc_rcm=history;

loglog(1:size(h_mg), h_mg, ';CG/MG/GS;',
       'linewidth', 2, 'color', 'r'); hold on
loglog(1:size(h_gs), h_gs, ';CG/GS;',
       'linewidth', 2, 'color', 'b'); hold on
loglog(1:size(h_icc), h_icc, ';CG/ICC0;',
       'linewidth', 2, 'color', 'g');
loglog(1:size(h_icc_rcm), h_icc_rcm, ';CG/ICC0/RCM;',
       'linewidth', 2, 'color', 'm');

xlabel('Iteration')
ylabel('norm(b-Ax)')
legend()

print("convergence_history.png","-dpng", "-F:15")
