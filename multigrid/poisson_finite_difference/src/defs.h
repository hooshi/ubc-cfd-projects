/** poisson.h
 *
 * A barebones multigrid solver.  Hardcoded for the poisson equation discretized by a finite-volume
 *  method on a square unifom grid.
 *
 * Shayan Hoshyari February 2018
 */

#ifndef __POISSON_MG_H__
#define __POISSON_MG_H__

#include <time.h>
#include <stdlib.h>
#include <stdio.h>


#define ABS(x)   ((x) > 0 ? (x) : -(x) )
#define MAX(x,y) ((x) > (y) ? (x) : (y) )
#define REAL double
#define PI   3.14159265359
#define EXIT() do{assert(0); exit(2);}while(0)
#define N_MG_VECTORS 4

#define TRUE 1
#define FALSE 0
#define BOOL int

#define UNSUSED(x) ((void)(x));

enum Numbers
  {
    ZERO=0,
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN
  };

// ===================================================================
// A Grid
// ===================================================================

struct Grid
{
  // The dimensions of the domain. THe lower left is always assumed to be at the origin.
  REAL x_max;
  REAL y_max;
  
  // Number of cells in each direction
  int n_cv_x;
  int n_cv_y;
};

// X and y coordinate of the center of each control volume.
void Grid_create(struct Grid**, REAL x_max, REAL y_max, int n_cv_x, int n_cv_y);
void Grid_destroy(struct Grid** g);
REAL Grid_cv_center_x(struct Grid*, int);
REAL Grid_cv_center_y(struct Grid*, int);
REAL Grid_vertex_x(struct Grid*, int);
REAL Grid_vertex_y(struct Grid*, int);
REAL Grid_delta_x(struct Grid*);
REAL Grid_delta_y(struct Grid*);

// ===================================================================
// A vector of solution
// ===================================================================
struct DOFVector
{
  struct Grid *grid;
  REAL *values;
};
void DOFVector_create(struct DOFVector**, struct Grid* );
void DOFVector_destroy(struct DOFVector**);
REAL DOFVector_get_value(struct DOFVector*,int,int);
void DOFVector_set_value(struct DOFVector*,int,int,REAL);
void DOFVector_copy(struct DOFVector* from, struct DOFVector* to);
void DOFVector_set_zero(struct DOFVector* dv);

// c = a * x +  b * y;
void DOFVector_axby(double a, struct DOFVector* x, double b, struct DOFVector* y, struct DOFVector* z);

void DOFVector_norms(struct DOFVector* dv, double *l1, double *l2, double *linf);


// ===================================================================
// Physics
// ===================================================================

//Function for the value of dT/dz or T on boundary
typedef double (*Function1D)(const double, void*);

//Function for the source term
typedef double (*Function2D)(const double, const double, void*);

enum BoundaryConditionType
  {
    BDRY_DIRICHLET,
    BDRY_NEUMANN
  };

struct BoundaryEdge
{
  enum BoundaryConditionType type;
  Function1D            value;
};

struct Physics
{    
  //Boundary conditions
  struct BoundaryEdge boundary_south;
  struct BoundaryEdge boundary_east;
  struct BoundaryEdge boundary_north;
  struct BoundaryEdge boundary_west;
    
  //Source term
  Function2D source_term;

};

struct Physics Physics_create(struct BoundaryEdge south,
                              struct BoundaryEdge east,
                              struct BoundaryEdge north,
                              struct BoundaryEdge west,
                              Function2D source_term);

// ===================================================================
// Multigrid solver
// ===================================================================


enum SmootherType
  {
    SMOOTHER_ADI,
    SMOOTHER_JACOBI,
    SMOOTHER_GAUSS_SEIDEL
  };

enum TransferType
  {
    TRANSFER_INJECTION,
    TRANSFER_INTERPOLATION
  };

enum CycleType
  {
    CYCLE_TYPE_V=0,
    CYCLE_TYPE_W,
  };

struct MGInfo
{
  enum SmootherType smoother_type;
  enum CycleType    cycle_type;
  int               n_levels;
  REAL relaxation_factor;
  REAL atol;
  int  n_smoothing[100]; // perlevel, both post and pre
};

struct MGInfo MGInfo_create(enum SmootherType, enum CycleType, int n_levels);

struct MG
{
  // Solver information
  struct MGInfo info;

  // Physics
  struct Physics physics;
    
  // The real time at which the solution starts
  clock_t cpu_start_time;

  // THe grids
  struct Grid **grids;

  // Verify that these are enough
  struct DOFVector **u[N_MG_VECTORS];  // solution
  struct DOFVector **f[N_MG_VECTORS];  // source term
  struct DOFVector **r[N_MG_VECTORS];  // residual
  struct DOFVector **du[N_MG_VECTORS]; // solution update
};

void MG_create(struct MG**, struct MGInfo, struct Physics physics, struct Grid* finest_grid);
void MG_destroy(struct MG**);

/*
  Set initial conditions
*/
void MG_first_time(struct MG* mg);
void MG_explicit_ghost_eval(struct MG*, int level, struct DOFVector *u);

/*
  Find the residual
  residual = f - Au
*/
void MG_residual(struct MG*,
                 int level,
                 struct DOFVector *u,
                 struct DOFVector *f,
                 struct DOFVector *r);

/*
  Smoothers
 */
REAL MG_smooth(struct MG*,
               int level,
               struct DOFVector *u,
               struct DOFVector *f,
               struct DOFVector *du);

REAL MG_smooth_jacobi(struct MG*,
                     int level,
                     struct DOFVector *u,
                     struct DOFVector *f,
                     struct DOFVector *du);
REAL MG_smooth_gauss_seidel(struct MG*,
                              int level,
                              struct DOFVector *u,
                              struct DOFVector *f,
                              struct DOFVector *du);
REAL MG_smooth_ADI(struct MG*,
                     int level,
                     struct DOFVector *u,
                     struct DOFVector *f,
                     struct DOFVector *du);


/*
  REstriction and prolongationa
 */
void MG_restrict_inject(struct MG* mg,
                        int from,
                        int to,
                        struct DOFVector* dvf,
                        struct DOFVector* dvt);

void MG_prolongate_inject(struct MG* mg,
                          int from,
                          int to,
                          struct DOFVector* values_from,
                          struct DOFVector* values_to);
void MG_prolongate_interpolate(struct MG* mg,
                               int from,
                               int to,
                               struct DOFVector* values_from,
                               struct DOFVector* values_to);



/*
  Full solve
 */
struct ConvergenceHistory
{
  int n_iterations;
  int n_max_iterations;
  REAL *update_l1_norms;
  REAL *update_l2_norms;
  REAL *update_linf_norms;
  REAL *time;
};
void ConvergenceHistory_create(struct ConvergenceHistory**, int);
void ConvergenceHistory_destroy(struct ConvergenceHistory**);

void MG_cycle(struct MG* mg, int level, struct ConvergenceHistory* ch, struct DOFVector *du);
void MG_solve(struct MG*, struct ConvergenceHistory**);


// ===================================================================
// IO
// ===================================================================
void IO_print_dof_vector(struct DOFVector*, FILE*, BOOL include_ghosts);

void IO_write_vtk(const char[], int n_vectors, struct DOFVector* dvs[], char const * names[]);


#endif /*__POISSON_MG_H__*/

/*
  Local Variables:
  mode:c
  c-file-style:"GNU"
  c-file-offsets:((innamespace . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/


