#include <assert.h>

#include "defs.h"
#include "visit_writer.h"

void IO_print_dof_vector(struct DOFVector* dv, FILE *stream, BOOL include_ghosts)
{
  int i, j;
  int offset = (include_ghosts ? 1 : 0);
  REAL u_i_j;
  struct Grid *grid = dv->grid;
  
  for (j = grid->n_cv_y + offset;  j >= 1-offset ; --j)
    {
      for (i = 1-offset ; i < grid->n_cv_x + 1 + offset ; ++i)
	{
	  u_i_j = DOFVector_get_value(dv, i, j);
          fprintf(stream, "%6.4e ", u_i_j);
        }
      fprintf(stream, "\n");
    }
}

void IO_write_vtk(const char filename[],
		  int n_vars,
		  struct DOFVector* dvs[],
		  char const * var_names[])
{
  struct Grid *grid;
  int varid, i, j, k;
  
  int dims[3];
  int vardim[50];
  int centering[50];
  float *vars[50];
  float *x, *y, z[1];
  
  assert(n_vars >= 1);
  assert(n_vars < 50);
  
  
  grid = dvs[0]->grid;    
  dims[0] = grid->n_cv_x+1; dims[1]=grid->n_cv_y+1; dims[2]=1;

  //
  // Prepare variables
  //
  for (varid = 0 ; varid < n_vars ; ++varid)
    {
      vardim[varid] = 1;
      centering[varid] = 0;
      vars[varid] = (float*)malloc(sizeof(float)*(grid->n_cv_x)*(grid->n_cv_y));

      k = 0;
      for (j = 1 ; j <= grid->n_cv_y ; ++j)
	{
	  for (i = 1 ; i <= grid->n_cv_x ; ++i)
	    {
	      vars[varid][k] = (float)DOFVector_get_value(dvs[varid], i, j); 
	      ++k;
	    }
	}

    }

  //
  // Prepare locations
  //
  z[0] = 0;
  x = (float*)malloc(sizeof(float)*dims[0]);
  y = (float*)malloc(sizeof(float)*dims[1]);
  for (int i = 0 ; i < grid->n_cv_x+1 ; i++)
    {
      x[i] = (float) i / grid->n_cv_x;
    }
  for (int j = 0 ; j < grid->n_cv_y+1 ; j++)
    {
      y[j] = (float) j / grid->n_cv_y;
    }


  //
  //write to a vtk file
  //
  write_rectilinear_mesh(filename, 0 /*binary*/, dims, x, y, z, n_vars, vardim, centering, var_names, vars);

  //free memory
  free(x);
  free(y);
  for (varid = 0 ; varid < n_vars ; ++varid)
    {
      free( vars[varid] );
    }
}
