#include <stdlib.h>
#include <assert.h>

#include "defs.h"


void Grid_create(struct Grid** g, REAL x_max, REAL y_max, int n_cv_x, int n_cv_y)
{
  (*g) = (struct Grid*)malloc( sizeof(struct Grid) );
  (*g)->x_max = x_max;
  (*g)->y_max = y_max;
  (*g)->n_cv_x = n_cv_x;
  (*g)->n_cv_y = n_cv_y;  
}

void Grid_destroy(struct Grid** g)
{
  free(*g);
  (*g) = (struct Grid*) NULL;
}


REAL Grid_cv_center_x(struct Grid* g, int ii)
{
  assert( ii >= 1);
  assert( ii <= g->n_cv_x);
  return (ii-0.5) * Grid_delta_x(g);
}

REAL Grid_cv_center_y(struct Grid* g, int jj)
{
  assert( jj >= 1);
  assert( jj <= g->n_cv_y);
  return (jj-0.5) * Grid_delta_y(g);
}

REAL Grid_vertex_x(struct Grid* g, int ii)
{
  assert( ii >= 0);
  assert( ii < g->n_cv_x + 1);
  return ii * Grid_delta_x(g);
}

REAL Grid_vertex_y(struct Grid* g, int jj)
{
  assert( jj >= 0);
  assert( jj < g->n_cv_y + 1);
  return jj * Grid_delta_y(g);
}

REAL Grid_delta_x(struct Grid* g)
{
  return g->x_max / g->n_cv_x;
}

REAL Grid_delta_y(struct Grid* g)
{
  return g->y_max / g->n_cv_y;
}

/*
  Local Variables:
  mode:c
  c-file-style:"GNU"
  c-file-offsets:((innamespace . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
