#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <math.h>
#include <stdio.h>

#include "defs.h"


struct MGInfo MGInfo_create(enum SmootherType st, enum CycleType ct, int n_levels)
{
  struct MGInfo mg_info;
  int i;

  // mandatory values
  mg_info.smoother_type = st;
  mg_info.cycle_type = ct;
  mg_info.n_levels = n_levels;

  // Default values
  mg_info.relaxation_factor = 1.;
  mg_info.atol = 1e-10;
  for ( i = 0 ; i < n_levels ; ++i)
    {
      mg_info.n_smoothing[i] = 20;
    }
  
  return mg_info;
}

void MG_create(struct MG** mg_ptr, struct MGInfo mg_info, struct Physics physics, struct Grid* finest_grid)
{
  struct MG *mg;
  int i, j;
  int coarsening_factor;
  int n_levels = mg_info.n_levels;
  
  mg = (struct MG*)malloc(sizeof(struct MG));
  mg->info = mg_info;
  mg->physics = physics;
  mg->cpu_start_time = clock();

  /* Make sure the grid dimenstions work */
  coarsening_factor = (int)pow(2, n_levels-1);
  if( n_levels < 1 )
    {
      fprintf(stdout, "n_levels = %d < 1 \n", n_levels);
      EXIT();
    }
  if( finest_grid->n_cv_x % coarsening_factor != 0 )
    {
      fprintf(stdout, "n_cv_x and n_levels mismatch: %d %% 2^(%d-1) != 0 \n",
              finest_grid->n_cv_x, n_levels);
      EXIT();
    }
  if( finest_grid->n_cv_y % coarsening_factor != 0 )
    {
      fprintf(stdout, "n_cv_y and n_levels mismatch: %d %% 2^(%d-1) != 0 \n",
              finest_grid->n_cv_y, n_levels);
      EXIT();
    }
  
  /* Create the Grids */
  mg->grids = (struct Grid**)malloc( n_levels*sizeof(struct Grid*));
  {
    REAL x_max, y_max;
    int  n_cv_x, n_cv_y;
    x_max = finest_grid->x_max;
    y_max = finest_grid->y_max;
    n_cv_x = finest_grid->n_cv_x*2;
    n_cv_y = finest_grid->n_cv_y*2;
    for (i=0 ; i < n_levels ; ++i)
      {
        assert(n_cv_x % 2==0); n_cv_x /= 2;
        assert(n_cv_y % 2==0); n_cv_y /= 2;
        Grid_create(&mg->grids[i], x_max, y_max, n_cv_x, n_cv_y);
        fprintf(stderr, "Created grid level %d: %dx%d \n", i, n_cv_x, n_cv_y);
      }
  }

  /* Create extra vectors */
  for (i=0 ; i < N_MG_VECTORS ; ++i)
    {
      mg->u[i] = (struct DOFVector**)malloc(n_levels*sizeof(struct DOFVector*));
      mg->f[i] = (struct DOFVector**)malloc(n_levels*sizeof(struct DOFVector*));
      mg->du[i] = (struct DOFVector**)malloc(n_levels*sizeof(struct DOFVector*));
      mg->r[i] = (struct DOFVector**)malloc(n_levels*sizeof(struct DOFVector*));
      for (j = 0 ; j < n_levels ; ++j)
        {
          DOFVector_create(&mg->u[i][j],mg->grids[j]);
          DOFVector_create(&mg->f[i][j],mg->grids[j]);
          DOFVector_create(&mg->du[i][j],mg->grids[j]);
          DOFVector_create(&mg->r[i][j],mg->grids[j]);
        }
    }
  

  /* Return the final answer */
  *mg_ptr = mg;
}

void MG_destroy(struct MG** mg_ptr)
{
  struct MG *mg = *mg_ptr;
  int i, j;
  int n_levels = mg->info.n_levels;
    
  /* Free the grids */
  for (i = 0 ; i < n_levels ; ++i)
    {
      Grid_destroy(&mg->grids[i]);
    }
  free(mg->grids);

  /* Free the solution vectors */
  for (i = 0 ; i < N_MG_VECTORS ; ++i)
    {
      for (j = 0 ; j < n_levels ; ++j)
        {
          DOFVector_destroy(&mg->u[i][j]);
          DOFVector_destroy(&mg->f[i][j]);
          DOFVector_destroy(&mg->du[i][j]);
          DOFVector_destroy(&mg->r[i][j]);
        }
      free(mg->u[i]);
      free(mg->f[i]);
      free(mg->du[i]);
      free(mg->r[i]);
    }

  /* Free the MG itself */
  free(mg);
  *mg_ptr = (struct MG*)NULL;
}

/* =========================================================
   FIRST TIME
   ========================================================= */
void MG_first_time(struct MG* mg)
{
  int i, j;
  REAL u_c = 1;
  REAL f_i_j;
  REAL x_cv, y_cv;
  
  // Set solution to zero
  // Set the source term
  DOFVector_axby(0,NULL,0,NULL,mg->u[0][0]);
  for (i=1; i<= mg->grids[0]->n_cv_x; ++i)
    {
      for (j=1; j<= mg->grids[0]->n_cv_y; ++j)
        {
	  x_cv = Grid_cv_center_x(mg->grids[0], i);
	  y_cv = Grid_cv_center_y(mg->grids[0], j);
	  f_i_j = mg->physics.source_term(x_cv, y_cv ,(void*)NULL);
	  DOFVector_set_value(mg->u[0][0], i, j, u_c);
	  DOFVector_set_value(mg->f[0][0], i, j, f_i_j);
        }
    }
}

/* =========================================================
   Find the residual
   ========================================================= */
void MG_residual(struct MG* mg,
                 int level,
                 struct DOFVector *u,
                 struct DOFVector *f,
                 struct DOFVector *r)
{
  int i,j;
  struct Grid *grid;
  REAL dy, dx, dx2inv , dy2inv;
  // smoothing
  REAL u_i_j, u_ip_j, u_im_j, u_i_jp, u_i_jm;
  REAL f_i_j;
  REAL r_i_j;

  grid  = mg->grids[level];
  dy = Grid_delta_y(grid);
  dx = Grid_delta_x(grid);
  dx2inv = 1. / (dx*dx);
  dy2inv = 1. / (dy*dy);

   
  //
  // Update the ghost cells
  //
  MG_explicit_ghost_eval(mg, level, u);
  
  //
  // Start finding the residual
  //
  for (i = 1 ; i <= grid->n_cv_x ; ++i)
    {
      for (j = 1; j <= grid->n_cv_y ; ++j)
        {
          // Get all the values
          u_i_j  = DOFVector_get_value(u, i,   j);
          u_ip_j = DOFVector_get_value(u, i+1, j);
          u_im_j = DOFVector_get_value(u, i-1, j);
          u_i_jp = DOFVector_get_value(u, i,   j+1);
          u_i_jm = DOFVector_get_value(u, i,   j-1);
          f_i_j  = DOFVector_get_value(f, i,   j);
          // if(j == grid->n_cv_x) printf("ughost: %lf\n",u_i_jp);
          
          r_i_j = f_i_j
            -  dx2inv  * (u_ip_j + u_im_j)
            -  dy2inv  * (u_i_jp + u_i_jm)
            +  2 * ( dy2inv + dx2inv ) *  u_i_j;
          DOFVector_set_value(r, i, j, r_i_j);
        }
    }
}

/* =========================================================
   SMOOTHERS
   WITH HARD-CODED PHYSICS
   ========================================================= */
REAL MG_smooth(struct MG* mg,
               int level,
               struct DOFVector *u,
               struct DOFVector *f,
               struct DOFVector *du)
{
  switch(mg->info.smoother_type)
    {
      case  SMOOTHER_ADI:
        EXIT();
        break;
      case  SMOOTHER_JACOBI:
        return MG_smooth_jacobi(mg, level, u, f, du);
        break;
      case  SMOOTHER_GAUSS_SEIDEL:
        return MG_smooth_gauss_seidel(mg, level, u, f, du);
        break;
    }
  return -1e100;
}


static double _MG_single_ghost_eval(REAL orientation,
                                    enum BoundaryConditionType btype,
                                    REAL         bvalue,
                                    REAL         u_adj,
                                    REAL         dxory)
{
  switch( btype )
    {
      case BDRY_DIRICHLET:
        return 2*bvalue - u_adj;
        break;
      case BDRY_NEUMANN:
        return u_adj + orientation * bvalue * dxory;
        break;
      default:
        EXIT();
    }
}

void MG_explicit_ghost_eval(struct MG *mg, int level, struct DOFVector *u)
{
  struct Grid *grid = u->grid;
  REAL u_adj, u_ghost;
  REAL dy, dx;
  int i, j;
  REAL bfunc_value;
  enum BoundaryConditionType btype;

  dy = Grid_delta_y(grid);
  dx = Grid_delta_x(grid);

  //
  // NOTE: other levels have to have homogenous boundary conditions, so that
  // they do not modify to the residual source term.
  //
  
  for (i = 1 ; i <= grid->n_cv_x ; ++i)
    {
      REAL x_cv = Grid_cv_center_x(grid, i);

      // Bottom
      u_adj = DOFVector_get_value(u, i, 1);
      bfunc_value = (level==0 ? mg->physics.boundary_south.value(x_cv, (void*)NULL ) : 0);
      btype =  mg->physics.boundary_south.type;
      u_ghost = _MG_single_ghost_eval(-1, btype, bfunc_value, u_adj, dy);
      DOFVector_set_value(u, i, 0, u_ghost);

      // Top
      u_adj = DOFVector_get_value(u, i, grid->n_cv_y);
      bfunc_value = (level==0 ?mg->physics.boundary_north.value(x_cv, (void*)NULL) : 0);
      btype =  mg->physics.boundary_north.type;
      u_ghost = _MG_single_ghost_eval(1, btype, bfunc_value, u_adj, dy);
      DOFVector_set_value(u, i, grid->n_cv_y+1, u_ghost);
      // printf("ughost:%lf\n",u_ghost);
    }
  for (j = 1 ; j <= grid->n_cv_y ; ++j)
    {
      REAL y_cv = Grid_cv_center_y(grid, j);

      // Right
      u_adj = DOFVector_get_value(u, grid->n_cv_x, j);
      bfunc_value = (level==0 ?mg->physics.boundary_east.value(y_cv, (void*)NULL)  : 0);
      btype =  mg->physics.boundary_east.type;
      u_ghost = _MG_single_ghost_eval( 1, btype, bfunc_value, u_adj, dx);
      DOFVector_set_value(u, grid->n_cv_x+1, j, u_ghost);
      //printf("%lf\n", u_ghost);
      //IO_print_dof_vector(u, stdout, TRUE);

      // Left
      u_adj = DOFVector_get_value(u, 1, j);
      bfunc_value = (level==0 ? mg->physics.boundary_west.value(y_cv, (void*)NULL) : 0);
      btype =  mg->physics.boundary_west.type;
      u_ghost = _MG_single_ghost_eval(-1, btype, bfunc_value, u_adj, dx);
      DOFVector_set_value(u, 0, j, u_ghost);
    }
}

REAL MG_smooth_jacobi(struct MG* mg,
                      int level,
                      struct DOFVector *u,
                      struct DOFVector *f,
                      struct DOFVector *du)
{
  int i,j;
  REAL omega;
  struct Grid *grid;
  REAL dy, dx, dydx2, dy2;
  // smoothing
  REAL u_i_j, u_ip_j, u_im_j, u_i_jp, u_i_jm;
  REAL f_i_j;
  REAL u_i_j_smooth;
  REAL du_i_j, du_max;

  omega =  mg->info.relaxation_factor;
  grid  = mg->grids[level];
  dy = Grid_delta_y(grid);
  dx = Grid_delta_x(grid);
  dydx2 = dy/dx; dydx2 *= dydx2;
  dy2 = dy*dy;
  du_max = 0;


  //
  // Update the ghost cells
  //
  MG_explicit_ghost_eval(mg, level, u);
  
  //
  // Start smoothing
  //
  for (i = 1 ; i <= grid->n_cv_x ; ++i)
    {
      for (j = 1; j <= grid->n_cv_y ; ++j)
        {
          // Get all the values
          u_i_j  = DOFVector_get_value(u, i,   j);
          u_ip_j = DOFVector_get_value(u, i+1, j);
          u_im_j = DOFVector_get_value(u, i-1, j);
          u_i_jp = DOFVector_get_value(u, i,   j+1);
          u_i_jm = DOFVector_get_value(u, i,   j-1);
          f_i_j  = DOFVector_get_value(f, i,   j);
          // if(j == grid->n_cv_x) printf("ughost: %lf\n",u_i_jp);
          
          u_i_j_smooth =
            (dydx2 * (u_im_j + u_ip_j) + (u_i_jp + u_i_jm)
             - f_i_j * dy2) / 2 / (dydx2+1);
          du_i_j = u_i_j_smooth - u_i_j;
          du_max = MAX(ABS(du_i_j), du_max);
          du_i_j *= omega;
          DOFVector_set_value(du, i, j, du_i_j);
	}
    }

  // Return maximum change in solution
  return du_max;
}

REAL MG_smooth_gauss_seidel(struct MG* mg,
                              int level,
                              struct DOFVector *u,
                              struct DOFVector *f,
                              struct DOFVector *du)
{
  int i,j;
  REAL omega;
  struct Grid *grid;
  REAL dy, dx, dydx2, dy2;
  // smoothing
  REAL u_i_j, u_ip_j, u_im_j, u_i_jp, u_i_jm;
  REAL f_i_j;
  REAL u_i_j_smooth;
  REAL du_i_j, du_max;

  omega =  mg->info.relaxation_factor;
  grid  = mg->grids[level];
  dy = Grid_delta_y(grid);
  dx = Grid_delta_x(grid);
  dydx2 = dy/dx; dydx2 *= dydx2;
  dy2 = dy*dy;
  du_max = 0;

  //
  // Update the ghost cells
  //
  MG_explicit_ghost_eval(mg, level, u);
  
  //
  // Start smoothing
  //
  // Use the SECOND entry for buffering
  struct DOFVector *ubuffer = mg->u[TWO][level];
  DOFVector_copy(u, ubuffer);
  for (i = 1 ; i <= grid->n_cv_x ; ++i)
    {
      for (j = 1; j <= grid->n_cv_y ; ++j)
        {
          // Get all the values
          u_i_j  = DOFVector_get_value(ubuffer, i,   j);
          u_ip_j = DOFVector_get_value(ubuffer, i+1, j);
          u_im_j = DOFVector_get_value(ubuffer, i-1, j);
          u_i_jp = DOFVector_get_value(ubuffer, i,   j+1);
          u_i_jm = DOFVector_get_value(ubuffer, i,   j-1);
          f_i_j  = DOFVector_get_value(f, i,   j);
          // if(j == grid->n_cv_x) printf("ughost: %lf\n",u_i_jp);
          
          u_i_j_smooth =
            (dydx2 * (u_im_j + u_ip_j) + (u_i_jp + u_i_jm)
             - f_i_j * dy2) / 2 / (dydx2+1);
          du_i_j = u_i_j_smooth - u_i_j;
          du_max = MAX(ABS(du_i_j), du_max);
          du_i_j *= omega;
          DOFVector_set_value(ubuffer, i, j, u_i_j + du_i_j);
	}
    }
  DOFVector_axby(-1, u, 1, ubuffer, du);

  // Return maximum change in solution
  return du_max;
}

// Theoretically I can also include the ghosts in the implicit solve.
// But I won't ...
// This is just a sketch, it does not work yet.
REAL MG_smooth_ADI(struct MG* mg,
                     int level,
                     struct DOFVector *u,
                     struct DOFVector *f,
                     struct DOFVector *du)
{
  #if 0
  int i,j;
  REAL omega;
  struct Grid *grid;
  REAL dy, dx, dydx2, dy2;
  
  // data on the current grid
  REAL u_i_j, u_ip_j, u_im_j, u_i_jp, u_i_jm;
  REAL f_i_j;
  REAL du_i_j, du_max;

  // Implicit matrix to be solved
  REAL *u_line;
  REAL *f_line;
  REAL (*A_line)[3];

  omega =  mg->info.relaxation_factor;
  grid  = mg->grids[level];
  dy = Grid_delta_y(grid);
  dx = Grid_delta_x(grid);
  dydx2 = dy/dx; dydx2 *= dydx2;
  dy2 = dy*dy;
  du_max = 0;

  //
  // Update the ghost cells
  //
  MG_explicit_ghost_eval(mg, level, u);
  
  //
  // Start smoothing
  //
  
  // Use the SECOND entry for buffering
  struct DOFVector *ubuffer = mg->u[TWO][level];
  DOFVector_copy(u, ubuffer);

  // Allocate memory
  u_line = ;
  f_line = ;
  A_line = ;

  // SOlve in J direction
  for (i = 1 ; i <= grid->n_cv_x ; ++i)
    {
      for (j = 1; j <= grid->n_cv_y ; ++j)
        {
          // Get all the values
          u_i_j  = DOFVector_get_value(ubuffer, i,   j);
          u_ip_j = DOFVector_get_value(ubuffer, i+1, j);
          u_im_j = DOFVector_get_value(ubuffer, i-1, j);
          u_i_jp = DOFVector_get_value(ubuffer, i,   j+1);
          u_i_jm = DOFVector_get_value(ubuffer, i,   j-1);
          f_i_j  = DOFVector_get_value(f, i,   j);
          // if(j == grid->n_cv_x) printf("ughost: %lf\n",u_i_jp);

          // Set up the equations
          u_i_j_smooth =
            (dydx2 * (u_im_j + u_ip_j) + (u_i_jp + u_i_jm)
             - f_i_j * dy2) / 2 / (dydx2+1);
          du_i_j = u_i_j_smooth - u_i_j;
          du_max = MAX(ABS(du_i_j), du_max);
          du_i_j *= omega;

          // Solve the equations 
          DOFVector_set_value(ubuffer, i, j, u_i_j + du_i_j);

          // Update ubuffer
	}
    }

  // SOlve in I direction
  for (i = 1 ; i <= grid->n_cv_x ; ++i)
    {
      for (j = 1; j <= grid->n_cv_y ; ++j)
        {
          // Get all the values
          u_i_j  = DOFVector_get_value(ubuffer, i,   j);
          u_ip_j = DOFVector_get_value(ubuffer, i+1, j);
          u_im_j = DOFVector_get_value(ubuffer, i-1, j);
          u_i_jp = DOFVector_get_value(ubuffer, i,   j+1);
          u_i_jm = DOFVector_get_value(ubuffer, i,   j-1);
          f_i_j  = DOFVector_get_value(f, i,   j);
          // if(j == grid->n_cv_x) printf("ughost: %lf\n",u_i_jp);

          // Set up the equations
          u_i_j_smooth =
            (dydx2 * (u_im_j + u_ip_j) + (u_i_jp + u_i_jm)
             - f_i_j * dy2) / 2 / (dydx2+1);
          du_i_j = u_i_j_smooth - u_i_j;
          du_max = MAX(ABS(du_i_j), du_max);
          du_i_j *= omega;

          // Solve the equations 
          DOFVector_set_value(ubuffer, i, j, u_i_j + du_i_j);

          // Update ubuffer
	}
    }

  // Free memory
  free(u_line);
  free(A_line);
  free(f_line);
  
  //Return the change in the solution
  DOFVector_axby(-1, u, 1, ubuffer, du);

  // Return maximum change in solution
  return du_max;
  
  #endif
  EXIT();
  return 0;
}

/* =========================================================
   RESTRICTION
   ========================================================= */

void MG_restrict_inject(struct MG* mg,
                        int from,
                        int to,
                        struct DOFVector* dvf,
                        struct DOFVector* dvt)
{
  struct Grid *gf, *gt;
  int i_f0, i_f1, j_f0, j_f1, i_t, j_t;
  REAL u_f_i0_j0, u_f_i0_j1, u_f_i1_j0, u_f_i1_j1, u_t_i_j;

  gf = dvf->grid;
  gt = dvt->grid;
  assert( gf == mg->grids[from] );
  assert( gt == mg->grids[to] );
  assert( from + 1 == to );

  for (i_t =1 ; i_t <= gt->n_cv_x ; ++i_t )
    {
      for (j_t =1 ; j_t <= gt->n_cv_y ; ++j_t )
        {
          i_f0 = 2*i_t - 1;
          i_f1 = 2*i_t ;
          j_f0 = 2*j_t - 1;
          j_f1 = 2*j_t ;

          u_f_i0_j0 = DOFVector_get_value(dvf, i_f0, j_f0);
          u_f_i0_j1 = DOFVector_get_value(dvf, i_f0, j_f1);
          u_f_i1_j0 = DOFVector_get_value(dvf, i_f1, j_f0);
          u_f_i1_j1 = DOFVector_get_value(dvf, i_f1, j_f1);
          u_t_i_j = 0.25 * (u_f_i0_j0 + u_f_i0_j1 + u_f_i1_j0 + u_f_i1_j1);
          DOFVector_set_value(dvt, i_t, j_t, u_t_i_j);
        }
    }
}

/* =========================================================
   PROLONGATION
   ========================================================= */

void MG_prolongate_inject(struct MG* mg,
                          int from,
                          int to,
                          struct DOFVector* dvf,
                          struct DOFVector* dvt)
{
  struct Grid *gf, *gt;
  int i_t0, i_t1, j_t0, j_t1, i_f, j_f;
  REAL u_t_i0_j0, u_t_i0_j1, u_t_i1_j0, u_t_i1_j1;
  REAL u_f_i_j; //, u_f_i_jm, u_f_i_jp, u_f_im_j, u_f_ip_j;

  gf = dvf->grid;
  gt = dvt->grid;
  assert( gf == mg->grids[from] );
  assert( gt == mg->grids[to] );
  assert( from - 1 == to );

  for (i_f =1 ; i_f <= gf->n_cv_x ; ++i_f )
    {
      for (j_f =1 ; j_f <= gf->n_cv_y ; ++j_f )
        {
          i_t0 = 2*i_f - 1;
          i_t1 = 2*i_f ;
          j_t0 = 2*j_f - 1;
          j_t1 = 2*j_f ;

          u_f_i_j = DOFVector_get_value(dvf, i_f, j_f);
          u_t_i0_j0 = u_f_i_j;
          u_t_i1_j0 = u_f_i_j;
          u_t_i0_j1 = u_f_i_j;
          u_t_i1_j1 = u_f_i_j;
          DOFVector_set_value(dvt, i_t0, j_t0, u_t_i0_j0);
          DOFVector_set_value(dvt, i_t0, j_t1, u_t_i0_j1);
          DOFVector_set_value(dvt, i_t1, j_t0, u_t_i1_j0);
          DOFVector_set_value(dvt, i_t1, j_t1, u_t_i1_j1);
        }
    }
}


//
// Uses boundary conditions for interpolation. This might or might
// not be desired. Let's see how it works. Later I might try not
// incorporating the boundary conditions as well.
void MG_prolongate_interpolate(struct MG* mg,
                               int from,
                               int to,
                               struct DOFVector* dvf,
                               struct DOFVector* dvt)
{
  struct Grid *gf, *gt;
  int i_t0, i_t1, j_t0, j_t1, i_f, j_f;
  REAL u_t_i0_j0, u_t_i0_j1, u_t_i1_j0, u_t_i1_j1;
  REAL u_f_im_jm, u_f_im_j, u_f_im_jp;
  REAL u_f_i_jm ,  u_f_i_j, u_f_i_jp;
  REAL u_f_ip_jm, u_f_ip_j, u_f_ip_jp;

  gf = dvf->grid;
  gt = dvt->grid;
  assert( gf == mg->grids[from] );
  assert( gt == mg->grids[to] );
  assert( from - 1 == to );

  //
  // Update the ghost points on the from DOFVector
  // Since we only interpolate the error, it should always use
  // homogenous boundary conditions.
  // Therefore,I pass the level as -1.
  //
  MG_explicit_ghost_eval(mg, -1, dvf);

  //
  // Interpolate for all (the corners will be garbage)
  // Then use injection for those four cases.
  // 
  for (i_f =1 ; i_f <= gf->n_cv_x ; ++i_f )
    {
      for (j_f =1 ; j_f <= gf->n_cv_y ; ++j_f )
        {
          // Numbers on fine grid
          i_t0 = 2*i_f - 1;
          i_t1 = 2*i_f ;
          j_t0 = 2*j_f - 1;
          j_t1 = 2*j_f ;

          // Values on coarse grid ( 9 point stencil )
          u_f_im_jm = DOFVector_get_value(dvf, i_f-1, j_f-1);
          u_f_im_j  = DOFVector_get_value(dvf, i_f-1, j_f  );
          u_f_im_jp = DOFVector_get_value(dvf, i_f-1, j_f+1);
          u_f_i_jm  = DOFVector_get_value(dvf, i_f  , j_f-1);
          u_f_i_j   = DOFVector_get_value(dvf, i_f  , j_f  );
          u_f_i_jp  = DOFVector_get_value(dvf, i_f  , j_f+1);
          u_f_ip_jm = DOFVector_get_value(dvf, i_f+1, j_f-1);
          u_f_ip_j  = DOFVector_get_value(dvf, i_f+1, j_f  );
          u_f_ip_jp = DOFVector_get_value(dvf, i_f+1, j_f+1);

          // interpolate
          u_t_i1_j1 = 0.5*u_f_i_j + 0.25*(u_f_ip_j + u_f_i_jp);
          u_t_i1_j0 = 0.5*u_f_i_j + 0.25*(u_f_ip_j + u_f_i_jm);
          u_t_i0_j0 = 0.5*u_f_i_j + 0.25*(u_f_im_j + u_f_i_jm);
          u_t_i0_j1 = 0.5*u_f_i_j + 0.25*(u_f_im_j + u_f_i_jp);
          UNSUSED(u_f_im_jp); UNSUSED(u_f_ip_jp);
          UNSUSED(u_f_im_jm); UNSUSED(u_f_ip_jm);

          // set values
          DOFVector_set_value(dvt, i_t0, j_t0, u_t_i0_j0);
          DOFVector_set_value(dvt, i_t0, j_t1, u_t_i0_j1);
          DOFVector_set_value(dvt, i_t1, j_t0, u_t_i1_j0);
          DOFVector_set_value(dvt, i_t1, j_t1, u_t_i1_j1);
        }
    }
}

/* =========================================================
   CYCLING
   ========================================================= */

// Assumes that u[ZERO][level] are set f[ZERO][level].
// Returns a correction to be added to u[ZERO][level].
// Internally uses u[ONE][level]
void MG_cycle(struct MG* mg,
              int level,
              struct ConvergenceHistory* ch,
              struct DOFVector *du)
{
  int i;
  int tau;
  
  switch(mg->info.cycle_type)
    {
      case CYCLE_TYPE_V: tau = 1;  break;
      case CYCLE_TYPE_W: tau = 2;  break;
      default: tau=-1; EXIT();
    }
  
  if( level == mg->info.n_levels - 1 )
    {
      // Copy the ZERO solution to ONE
      DOFVector_copy(mg->u[ZERO][level], mg->u[ONE][level]);
      DOFVector_copy(mg->f[ZERO][level], mg->f[ONE][level]);
      
      // Perform smoothing
      for( i = 0 ; i < mg->info.n_smoothing[level] ; ++i )
        {
          MG_smooth(mg, level, mg->u[ONE][level], mg->f[ONE][level], mg->du[ONE][level]);
          DOFVector_axby(1, mg->u[ONE][level], 1., mg->du[ONE][level], mg->u[ONE][level]);
        }

      // Return the change in the solution
      DOFVector_axby(-1, mg->u[ZERO][level], 1., mg->u[ONE][level], du);
    }
  else
    {
      assert( level >= 0 );
      assert( level < mg->info.n_levels - 1 );

      // Copy the solution from ZERO to ONE
      DOFVector_copy(mg->u[ZERO][level], mg->u[ONE][level]);
      DOFVector_copy(mg->f[ZERO][level], mg->f[ONE][level]);

      // Perform pre-smoothing
      for( i = 0 ; i < mg->info.n_smoothing[level] ; ++i )
        {
          MG_smooth(mg, level, mg->u[ONE][level], mg->f[ONE][level], mg->du[ONE][level]);
          DOFVector_axby(1, mg->u[ONE][level], 1., mg->du[ONE][level], mg->u[ONE][level]);
        }

      // Find the residual and move it to the upper level
      // Set the solution (correction) on the upper level
      // Move the solution to the upper level
      MG_residual(mg, level, mg->u[ONE][level], mg->f[ONE][level], mg->r[ONE][level]);
      MG_restrict_inject(mg, level, level+1, mg->r[ONE][level], mg->f[ZERO][level+1]);
      DOFVector_set_zero(mg->u[ZERO][level+1]);

      // Get the correction on the upper level and add it to the solution
      for( i = 0 ; i < tau ; ++i)
        {
          MG_cycle(mg, level+1, ch, mg->du[ZERO][level+1]);
          DOFVector_axby(1., mg->u[ZERO][level+1], 1., mg->du[ZERO][level+1], mg->u[ZERO][level+1]);
        }

      // Move the correction from the upper level to the current level
      // and add it to the solution on currecnt level
      MG_prolongate_interpolate(mg, level+1, level, mg->u[ZERO][level+1], mg->du[ONE][level]);
      // MG_prolongate_inject(mg, level+1, level, mg->u[ZERO][level+1], mg->du[ONE][level]);
      DOFVector_axby(1., mg->u[ONE][level], 1., mg->du[ONE][level], mg->u[ONE][level]);

      // Post smoothing
      for( i = 0 ; i < mg->info.n_smoothing[level] ; ++i )
        {
          MG_smooth(mg, level, mg->u[ONE][level], mg->f[ONE][level], mg->du[ONE][level]);
          DOFVector_axby(1, mg->u[ONE][level], 1., mg->du[ONE][level], mg->u[ONE][level]);
        }

      // Return the change in the solution
      DOFVector_axby(-1, mg->u[ZERO][level], 1., mg->u[ONE][level], du);
    }
}

/*
  Local Variables:
  mode:c
  c-file-style:"GNU"
  c-file-offsets:((innamespace . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
