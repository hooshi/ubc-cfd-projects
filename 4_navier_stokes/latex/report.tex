%% -------------------------------------------------------
% Preamable ---------------------------------------------
%--------------------------------------------------------
%\documentclass[letter,12pt,draft]{article}
\documentclass[letter,11pt]{article}
%-------------------------------------------------------
 \usepackage [colorlinks, linkcolor=blue,
 citecolor=magenta, urlcolor=cyan]{hyperref}   % pdf links
% \usepackage {hyperref}
\usepackage{graphicx} % inserting image
\usepackage{setspace} % double/single spacing
\usepackage[top=2cm,right=2cm,bottom=2cm,left=2cm]{geometry} % margins 
\usepackage{amsmath}  %math formulas
\usepackage{array}    %beautiful tables
\usepackage[export]{adjustbox} %aligning images
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{color,soul} %highlight
\usepackage{subcaption}        %figures with subfigures
\usepackage{amssymb}         %fonts for N, R, ...
\usepackage[font={small}]{caption} %smaller caption font
\usepackage[parfill]{parskip}

\graphicspath{{img/}} %where the images are

\newenvironment{tight_itemize}{
\begin{itemize}
  \setlength{\itemsep}{0pt}
  \setlength{\parskip}{0pt}
}{\end{itemize}}

\newenvironment{tight_enumerate}{
\begin{enumerate}
  \setlength{\itemsep}{0pt}
  \setlength{\parskip}{0pt}
}{\end{enumerate}}


\newcommand*\rfrac[2]{{}^{#1}\!/_{#2}}
\newcommand*{\hvec}[1]{\mathbf{#1}}
\newcommand*{\bvec}[1]{\bar{\hvec{#1}}}
\newcommand*{\hmat}[1]{\mathbf{#1}}

% -------------------------------------------------------
% Author and title---------------------------------------
%--------------------------------------------------------
\title{Final Project, MECH510}
\date{December 2015}
\author{Shayan Hoshyari\\ Student \#: 81382153}

% -------------------------------------------------------
% Begin document-----------------------------------------
%--------------------------------------------------------
\begin{document}
\pagenumbering{arabic}
\maketitle

% -------------------------------------------------------
% Tables--------------------------------------------------
%--------------------------------------------------------
\tableofcontents

% -------------------------------------------------------
% ch1-Problem--------------------------------------------
%--------------------------------------------------------
\section{The Problem}
\label{ch:problem}

In this project we will solve for the steady state solution of the
two-dimensional, incompressible, laminar Navier-Stokes equations on
regular Cartesian grids. Combined with the artificial compressibility
method the equations read as:
\[
\hvec{U}_t + \hvec{F}_x + \hvec{G}_y = 0
\]
The vectors $\hvec{U}$, $\hvec{F}$ and $\hvec{G}$ are the unknowns, the
flux in the $x$ direction, and the flux in the $y$ direction
respectively.  They are defined as:
\[
\hvec{U}=
\begin{bmatrix}
  p  \\
  u  \\
  v  \\
\end{bmatrix} \quad
%
\hvec{F} =
\begin{bmatrix}
   u/\beta - A  p_x \Delta x \Delta y\\
   u^2 + p - u_x/\text{Re}\\
   uv - v_x/\text{Re}\\
\end{bmatrix} \quad
%
\hvec{G} =
\begin{bmatrix}
   v/\beta - A  p_y \Delta x \Delta y\\
   uv - u_y/\text{Re}\\
   v^2 + p - v_y/\text{Re}\\
\end{bmatrix}
\]
The Reynolds number is the governing dimensionless number and is shown
by Re. $p$ is the pressure, while $u$ and $v$ represent the velocities
in $x$ and $y$ direction respectively. $\beta$ is the artificial
compressiblity factor and $A$ introduces an additional second order
error term in the solution in expense of removing pressure
oscillations.
 
The computational domain is a rectangular box with width $W=1$ and
height $H$. Wall boundary conditions are used for all the four sides
of the box:
\[
p_n=0, \quad u=U_\text{wall}, \quad v=V_\text{wall}
\]
Finally, the upper wall moves with the speed $U_\text{top}$, while the other walls 
are stationary. 

% -------------------------------------------------------
% ch2-Numerical Method-----------------------------------
% --------------------------------------------------------
\section{Numerical Method}
\label{ch:numeric}

Solving the Navier-Stokes equations is divided into two main parts: space
discretization and time discretization. To discretize the equations in
space we use the finite volume method and the second order central
interpolation scheme to transform the original PDE into a set of
algebraic differential equations:
\[ \frac{d \bvec{U}}{d t} = \hvec{R}(\bvec{U}) \] 
%
Where $\hvec{R}$ is the flux integral of all cells divided by cell
size and $\bvec{U}$ is the average value of solution for all cells.
%
We are only interested in the steady-state solution
$\hvec{R}(\bar{\hvec{U}}) = \hvec{0}$. However, we will take advantage
of the transient form of the equations and use a combination of the
Implicit Euler method and the Newton's method to find the steady state
solution. The Implicit Euler method transforms the set of algebraic
differential equations into a system of nonlinear equations (subscript $n$
means at time step $n$):
\[ 
   \bvec{U}_{n+1} = \bvec{U}_n + \Delta t \hvec{R}(\bvec{U}_{n+1})
\] 
%
As we are not interested in the transient solution, instead of solving
this system exactly we will only perform one Newton iteration on it:
\[ 
\left( \frac{\hmat{I}} { \Delta t} -
\frac{\partial \hvec{R}}{\partial\bvec{U}}(\bvec{U}_n) \right)
\delta \hvec{U} = \hvec{R}(\bvec{U}_n)
\]
We solve this system using the approximate factorization method.
% 
Finally we update the solution, optionally using over-relaxation:
\[ \bvec{U}_{n+1} = \bvec{U}_n + \omega \delta \hvec{U}\] 

This process in continued until certain criteria are met, e.g., $\|
\delta \hvec{U} \| < \epsilon$ where $\epsilon$ is a very small
number.

% -------------------------------------------------------
% ch3-results--------------------------------------------
% -------------------------------------------------------
 \section{Results}
 \label{ch:result} 

In this section the results for each part of the project are
presented.

%----------------------------------------------------------
% RESULT: verifres
%----------------------------------------------------------
\subsection{Correctness of Residual}
\label{ch:res-verifres}

The method of manufactured solutions will be used to
verify the correctness of the flux integral code. The computational
domain is defined as the region: $[0 \quad 1]\times[0 \quad 1]$. The
average values of unknowns for internal cells are found using the
midpoint integration rule and the formula:
%
\[
\hvec{U} = 
\begin{bmatrix}
  p \\
  u\\
  v  \\
\end{bmatrix} =
\begin{bmatrix}
  \cos(\pi x) \cos(\pi y) \\
  \sin(\pi x) \sin(2 \pi y)\\
  \sin(2 \pi x) \sin(\pi y)  \\
\end{bmatrix}
\]
%
The average solution values for the ghost cells are found using the
boundary conditions ($U_\text{top} = 0$). Then the values $R_{ij}$
will be calculated and compared with their exact counterparts. Due to their
huge size the exact values are not given here. However, they can be
found by inserting the known values of $p$, $u$ and $v$ into the
definitions of $\hvec{F}$ and $\hvec{G}$.

In Figure \ref{fig:verifres} the error norms of the flux integral
(both all together and separately) are shown for a set of refined
meshes, while Re=$10$, $\beta=1$ and $A=0$. To study the behavior of
flux integral error more closely, Table \ref{tab:verifres} shows the
$L_2$ error norms of the flux integral for the $20\times20$ and
$40\times40$ meshes. The fact that all error norms get four times
smaller when $\Delta x$ and $\Delta y$ are halved verifies that the
flux integral is coded correctly. It is also interesting that the 
error norms for the velocity fluxes are almost identical.

\begin{table}
\center
\caption{$L_2$ Error norms of flux integral for two different meshes.}
\label{tab:verifres}
  \begin{tabular}{c c c c c}
    \hline
    Mesh size &All terms &$p$ flux &$u$ flux &$v$ flux \\
    \hline
    $20\times20$ &$4.142\times 10^{-2}$ &$1.198\times 10^{-2}$
    &$5.002\times 10^{-2}$ &$5.002\times 10^{-2}$ \\ 
    $40\times40$ &$1.042\times 10^{-2}$ &$0.299\times 10^{-2}$
    &$1.258\times10^{-2}$ &$1.258\times 10^{-2}$ \\
    \hline
  \end{tabular}
\end{table}

\begin{figure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{verif-flux1.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{verif-flux2.eps}
  \end{subfigure}
  % 
  \caption{The left figure shows the norms of the error in all fluxes
    all together, while the right figure shows the $L_2$ norm of the
    error in each flux separately. The horizontal axis shows the number of
    control volumes in each direction and the slopes are found using least
    square curve fitting.}
  \label{fig:verifres}
\end{figure}

%----------------------------------------------------------
% RESULT: verifjac
%----------------------------------------------------------
\subsection{Correctness of Jacobian}
\label{ch:res-verifjac}

To verify the Jacobian code, the following approximation will be used:
\[ 
\left( \frac{\partial \hvec{R}}{\partial\bvec{U}} \right)
\delta \hvec{U} \approx 
\hvec{R}( \bvec{U} + \delta \hvec{U} ) - \hvec{R}( \bvec{U})
\]

Generally, $\delta \hvec{U}$ can be any vector with a small
magnitude. However, for simplicity, we set it to be:
\[ 
\delta \hvec{U}_{ij} = 
\begin{cases}
10 ^ {-6}\hmat{I}  & i = j = c \\
0           & \text{otherwise}
\end{cases}
\]
As we are using the central method to interpolate the solution at
control volume faces, we know that the only fluxes that are going to
change are $\hvec{R}_{c-1,c}, \hvec{R}_{c+1,c}, \hvec{R}_{c,c+1}$, and
$\hvec{R}_{c,c-1}$. 

In Table \ref{tab:verifjac} the difference in the two methods to
approximate the change in flux integral is shown for
$\hvec{R}_{c-1,c}, \hvec{R}_{c+1,c}, \hvec{R}_{c,c+1}$, and
$\hvec{R}_{c,c-1}$, when the values of $\bvec{U}$ and Re are the same
as Section \ref{ch:res-verifres}, $c=10$, and a $20\times20$ mesh is
used. Being smaller than $10^{-10}$, these small differences verify
the correctness of the Jacobian. It is also worth mentioning that the
errors for the $p$ flux are the smallest because this flux is linear
in $u$ and $v$, so the approximation is actually exact and the
difference is due to computer round off errors.

\begin{table}
\center
\caption{Differences in approximating the change in flux integral
  using the Jacobian or the flux integral itself. The initial solution
  values are the same as Section \ref{ch:res-verifres}, and the solution
  at cell $i=j=10$ is perturbed by an amount of $10 ^ {-6}$. }
\label{tab:verifjac}
  \begin{tabular}{c c c c c}
    \hline
    Cell &$p$ flux &$u$ flux &$v$ flux \\
    \hline
    $(10,9)$ &$-5.09\times 10^{-16}$ &$-4.99\times 10^{-12}$
    &$-4.99\times 10^{-12}$ \\
    $(9,10)$ &$-1.39\times 10^{-15}$ &$-5.00\times 10^{-12}$
    &$-4.99\times 10^{-12}$ \\
    $(10,10)$ &$8.88\times 10^{-16}$ &$-7.11\times 10^{-16}$
    &$-7.66\times 10^{-16}$ \\
    $(10,11)$ &$6.55\times 10^{-17}$ &$5.00\times 10^{-12}$
    &$4.99\times 10^{-12}$ \\
    $(11,10)$ &$6.55\times 10^{-17}$ &$4.99\times 10^{-12}$
    &$4.99\times 10^{-12}$ \\
    \hline
  \end{tabular}
\end{table}


%----------------------------------------------------------
% RESULT: Stability Validation
%----------------------------------------------------------
\subsection{Validation Case: Stability}
\label{ch:res-stab}

In this section the equations will be solved for $W=H=1$ and
$U_\text{top} = 0$ on a $20\times20$ grid. It is expected that the
solution converge to the value of zero everywhere. The values
introduced in Section \ref{ch:res-verifres} are used as initial
conditions (except that the origin is at the top left corner of the
box instead of the lower left corner). Other parameters are set to
$A=0$, $\beta=1$, Re$=100$ and $\Delta t = 0.05$.  To study the effect
of over-relaxation three different values for $\omega$ will be tested.

Figure \ref{fig:stab} shows the $L_2$ norm of change in solution for
the first $200$ iterations. It is observed that over-relaxing the
solution enhances convergence after a certain amount of iterations
($50$ in this case). The $L_2$ norm of change in all variables is
monotonically decreasing. However, individual variables demonstrate an
oscillatory convergence history. In Section \ref{ch:res-basic1} it
will be shown that a smart choice of time step can prevent this
behavior and enhance convergence considerably. Fortunately, these
oscillation begin to damp after a few hundred iterations and the
convergence history becomes completely monotonic (not shown in the
graph).

\begin{figure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{stabil-tot.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{stabil-p.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{stabil-u.eps}
  \end{subfigure}
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{stabil-v.eps}
  \end{subfigure}
  \caption{Convergence history for $U_\text{top} = 0$. Different values of $\omega$ are
    used to study the effect of over-relaxation.}
  \label{fig:stab}
\end{figure}

%----------------------------------------------------------
% RESULT: Basic Utop = 1
%----------------------------------------------------------
\subsection{Solution when $U_{top} = 1$}
\label{ch:res-basic1}

In this section the problem will be solved for $U_{top} = 1$ on a
$20\times20$ grid. Optimum values of $\Delta t = 0.27$ and
$\omega=1.3$ are found experimentally and will be used. We iterate
until the largest $L_2$ norm in solution gets smaller than $10
^{-6}$. Other parameters have the same value as Section
\ref{ch:res-stab}.

Figure \ref{fig:basic1-press} shows the contour plots of the pressure.
As $A$ is chosen to be zero the solution converges to an oscillatory
pressure field. However, Paraview's interpolation mechanism does not
show this phenomenon in the plot. The figure also shows that the
pressure is unbounded at the top right and left corners, where it has
a maximum and minimum respectively. There is also a local minimum for
the pressure field somewhere around $(0.7, 0.7)$.

Figure \ref{fig:basic1-conv} shows the convergence history. The choice
of $\Delta t$ in this case has improved convergence immensely. The
oscillatory behavior of $L_2$ norm in change of solution is gone and
the number of iterations is four times smaller compared to $\Delta t =
0.05$. My experiments in using different values of $\Delta t$ showed
that there is a range of $\Delta t$ for which the solution converges,
with the optimum time step somewhere in between. For $\Delta t = 0.05$
the solution always converges even for $300$ meshes per unit
length. However, the maximum and optimum time steps get smaller as the
mesh gets finer. For example, the optimum time step is around $0.27$
and $0.10$ for $20\times20$ and $180\times180$ meshes respectively.

Figure \ref{fig:basic1-vel} shows the $u$ velocity on the line
$x=0.5$. In this graph the value of $u$ at $y=0,1$ is obtained using
the exact enforced boundary conditions, whereas other values of $u$
are obtained by interpolating from the average control volume values
of $u$ from the two cells adjacent to the line $x=0.5$.

\begin{figure}
%
  \includegraphics[width=.45\textwidth,center]{basic-20-pressure.png}
  \caption{Pressure contours for $U_\text{top} = 1$, $H=1$ and a
    $20\times20$ grid. As the pressure is unbounded at the top corners the
    contours are plotted for $|p|<0.1$, and the numerical values of the
    pressure at the corners are presented separately.}
  \label{fig:basic1-press}
  \vspace{1.5cm}
  % 
  \begin{minipage}{.45\textwidth} 
    \includegraphics[width=1.1\linewidth,center]{basic-20-conv.eps}     
    \caption{Convergence history for $U_\text{top} = 1$, $H=1$ and a
      $20\times20$ grid.}
    \label{fig:basic1-conv}
  \end{minipage} \hspace{2cm}
  % 
  \begin{minipage}{0.45\textwidth}
    \includegraphics[width=1.1\linewidth,center]{basic-20-vline.eps}
    \caption{$u$ velocity on the vertical symmetry line for
      $U_\text{top} = 1$, $H=1$ and a $20\times20$ grid.}
    \label{fig:basic1-vel}
  \end{minipage}
  % 
\end{figure}

%----------------------------------------------------------
% RESULT: Basic Symmetry
%----------------------------------------------------------
\subsection{Sanity check: Symmetry}
\label{ch:res-basic2}

In this section the problem will be solved with the same parameters as
Section \ref{ch:res-basic1}, except for $U_\text{top}=-1$. Because of
symmetry it is expected that the value $u_{U\text{top}=-1}(1-x,y) +
u_{U\text{top}=1}(x,y)$ be very small through out the domain. Figure
\ref{fig:basic2} shows this property for two different cases: when the
initial conditions are chosen like those of Section
\ref{ch:res-verifres}, and when the initial conditions are zero for all
unknowns. Interestingly, when the nonzero initial conditions
are used, the deviation from symmetry follows a pattern similar to the
initial conditions. On the other hand, for the zero initial conditions
the deviation does not follow any pattern and is even smaller by a
factor of $10^{-10}$.

\begin{figure}
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{basic-20-diff-nosin.png}
  \end{subfigure}
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{basic-20-diff.png}
  \end{subfigure}
  % 
  \caption{The value of $u_{U\text{top}=-1}(1-x,y) +
    u_{U\text{top}=1}(x,y)$ for a $20\times20$ mesh and Re=$100$. In
    the left figure zero initial conditions are used for all the
    solution variables, whereas on the right figure the initial
    conditions were chosen similar to those of Section
    \ref{ch:res-verifres}.}
  \label{fig:basic2}
\end{figure}

%----------------------------------------------------------
% RESULT: Basic Symmetry
%----------------------------------------------------------
\subsection{Grid Convergence}
\label{ch:res-basic3}

The same problem from Section \ref{ch:res-basic1} will be solved on a
series of refined meshes ($10\times10$, $20\times20$, $40\times40$ and
$80\times80$) to study grid convergence. I will use Richardson
Extrapolation on the $u$ velocity profile on the vertical symmetry
line. Figure \ref{fig:basic3-vel} shows the $u$ velocity on this
line for all the meshes. The solution changes considerably from the
$10\times10$ mesh to the $20\times20$, while the change for further 
refinements is relatively subtle.

To investigate grid convergence further, I will compute the norm of
the change in the velocity from the coarse to the refined mesh for
each case.  A simple interpolation was needed to find the solution at
the same points for the two different meshes. Figure
\ref{fig:basic3-err} shows these norms. Then I will use Richardson
Extrapolation to find order of convergence according to the formula:
\[ p = \log_2 \left( \frac{\|u_1 - u_2\|}{\|u_2 - u_3\|} \right) \]
where $1$ and $3$ stand for the coarsest and finest meshes
respectively.
%
Table \ref{tab:basic3} shows the order of convergence found using
different norm types and two different sets of meshes. As we are using
a second order method to solve a non-linear system of equations, we
expect the order to be somewhere between one and two. The fact that
for the first series of meshes the $L_\infty$ based order of
convergence is bigger than two, suggests that these meshes might
not be fine enough and the change in solution has not approached its
asymptotic behavior yet. The orders of convergence for the second
series of meshes are all in the acceptable range, and demonstrate grid
convergence.


\begin{table}
\centering
%
\caption{The order of convergence obtained for the velocity profile on
  the vertical symmetry line, when Re$=100$, $H=1$ and $U=1$. Two
  series of meshes are used, and the analysis is carried out using the
  $L_1$, $L_2$ and $L_\infty$ norm of the change in the profile.}
%
\label{tab:basic3}
  \begin{tabular}{c c c c}
    \hline
    Meshes &$L_1$ &$L_2$ &$L_\infty$ \\
    \hline
    $10\times10$, $20\times20$, $40\times40$
    &1.45 &1.70 &2.43 \\ 
    $20\times20$, $40\times40$, $80\times80$ 
    &1.90 &1.77 &1.41 \\
    \hline
  \end{tabular}
\end{table}


\begin{figure}
  % 
  \begin{minipage}{.45\textwidth}
    \centering
    \includegraphics[width=1.1\linewidth]{basic-conv-vline.eps}
    \caption{ The $u$ velocity on the vertical symmetry line for
      various mesh sizes.\\}
    \label{fig:basic3-vel}
  \end{minipage} \hspace{1cm}
  % 
  \begin{minipage}{0.45\textwidth}
    \centering
    \includegraphics[width=1.1\linewidth]{basic-conv-norm.eps}
    \caption{ The norms of change in the velocity profile
      $u(x=0.5,y)$, with respect to mesh refinement.}
    \label{fig:basic3-err}
  \end{minipage}
  % 
\end{figure}

%----------------------------------------------------------
% RESULT: Result Parameter Tuning
%----------------------------------------------------------
\subsection{Effect of parameters A and $\beta$}
\label{ch:res-param}

In this section I will study the effect of $A$ and $\beta$. The
problem will be solved on a $40\times40$ mesh, with Re$=100$,
$\omega=1$ and $\Delta t=0.05$. The convergence criterion is that the
$L_2$ norm of the change in all unknowns falls below $10^{-13}$.

Figure \ref{fig:res-A} shows the pressure distribution on the line
$y=0.5$ when $\beta=1$. The value $A=0.1$ reduces the oscillations,
but they are still present. The value $A=0.6$ seems to be a good
choice, as it removes most of the oscillations. Increasing $A$ further
does not seem to have any effect on the solution. Not only does a good
choice for $A$ removes pressure oscillations, but it also improves
convergence, reducing number of iterations from 3000 to 1000.

Figure \ref{fig:res-B} shows the number of iterations required for
convergence when $A=0.6$. Apparently, $\beta=0.6$ improves the
convergence a little bit, although this improvement is subtle compared
to the effect of $\Delta t$, $\omega$ or $A$.

\begin{figure}
  % 
  \begin{minipage}{.45\textwidth}
    \centering
      \includegraphics[width=.9\linewidth]{A2.eps}  
    \caption{Pressure distribution on $y=0.5$ for different values of
      $A$.}
    \label{fig:res-A}
  \end{minipage}\hspace{1cm}
  % 
  \begin{minipage}{0.45\textwidth}
    \centering
    \includegraphics[width=1.1\linewidth]{B-iter.eps} 
    \caption{Number of iterations required for convergence for
      different values of $\beta$.}
    \label{fig:res-B}
  \end{minipage}
  % 
\end{figure}

%----------------------------------------------------------
% RESULT: h=  2
%----------------------------------------------------------
\subsection{Solution for $H=2$}
\label{ch:res-h2}

I will solve the problem for $H=2$, Re$=200$, $A=0.6$ and
$\epsilon=10^{-13}$ then I will estimate the center location and the
strength of the vortices. Figure \ref{fig:h2} shows the streamlines
obtained using a $180\times360$ mesh for this case, verifying that
there are four vortices present. The stream function is found by
integrating the $v$ velocity on horizontal lines using the trapezoidal
integration rule and the initial condition of $\psi = 0$ on the left
wall.

To find the strength of the vortices, I will integrate the vorticity
in the area corresponding to each vortex and then divide it by the
area:
%
\[
\text{strength} = 
\frac{1}{A} \left| \int_A (v_x - u_y)  dA \right| 
\]
%
The regions are identified, using the stream function. Because no
fluid crosses the vortex boundaries, $\psi$ should be constant on
them. As the boundaries are connected to the walls, the value of
$\psi$ on the vortex boundaries is zero. Figure \ref{fig:h2} shows
that $\psi$ also changes sign from one vortex to another, so we can
use it to identify which cell belongs to which vortex.

To find the vortex centers, I used Paraview's glyph filter to plot 
the velocity at the center of all the cells. As I already knew the
approximate location of the centers from the streamlines, I searched
there to find:
%
\begin{tight_itemize}
\item Four cells whose velocity is turning around the common vertex.
  In this case the reference point is the common vertex.
\item A cell with the velocity of its adjacent cells turning around
  it. In this case the reference point will be the center of the
  middle cell.
\end{tight_itemize}
%
Then I did a reconstruction of velocities around the reference point
and solved for $u=v=0$ to find the vortex center. The best reference
point is the one that gives the smallest $x - x|_\text{ref}$ and $y -
y|_\text{ref}$.
%
\[
\begin{bmatrix}
  u_x|_\text{ref} &u_y|_\text{ref}\\
  v_x|_\text{ref} &v_y|_\text{ref}\\
\end{bmatrix}
%
\begin{bmatrix}
 x - x|_\text{ref}\\
 y - y|_\text{ref}\\
\end{bmatrix}
=
\begin{bmatrix}
  - u|_\text{ref}\\
  - v|_\text{ref}\\
\end{bmatrix}
\]

Table \ref{tab:h2} shows the results obtained for each vortex. The
upper, middle, lower right and lower left vortices are numbered $1$,
$2$, $3$ and $4$ respectively. Judging by the strength value of vortex
$3$ and $4$, it seems that the $80\times160$ mesh is too coarse for
this case. Unfortunately, I can not afford finer meshes than
$180\times360$, so I am going to stick with the $80\times160$ mesh for
error estimation.  The cases with the order of convergence higher than
two might also mean that the meshes are not fine enough.

\begin{table}
  \center
  \caption{The center location and the strength of the vortices, and
    their error estimates for $H=2$.}
  \label{tab:h2}
  \begin{tabular}{|l|*3c|*3c|}
    \hline
    &\multicolumn{3}{c|}{vortex $1$} 
    &\multicolumn{3}{c|}{vortex $2$} \\
    &$x_0$ &$y_0$ &strength &$x_0$ &$y_0$ &strength \\    
    \hline
    % 
    Value for $80\times160$ 
    &0.599132&1.661717&1.0756e+00 
    &0.512717&0.779242&2.8117e-02\\
    % 
    Value for $120\times240$
    &0.598578&1.661251&1.0781e+00
    &0.511910&0.781485&2.8389e-02\\
    % 
    Value for $180\times360$
    &0.598308&1.661127&1.0787e+00
    &0.511544&0.782432&2.8438e-02\\
    % 
    Extrapolated Value 
    &0.598051&1.661082&1.0789e+00
    &0.511242&0.783124&2.8448e-02\\
    % 
    Order of Convergence 
    &1.77&3.25&3.55
    &1.95&2.12&4.22\\
    % 
    Estimated Error 
    &0.042934&0.002727&0.017510
    &0.059202&0.088366&0.037848\\    
    % 
    \hline       
    &\multicolumn{3}{c|}{vortex $3$}
    &\multicolumn{3}{c|}{vortex $4$} \\    
    &$x_0$ &$y_0$ &strength &$x_0$ &$y_0$ &strength \\
    \hline 
    % 
    Value for $80\times160$ 
    &0.960495&0.036851&4.0825e-04
    &0.039849&0.037823&5.2809e-04\\
    Value for $120\times240$
    &0.962505&0.037983&2.8740e-04
    &0.038071&0.038651&2.7866e-04\\
    Value for $180\times360$
    &0.962411&0.037348&2.3729e-04
    &0.038047&0.038535&2.2377e-04\\    
    Extrapolated Value
    &0.962406&0.036534&2.0181e-04
    &0.038046&0.038516&2.0827e-04\\
    Order of Convergence
    &7.55&1.42&2.17
    &10.57&4.84&3.73\\    
    Estimated Error 
    &0.000480&2.226166&17.580681
    &0.000891&0.049124&7.437678\\    
    % 
    \hline
  \end{tabular}
\end{table}


\begin{figure} 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=0.8\linewidth]{vort1_psi2.png}
  \end{subfigure}
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.8\linewidth]{vort1_psi3.png}
  \end{subfigure} \vspace{0.5cm}
  \caption{The streamlines for the case $H=2$. The left figure shows
    the streamlines found using the stream function and the regions
    where this function changes sign. The right figure shows the
    streamlines found using Paraview's stream tracer filter, which
    agree with the stream function results.}
  \label{fig:h2}
  % 
\end{figure}


%----------------------------------------------------------
% RESULT: h=  2
%----------------------------------------------------------
\subsection{Creation of a Third Main Vortex}
\label{ch:res-3v}

As the height of the box increases at fixed Reynolds number, the flow
pattern changes. Vortices in the bottom corners of the box grow in
size until eventually they merge to form a third vortex. Using the
same parameters as Section \ref{ch:res-h2}, I found the height at
which the two corner vortices have merged into a single vortex, to be
$2.79\pm0.01$.

Figure \ref{fig:3v1} shows the stream lines for this case when $\Delta
x = \Delta y = \frac{1}{120}$. We can see that there truly are exactly
three closed flow loops. Figure \ref{fig:3v1} confirms that the fourth
vortex is still present for $H=2.78$. I also obtained the same results
for $\Delta x = \Delta y = \frac{1}{180}$ to show that the results are
not grid dependent.

\begin{figure}
% 
{
  \center
  \includegraphics[width=.8\textwidth]{vort2_all.png}
  \caption{The stream lines for $H=2.79$. It is clear that three main
    vortices are present in this case.}
  \label{fig:3v1}
}
\vspace{1cm}
  % 
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{vort2_278_120.png}
  \end{subfigure}
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{vort2_279_120.png}
  \end{subfigure} \vspace{0.5cm}
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{vort2_278_180.png}
  \end{subfigure}
  % 
  \begin{subfigure}{0.5\textwidth}
    \includegraphics[width=.95\linewidth,center]{vort2_279_180.png}
  \end{subfigure}
  \caption{ Merging of the two lower vortices. The stream lines on the left
    are obtained for $H=2.79$ and the stream lines on the right are
    obtained for $H=2.78$. The upper and lower figures correspond to
    $\Delta x = \Delta y = \frac{1}{120}$ and $\Delta x = \Delta y =
    \frac{1}{180}$ respectively. }
  \label{fig:3v2}
\end{figure}

% -------------------------------------------------------
% ch4-Conclusion-----------------------------------------
%--------------------------------------------------------
\section{Conclusion}
\label{ch:conclusion}

In this assignment the incompressible laminar Navier-Stokes equations
were solved using the finite volume method. The fluxes were evaluated
using the central approximation scheme while the temporal
discretization was done using the Implicit Euler method. The pressure 
decoupling was handled using the artificial compressibility method.

Firstly, the flux integral was verified to be second order
accurate. Then the Jacobian was verified by perturbing the solution
and predicting the change in flux integral using the Jacobian. Several
other tests were consequently performed to verify the correctness of
the numerical method.

Finally, the vortex patterns of the flow in a box with a moving
top were studied.  The centers of the vortices were found using
interpolation, while their strength was evaluated using the stream
function and the vorticity. 

 \end{document}
