/* driver.cxx
 *
 * driver for the  sections "Stability and Accuracy Check",
 * "Efficiency Test" and "The Real Problem".
 */


#include "energy.hxx"
#include <cstdio>
#include <ctime>

// Prints the instructions to run the code.
void vHelp(){
	
	fprintf(stderr, "The right input for this executable is: \n");
	fprintf(stderr, "./driver qReal qIm dEc dDt dLength dHeight iNx strFileName dTupWall\n");
	fprintf(stderr, "qReal: 0 or 1\n\tIndicates whether we want to solve the fully developed input\n\ttemperature profile.\n");
	fprintf(stderr, "qIm: 0 or 1\n\tUse implicit time advance or RK2\n");
	fprintf(stderr, "dEc: Eckert number (default is 0.1)\n");
	fprintf(stderr, "dDt: Time step\n");
	fprintf(stderr, "dLength: Length of channel\n");	
	fprintf(stderr, "dHeight: Height of channel\n");
	fprintf(stderr, "iNx: Number of meshes in the Length direction. iNy will be chosen so that dx/dy=2.5\n");
	fprintf(stderr, "strFileName: Name for the output files\n");
	fprintf(stderr, "dTupWall: The temperature at the top wall (if not given the value of 1 will be chosen)\n");
}


int main(int argc, char *argv[]){

	/*
	  options to get from input
	*/
	int qReal, // The real problem or the test problem
		qImpl; // Use implicit time stepping?
	int iNx,   // Number of meshes in the x direction.
		iNy;
	double dLength, //Length of the channel
		dHeight;    //Height of the channel
	double ddt;     //Time step
	double dEc;     //Eckert number
	double dTUpperwall; //Upper wall temperature
	
	//dimless numbers
	double	dre = 50, //Reynolds number
		dpr = 0.7,    //Prandtl number
		depsilon = 1e-10, //Desired final change in solution
		dlchange;         //Final change in solution
	
	double dubar = 3,     //average velocity
		dumax= 1.5*dubar, //maximum velocity
		dtcoeff;          //will store 3/4*pr*re*ubar^2

	double dcfl,         //cfl number
		dtcompute;       //stores the run time
	double dcoord[2];    //coordinates of a cell centre
	int initer;          //number of iterations


	//strings
	char strbnd1[50] = "nc0", //string for boundary left wall factory
		strbnd2[50] ,         //string for boundary upper wall factory
		strbnd3[50] ,         //string for boundary left wall factory
		strbnd4[50]  = "dc0"; //string for boundary lower wall factory
	char *strbnd[] =
		{strbnd1, strbnd2, strbnd3, strbnd4}; //store all boundary strings in an array
	char strvel[50] ;  //string for velocity field factory
 	char strfname[200], //string for output file name
		strtmp[200];    //temporary string, usually used for creating output file names

	/******************************************************
	 *
	 *Read the input
	 *
	 ******************************************************/
	
	if (argc < 9){
		fprintf(stderr, "WRONG NUMBER OF INPUT (%d) should be (%d) \n", argc-1, 8);
		vHelp();
		return 0;
	}
	else{
		int ii;
		
		//read Real or fully developed input velocity
		ii = sscanf(argv[1], "%d", &qReal);
		if (!ii){
			fprintf(stderr, "Bad qReal\n");
			return 0;
		} else if (qReal){
			// do something for real 
		} else {
			// do something for fully developed
		}
		//read implicit or explicit scheme
		ii = sscanf(argv[2], "%d", &qImpl);
		if (!ii){
		    fprintf(stderr, "Bad qImpl\n");
			return 0;
		} 
		//read ec number
		ii = sscanf(argv[3], "%lf", &dEc);
		if (!ii){
			fprintf(stderr, "Ecart number not given setting it to 0.1\n");
			dEc = 0.1;
		}
		//read ddt
		ii = sscanf(argv[4], "%lf", &ddt);
		if (!ii){
		    fprintf(stderr, "Bad dt\n");
			return 0;
		}
		//read dLength
		ii = sscanf(argv[5], "%lf", &dLength);
		if (!ii){
			fprintf(stderr, "Bad length");
			return 0;
		}
		//read width
		ii = sscanf(argv[6], "%lf", &dHeight);
		if (!ii){
		    fprintf(stderr, "Bad Height\n");
			return 0;
		}
		//read number of cells in the x direction
		ii = sscanf(argv[7], "%d", &iNx);
		if (!ii){
		    fprintf(stderr, "Bad Nx\n");
			return 0;
		}
		//read output file name
		ii = sscanf(argv[8], "%s", strfname);
		if (!ii){
			fprintf(stderr, "Bad output file name\n");
			return 0;
		}
		dTUpperwall = 1;
	}
	// If given, read upper wall temperature
	if (argc == 10){
		int ii;
		ii = sscanf(argv[9], "%lf", &dTUpperwall);
		if (!ii){
			fprintf(stderr, "Bad dTUpperWall\n");
			return 0;
		}		
	}

	fprintf(stderr, "=============================================\n");
	fprintf(stderr, "=        MECH510 Assignemt 3                =\n");
	fprintf(stderr, "=============================================\n");
	fprintf(stderr, "\n");	

	/******************************************************
	 *
	 * Calculations on the input
	 *
	 ******************************************************/
	iNy = dHeight / dLength * iNx * 2.;
	dtcoeff = 0.75 * dpr * dEc * dubar * dubar;
	dcfl = dumax * ddt / dLength * iNx;

	// create the strings for upperwall, left wall, and velocity field
	// factories.
	sprintf(strvel, "c%lf,%lf", dubar, dHeight);	
	if (qReal)	sprintf(strbnd3, "dl%lf", 1/dHeight);
	else sprintf(strbnd3, "dr%lf", dtcoeff);
	sprintf(strbnd2, "dc%lf", dTUpperwall);
	
	fprintf(stderr, "Time step data: cfl=%lf dt=%lf\n", dcfl, ddt);
	fprintf(stderr, "Desired final change in solution: %e\n", depsilon);
	
	/******************************************************
	 *
	 * Create the important object
	 *
	 ******************************************************/
	Grid gr(iNx, dLength, iNy, dHeight); int M=gr.iNCY(), N=gr.iNCX();
	
	Memory mem;
	if (qImpl) vInitMemImplict(gr, &mem);
	else vInitMemRK2(gr, &mem);
	
	Physics ph(strvel, strbnd, dEc, dpr, dre, gr);
	
     /******************************************************
	 *
	 * Finding the solution
	 *
	 ******************************************************/
	
	/*
	  get the time
	*/
	dtcompute = clock();

	/*
	  set the initial condition
	*/
    for (int jj = 1 ; jj < M-1 ; jj++){
		for (int ii=1 ; ii < N-1 ; ii++){
			gr.vCellCent(ii, jj, dcoord);
			mem.adsol[jj*N + ii] = dcoord[dirY];
		}
	}
	ph.vSetGhosts(mem.adsol, gr);
	//write initial condition (debugging)
	sprintf(strtmp, "%s_init.vtk", strfname);
	gr.vWriteVtk(mem.adsol, NULL, strtmp);

	/*
	  solve for the solution
	*/
	initer = 0;
	do{
		
		if (qImpl) dlchange=vTimeAdvImplicit(gr, ph, ddt, &mem);
		else dlchange=vTimeAdvRK2(gr, ph, ddt, &mem);		  
		//fprintf(stderr, "[%d] change in solution is %lf\n", initer, dlinfchang); 		
		initer++;		
	}while(dlchange > depsilon);

	/*
	  get the time
	*/
	dtcompute = (clock() - dtcompute) / CLOCKS_PER_SEC;

	/******************************************************
	 *
	 * Post processing
	 *
	 ******************************************************/

	/*
	  print runtime, final change in solution, number of iterations
	  and the final Linf norm of flux integral.
	*/
	{
		double dLinfRhs=0;
		ph.vFluxInteg(mem.adsol, gr, mem.adflux);
		for (int jj = 1 ; jj < M-1 ; jj++)
			for (int ii=1 ; ii < N-1 ; ii++)
				dLinfRhs= MAX( dLinfRhs, mem.adflux[jj*N+ii] );

		fprintf(stderr, "\n");
		fprintf(stderr, "FinalChange=%e, Linf(RHS)=%e, Time=%e, Niter: %d\n", dlchange, dLinfRhs, dtcompute, initer);
	}

	/*
	  post processing for the real problem
	*/
	if (qReal){
		
		FILE *fl;

		//write the derivative of temperature at the lower wall
		{
			double dderiv1, dderiv2, dsolw, dsolu, dsoluu;
		
			sprintf(strtmp, "%s.down", strfname);
			fl = fopen(strtmp, "w"); assert(fl);

			fprintf(fl, "## %-10s %-20s %-20s\n", "X", "dT/dy(1)", "dT/dy(2)");
			for(int ii = 1; ii < N-1 ; ii++){
				
				gr.vCellCent(ii, 1, dcoord);
				dsolw = ph.bnd[BPatch::down]->dVal(dcoord[dirY]);assert(dsolw<1e-6);
				dsolu = mem.adsol[1*N + ii];
				dsoluu = mem.adsol[2*N + ii];

				// first order approximation, this form will be used
				dderiv1 = 2 * (dsolu - dsolw) / gr.dDy();
				//this second order derivative is inconsistent
				dderiv2 = (-dsoluu + 7*dsolu - 6*dsolw) / 2. / gr.dDy();

				fprintf(fl, "   %-10lf %-20e %-20e\n", dcoord[dirX], dderiv1, dderiv2);
			}

			fclose(fl);
		}

		//write the velocity field at the exit
		// + dl2 norm from steady state solution
		{
			double dsol, dl2=0, dl1=0, dlinf=0, derr;
		
			sprintf(strtmp, "%s.right", strfname);
			fl = fopen(strtmp, "w"); assert(fl);

			fprintf(fl, "## %-10s %-20s %-20s\n", "Y", "T", "Error");
			for(int jj = 1; jj < M-1 ; jj++){
				double dy;
				gr.vCellCent(N-2,jj, dcoord);
				dy = dcoord[dirY];
				
				dsol = mem.adsol[jj*N + N-2];
				derr = ABS( dsol - ( dy + dtcoeff * ( 1 - pow(1-2*dy ,4) ) ) );
				dl1 += derr;
				dl2 += derr*derr;
				dlinf = MAX(dlinf, derr);

				fprintf(fl, "  %-10lf %-20e %-20e\n", dcoord[dirY], dsol, derr);
			}
			dl1 = dl1 / (M-2);
			dl2 = dl2 / (M-2); dl2=sqrt(dl2);
			fprintf(fl, "## l1=%e, l2=%e, linf=%e\n", dl1, dl2, dlinf);

			fclose(fl);
		}

		//write the solution
		sprintf(strtmp, "%s_final.vtk", strfname);
		gr.vWriteVtk(mem.adsol, NULL , strtmp);
	}
	
	/*
	  post processing for the test problem
	*/
	else{
		
		double dl1=0, dl2=0, dlinf=0;
		double *derr = new double[M*N];

		//find the error and its norms
		for (int jj = 1 ; jj < M-1 ; jj++){
			for (int ii=1 ; ii < N-1 ; ii++){
				gr.vCellCent(ii, jj, dcoord);
				derr[jj*N+ii] = ABS(ph.bnd[BPatch::left]->dVal(dcoord[dirY]) - mem.adsol[jj*N+ii]);
				dl1 += derr[jj*N+ii];
				dl2 += derr[jj*N+ii] * derr[jj*N+ii];
				dlinf = MAX(dlinf, derr[jj*N+ii] );
			}
		}
		dl1 /= (M-2); dl1 /= (N-2);
		dl2 /= (M-2); dl2 /= (N-2);  dl2 = sqrt(dl2);

		//write the solution and error field
		sprintf(strtmp, "%s_final.vtk", strfname);
		gr.vWriteVtk(mem.adsol, derr , strtmp);
		printf("## %-10s %-20s %-20s %-20s\n", "N", "l1", "l2", "linf");
		printf("   %-10lf %-20e %-20e %-20e\n",
			   sqrt((N-1)*(M-1)), dl1, dl2, dlinf);
		
		delete[] derr;
	}

	
	mem.vDestroy();
	return 0;
}
