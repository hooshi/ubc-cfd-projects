#include "energy.hxx"
#include <cstdio>

static double dExactFlux(const double dcoord[], const double dT0, const double du0, const double dv0, const double dkappa){
	double dx = dcoord[dirX];
	double dy = dcoord[dirY];
	double dcospix =  cos (PI * dx);
	double dsinpiy =  sin (PI * dy);
	
	return -du0 * dT0 * PI * dy * cos(2*PI*dx) * dsinpiy
		- dv0 * dT0 * PI * dx * dcospix * cos(2*PI*dy)
		- dkappa * 2 * dT0 * PI * PI * dcospix * dsinpiy;
}

static double dExactSource(const double dcoord[], const double dT0, const double du0, const double dv0, const double dchi){
	double dx = dcoord[dirX];
	double dy = dcoord[dirY];
	double dcospix =  cos (PI * dx);
	double dcospiy =  cos (PI * dy);
	double dsinpix =  sin (PI * dx);
	double dsinpiy =  sin (PI * dy);

	return dchi * (
		+ 2 * pow ( du0 * PI * dy * dcospix , 2)
		+ 2 * pow ( dv0 * PI * dx * dsinpiy , 2)
		+ pow ( du0 * dsinpix + dv0 * dcospiy, 2)		
		);
	
}

int main(int argc, char *argv[]){

	char ctesttype;
	int inread;

	if(argc>1){
		inread = sscanf(argv[1], "%s", &ctesttype);
		assert(inread);
	} else{
		ctesttype = 'a';
	}

	

	if (ctesttype == 'a'){
		/*	  
		  test grid
		*/
		Grid gr(10, 2, 2, 5);
		double dcoord[4];

		fprintf(stderr,"nx:%d, ny:%d, dx:%lf, dy:%lf \n", gr.iNCX(), gr.iNCY(), gr.dDx(), gr.dDy());
	
		gr.vCellCent(3, 1, dcoord);
		fprintf(stderr,"centre of (3,1): %lf, %lf\n", dcoord[0], dcoord[1]);

		gr.vCellVerts(3, 1, dcoord);
		fprintf(stderr,"verts (3,1): %lf, %lf and %lf, %lf\n", dcoord[0], dcoord[1], dcoord[2], dcoord[3]);
	}
	
	else if (ctesttype == 'b'){
		/*	  
			  test the vel field
		*/
		VelField *vfi;
		char name[300];
		double du, dv;
		double dcoord[4];

		dcoord[dirY] = (dcoord[dirX] = .5) /2.;

		vfi = factory_VelField("t1.2,3.1");
		vfi->vName(name);
		fprintf(stderr,"%s\n", name);
		vfi->vVels(dcoord, du, dv);
		fprintf(stderr,"velocity at (%lf,%lf): %lf, %lf\n", dcoord[dirX], dcoord[dirY], du, dv);
		delete vfi;

		vfi = factory_VelField("c1.2");
		vfi->vName(name);
		fprintf(stderr,"%s\n", name);
		vfi->vVels(dcoord, du, dv);
		fprintf(stderr,"velocity at (%lf,%lf): %lf, %lf\n", dcoord[dirX], dcoord[dirY], du, dv);
		delete vfi;
	}
	
	else if (ctesttype == 'c'){
		/*	  
			  test the b patch
		*/
		BPatch *bp;
		char name[200];

		bp=factory_BPatch("ds10.34");
		bp->vName(name);
		fprintf(stderr,"%s\n", name);
		fprintf(stderr,"type: %d\n", bp->ctype);
		fprintf(stderr,"val at .3 %lf\n", bp->dVal(.3));
		delete bp;
	}
	
	else if (ctesttype == 'd'){
		/*
	  	  test physics
		*/

		const char *bnd1 = "nc0";
		const char *bnd2 = "dc1";
		const char *bnd3 = "ds1";
		const char *bnd4 = "dc0";	
		char const *strbnd[4] = {bnd1,bnd2,bnd3,bnd4};
		double ec=1, re=2, pr=3;
		Grid gr2(4, 2, 2, 1);
	
		Physics ph("t1,2", strbnd, ec, pr, re, gr2);
		char strout[200];
		double dco[2];

		fprintf(stderr,"=====================================\n");
		fprintf(stderr,"=        Testing physics            =\n");
		fprintf(stderr,"=====================================\n");

		//test boundary patches
		ph.bnd[BPatch::right]->vName(strout);
		fprintf(stderr,"right wall: %s \n", strout);
		ph.bnd[BPatch::up]->vName(strout);
		fprintf(stderr,"up wall: %s \n", strout);
		ph.bnd[BPatch::left]->vName(strout);
		fprintf(stderr,"left wall: %s \n", strout);
		ph.bnd[BPatch::down]->vName(strout);
		fprintf(stderr,"down wall: %s \n", strout);

		//test ec, re and pr
		fprintf(stderr,"ec: %lf, re:%lf, pr:%lf, kappa:%lf, chi: %lf\n", ph.dec, ph.dre, ph.dpr, ph.dk, ph.dc);

		//fprintf(stderr, cell centres
		fprintf(stderr,"centres: \n");
		for (int jj = gr2.iNCY() - 1 ; jj >= 0 ; jj--){
			for (int ii = 0 ; ii < gr2.iNCX() ; ii++){
				gr2.vCellCent(ii, jj, dco);
				fprintf(stderr,"%9.4lf, %-9.4lf", dco[0], dco[1]);
			}
			fprintf(stderr,"\n");
		}
	
		//test the velocities
		ph.vfield->vName(strout);
		fprintf(stderr,"velocities: %s\n", strout);
		for (int jj = gr2.iNCY() - 1 ; jj >= 0 ; jj--){
			for (int ii = 0 ; ii < gr2.iNCX() ; ii++)
				fprintf(stderr,"%9.4lf, %-9.4lf", ph.adu[jj*gr2.iNCX() + ii], ph.adv[jj*gr2.iNCX() + ii]);
			fprintf(stderr,"\n");
		}
	} 

	else if (ctesttype == 'e'){
		/*
		  test the flux integral and the source term
		*/

		double dT0, du0, dv0;
		double dre = 50,
			dpr = 0.7,
			dec = 0.1;

		char strbnd1[100] = "nc0",
			strbnd2[100] = "dc0",
			strbnd3[100] ,
			strbnd4[100] = "dc0";
		char *strbnd[] = {strbnd1, strbnd2, strbnd3, strbnd4};
		char strvel[100];

		int aidim[] = {20, 40, 80, 160, 320, 640};
		int indim = 6;
		double dl1, dl2, dlinf, dexact, derr;
		
		//anounce
		fprintf(stderr,"=========================================\n");
		fprintf(stderr,"= Testing source term and flux integral =\n");
		fprintf(stderr,"=========================================\n");

		//read 
		if (argc < 5){
			fprintf(stderr, "T0, u0 and v0 not given");
			assert(0);
		}
		inread=sscanf(argv[2], "%lf", &dT0);assert(inread);
		inread=sscanf(argv[3], "%lf", &du0);assert(inread);
		inread=sscanf(argv[4], "%lf", &dv0);assert(inread);
		//sprintf(strbnd3, "ds%lf", dT0); //dirichlet left
		sprintf(strbnd3, "nc0"); //neumann left		
		sprintf(strvel, "t%lf,%lf", du0, dv0);

		/*
		  For each dimension create the physics find the source term
		  and flux and then evaluate the error.
		*/
		printf("## %-10s %-20s %-20s %-20s %-20s %-20s %-20s \n\n", "N", "Fl1", "Fl2", "Flinf", "Sl1", "Sl2", "Slinf");
	    for (int iidim = 0 ; iidim < indim ; iidim++){
			Grid  gr(aidim[iidim], 1, aidim[iidim], 1);
			Physics ph(strvel, strbnd, dec, dpr, dre, gr);

			int N = gr.iNCX(), M = gr.iNCY();
			double dcoord[2];
			double *adflux = new double [N*M];
			double *adsol = new double [N*M];

			//set the manufactured solution
			for (int jj = 0 ; jj < M ; jj++){
				for (int ii=0 ; ii < N ; ii++){					
					gr.vCellCent(ii, jj, dcoord);
					adsol[jj*N+ii] = dT0 * cos(PI*dcoord[dirX]) * sin(PI*dcoord[dirY]);
				}
			}
			
			//set ghost cells
			ph.vSetGhosts(adsol, gr);
			// char  strbndname[200];
			// printf("Boundaries: \n");
			// for (int ii = 0 ; ii < 4 ; ii++){
			//  	ph.bnd[ii]->vName(strbndname);
			//  	printf(strbndname);
			//  	printf("\n");
			//  }
			//  for (int jj = 1 ; jj < M-1 ; jj++){
			//  	gr.vCellCent(0, jj, dcoord);
				
			//  	double dy = dcoord[1];
			// 	printf("real left: %lf, mid: %lf real right: %lf \n",
			// 		   dT0*cos(-PI*gr.dDx()/2.)*sin(PI*dy),  dT0*cos(0)*sin(PI*dy),  dT0*cos(PI*gr.dDx()/2.)*sin(PI*dy));
			// 	printf("ghost left: %lf, mid: %lf comp right: %lf \n",
			// 		    adsol[jj*N+0], ph.bnd[BPatch::left]->dVal(dy), adsol[jj*N+1]); 			
			//  }

			//find flux integral
			ph.vFluxInteg(adsol, gr, adflux);

			//find the flux integral norms
			dl1 = dl2 = dlinf = 0;
			for (int jj = 1 ; jj < M-1 ; jj++){
				for (int ii=1 ; ii < N-1 ; ii++){
					gr.vCellCent(ii, jj, dcoord);
					dexact = dExactFlux(dcoord, dT0, du0, dv0, ph.dk);
					derr = adflux[jj*N+ii] - dexact;
					derr = ABS(derr);

					dl1 += derr;
					dl2 += derr * derr;
					dlinf = MAX(dlinf, derr);

					// double dx = dcoord[dirX];
					// double dy = dcoord[dirY];
					// printf("%d,%d u:(%lf, %lf) v:(%lf, %lf) T:(%lf, %lf)\n", ii, jj,
					// 	   ph.adu[jj*N+ii], du0*dy*sin(PI*dx), ph.adv[jj*N+ii], dv0*dx*cos(PI*dy),
					// 	   dT0*cos(PI*dx)*sin(PI*dy) ,adsol[jj*N+ii]);
					// printf("%d %d flux:(%lf, %lf) err: %lf\n", ii, jj,  dexact, adflux[jj*N+ii], derr);
				}
			}
			dl1 /= ( (N-2) * (M-2) );
			dl2 /= ( (N-2) * (M-2) );
			dl2 = sqrt(dl2);

		    printf("  %-10d %-20e %-20e %-20e ", aidim[iidim], dl1, dl2, dlinf);
			
			//find the source term norms
			dl1 = dl2 = dlinf = 0;
			for (int jj = 1 ; jj < M-1 ; jj++){
				for (int ii=1 ; ii < N-1 ; ii++){
					gr.vCellCent(ii, jj, dcoord);
					dexact = dExactSource(dcoord, dT0, du0, dv0, ph.dc);
					derr = ph.ads[jj*N+ii] - dexact;
					derr = ABS(derr);

					dl1 += derr;
					dl2 += derr * derr;
					dlinf = MAX(dlinf, derr);
				}
			}
			dl1 /= ( (N-2) * (M-2) );
			dl2 /= ( (N-2) * (M-2) );
			dl2 = sqrt(dl2);

			printf("%-20e %-20e %-20e\n", dl1, dl2, dlinf);

			delete[] adflux;
			delete[] adsol;
		}
	}

	else if ( (ctesttype == 'f') || (ctesttype == 'g') ){
	    /*
		  solve the case 5, checking for jacobians and also
		  checking for the steady state solution
		*/
		
		//anounce
		if (ctesttype == 'f'){
			fprintf(stderr,"=========================================\n");
			fprintf(stderr,"=          RK convergence               =\n");
			fprintf(stderr,"=========================================\n");
		} else{
			fprintf(stderr,"=========================================\n");
			fprintf(stderr,"=      Implicit Euler convergence       =\n");
			fprintf(stderr,"=========================================\n");
		
		}

		//dimless numbers
		double dubar = 3, dre = 50, dpr = 0.7,	dec = 0.1, depsilon = 1e-10, dlinfchang, dl2, dlinf;
		double dtempcon = 3/4. * dpr * dec * dubar * dubar ;

		//strings
		char strbnd1[100] ,
			strbnd2[100] ,
			strbnd3[100] ,
			strbnd4[100] ;
		char *strbnd[] = {strbnd1, strbnd2, strbnd3, strbnd4};
		char strvel[100];

		sprintf(strbnd1, "nc0");
		sprintf(strbnd2, "dc1");
		sprintf(strbnd3, "dr%lf", dtempcon);
		sprintf(strbnd4, "dc0");
		sprintf(strvel, "c%lf,1", dubar);

		// real stuff
		double dcfl; 
		if (ctesttype == 'f') dcfl = .1;
		else		dcfl = 5;
		fprintf(stderr, "CFL is: %lf \t", dcfl);
		Grid gr(25, 5, 10, 1); int M=gr.iNCY(), N=gr.iNCX();
    
		//double ddt = gr.dDx() / (1 + 3/4. * dpr * dec * dubar * dubar) * dcfl;
		double ddt = .1;
		fprintf(stderr, "Time step is: %lf \n", ddt);
		Memory mem;
		Physics ph(strvel, strbnd, dec, dpr, dre, gr);

		//helpers
		double dcoord[2];

		//create the memory
		if (ctesttype == 'f') vInitMemRK2(gr, &mem);
		else		 vInitMemImplict(gr, &mem);		

		//set the initial conditon
		for (int jj = 1 ; jj < M-1 ; jj++){
			for (int ii=1 ; ii < N-1 ; ii++){
				gr.vCellCent(ii, jj, dcoord);
				mem.adsol[jj*N + ii] = dcoord[dirY];
			}
		}
		ph.vSetGhosts(mem.adsol, gr);
		gr.vWriteVtk(mem.adsol, NULL, "init");

		//solve one step
		int iit = 0;
		do{
			
			if (ctesttype == 'f') dlinfchang=vTimeAdvRK2(gr, ph, ddt, &mem);
			else  dlinfchang=vTimeAdvImplicit(gr, ph, ddt, &mem);
			//fprintf(stderr, "l2 norm is %lf\n", dlinfchang); 

			iit++;
		}while(dlinfchang > depsilon);


		//find the norm
		//set the initial conditon
		dl2 = 0; dlinf = 0;
		double *derr = new double[M*N];
		for (int jj = 1 ; jj < M-1 ; jj++){
			for (int ii=1 ; ii < N-1 ; ii++){
				gr.vCellCent(ii, jj, dcoord);
				derr[jj*N+ii] = ABS(ph.bnd[BPatch::left]->dVal(dcoord[dirY]) - mem.adsol[jj*N+ii]);

				dl2 += derr[jj*N+ii] * derr[jj*N+ii];
				dlinf = MAX(dlinf, derr[jj*N+ii] );
				//	printf("sol: %lf, num: %lf, err: %lf\n", ph.bnd[BPatch::left]->dVal(dcoord[dirY]), mem.adsol[jj*N+ii], derr[]);
			}
		}
		dl2 /= (M-2);
		dl2 /= (N-2);
		dl2 = sqrt(dl2);
		
		fprintf(stderr, "Error is L2: %e Linf = %e \n", dl2, dlinf);
		fprintf(stderr, "Num iterations: %d final dlinf: %e \n", iit, dlinfchang);
		gr.vWriteVtk(mem.adsol, derr, "final");

		delete[] derr;
		
		//destroy memory
		mem.vDestroy();
	}

	else{
		/*
		  no valid test option
		*/
		fprintf(stderr, "`%c' is no valid test option\n", ctesttype);
	}
	return 0;

}
