/*
 * pp_sym.cxx (Post Processor Symmetry check)
 *
 * Reads the solution for the same mesh on two cases and finds
 * u1(x,y) - u2(1-x,y)
 *
 * to run it use: pp_conv first_solution second_solution
 *
 */


#include <cstdio>
#include "ns.hxx"


int main(int argc, char *argv[]){
	assert(argc == 3);

	Options optUr, optUl;
	sprintf(optUr.strFName, "%s.dat", argv[1]);
	sprintf(optUl.strFName, "%s.dat", argv[2]);


	VEC *avsolUr, *avsolUl;
	double *adu[3];

	vDatRead(&optUr, &avsolUr);
	vDatRead(&optUl, &avsolUl);

	/* make sure the meshes are the same */
	assert(optUr.M == optUl.M);
	assert(optUr.N == optUl.N);
	assert(GABS(optUr.X - optUl.X) < 1e-6);
	assert(GABS(optUr.Y - optUl.Y) < 1e-6);

	/* create the grid */
	Grid gr(&optUr);
	int m = gr.iNCY(), n=gr.iNCX();
	int M = m-2, N=n-2;

	/* create data and find the diff */
	adu[0] = new double[M*N];
	adu[1] = new double[M*N];
	adu[2] = new double[M*N];

	for(int jj = 1 ; jj < m-1 ; jj++){
		for(int ii = 1 ; ii < n-1 ; ii++){
			int II = ii-1;
			int JJ = jj-1;
			
			adu[0][JJ*N + II] = avsolUr[jj*n+ii][U];
			adu[1][JJ*N + II] = avsolUl[jj*n+(n-ii-1)][U];
			adu[2][JJ*N + II] = adu[0][JJ*N + II] + adu[1][JJ*N + II];
			//printf("%lf %lf %e \n", adu[0][JJ*N + II], adu[1][JJ*N + II], adu[2][JJ*N + II]);
		}
	}

	/* write the data */
	{
		const char *c1 = "u_right";
		const char *c2 = "u_left";
		const char *c3 = "difference";
		char const *cc[] = {c1, c2, c3};
		char tmp[300];
		sprintf(tmp, "%s_comsym.vtk", argv[2]);
		
		gr.vWriteVtk(adu, 3, cc, tmp);
	}	

	delete[] adu[0];
	delete[] adu[1];
	delete[] adu[2];
	delete[] avsolUr;
	delete[] avsolUl;
	
	return 0;
}
