/*
 * pp_vort.cxx (Post Processor Vortex)
 *
 * Reads the solution + the ID of the candidate cell for finding
 * the vortex centre which is found using paraview, and interpolates
 * for the centre of the vertex.
 *
 * to run it use: pp_vort solution_path ID_of_cell (4 or 5)
 *                4 -> a four cell stencil
 *                5 -> a five cell stencil 
 */

#include <cstdio>
#include "ns.hxx"

enum EnumVort{ePsi=1, eOmega=0};

int main(int argc, char *argv[]){
	assert(argc == 4);

	
	Options opt;
	vOptionsDef(&opt);
	sprintf(opt.strFName, "%s.dat", argv[1]);
	VEC *avsol;
	vDatRead(&opt, &avsol);

	/* create the grids */
	Physics ph(&opt);
	
	Grid gr(&opt);
	int n=gr.iNCX();
	double ddx = gr.dDx();
	double ddy = gr.dDy();	
	int N=n-2;
	
	vSetGhosts(&ph, &gr, avsol);
	

	/* read the option */
	int iMethod, iID, ii, jj, II, JJ;

	sscanf(argv[2], "%d", &iID);
	sscanf(argv[3], "%d", &iMethod);

	/* find ii and jj */
	II = iID % N; ii = II + 1;
	JJ = iID / N; jj = JJ + 1;

	/*
	  for a five square stencil
	*/
	if (iMethod == 5){
		double dc[2];
		double domega, dc0[2];
		gr.vCellCent(ii, jj, dc0);

	    //find the vortex of the guy
		domega = (avsol[jj*n + ii+1][V] - avsol[jj*n + ii-1][V])/2./ddx
			+ (avsol[(jj-1)*n + ii][U] - avsol[(jj+1)*n + ii][U])/2./ddy ;

		//find the vortex centre
		double A[2][2], AInv[2][2];
		double b[2], delta[2];
		
		A[0][0] = (avsol[jj*n + ii+1][U] - avsol[jj*n + ii-1][U])/2./ddx;
		A[0][1] = (avsol[(jj+1)*n + ii][U] - avsol[(jj-1)*n + ii][U])/2./ddy;

		A[1][0] = (avsol[jj*n + ii+1][V] - avsol[jj*n + ii-1][V])/2./ddx;
		A[1][1] = (avsol[(jj+1)*n + ii][V] - avsol[(jj-1)*n + ii][V])/2./ddy;

		b[0] = -avsol[jj*n + ii][U];
		b[1] = -avsol[jj*n + ii][V];

		Invert2x2(A, AInv);
		MultVec(AInv, b, delta);

		dc[eX] = dc0[eX] + delta[eX];
		dc[eY] = dc0[eY] + delta[eY];

		fprintf(stderr, "dX, dY: %.15lf, %.15lf \n", ddx, ddy);
		fprintf(stderr,"Omega: %.15lf \n", domega);
		fprintf(stderr,"delX, delY: %.15lf, %.15lf \n", delta[eX], delta[eY]);
		fprintf(stderr,"x0, y0: %.15lf, %.15lf \n", dc0[eX], dc0[eY]);
		fprintf(stderr, "x, y: %.15lf, %.15lf \n", dc[eX], dc[eY]);

		printf("%25.15e %25.15e %25.15e \n", domega, dc[eX], dc[eY]);

		
	}

	/*
	  for a four square stencil
	*/
	else if (iMethod == 4){
		double dc[2];
		double domega, dc0[2];

		/* find the centre */
		gr.vCellCent(ii, jj, dc0);
		dc0[eX] += ddx / 2.;
		dc0[eY] += ddy / 2.;

	    //find the vortex of the guy
		domega = (
			avsol[jj*n + ii+1][V] + avsol[(jj+1)*n + ii+1][V]  
			- avsol[jj*n + ii][V] - avsol[(jj+1)*n + ii][V]
			)/2./ddx
			+
			(avsol[(jj)*n + ii][U] + avsol[(jj)*n + ii+1][U]
			 - avsol[(jj+1)*n + ii][U] - avsol[(jj+1)*n + ii+1][U]
				)/2./ddy ;

		//find the vortex centre
		double A[2][2], AInv[2][2];
		double b[2], delta[2];
		
		A[0][0] = (avsol[jj*n + ii+1][U] + avsol[(jj+1)*n + ii+1][U] 
				   -avsol[jj*n + ii][U] - avsol[(jj+1)*n + ii][U] )/2./ddx;		
		A[0][1] = (avsol[(jj+1)*n + ii][U] + avsol[(jj+1)*n + ii+1][U] 
				   -avsol[(jj)*n + ii][U] - avsol[(jj)*n + ii+1][U] )/2./ddx;	

		A[1][0] = (avsol[jj*n + ii+1][V] + avsol[(jj+1)*n + ii+1][V] 
				   -avsol[jj*n + ii][V] - avsol[(jj+1)*n + ii][V] )/2./ddx;		
		A[1][1] = (avsol[(jj+1)*n + ii][V] + avsol[(jj+1)*n + ii+1][V] 
				   -avsol[(jj)*n + ii][V] - avsol[(jj)*n + ii+1][V] )/2./ddx;	


		b[0] = -( avsol[jj*n + ii][U] +avsol[(jj+1)*n + ii][U] +
				  avsol[jj*n + ii+1][U] +avsol[(jj+1)*n + ii+1][U] ) / 4.;
		b[1] = -( avsol[jj*n + ii][V] +avsol[(jj+1)*n + ii][V] +
				  avsol[jj*n + ii+1][V] +avsol[(jj+1)*n + ii+1][V] ) / 4.;

		Invert2x2(A, AInv);
		MultVec(AInv, b, delta);

		dc[eX] = dc0[eX] + delta[eX];
		dc[eY] = dc0[eY] + delta[eY];

		fprintf(stderr, "dX, dY: %.15lf, %.15lf \n", ddx, ddy);
		fprintf(stderr,"Omega: %.15lf \n", domega);
		fprintf(stderr,"delX, delY: %.15lf, %.15lf \n", delta[eX], delta[eY]);
		fprintf(stderr,"x0, y0: %.15lf, %.15lf \n", dc0[eX], dc0[eY]);
		fprintf(stderr, "x, y: %.15lf, %.15lf \n", dc[eX], dc[eY]);

		printf("%25.15e %25.15e %25.15e \n", domega, dc[eX], dc[eY]);

	}

	delete[] avsol;
	
	return 0;
}
