/* driver_verif.cxx
 *
 * driver for the sections "Code Source term and verify correctness",
 * and "Code flux integral and verify correctness".
 */


#include "energy.hxx"
#include <cstdio>

// Returns the exact value of the flux integral at a certain point
// param dcoord: the coordinates of that point
// param dT0, du0, dv0: arbitrary values defined in the assignment
// param dkappa: dimensionless number 1/PR/RE
static double dExactFlux(const double dcoord[], const double dT0, const double du0, const double dv0, const double dkappa){
	double dx = dcoord[dirX];
	double dy = dcoord[dirY];
	double dcospix =  cos (PI * dx);
	double dsinpiy =  sin (PI * dy);
	
	return -du0 * dT0 * PI * dy * cos(2*PI*dx) * dsinpiy
		- dv0 * dT0 * PI * dx * dcospix * cos(2*PI*dy)
		- dkappa * 2 * dT0 * PI * PI * dcospix * dsinpiy;
}

// Returns the exact value of the source term at a certain point
// param dcoord: the coordinates of that point
// param dT0, du0, dv0: arbitrary values defined in the assignment
// param dchi: dimensionless number EC/RE
static double dExactSource(const double dcoord[], const double dT0, const double du0, const double dv0, const double dchi){
	double dx = dcoord[dirX];
	double dy = dcoord[dirY];
	double dcospix =  cos (PI * dx);
	double dcospiy =  cos (PI * dy);
	double dsinpix =  sin (PI * dx);
	double dsinpiy =  sin (PI * dy);

	return dchi * (
		+ 2 * pow ( du0 * PI * dy * dcospix , 2)
		+ 2 * pow ( dv0 * PI * dx * dsinpiy , 2)
		+ pow ( du0 * dsinpix + dv0 * dcospiy, 2)		
		);
	
}

int main(int argc, char *argv[]){

	// Error checking for sscanf
	int inread;
	int indim = 6;


	// Test values
	double dT0, du0, dv0;

	// Re, Pr and Ec numbers
	double dre = 50, dpr = 0.7,	dec = 0.1;

	// Strings for the boundary conditions and velocity field factory
	char strbnd1[100] = "nc0",
		strbnd2[100] = "dc0",
		strbnd3[100] = "nc0",
		strbnd4[100] = "dc0";
	char *strbnd[] = {strbnd1, strbnd2, strbnd3, strbnd4};
	char strvel[100];

	// Mesh sizes
	int aidim[] = {20, 40, 80, 160, 320, 640};
	// number of meshes
	
	double dl1, dl2, dlinf, // error norms 	
		dexact, derr;       // error and exact solution
		 
	fprintf(stderr,"=========================================\n");
	fprintf(stderr,"= Testing source term and flux integral =\n");
	fprintf(stderr,"=========================================\n");

	/*
	  Read the input 
	*/
	if (argc < 5){
		fprintf(stderr, "T0, u0 and v0, indim not given");
		assert(0);
	}
	inread=sscanf(argv[1], "%lf", &dT0);assert(inread);
	inread=sscanf(argv[2], "%lf", &du0);assert(inread);
	inread=sscanf(argv[3], "%lf", &dv0);assert(inread);
	inread=sscanf(argv[4], "%d", &indim);assert(inread);
	
	sprintf(strvel, "t%lf,%lf", du0, dv0); //set the velocity field

	/*
	  For each dimension create the physics find the source term
	  and flux and then evaluate the error.
	*/
	
	printf("## %-10s %-20s %-20s %-20s %-20s %-20s %-20s \n\n", "N", "Fl1", "Fl2", "Flinf", "Sl1", "Sl2", "Slinf");
	
	for (int iidim = 0 ; iidim < indim ; iidim++){

		fprintf(stderr, "====      N = %d    ======\n", aidim[iidim]);

		/*
		  create grid and physics
		*/
		Grid  gr(aidim[iidim], 1, aidim[iidim], 1);
		Physics ph(strvel, strbnd, dec, dpr, dre, gr);
		int N = gr.iNCX(), M = gr.iNCY();
		
		double dcoord[2]; //coordinates of a point
		double *adflux = new double [N*M]; //flux integral
		double *adsol = new double [N*M];  //solution

		//set the manufactured solution
		for (int jj = 0 ; jj < M ; jj++){
			for (int ii=0 ; ii < N ; ii++){					
				gr.vCellCent(ii, jj, dcoord);
				adsol[jj*N+ii] = dT0 * cos(PI*dcoord[dirX]) * sin(PI*dcoord[dirY]);
			}
		}
			
		//set ghost cells (not applicable for this problem)
		ph.vSetGhosts(adsol, gr);
		//find flux integral
		ph.vFluxInteg(adsol, gr, adflux);

		//find the flux integral norms
		dl1 = dl2 = dlinf = 0;
		for (int jj = 1 ; jj < M-1 ; jj++){
			if(indim==1)printf("\n");
			for (int ii=1 ; ii < N-1 ; ii++){
				gr.vCellCent(ii, jj, dcoord);
				dexact = dExactFlux(dcoord, dT0, du0, dv0, ph.dk);
				derr = adflux[jj*N+ii] - dexact;
				derr = ABS(derr);

				if(indim==1)
					printf("i: %-3d, j: %-3d, exa:%-10.5lf num:%-10.5lf error:%e\n",
					   ii, jj, dexact, adflux[jj*N+ii], derr);

				dl1 += derr;
				dl2 += derr * derr;
				dlinf = MAX(dlinf, derr);
			}
		}
		dl1 /= ( (N-2) * (M-2) );
		dl2 /= ( (N-2) * (M-2) );
		dl2 = sqrt(dl2);

		printf("  %-10d %-20e %-20e %-20e ", aidim[iidim], dl1, dl2, dlinf);
			
		//find the source term norms
		dl1 = dl2 = dlinf = 0;
		for (int jj = 1 ; jj < M-1 ; jj++){
			for (int ii=1 ; ii < N-1 ; ii++){
				gr.vCellCent(ii, jj, dcoord);
				dexact = dExactSource(dcoord, dT0, du0, dv0, ph.dc);
				derr = ph.ads[jj*N+ii] - dexact;
				derr = ABS(derr);

				dl1 += derr;
				dl2 += derr * derr;
				dlinf = MAX(dlinf, derr);
			}
		}
		dl1 /= ( (N-2) * (M-2) );
		dl2 /= ( (N-2) * (M-2) );
		dl2 = sqrt(dl2);

		printf("%-20e %-20e %-20e\n", dl1, dl2, dlinf);

		delete[] adflux;
		delete[] adsol;

		fprintf(stderr, "\n");
	}
	return 0;
}
