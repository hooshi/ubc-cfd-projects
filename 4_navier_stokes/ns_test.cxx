/* ns_test.cxx
 *
 * The solver for the Navier Stokes project.
 * I agree that the name of the file is chosen irrelevantly.
 */

#include <cstdio>
#include "ns.hxx"
#include <ctime>

// Struct that holds the input for the manufactured soltuion
struct ExactInput{
	double dp, dv, du, dbeta, dre;
	void vInit(Options *opt){
		dre = opt->dRe;
		dbeta = opt->dBeta;
		du = opt->dU0;
		dv = opt->dV0;
		dp = opt->dP0;

		fprintf(stderr, "Creating manufactured solution, re:%lf, beta:%lf, p0:%lf, u0:%lf, v0:%lf\n",
				dre, dbeta, dp, du, dv);
	}
};

// Exact flux value of the manufactured solution
// param dc coordinate for which the flux has to be found
// param z the parameters
// param vflux the flux
static void vExactFlux(double dc[2], const ExactInput *z, pVEC vflux){
	double cx = cos(PI*dc[eX]);
	double sx = sin(PI*dc[eX]);
	double cy = cos(PI*dc[eY]);
	double sy = sin(PI*dc[eY]);
	double c2x = cos(2*PI*dc[eX]);
	double s2x = sin(2*PI*dc[eX]);
	double c2y = cos(2*PI*dc[eY]);
	double s2y = sin(2*PI*dc[eY]);

	vflux[P] = -PI/z->dbeta * (z->du*cx*s2y + z->dv*s2x*cy);
	
	vflux[U] = z->dp * PI * sx * cy -
		z->du * z->du * PI * s2x * s2y * s2y -
		z->du * z->dv * PI * sx * s2x * (cy * s2y + 2 * c2y * sy) -
		z->du / z->dre * 5 * PI * PI * sx * s2y;
	
	vflux[V] = z->dp * PI * cx * sy -
		z->dv * z->dv * PI * s2x *s2x * s2y -
		z->du * z->dv * PI * sy * s2y * (cx*s2x + 2 * c2x * sx) -
		z->dv / z->dre * 5 * PI * PI * s2x * sy;
}

// Exact value of the manufactured solution
// param dc coordinate for which the solution has to be found
// param z the parameters
// param vflux the flux
static void vInitSol(double dc[2], const ExactInput *z, pVEC vsol){
	vsol[P] = z->dp * cos(PI * dc[eX])      * cos(PI * dc[eY]);
	vsol[U] = z->du * sin(PI * dc[eX])      * sin(2 * PI * dc[eY]);
	vsol[V] = z->dv * sin(2 * PI * dc[eX])  * sin(PI * dc[eY]);	
}

// gets the old coordiates gives the new coordinates
// param dc0 the new reference point
// param dc gets the old coordinate and returns the new one
static void vTransferCoord(const double dc0[2], double dc[2]){
	dc[eX] = dc[eX] - dc0[eX];
	dc[eY] = dc[eY] - dc0[eY];
}

// the main !!!
int main(int argc, char *argv[]){


	/* set the options */
	Options opt;
	vOptionsDef(&opt);
	vOptionsInit(argc, argv, &opt);
		
	/*
	  test the tri-diag solver
	*/
	if(opt.iMode == 1)
	{
		MAT LHS[100][3];
		VEC RHS[100];
		int NRows = 20, i;

		for (i = 0; i < NRows; i++) {
			LHS[i][0][0][0] = 1.-2.;
			LHS[i][0][0][1] = 2.;
			LHS[i][0][0][2] = 3.;
			LHS[i][0][1][0] = 4.;
			LHS[i][0][1][1] = 5.-2.;
			LHS[i][0][1][2] = 6.;
			LHS[i][0][2][0] = 7.;
			LHS[i][0][2][1] = 8.;
			LHS[i][0][2][2] = 0.-2.;

			LHS[i][1][0][0] = 1.;
			LHS[i][1][0][1] = 2.;
			LHS[i][1][0][2] = 3.;
			LHS[i][1][1][0] = 4.;
			LHS[i][1][1][1] = 5.;
			LHS[i][1][1][2] = 6.;
			LHS[i][1][2][0] = 7.;
			LHS[i][1][2][1] = 8.;
			LHS[i][1][2][2] = 0.;

			LHS[i][2][0][0] = 1.-3.;
			LHS[i][2][0][1] = 2.;
			LHS[i][2][0][2] = 3.;
			LHS[i][2][1][0] = 4.;
			LHS[i][2][1][1] = 5.-3.;
			LHS[i][2][1][2] = 6.;
			LHS[i][2][2][0] = 7.;
			LHS[i][2][2][1] = 8.;
			LHS[i][2][2][2] = 0.-3.;

			RHS[i][0] = 2*i+1;
			RHS[i][1] = 2*i+2;
			RHS[i][2] = 2*i+3;
		}

		LHS[0][0][0][0] = 
			LHS[0][0][0][1] = 
			LHS[0][0][0][2] = 
			LHS[0][0][1][0] = 
			LHS[0][0][1][1] = 
			LHS[0][0][1][2] = 
			LHS[0][0][2][0] = 
			LHS[0][0][2][1] = 
			LHS[0][0][2][2] = 0.;

		LHS[NRows-1][2][0][0] = 
			LHS[NRows-1][2][0][1] = 
			LHS[NRows-1][2][0][2] = 
			LHS[NRows-1][2][1][0] = 
			LHS[NRows-1][2][1][1] = 
			LHS[NRows-1][2][1][2] = 
			LHS[NRows-1][2][2][0] = 
			LHS[NRows-1][2][2][1] = 
			LHS[NRows-1][2][2][2] = 0.;

		SolveBlockTri(LHS, RHS, NRows);

		for (i = 0; i < NRows; i++) {
			printf("%d %15.10f %15.10f %15.10f\n",
				   i, RHS[i][0], RHS[i][1], RHS[i][2]);
		}
	}

	/*
	  check the flux integration
	*/
	if(opt.iMode==11)
	{
		fprintf(stderr, "=======================================\n");
		fprintf(stderr, "=           FLUX INTEG TEST           =\n");
		fprintf(stderr, "=======================================\n");
		
		/* important options */
		opt.dUtop = 0;
		opt.dP0 = 1;
		opt.dU0 = 1;
		opt.dV0 = 1;
		opt.dRe = 10;
		opt.dBeta = 1;

		Lnorm lnorm[4];
		ExactInput einp;einp.vInit(&opt);
		Physics ph(&opt);
		Grid gr(&opt); int N=gr.iNCX(), M=gr.iNCY();
		
		VEC *avsol, *avfnum, *avferr, *avfexa;
		avsol = new VEC[N*M];
		avfnum = new VEC[N*M];
		avfexa = new VEC[N*M];		
		avferr = new VEC[N*M];

		/* assign the initial condition */
		for (int jj = 1 ; jj < M-1 ; jj++){
			for (int ii = 1 ; ii < N-1 ; ii++){
				double adc[2];

				gr.vCellCent(ii, jj, adc);
				vInitSol(adc, &einp, avsol[jj*N + ii]);
			}
		}
		/* set ghost values */
		vSetGhosts(&ph, &gr, avsol);
		//gr.vWriteVtk(vsol, (VEC *) NULL, "init.vtk");

		/* find the flux and calculate the error */
		for (int ii = 0 ; ii < 4 ; ii++) lnorm[ii].vInit();
		vFluxAssem(&ph, &gr, avsol , avfnum);
		for (int jj = 1 ; jj < M-1 ; jj++){
			for (int ii = 1 ; ii < N-1 ; ii++){
				double adc[2];

				gr.vCellCent(ii, jj, adc);
				vExactFlux(adc, &einp, avfexa[jj*N + ii]);
				AddVec(avfexa[jj*N + ii], avfnum[jj*N + ii], -1, avferr[jj*N + ii]);
				
				lnorm[3].vAddData(avferr[jj*N + ii][P]);
				lnorm[3].vAddData(avferr[jj*N + ii][U]);
				lnorm[3].vAddData(avferr[jj*N + ii][V]);
				
				lnorm[P].vAddData(avferr[jj*N + ii][P]);
				lnorm[U].vAddData(avferr[jj*N + ii][U]);
				lnorm[V].vAddData(avferr[jj*N + ii][V]);
			}
		}
		for (int ii = 0 ; ii < 4 ; ii++) lnorm[ii].vCompute();
		//gr.vWriteVtk(avfexa, avfnum, "flux.vtk");
		//gr.vWriteVtk(avferr, NULL, "error.vtk");
		
		printf("%d %e %e %e %e %e %e\n", N-2,
			   lnorm[3].dL1(), lnorm[3].dL2(), lnorm[3].dLInf(),
			   lnorm[P].dL2(), lnorm[U].dL2(), lnorm[V].dL2());

		/* delete */
		delete[] avsol;
		delete[] avfnum;
		delete[] avferr;
		delete[] avfexa;
	}

	/*
	  check the jacobian
	*/
    if(opt.iMode == 12)
	{
		fprintf(stderr, "=======================================\n");
		fprintf(stderr, "=          JACOBIAN TEST              =\n");
		fprintf(stderr, "=======================================\n");
		
		/* important options */
		opt.N = 20;
		opt.M = 20;
		opt.dUtop = 0;
		opt.dP0 = 1;
		opt.dU0 = 1;
		opt.dV0 = 1;
		opt.dRe = 10;
		opt.dBeta = 1;

		VEC vChange = {1e-6, 1e-6, 1e-6};
		int II=10, JJ= 10;
		//Lnorm lnorm;
		ExactInput einp;einp.vInit(&opt);
		Physics ph(&opt);
		Grid gr(&opt); int N=gr.iNCX(), M=gr.iNCY();
		
		VEC *avsol, *avf;
		MAT (*a2mjacX)[3], (*a2mjacY)[3];
		
		avsol = new VEC[N*M];
		avf = new VEC[N*M];
		a2mjacX = new MAT[N][3];
		a2mjacY = new MAT[M][3];		


		/* assign the initial condition */
		for (int jj = 1 ; jj < M-1 ; jj++){
			for (int ii = 1 ; ii < N-1 ; ii++){
				double adc[2];

				gr.vCellCent(ii, jj, adc);
				vInitSol(adc, &einp, avsol[jj*N + ii]);
			}
		}
		/* set ghost values */
		vSetGhosts(&ph, &gr, avsol);

		//lets assembe two jacobians
		MAT mUnit = {{1,0,0}, {0,1,0}, {0,0,1}};
		MAT mDeriv[5];
		VEC vf[5], vDiffFD[5], vDiffJAC[5];
		int ii=II, jj=JJ;

	    memset(mDeriv, 0, 5*sizeof(MAT));
		
		vJacAssemX(jj, &ph, &gr, 1, avsol, a2mjacX);
		vJacAssemY(ii, &ph, &gr, 1, avsol, a2mjacY);

	    /*
		  Find the change using jacobian
		*/
		//center
		Add3x3(mDeriv[eCt], a2mjacX[ii][eBx], -1, mDeriv[eCt]);
		Add3x3(mDeriv[eCt], mUnit, 1, mDeriv[eCt]);		
		Add3x3(mDeriv[eCt], a2mjacY[jj][eBy], -1, mDeriv[eCt]);
		Add3x3(mDeriv[eCt], mUnit, 1, mDeriv[eCt]);

		//right
		Add3x3(mDeriv[eDt], a2mjacX[ii+1][eAx], -1, mDeriv[eDt]);

		//left
		Add3x3(mDeriv[eBt], a2mjacX[ii-1][eCx], -1, mDeriv[eBt]);

		//top
		Add3x3(mDeriv[eEt], a2mjacY[jj+1][eAy], -1, mDeriv[eEt]);

		//bottom
		Add3x3(mDeriv[eAt], a2mjacY[jj-1][eCy], -1, mDeriv[eAt]);
		
		// change in flux integral
		for (int kk = 0 ; kk < 5 ; kk++)
		MultVec(mDeriv[kk], vChange, vDiffJAC[kk]);

		/*
		  find the change using finite difference
		*/

		//find the first flux
		vFluxAssem(&ph, &gr, avsol, avf);
		
		CopyVec(avf[(jj-1)*N + ii], vf[eAt]);
		CopyVec(avf[jj*N + ii-1], vf[eBt]);
		CopyVec(avf[jj*N + ii], vf[eCt]);
		CopyVec(avf[jj*N + ii+1], vf[eDt]);
		CopyVec(avf[(jj+1)*N + ii], vf[eEt]);


	    //perturb
		AddVec(avsol[jj*N + ii], vChange, 1, avsol[jj*N + ii]);

		//find the second flux and the difference
		vFluxAssem(&ph, &gr, avsol, avf);
		
		AddVec(avf[(jj-1)*N + ii], vf[eAt], -1, vDiffFD[eAt]);
		AddVec(avf[(jj)*N + ii-1], vf[eBt], -1, vDiffFD[eBt]);
		AddVec(avf[(jj)*N + ii], vf[eCt], -1, vDiffFD[eCt]);
		AddVec(avf[(jj)*N + ii+1], vf[eDt], -1, vDiffFD[eDt]);
		AddVec(avf[(jj+1)*N + ii], vf[eEt], -1, vDiffFD[eEt]);

		/*
		  print them
		*/

		//lnorm.vInit();
		//printf("==========================\n");
		for (int kk = 0 ; kk < 5 ; kk++){
			//SpewMatrix(mDeriv[kk]);
			//printf("\n");
			//SpewVector(vDiffFD[kk]);
			//SpewVector(vDiffJAC[kk]);
			//printf("==========================\n");

			AddVec(vDiffFD[kk], vDiffJAC[kk], -1, vf[kk]);
			printf("## %e %e %e\n", vf[kk][P], vf[kk][U], vf[kk][V]);
			//lnorm.vAddData(vf[kk][P]);
			//lnorm.vAddData(vf[kk][U]);
			//lnorm.vAddData(vf[kk][V]);

		}
		//lnorm.vCompute();

		//printf("%lf %lf %lf\n", lnorm.dL1(), lnorm.dL2(), lnorm.dLInf());
	
		  
		/* delete */
		delete[] avsol;
		delete[] avf;
		delete[] a2mjacX;
		delete[] a2mjacY;
	}

	/*
	  check approximate factorization
	*/
	if( (opt.iMode == 31) || (opt.iMode == 32) ){
		fprintf(stderr, "=======================================\n");
		fprintf(stderr, "=               STABILITY             =\n");
		fprintf(stderr, "=======================================\n");

		/* Hardcoded options (Mandatory) */
		if (opt.iMode == 31) {
			opt.dU0 = opt.dV0 = opt.dP0 = 1;
			opt.dUtop = 0;
		}else{
			//opt.dU0 = opt.dV0 = opt.dP0 = 0;
		}

		//may be changed M,N(20), A(0), Omega(1)
		//   dt(0.05), beta(1), epsilon(1e-7)

		/* Needed Objects */
		ExactInput einp; einp.vInit(&opt);
		double dc[2], dc0[] = {0,1};
		Physics ph(&opt);
		Grid gr(&opt); int N=gr.iNCX(), M=gr.iNCY();
		Memory mem; vInitMemImplicit(&gr, &mem, &opt);

		/*
		  Initial condition
		*/
		for (int jj = 1 ; jj < M-1 ; jj++){
			for (int ii = 1 ; ii < N-1 ; ii++){
				
				gr.vCellCent(ii, jj, dc);
				vTransferCoord(dc0, dc);
				vInitSol(dc, &einp, mem.avsol[jj*N + ii]);
			}
		}
		// set ghost values 
		vSetGhosts(&ph, &gr, mem.avsol);
		//write the initial condition
		{
			char tmp[300];
			sprintf(tmp, "%s_init.vtk", opt.strFName);
			gr.vWriteVtk(mem.avsol, (VEC *) NULL, tmp);			
		}

		/*
		  solve the system
		*/
		{
			int nn = 0;
			Lnorm lChange[4], lError[3];
			double dtemp;
			double dcomputetime;

			// write a header for the error and change in solution norms
			if (opt.iMode == 31) {
			    printf("## Iteration l2_e_p l2_e_u l2_e_v l2_c_p l2_c_u l2_c_v l2_t \n\n");
			}else{
			    printf("## Iteration l2_c_p l2_c_u l2_c_v l2_t \n\n");
			}

			dcomputetime = 0;
			do{
				
				// advance the solution - l norms of change
				dtemp = clock();
				vTimeAdvImplicit(&gr, &ph, &mem, lChange);
				nn++;
				dtemp = clock() - dtemp;
				dcomputetime += dtemp;

			    /* print the error and change in solution norms */
				if (opt.iMode == 31) {
					lError[P].vInit();
					lError[U].vInit();
					lError[V].vInit();
					for (int jj = 1 ; jj < M-1 ; jj++){
						for (int ii = 1 ; ii < N-1 ; ii++){
							lError[P].vAddData(mem.avsol[jj*N + ii][P] - mem.avsol[1*N + 1][P]);
							lError[U].vAddData(mem.avsol[jj*N + ii][U]);
							lError[V].vAddData(mem.avsol[jj*N + ii][V]);
						}
					}
					lError[P].vCompute();
					lError[U].vCompute();
					lError[V].vCompute();
				
				
				printf("%d %e %e %e %e %e %e %e \n", nn,
					   lError[P].dL2(), lError[U].dL2(), lError[V].dL2(),
					   lChange[P].dL2(), lChange[U].dL2(), lChange[V].dL2(),
					   lChange[3].dL2() );
				} else{
					printf("%d %e %e %e %e  \n", nn,
					   lChange[P].dL2(), lChange[U].dL2(), lChange[V].dL2(),
					   lChange[3].dL2() );
				}
				
			}while((lChange[P].dL2() > opt.dEpsilon) ||
				   (lChange[U].dL2() > opt.dEpsilon) ||
				   (lChange[V].dL2() > opt.dEpsilon) );

			fprintf(stderr, "CompTime: %lf, Niter: %d\n", dcomputetime/CLOCKS_PER_SEC, nn);

		}

		/* make the average of pressure zero */
		if(1)
		{
			double dave = 0;

			for (int jj = 1 ; jj < M-1 ; jj++)
				for (int ii = 1 ; ii < N-1 ; ii++)
					dave+=mem.avsol[jj*N + ii][P] ;
			dave /= (M-2); dave /= (N-2);

			for (int jj = 1 ; jj < M-1 ; jj++)
				for (int ii = 1 ; ii < N-1 ; ii++)
					mem.avsol[jj*N + ii][P] -= dave;
			
			fprintf(stderr, "average pressure: %lf\n", dave);
			fprintf(stderr, "Pressure at center: %lf\n", mem.avsol[M/2*N + N/2][P]);
		}


		/* write the solution */
		{
			char tmp[300];

			// vtk file
			sprintf(tmp, "%s.vtk", opt.strFName);
			gr.vWriteVtk(mem.avsol, (VEC *) NULL, tmp);

			// dat file and solution on midline
			if(opt.iMode == 32){

				FILE *fl;
				int im, im0, im1;
				double uu;

				// write dat file
				sprintf(tmp, "%s.dat", opt.strFName);
				vDatWrite(&ph ,&gr, tmp, mem.avsol);

				// write solution on  midline
				sprintf(tmp, "%s.vline", opt.strFName);
				fl = fopen(tmp, "w"); assert(fl);
				fprintf(fl, "## %-27s %-30s\n", "Y", "U velocity at center");

				fprintf(fl, "%-30.15lf %-30.15lf\n", 0.0, 0.0);
				for (int jj = 1 ; jj < M - 1 ; jj++){
					if ( N % 2 == 0 ) {
						im0 = N / 2;
						im1 = im0 + 1;
						uu = ( mem.avsol[jj*N + im0][U] + mem.avsol[jj*N + im1][U] )/ 2.;					
					} else{
						im = N/2 + 1;
			    		uu = mem.avsol[jj*N + im][U];										
					}
					gr.vCellCent(0, jj, dc);
					fprintf(fl, "%-30.15lf %-30.15lf\n", dc[eY], uu);
				}
				fprintf(fl, "%-30.15lf %-30.15lf\n", gr.dY(), opt.dUtop);
				
				fclose(fl);
			}

		}
		
		mem.vDestroy();
	} /* end of case 31 or 32 */		

	return 0;
}










