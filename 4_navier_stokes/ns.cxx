/* ns.cxx
 *
 * Definitions for functions in Navier Stokes project.
 */

#include "ns.hxx"
#include "visit_writer.hxx"

#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstring>


/****************************************************************************
 *                                    Grid                                  *
 ****************************************************************************/

Grid::Grid(const int inx, const double dxmax,
		   const int iny, const double dymax){
	inx_ = inx + 2;
	iny_ = iny + 2;
	dxmax_ = dxmax;
	dymax_ = dymax;
	ddx_ = dxmax_ / inx;
	ddy_ = dymax_ / iny;

	fprintf(stderr, "Creating Grid NX=%d NY=%d Nbar=%lf \n",
			inx, iny, sqrt(inx*iny));
	fprintf(stderr, "\t Xmax=%lf Ymax=%lf dx=%lf dy=%lf dx/dy=%lf\n",
			dxmax_, dymax_, ddx_, ddy_, ddx_/ddy_);
}

Grid::Grid(Options *opt){
	inx_ = opt->N + 2;
	iny_ = opt->M + 2;
	dxmax_ = opt->X;
	dymax_ = opt->Y;
	ddx_ = dxmax_ / (inx_ - 2);
	ddy_ = dymax_ / (iny_ - 2);

	fprintf(stderr, "Creating Grid NX=%d NY=%d Nbar=%lf \n\t Xmax=%lf Ymax=%lf dx=%lf dy=%lf dx/dy=%lf\n",
			inx_-2, iny_-2, sqrt((inx_-2)*(iny_-2)), dxmax_, dymax_, ddx_, ddy_, ddx_/ddy_);
}

void Grid::vRefine(const int inx, const int iny){
	inx_ = inx + 2;
	iny_ = iny + 2;
	ddx_ = dxmax_ / inx;
	ddy_ = dymax_ / iny;

}
	

void Grid::vCellVerts(const int ii, const int jj, double* adcoord) const{
	assert( (ii < inx_) && (jj < iny_) );
	adcoord[0*2 + eX] = (ii - 1) * ddx_;
	adcoord[0*2 + eY] = (jj - 1) * ddy_;
	adcoord[1*2 + eX] = ii  * ddx_ ;
	adcoord[1*2 + eY] = jj  * ddy_ ;	
}

// write a vtk file
void  Grid::vWriteVtk(VEC *num, VEC *err, const char *namevtk) const{
	
	int dims[] = {inx_-1, iny_-1, 1}, k;
	float *x, *y, z[] = {0};
	int nvars = (err ? 4 : 2);
	int vardim[] = {1,3, 1,3};
	int centering[] = {0,0,0,0};
	double *vars[4];
	const char varname1[] = "Pressure";
	const char varname2[] = "Velocity";
	const char varname3[] = "PressureError";
	const char varname4[] = "VelocityError";
	const char *varnames[] = {varname1, varname2, varname3, varname4};

	//allocate memory
	x = new float[inx_-1];
	y = new float[iny_-1];
	vars[0] = new double[ (inx_-2) * (iny_-2)];
	vars[1] = new double[ 3 * (inx_-2) * (iny_-2)];
	if (err){
		vars[2] = new double[ (inx_-2) * (iny_-2)];
		vars[3] = new double[ 3 * (inx_-2) * (iny_-2)];
	}

	//copy data
	for (int i = 0 ; i < inx_-1 ; i++)
		x[i] = (float) dxmax_ * i / (inx_-2);
	for (int j = 0 ; j < iny_-1 ; j++)
		y[j] = (float) dymax_ * j / (iny_-2);

	k =0;
	for (int j = 1 ; j < iny_-1 ; j++){
		for (int i = 1 ; i < inx_-1 ; i++){
			vars[0][k] = num[j*inx_+i][P];
			vars[1][3*k] = num[j*inx_+i][U];
			vars[1][3*k+1] = num[j*inx_+i][V];
			vars[1][3*k+2] = 0;
			if (err){
				vars[2][k] = err[j*inx_+i][P];
				vars[3][3*k] = err[j*inx_+i][U];
				vars[3][3*k+1] = err[j*inx_+i][V];
				vars[3][3*k+2] = 0;
			}
			k++;
		}
	}

	//write to a vtk file
	write_rectilinear_mesh(namevtk, 1, dims, x, y, z, nvars, vardim, centering, varnames, vars);

	//free memory
	delete[] x;
	delete[] y;
	delete[] vars[0];
	delete[] vars[1];
	if (err){
		delete[] vars[2];
		delete[] vars[3];
	}
}

void  Grid::vWriteVtk(double *vars[], int nvar,
				const char * const * varnames, const char *namevtk) const{

	int dims[] = {inx_-1, iny_-1, 1};
	float *x, *y, z[] = {0};
	int vardim[] = {1,1,1,1,1,1,1,1,1,1,1,1,1};
	int centering[] = {0,0,0,0,0,0,0,0,0,0,0,0,0};

	//allocate memory
	x = new float[inx_-1];
	y = new float[iny_-1];

	//copy data
	for (int i = 0 ; i < inx_-1 ; i++)
		x[i] = (float) dxmax_ * i / (inx_-2);
	for (int j = 0 ; j < iny_-1 ; j++)
		y[j] = (float) dymax_ * j / (iny_-2);


	//write to a vtk file
	write_rectilinear_mesh(namevtk, 1, dims, x, y, z, nvar, vardim, centering, varnames, vars);

	//free memory
	delete[] x;
	delete[] y;

}


/****************************************************************************
 *                          Linear Algebra                                  *
 ****************************************************************************/
void SolveBlockTri(MAT LHS[][3], VEC RHS[], int iNRows)
{
  int j;
  MAT Inv;

  for (j = 0; j < iNRows-1; j++) {
    /* Compute the inverse of the main block diagonal. */
    Invert3x3(LHS[j][1], Inv);
    /* Scale the right-most block diagonal by the inverse. */
    {
      double Temp[3][3];
      Mult3x3(Inv, LHS[j][2], Temp);
      Copy3x3(Temp, LHS[j][2]);
    }

    /* Scale the right-hand side by the inverse. */
    {
      double Temp[3];
      MultVec(Inv, RHS[j], Temp);
      CopyVec(Temp, RHS[j]);
    }      


    /* Left-multiply the jth row by the sub-diagonal on the j+1st row
       and subtract from the j+1st row.  This involves the
       super-diagonal term and the RHS of the jth row. */
    {
      /* First the LHS manipulation */
#define A LHS[j+1][0]
#define B LHS[j+1][1]
#define C LHS[j  ][2]
      double Temp[3][3], Temp2[3][3];
      double TVec[3], TVec2[3];
      Mult3x3(A, C, Temp);
      Add3x3(B, Temp, -1., Temp2);
      Copy3x3(Temp2, B);

      /* Now the RHS manipulation */
      MultVec(A, RHS[j], TVec);
      AddVec(RHS[j+1], TVec, -1., TVec2);
      CopyVec(TVec2, RHS[j+1]);
#undef A
#undef B
#undef C
    }
  } /* Done with forward elimination loop */
  /* Compute the inverse of the last main block diagonal. */
  j = iNRows-1;
  Invert3x3(LHS[j][1], Inv);

  /* Scale the right-hand side by the inverse. */
  {
    double Temp[3];
    MultVec(Inv, RHS[j], Temp);
    CopyVec(Temp, RHS[j]);
  }      
  
  /* Now do the back-substitution. */
  for (j = iNRows-2; j >= 0; j--) {
    /* Matrix-vector multiply and subtract. */
#define C LHS[j][2]
    RHS[j][0] -= (C[0][0]*RHS[j+1][0] +
		  C[0][1]*RHS[j+1][1] +
		  C[0][2]*RHS[j+1][2]);
    RHS[j][1] -= (C[1][0]*RHS[j+1][0] +
		  C[1][1]*RHS[j+1][1] +
		  C[1][2]*RHS[j+1][2]);
    RHS[j][2] -= (C[2][0]*RHS[j+1][0] +
		  C[2][1]*RHS[j+1][1] +
		  C[2][2]*RHS[j+1][2]);
#undef C
  }
}

void SpewMatrix(pMAT Source, FILE* fl){
	fprintf(fl, "%-10.6lf %-10.6lf %-10.6lf\n", Source[0][0], Source[0][1], Source[0][2]);
	fprintf(fl, "%-10.6lf %-10.6lf %-10.6lf\n", Source[1][0], Source[1][1], Source[1][2]);
	fprintf(fl, "%-10.6lf %-10.6lf %-10.6lf\n", Source[2][0], Source[2][1], Source[2][2]);
}

void SpewVector(pVEC Source, FILE* fl){
	fprintf(fl, "%-10.6lf %-10.6lf %-10.6lf\n", Source[0], Source[1], Source[2]);
}
/****************************************************************************
 *                                Options                                   *
 ****************************************************************************/
void vOptionsDef(Options *opt){

	//physics
	opt->dUtop = 1;
	opt->dRe = 100;
	opt->dBeta = 1;
	opt->dA = 0;

	//time advance
	opt->dTimeStep = 0.05;
	opt->dOmega = 1;
	opt->dEpsilon = 1e-7;

	//grid
	opt->N = opt->M = 20;
	opt->X = opt->Y = 1;

	// manufactured solution
	opt->dP0 = opt->dU0 = opt->dV0 = 0;

	// file name
	sprintf(opt->strFName, "out");
	opt->iMode = 31;
}

// copied from Schewchuck's Triangle 
static int qReadOptionDouble(const char *argvi, int& j, double& val){

	int k;
	char workstring[300];
	
	if (((argvi[j+1] >= '0') && (argvi[j+1] <= '9')) ||
		(argvi[j+1] == '.') || (argvi[j+1] == '-')  ) {
		k = 0;
		while (((argvi[j + 1] >= '0') && (argvi[j + 1] <= '9')) ||
			   (argvi[j + 1] == '.') || (argvi[j+1] == '-') ) {
			j++;
			workstring[k] = argvi[j];
			k++;
		}
		workstring[k] = '\0';
		val = strtod(workstring, (char **) NULL);
		return 1;
	}
	else {
		return 0;
	}
}

static int qReadOptionInt(const char *argvi, int& j, int& val){

	int k;
	char workstring[300];
	
	if ((argvi[j+1] >= '0') && (argvi[j+1] <= '9')) {
		k = 0;
		while ((argvi[j + 1] >= '0') && (argvi[j + 1] <= '9')) {
			j++;
			workstring[k] = argvi[j];
			k++;
		}
		workstring[k] = '\0';
		val = strtol(workstring, (char **) NULL, 10);
		return 1;
	}
	else {
		return 0;
	}
}

void vOptionsInit(int argc, char *argv[], Options *opt){
	int i, j;
	
	for (i = 1; i < argc; i++) {
		if (argv[i][0] == '-') {
			fprintf(stderr, "input_options: %s\n", argv[i]);
			for (j = 0; argv[i][j] != '\0'; j++) {

				// p0, u0 and v0 for test
				if(argv[i][j] == 'u') qReadOptionDouble(argv[i], j, opt->dU0);
				if(argv[i][j] == 'v') qReadOptionDouble(argv[i], j, opt->dV0);
				if(argv[i][j] == 'p') qReadOptionDouble(argv[i], j, opt->dP0);

				// mesh size
				if(argv[i][j] == 'M') qReadOptionInt(argv[i], j, opt->M);
				if(argv[i][j] == 'N') qReadOptionInt(argv[i], j, opt->N);
				if(argv[i][j] == 'X') qReadOptionDouble(argv[i], j, opt->X);
				if(argv[i][j] == 'Y') qReadOptionDouble(argv[i], j, opt->Y);

				// reynolds and beta
				if(argv[i][j] == 'B') qReadOptionDouble(argv[i], j, opt->dBeta);
				if(argv[i][j] == 'R') qReadOptionDouble(argv[i], j, opt->dRe);
				if(argv[i][j] == 'A') qReadOptionDouble(argv[i], j, opt->dA);

				// velocity on top surface
				if(argv[i][j] == 'U') qReadOptionDouble(argv[i], j, opt->dUtop);

				//time advance
				if(argv[i][j] == 'O') qReadOptionDouble(argv[i], j, opt->dOmega);
				if(argv[i][j] == 'T') qReadOptionDouble(argv[i], j, opt->dTimeStep);
				if(argv[i][j] == 'E'){
					double dPow;
					qReadOptionDouble(argv[i], j, dPow);
					opt->dEpsilon = pow(10,-dPow);
				}
				
				//mode
				if(argv[i][j] == 'm') qReadOptionInt(argv[i], j, opt->iMode);
				

			}
		} else{
			//file name
			memcpy(opt->strFName, argv[i], strlen(argv[i])+1);
		}
	}
}

/****************************************************************************
 *                                Phsyics                                   *
 ****************************************************************************/
Physics::Physics(const Options *opt){
	dRe = opt->dRe;
	dReInv = 1/dRe;
	dBeta = opt->dBeta;
	dUtop = opt->dUtop;
	dA = opt->dA;

	fprintf(stderr, "Created a physics: Re: %lf, A:%lf, Beta:%lf, dUtop:%lf\n",
			dRe, dA, dBeta, dUtop);
}



/****************************************************************************
 *                             Assemblers                                   *
 ****************************************************************************/
void vFluxAssem(const Physics *ph, const Grid *gr,
				VEC avsol[], VEC avflux[]){

	VEC vzero = {0, 0, 0};
	VEC vflux;
	
	int M,N;
	N = gr->iNCX();
	M = gr->iNCY();
	
	FITS inp;
	inp.ddx = gr->dDx();
	inp.ddy = gr->dDy();

	/*
	  set the flux to zero
	*/
	memset(avflux, 0, (size_t)(sizeof(VEC)*N*M) );
    
	/*
	  Assemble the flux integral for the faces oriented in
	  the x direction.
	*/
	for (int jj = 1 ; jj < M-1 ; jj++){
		int iface, iil, iir;

		// loop over all faces (no ghost)
		for (iface = 1 ; iface < gr->iNFX()-1 ; iface++){
			
			//find adjacent cells
			gr->vCLeft (iface, jj, iil);
			gr->vCRight(iface, jj, iir);

			// find the value of solution
			inp.vsoll = avsol[jj*N + iil];
			inp.vsolr = avsol[jj*N + iir];

			// find the flux
			ph->vFluxX(&inp, vflux);
			//SpewVector(vflux);
			AddVec(vzero, vflux, 1/inp.ddx, vflux);
			//SpewVector(vflux); 

			// assemble
			//SpewVector(avflux[jj*N + iir]);
			AddVec(avflux[jj*N + iir], vflux, -1, avflux[jj*N + iir]);
			//SpewVector(avflux[jj*N + iir]);
			AddVec(avflux[jj*N + iil], vflux, +1, avflux[jj*N + iil]);
			//printf("\n");
		    
		}
	}
	inp.vsoll = inp.vsolr = (pVEC) NULL;

	/*
	  Assemble the flux integral for the faces oriented in
	  the Y direction.
	*/
	for (int ii = 1 ; ii < N-1 ; ii++){
		int jface, jjt, jjb;
		
		// loop over all faces (no ghost)
		for (jface = 1 ; jface < gr->iNFY()-1 ; jface++){
			
			//find adjacent cells
			gr->vCTop (ii, jface, jjt);
			gr->vCBott(ii, jface, jjb);

			// find the value of solution
			inp.vsolt = avsol[jjt*N + ii];
			inp.vsolb = avsol[jjb*N + ii];

			// find the flux
			ph->vFluxY(&inp, vflux);
			//SpewVector(vflux);
		    AddVec(vzero, vflux, 1/inp.ddy, vflux);
			//SpewVector(vflux);
			
			// assemble
			//SpewVector(avflux[jjt*N + ii]);
			AddVec(avflux[jjt*N + ii], vflux, -1, avflux[jjt*N + ii]);
			//SpewVector(avflux[jjt*N + ii]);
			AddVec(avflux[jjb*N + ii], vflux, +1, avflux[jjb*N + ii]);
			//printf("\n");
		}
	}
	inp.vsolb = inp.vsolt = (pVEC) NULL;
	
}


void vSetGhosts(const Physics *ph, const Grid *gr,  VEC avsol[]){
	
	int ii, jj, N=gr->iNCX(), M=gr->iNCY();

	for (jj = 1 ; jj < M-1 ; jj++){
		ph->vGhostVal(eLeft, avsol[jj*N+1], avsol[jj*N + 0]);
		ph->vGhostVal(eRight, avsol[jj*N+(N-2)], avsol[jj*N + (N-1)]);
	}

	for (ii = 1 ; ii < N-1 ; ii++){
		ph->vGhostVal(eDown, avsol[1*N + ii], avsol[0*N + ii]);
		ph->vGhostVal(eUp, avsol[(M-2)*N + ii], avsol[(M-1)*N + ii]);
	}

	//remove some valgrind warnings (four corner ghosts)
	SetVec(avsol[0], 0);
	SetVec(avsol[N-1], 0);
	SetVec(avsol[(M-1)*N], 0);
	SetVec(avsol[(M-1)*N + N-1], 0);

}

void vJacAssemX(const int jj, const Physics *ph, const Grid *gr,
				const double ddt, VEC avsol[], MAT a2mjac[][3]){

	double ddt_dx = ddt / gr->dDx();
	int N;
	N = gr->iNCX();
	
	FITS inp;
	inp.ddx = gr->dDx();
	inp.ddy = gr->dDy();

	MAT mUnit = {{1,0,0}, {0,1,0}, {0,0,1}};
	MAT mZero = {{0,0,0}, {0,0,0}, {0,0,0}};

	/* set the jac to I */
	for (int ii = 0; ii < N ; ii++){
		Copy3x3(mUnit, a2mjac[ii][eBx]);
		Set3x3(a2mjac[ii][eCx], 0);
		Set3x3(a2mjac[ii][eAx], 0);
	}

	/* start assembling */
	for (int iface = 1 ; iface < gr->iNFX()-1 ; iface++){
		MAT mjl, mjr;
		int iil, iir;
		
		//find adjacent cells
		gr->vCLeft(iface, jj, iil);
		gr->vCRight(iface, jj,iir);

		// find the value of solution
		inp.vsoll = avsol[jj*N + iil];
		inp.vsolr = avsol[jj*N + iir];

		// find the jac
		ph->vLocJacX(&inp, mjl, mjr);
		Add3x3(mZero, mjl, ddt_dx, mjl);
		Add3x3(mZero, mjr, ddt_dx, mjr);


		/* assemble */
		// a2djac[iil][Bx] -= mjl;
		// a2djac[iil][Cx] -= mjr;
		Add3x3(a2mjac[iil][eBx], mjl, -1, a2mjac[iil][eBx]);
		Add3x3(a2mjac[iil][eCx], mjr, -1, a2mjac[iil][eCx]);

		// a2djac[iir][Ax] += mjl;
		// a2djac[iir][Bx] += mjr;
		Add3x3(a2mjac[iir][eAx], mjl, +1, a2mjac[iir][eAx]);
		Add3x3(a2mjac[iir][eBx], mjr, +1, a2mjac[iir][eBx]);

	}

	/* set the ghost cells */
    Copy3x3(mUnit, a2mjac[0][eBx]);
	ph->vGhostJac(eLeft, a2mjac[0][eCx]);
	
	Copy3x3(mUnit, a2mjac[N-1][eBx]);
	ph->vGhostJac(eRight, a2mjac[N-1][eAx]);

}

void vJacAssemY(const int ii, const Physics *ph, const Grid *gr,
				const double ddt, VEC avsol[], MAT a2mjac[][3]){
	
	double ddt_dy = ddt / gr->dDy();
	int M,N;
	N = gr->iNCX();
	M = gr->iNCY();
	
	FITS inp;
	inp.ddx = gr->dDx();
	inp.ddy = gr->dDy();

	MAT mUnit = {{1,0,0}, {0,1,0}, {0,0,1}};
	MAT mZero = {{0,0,0}, {0,0,0}, {0,0,0}};

	/* set the jac to I */
	for (int jj = 0; jj < M ; jj++){
		Set3x3(a2mjac[jj][eCy], 0);
		Set3x3(a2mjac[jj][eAy], 0);
		Copy3x3(mUnit, a2mjac[jj][eBy]);
	}

	/* start assembling */
	for (int jface = 1 ; jface < gr->iNFY()-1 ; jface++){
		MAT mjb, mjt;
		int jjb, jjt;
		
		//find adjacent cells
		gr->vCBott(ii, jface, jjb);
		gr->vCTop(ii, jface, jjt);

		// find the value of solution
		inp.vsolb = avsol[jjb*N + ii];
		inp.vsolt = avsol[jjt*N + ii];

		// find the jac
		ph->vLocJacY(&inp, mjb, mjt);
		Add3x3(mZero, mjt, ddt_dy, mjt);
		Add3x3(mZero, mjb, ddt_dy, mjb);


		// assemble
		//a2djac[inp.jjb][By] -= djb;
		// a2djac[inp.jjb][Cy] -= djt;
		Add3x3(a2mjac[jjb][eBy], mjb, -1, a2mjac[jjb][eBy]);
		Add3x3(a2mjac[jjb][eCy], mjt, -1, a2mjac[jjb][eCy]);

		//a2djac[inp.jjt][Ay]	+= djb;
		//a2djac[inp.jjt][By] += djt;
		Add3x3(a2mjac[jjt][eAy], mjb, +1, a2mjac[jjt][eAy]);
		Add3x3(a2mjac[jjt][eBy], mjt, +1, a2mjac[jjt][eBy]);

	}

	/* set the ghost cells */
	Copy3x3(mUnit, a2mjac[0][eBy]);
	ph->vGhostJac(eDown, a2mjac[0][eCy]);
	
	Copy3x3(mUnit, a2mjac[M-1][eBy]);
	ph->vGhostJac(eUp, a2mjac[M-1][eAy]);


}


/****************************************************************************
 *                                  TimeAdvance                             *
 ****************************************************************************/
static void vPrintSol(const int N, const int M, const VEC *adx, const int qGhost = 0, FILE* fl = stdout){

	int ibeg, iend;
	int jbeg, jend;
	
	if (qGhost){
		ibeg = jbeg = 0;
		jend = M;
		iend = N;
	}else{
		jbeg = ibeg = 1;
		jend = M -1;
		iend = N -1;
	}
		
	for (int ii=ibeg ; ii < iend ; ii++){
		for (int jj=jbeg; jj < jend; jj++){
			fprintf(fl, "I:%3d J:%3d ", ii, jj);
			fprintf(fl, "P: %12.5lf ", adx[jj*N+ii][P]);
			fprintf(fl, "U: %12.5lf ", adx[jj*N+ii][U]);
			fprintf(fl, "V: %12.5lf ", adx[jj*N+ii][V]);
			fprintf(fl, "\n");
		}
		fprintf(fl, "\n");				
	}
}

void vInitMemImplicit(const Grid* gr, Memory *mem, Options *opt){
	
	int N=gr->iNCX(), M=gr->iNCY();

	mem->avsol = new VEC[M*N];
	mem->avflux = new VEC[M*N];
	mem->avsolcol = new VEC[M];
	mem->a2mjac = new MAT[GMAX(M,N)][3];

	mem->ddt = opt->dTimeStep;
	mem->domega = opt->dOmega;

	fprintf(stderr, "Setting up implicit time advance: dt:%lf omega:%lf epsilon:%e\n",
			mem->ddt, mem->domega, opt->dEpsilon); 
}

void vTimeAdvImplicit(const Grid *gr, const Physics *ph,
					  Memory * mem, Lnorm lnorm[4]){

	int N=gr->iNCX(), M=gr->iNCY();
	VEC vZero = {0, 0, 0};

	// printf("Solution Before:\n");
	// vPrintSol(N, M, mem->avsol, 1);

	/* Flux integral */
	vFluxAssem(ph, gr, mem->avsol, mem->avflux);
		
		
	/*
	  solve along rows
	*/
	for (int jj = 1 ; jj < M -1 ; jj++){
			
		for (int ii=1 ; ii < N-1 ; ii++)
			AddVec(vZero, mem->avflux[jj*N+ii], mem->ddt, mem->avflux[jj*N+ii]);
		
		SetVec(mem->avflux[jj*N + 0], 0);
		SetVec(mem->avflux[jj*N + N-1], 0);
		vJacAssemX(jj, ph, gr, mem->ddt, mem->avsol, mem->a2mjac);
		SolveBlockTri(mem->a2mjac, mem->avflux+jj*N, N);
	}

	// printf("Solution After row pass:\n");
	// vPrintSol(N, M, mem->avflux, 0);


	/*
	  solve along columns
	*/
	for (int ii = 1 ; ii < N-1 ; ii++){
		    
		vJacAssemY(ii, ph, gr, mem->ddt, mem->avsol, mem->a2mjac);
			
		//extract the iith column
		for (int jj = 1 ; jj < M -1 ; jj++)
			CopyVec(mem->avflux[jj*N + ii], mem->avsolcol[jj]);
		SetVec(mem->avsolcol[0], 0);
		SetVec(mem->avsolcol[M-1], 0);

		// solve
	    SolveBlockTri(mem->a2mjac, mem->avsolcol, M);			

		//put the solution back
		for (int jj = 1 ; jj < M -1 ; jj++)
			CopyVec(mem->avsolcol[jj], mem->avflux[jj*N + ii]);			
	}

	// printf("Solution After column pass:\n");
	// vPrintSol(N, M, mem->avflux, 0);


	/*
	  update solution and find the norms
	*/
	lnorm[P].vInit();
	lnorm[U].vInit();
	lnorm[V].vInit();
	lnorm[3].vInit();
	for (int ii=1 ; ii < N-1 ; ii++){
		for (int jj=1; jj < M-1; jj++){
			int kk = jj*N+ii;
			AddVec(mem->avsol[kk], mem->avflux[kk], mem->domega, mem->avsol[kk]);
			lnorm[3].vAddData(mem->domega*mem->avflux[kk][P]);
			lnorm[3].vAddData(mem->domega*mem->avflux[kk][U]);
			lnorm[3].vAddData(mem->domega*mem->avflux[kk][V]);
			lnorm[P].vAddData(mem->domega*mem->avflux[kk][P]);
			lnorm[U].vAddData(mem->domega*mem->avflux[kk][U]);
			lnorm[V].vAddData(mem->domega*mem->avflux[kk][V]);    
		}
	}
	lnorm[P].vCompute();
	lnorm[U].vCompute();
	lnorm[V].vCompute();
	lnorm[3].vCompute();

	/* update the ghosts */
	vSetGhosts(ph, gr, mem->avsol);
	
}




/****************************************************************************
 *                               DAT File Format                            *
 ****************************************************************************/
void vDatRead (Options *opt, VEC **avsol){

 	FILE *fl;
 	char strLine[1024];

 	fl = fopen(opt->strFName, "r"); assert(fl);

	/* read the first line */
	{
		char *cStatus;
	    cStatus = fgets ( strLine, 1024, fl ); assert(cStatus == strLine);
	}

	/* read the options */
	{
		char *argv[] = {NULL, strLine};
		vOptionsInit(2, argv, opt);
	}

	/* read the data */
	Grid gr(opt);
	int M=gr.iNCY(), N=gr.iNCX();
	(*avsol) = new VEC[M*N];
	{
		int itmp;
		pVEC vcrnt;
		for (int jj = 1 ; jj < M-1 ; jj++){
			for (int ii = 1 ; ii < N-1 ; ii++){
				
				vcrnt = (*avsol)[jj*N + ii];
				fscanf(fl, "%d", &itmp);assert(itmp == jj);
				fscanf(fl, "%d", &itmp);assert(itmp == ii);
				itmp=fscanf(fl, "%lf %lf %lf", vcrnt+P, vcrnt+U, vcrnt+V);assert(itmp==3);
			}
		}
	}

	fclose(fl);
}

void vDatWrite(Physics *ph, Grid *gr, const char *fname, VEC avsol[]){
	
	int M=gr->iNCY(), N=gr->iNCX();
 	FILE *fl;

	fl = fopen(fname, "w"); assert(fl);

	//write the header
	fprintf(fl,"-M%dN%dX%lfY%lfU%lf\n\n", M-2, N-2, gr->dX(), gr->dY(), ph->dUtop);

	//write the data
	for (int jj = 1 ; jj < M-1 ; jj++){
		for (int ii = 1 ; ii < N-1 ; ii++){
			pVEC vec;
			
			vec = avsol[jj*N + ii];
		    fprintf(fl, "%-4d %-4d %-25.20lf %-25.20lf %-25.20lf\n", jj, ii, vec[P], vec[U], vec[V]);
		}
		fprintf(fl, "\n");
	}

	fclose(fl);
}
