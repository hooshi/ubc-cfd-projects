/*         ..######..########.########..........##....##..######.            */
/*         .##....##....##....##.....##.........###...##.##....##            */
/*         .##..........##....##.....##.........####..##.##......            */
/*         ..######.....##....########..#######.##.##.##..######.            */
/*         .......##....##....##...##...........##..####.......##            */
/*         .##....##....##....##....##..........##...###.##....##            */
/*         ..######.....##....##.....##.........##....##..######.            */
/*                                                                           */
/*                   Structured Navier Stokes - ns.hxx                       */



#ifndef NS_HXX
#define NS_HXX

#include <cmath>
#include <cassert>
#include <cstring>
#include <cstdio>
#include <cmath>

/****************************************************************************
 *                         Macro, Enum, ...                                 *
 ****************************************************************************/

#define PI 3.141592653589793238462643383

#define GABS(x)   ( ((x)>0) ? (x) : (-(x)) )
#define GMAX(x,y) ( ((x)>(y)) ? (x) : (y) )
#define GMIN(x,y) ( ((x)>(y)) ? (y) : (x) )

enum EnumDirection {eX=0, eY};
enum EnumJacX {eAx=0, eBx, eCx};
enum EnumJacY {eAy=0, eBy, eCy};
enum EnumJacTot {eAt=0, eBt, eCt, eDt, eEt};
enum EnumWall {eRight=0, eUp=1, eLeft=2, eDown=3};
enum EnumSoln {P=0, U, V};

typedef double REAL;
typedef REAL MAT[3][3];
typedef REAL VEC[3];
typedef REAL (*pMAT)[3];
typedef REAL *pVEC;

/****************************************************************************
 *                               Grid                                       *
 ****************************************************************************/
/* Grid
 *
 *  Saves a two-dimensional grid.
 */
struct Options;
class Grid{
private:
	int inx_, iny_;         //number of cells (including ghosts)
	double dxmax_, dymax_;  //domain sizes
	double ddx_, ddy_; //mesh sizes in each direction
	Grid() {};        //prohibited constructor

public:
	// Creates the grid
	// param inx: number of cells in x direction excluding ghosts
	// param iny: number of cells in y direction excluding ghosts
	// param dxmax: domain size in x direction
	// param dymax: domain size in y direction
	Grid(const int inx, const double dxmax,
		 const int iny, const double dymax);
	Grid(Options *opt);
	
	// Refines the mesh
	// param inx: new number of cells in x direction excluding ghosts
	// param iny: new number of cells in y direction excluding ghosts
	void vRefine(const int inx, const int iny);

	// Returns number of cells including ghosts in the x direction
    int iNCX() const {return inx_; }

	// Returns number of cells including ghosts in the y direction
    int iNCY() const {return iny_; }

	// Returns mesh size in x direction
	double dDx() const {return ddx_;}

	// Returns mesh size in y direction
    double dDy() const {return ddy_;}

	// Finds the cell center coordinates
	// param ii: column numer of the cell
	// param jj: row number of the cell
	// param adcoord: the coordinates to be returned
    void vCellCent(const int ii, const int jj, double* adcoord) const{
		assert( (ii < inx_) && (jj < iny_) );
		adcoord[eX] = (ii - 0.5) * ddx_;
		adcoord[eY] = (jj - 0.5) * ddy_;
	}

	// Finds the lower left and upper right vertex coordinates of a cell
	// param ii: column numer of the cell
	// param jj: row number of the cell
	// param adcoord: the coordinates to be returned the format is
	//          [xlowerleft] [ylowerleft] [xupperright] [yupperright]
	void vCellVerts(const int ii, const int jj, double* adcoord) const;
	
	// Writes a vtk file
	// param num: the values of solution
	// param err: the values of error (can be given as NULL)
	// param namevtk: name of the vtk file
	void  vWriteVtk(VEC *num, VEC *err, const char *namevtk) const;
	void  vWriteVtk(double *vars[], int nvar,
					const char * const * varnames, const char *namevtk) const;
		
    // returns number of vertical faces
	int iNFX() const { return inx_ + 1;}

	// returns number of horizontal faces
	int iNFY() const { return iny_ + 1;}

	// Finds the number of a cell, left to a face
	// param iface number of the face
	// param jj number of the row the face is in
	// param ii number of cell to be returned
	void vCLeft(const int iface, const int jj, int &ii) const{
		assert( (iface > 0) && (iface < iNFX()) );
		ii = iface-1;
	}

	// Finds the number of a cell, right to a face
	// param iface number of the face
	// param jj number of the row the face is in
	// param ii number of cell to be returned
	void vCRight(const int iface, const int jj, int &ii) const{
		assert( (iface >= 0) && (iface < iNFX()-1) );
		ii = iface;
	}

	// Finds the number of a cell, above a face
	// param jface number of the face
	// param ii number of the column the face is in
	// param jj number of cell to be returned
	void vCTop(const int ii, const int jface, int &jj) const{
		assert( (jface >= 0) && (jface < iNFY()-1) );
		jj = jface;
	}

	// Finds the number of a cell, below a face
	// param jface number of the face
	// param ii number of the column the face is in
	// param jj number of cell to be returned
	void vCBott(const int ii, const int jface, int &jj) const {
		assert( (jface > 0) && (jface < iNFY()) );
		jj = jface-1;
	}

	// returns the width of the box
	double dX(){return dxmax_;}
	
	// returns the height of the box
	double dY(){return dymax_;}
};

/****************************************************************************
 *                         Linear Algebra                                   *
 ****************************************************************************/
void SpewMatrix(pMAT Source, FILE* =stdout);

void SpewVector(pVEC Source, FILE* =stdout);

inline void SetVec(pVEC Target, const double val)
{
	Target[0] = val;
	Target[1] = val;
	Target[2] = val;
}

inline void Set3x3(pMAT Target, const double val)
{
	Target[0][0] = val;
	Target[0][1] = val;
	Target[0][2] = val;
	
	Target[1][0] = val;
	Target[1][1] = val;
	Target[1][2] = val;

	Target[2][0] = val;
	Target[2][1] = val;
	Target[2][2] = val;
}

inline void CopyVec(const pVEC Source, pVEC Target)
{
  Target[0] = Source[0];
  Target[1] = Source[1];
  Target[2] = Source[2];
}

inline void Copy3x3(pMAT Source, pMAT Target)
{
  Target[0][0] = Source[0][0];
  Target[0][1] = Source[0][1];
  Target[0][2] = Source[0][2];

  Target[1][0] = Source[1][0];
  Target[1][1] = Source[1][1];
  Target[1][2] = Source[1][2];

  Target[2][0] = Source[2][0];
  Target[2][1] = Source[2][1];
  Target[2][2] = Source[2][2];
}

inline void Mult2x2(double A[2][2], double B[2][2], double C[2][2])
{
  C[0][0] = A[0][0]*B[0][0] + A[0][1]*B[1][0] ;
  C[0][1] = A[0][0]*B[0][1] + A[0][1]*B[1][1] ; 

  C[1][0] = A[1][0]*B[0][0] + A[1][1]*B[1][0] ; 
  C[1][1] = A[1][0]*B[0][1] + A[1][1]*B[1][1] ; 
}


inline void Mult3x3(pMAT A, pMAT B, pMAT C)
{
  C[0][0] = A[0][0]*B[0][0] + A[0][1]*B[1][0] + A[0][2]*B[2][0];
  C[0][1] = A[0][0]*B[0][1] + A[0][1]*B[1][1] + A[0][2]*B[2][1]; 
  C[0][2] = A[0][0]*B[0][2] + A[0][1]*B[1][2] + A[0][2]*B[2][2]; 

  C[1][0] = A[1][0]*B[0][0] + A[1][1]*B[1][0] + A[1][2]*B[2][0]; 
  C[1][1] = A[1][0]*B[0][1] + A[1][1]*B[1][1] + A[1][2]*B[2][1]; 
  C[1][2] = A[1][0]*B[0][2] + A[1][1]*B[1][2] + A[1][2]*B[2][2]; 

  C[2][0] = A[2][0]*B[0][0] + A[2][1]*B[1][0] + A[2][2]*B[2][0]; 
  C[2][1] = A[2][0]*B[0][1] + A[2][1]*B[1][1] + A[2][2]*B[2][1]; 
  C[2][2] = A[2][0]*B[0][2] + A[2][1]*B[1][2] + A[2][2]*B[2][2]; 
}

inline void MultVec(pMAT A, const pVEC Vec, pVEC Result)
{
  Result[0] = A[0][0]*Vec[0] + A[0][1]*Vec[1] + A[0][2]*Vec[2]; 
  Result[1] = A[1][0]*Vec[0] + A[1][1]*Vec[1] + A[1][2]*Vec[2]; 
  Result[2] = A[2][0]*Vec[0] + A[2][1]*Vec[1] + A[2][2]*Vec[2]; 
}

inline void MultVec(double A[2][2], double Vec[2], double Result[2])
{
  Result[0] = A[0][0]*Vec[0] + A[0][1]*Vec[1]; 
  Result[1] = A[1][0]*Vec[0] + A[1][1]*Vec[1]; 
}

inline void Add3x3(pMAT A, pMAT B, const double Factor, pMAT C)
{
  C[0][0] = A[0][0] + Factor * B[0][0];
  C[0][1] = A[0][1] + Factor * B[0][1];
  C[0][2] = A[0][2] + Factor * B[0][2];

  C[1][0] = A[1][0] + Factor * B[1][0];
  C[1][1] = A[1][1] + Factor * B[1][1];
  C[1][2] = A[1][2] + Factor * B[1][2];

  C[2][0] = A[2][0] + Factor * B[2][0];
  C[2][1] = A[2][1] + Factor * B[2][1];
  C[2][2] = A[2][2] + Factor * B[2][2];
}

inline void AddVec(const pVEC A, const pVEC B, const double Factor, pVEC C)
{
  C[0] = A[0] + Factor * B[0];
  C[1] = A[1] + Factor * B[1];
  C[2] = A[2] + Factor * B[2];
}

inline void Invert3x3(pMAT Block, pMAT Inverse)
{
  double DetInv = 1. / (+ Block[0][0]*Block[1][1]*Block[2][2]
			+ Block[0][1]*Block[1][2]*Block[2][0]
			+ Block[0][2]*Block[1][0]*Block[2][1]
			- Block[0][2]*Block[1][1]*Block[2][0]
			- Block[0][1]*Block[1][0]*Block[2][2]
			- Block[0][0]*Block[1][2]*Block[2][1]);

  /* Expand by minors to compute the inverse */
  Inverse[0][0] = + DetInv * (Block[1][1]*Block[2][2] -
			      Block[2][1]*Block[1][2]); 
  Inverse[1][0] = - DetInv * (Block[1][0]*Block[2][2] -
			      Block[2][0]*Block[1][2]); 
  Inverse[2][0] = + DetInv * (Block[1][0]*Block[2][1] -
			      Block[2][0]*Block[1][1]); 
  Inverse[0][1] = - DetInv * (Block[0][1]*Block[2][2] -
			      Block[2][1]*Block[0][2]); 
  Inverse[1][1] = + DetInv * (Block[0][0]*Block[2][2] -
			      Block[2][0]*Block[0][2]); 
  Inverse[2][1] = - DetInv * (Block[0][0]*Block[2][1] -
			      Block[2][0]*Block[0][1]); 
  Inverse[0][2] = + DetInv * (Block[0][1]*Block[1][2] -
			      Block[1][1]*Block[0][2]); 
  Inverse[1][2] = - DetInv * (Block[0][0]*Block[1][2] -
			      Block[1][0]*Block[0][2]); 
  Inverse[2][2] = + DetInv * (Block[0][0]*Block[1][1] -
			      Block[1][0]*Block[0][1]); 
}

inline void Invert2x2(double Block[2][2], double Inverse[2][2])
{
  double DetInv = 1. /
	  (Block[0][0]*Block[1][1] - Block[1][0]*Block[0][1]);
	  
  /* Expand by minors to compute the inverse */
  Inverse[0][0] = + DetInv * Block[1][1];
  
  Inverse[1][1] = + DetInv * Block[0][0];
  
  Inverse[0][1] = - DetInv * Block[0][1];
  
  Inverse[1][0] = - DetInv * Block[1][0];

}

/*
  Code for solving block-tridiagonal matrix problems, using the Thomas
  algorithm.
   
  LHS array is sized as [*][3][3][3].  The last two indices identify
  the element within a block; the third index is the row and the fourth
  is the column of the Jacobian matrix.  The second index tells which
  block it is: 0 is below the main diagonal, 1 is on the main diagonal,
  2 is above the main diagonal.  The first index tells which block row
  you're looking at (the i or j index from the discretization). 
  
  RHS array is [*][3].  The second index tells which element of the
  solution vector, and the first is the block row.
*/

void SolveBlockTri(MAT LHS[][3], VEC RHS[], int iNRows);

/****************************************************************************
 *                           L norm stuff                                   *
 ****************************************************************************/

/* Class for finding L norms of data */
class Lnorm{
private:
	int N,      /* number of data used as input */
		qready; /* are the norms calculated? */
	double dl1, dl2, dlinf;

public:
	// call this before finding the norms
	void vInit(){
		N = qready = 0;
		dl1 = dl2 = dlinf = 0;
	}
	// use this to add data
	void vAddData(double dnew){
		N++;
		dnew = GABS(dnew);
		dl1 += dnew;
		dl2 += dnew*dnew;
		dlinf = GMAX(dlinf,dnew);
	}
	//use this to compute the norm of the added data
	void vCompute(){
		assert(N);
		qready = 1;
		dl1 /= N;
		dl2 /= N; dl2 = sqrt(dl2);
	}

	//returns L1 norm
	double dL1() const{assert(qready); return dl1;}
	//returns L2 norm
	double dL2() const{assert(qready); return dl2;}
	//returns Linf norm
	double dLInf() const{assert(qready); return dlinf;}
	//returns number of data used
	int iN() const{return N;}
};

/****************************************************************************
 *                             Options                                      *
 ****************************************************************************/
/* Options for the solver */
struct Options{

	//physics
	double dUtop;
	double dRe;
	double dBeta;
	double dA;
	
	//time advance
	double dTimeStep;
	double dOmega;
	double dEpsilon;

	//grid
	int N, M;
	double X,Y;

	/* manufactured solution */
	double dU0, dV0, dP0;

	/* file names */
	char strFName[300];
	int iMode;
};

// sets the default values for an option
void vOptionsDef(Options *opt);

// reads the options from the command line
void vOptionsInit(int argc, char *argv[], Options *opt);

/****************************************************************************
 *                             Phsyics                                      *
 ****************************************************************************/
/* FITS
 * This class stores the solution at the adjacent
 * cells of a face
*/
struct FITS{
	pVEC vsoll, vsolr;
	pVEC vsolt, vsolb;
	pVEC vsolc;
	
	double ddx, ddy;
	
	FITS(){
		vsoll = (pVEC) NULL;
		vsolr = (pVEC) NULL;
		vsolt = (pVEC) NULL;
		vsolb = (pVEC) NULL;
		vsolc =	(pVEC) NULL;
	}
};

/* Physics
 *
 * Defines the model that has to be solved.
 */
class Physics{
private:
	Physics();
public:

	// Boundary conditions
    double dUtop;
	
	// Dimensionless numbers
	double dRe, dReInv, dBeta, dA;

	//Constructor.	
	Physics(const Options *opt);

    // Jacobian of the flux in x direction
	// param q the solutions at the adjacent cells
	// returns mjl Jacobian with respect to the left solution
	// returns mjr Jacobian with respect to the right solution
	void vLocJacX(const FITS *q, pMAT mjl, pMAT mjr) const{

		double du = (q->vsoll[U] + q->vsolr[U]) ;
		double dv = (q->vsoll[V] + q->vsolr[V]) ;
		
		Set3x3(mjl, 0);

		// the common 
		mjl[0][1] = -1 / 2. / dBeta;

		mjl[1][0] = -0.5;
		mjl[1][1] = -du / 2.;

		mjl[2][1] = -dv / 4.;
		mjl[2][2] = -du / 4.;

		//copy
		Copy3x3(mjl, mjr);

		//special left
		mjl[1][1] += -1/q->ddx/dRe;
		mjl[2][2] += -1/q->ddx/dRe;
		mjr[1][1] -= -1/q->ddx/dRe;
		mjr[2][2] -= -1/q->ddx/dRe;

		
	}

	// Jacobian of the flux in y direction
	// param q the solutions at the adjacent cells
	// returns mjl Jacobian with respect to the bottom solution
	// returns mjl Jacobian with respect to the top solution
	void vLocJacY(const FITS *q, pMAT mjb, pMAT mjt) const{

		double du = (q->vsolt[U] + q->vsolb[U]) ;
		double dv = (q->vsolt[V] + q->vsolb[V]) ;
		
		Set3x3(mjb, 0);

		// the common 
		mjb[0][2] = -1 / 2. / dBeta;

		mjb[1][1] = -dv / 4.;
		mjb[1][2] = -du / 4.;

		mjb[2][0] = -0.5;
		mjb[2][2] = -dv / 2.;

		//copy
		Copy3x3(mjb, mjt);

		//special left
		mjb[1][1] += -1/q->ddy/dRe;
		mjb[2][2] += -1/q->ddy/dRe;
		mjt[1][1] -= -1/q->ddy/dRe;
		mjt[2][2] -= -1/q->ddy/dRe;

 	}

	// Flux in the x direction
	// param q solution at the adjacent cells
	// returns vflux the flux at that face
	void vFluxX(const FITS *q, pVEC vflux) const{

		double dp = (q->vsoll[P] + q->vsolr[P]) / 2. ;
		double du = (q->vsoll[U] + q->vsolr[U]) / 2. ;
		double dv = (q->vsoll[V] + q->vsolr[V]) / 2. ;
		
	    	
		vflux[P] = -du/dBeta ; 

		vflux[U] = -du*du - dp + (q->vsolr[U] - q->vsoll[U])/dRe/q->ddx;

		vflux[V] = -du*dv      + (q->vsolr[V] - q->vsoll[V])/dRe/q->ddx;

		//smoothing p
		vflux[P] += dA * q->ddy * (q->vsolr[P] - q->vsoll[P]);
			
	}


	// Flux in the Y direction
	// param q solution at the adjacent cells
	// returns vflux the flux at that face
	void vFluxY(const FITS *q, pVEC vflux) const{
		
		double dp = (q->vsolb[P] + q->vsolt[P]) / 2. ;
		double du = (q->vsolb[U] + q->vsolt[U]) / 2. ;
		double dv = (q->vsolb[V] + q->vsolt[V]) / 2. ;
			
		vflux[P] = -dv/dBeta;

		vflux[U] = -du*dv      + (q->vsolt[U] - q->vsolb[U])/dRe/q->ddy;

		vflux[V] = -dv*dv - dp + (q->vsolt[V] - q->vsolb[V])/dRe/q->ddy;

		//smoothing P
		vflux[P]  += dA * q->ddx * (q->vsolt[P] - q->vsolb[P]);


	}

	// Returns the value of a ghost cell
	// param ewall: the wall next to which the cell is located
	// param vsoladj: the solution at the adjacent real cell
	// returns vsolghost: value of the ghost cell
	void vGhostVal(const EnumWall ewall, const pVEC vsoladj, pVEC vsolghost) const{
		
		switch (ewall){

		// U,V=0, dP/dN=0 on right, left and down	
		case eRight:
		case eLeft:
		case eDown:
			vsolghost[U] = -vsoladj[U];
			vsolghost[V] = -vsoladj[V];
			vsolghost[P] = vsoladj[P];			
			break;

		// U = Utop, V=0, dp/dn=0 on top.	
		case eUp:
			vsolghost[U] = 2*dUtop-vsoladj[U];
			vsolghost[V] = -vsoladj[V];
			vsolghost[P] = vsoladj[P];	
			break;
			
		default:
			assert(0);
		}
	}

	// Returns the block that should sit in the jacobian in the ghost cell
	//         row and the adjacent real cell column.
	// param ewall the wall at which the cell is located
	// returns mjacghost the block to be returned
	void vGhostJac(const EnumWall ewall, pMAT mjacghost) const{
		Set3x3(mjacghost, 0);
		mjacghost[P][P] = -1;
		mjacghost[U][U] = 1;
		mjacghost[V][V] = 1;
	}
	
	void vSource (const FITS *q, pVEC vsource)const{
		assert(0);
	}
};

/****************************************************************************
 *                             Assemblers                                   *
 ****************************************************************************/
// Assembles the flux by iterating over all faces.
// param ph the physics
// param grid
// param avsol the value of the solution
// return avflux the value of flux integral
void vFluxAssem(const Physics *ph, const Grid *gr,
				VEC avsol[], VEC avflux[]);

// Sets the value of all the ghost cells
// param ph the physcis
// param gr the grid
// param avsol the value of solutions whose ghost values should be set
void vSetGhosts(const Physics *ph, const Grid *gr,  VEC avsol[]);

// Assembles the matrix (I + dt [Dx]) by iterating over all faces in
//           the corresponding row.
// param jj the row number for which the jacobian has to found
// param ph the physics
// param gr the grid
// param ddt time step
// param avsol the solution
// returns a2mjac the matrix (I + dt [Dx])
void vJacAssemX(const int jj, const Physics *ph, const Grid *gr,
				const double ddt, VEC avsol[], MAT a2mjac[][3]);

// Assembles the matrix (I + dt [Dy]) by iterating over all faces in
//           the corresponding column.
// param ii the column number for which the jacobian has to found
// param ph the physics
// param gr the grid
// param ddt time step
// param avsol the solution
// returns a2mjac the matrix (I + dt [Dy])
void vJacAssemY(const int ii, const Physics *ph, const Grid *gr,
				const double ddt, VEC avsol[], MAT a2mjac[][3]);


/****************************************************************************
 *                                  TimeAdvance                             *
 ****************************************************************************/
/* Memory
 *
 * This struct stores the solution and the additional data that
 * the time advance mathod might require, e.g. the flux integral
 * and an array to store the Jacobians.
 */

struct Memory{
	
	VEC *avsol, /* solution */
		*avflux,   /* flux integral */
		*avsolcol; /* an array to extract a column of the solution */
		
	MAT	(*a2mjac)[3] ; /* JACOBIAN */

	double ddt, /* time step */
		domega; /* over relaxation parameter */

	Memory() {vNullify();}

	// Set all the array to NULL
	void vNullify(){
		ddt = domega = 0;
		avsol = avflux = avsolcol = (VEC*)NULL;
	    a2mjac = (MAT(*)[3]) NULL;
	}

	// Frees the memory
	// This function always has to be called before the end of program.
	void vDestroy(){
		if(avsol) delete[] avsol;
		if(avflux) delete[] avflux;
		if(avsolcol) delete[] avsolcol;

		if (a2mjac) delete[] a2mjac;
	}
};

// Sets up a memory
void vInitMemImplicit(const Grid* , Memory *, Options *);

// Advance the time
// param gr the grid
// param ph the Physics
// param mem the memory
// param lnorm the lnorms of the change in solution
//       lnorm[P], lnorm[U], lnorm[V] and lnorm[3] -> all unknowns.
void vTimeAdvImplicit(const Grid *gr, const Physics *ph,
					  Memory * mem, Lnorm lnorm[4]);

/****************************************************************************
 *                             DAT FORMAT I/O                               *
 ****************************************************************************/
// Write a dat file
void vDatRead (Options *opt, VEC **avsol);

// Read a dat file
void vDatWrite(Physics *ph, Grid *gr, const char *fname, VEC avsol[]);

// this is a change on saeed's station.
	
#endif /* NS_HXX */
