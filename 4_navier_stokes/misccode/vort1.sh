#!/bin/bash 

## -------------------------------------------------------------------
## Basic solution
## -------------------------------------------------------------------

EXEC=./ns_test
COPT=-m32A0.6O1.3U1R200X1Y2E13



##=============================================================================
ERR=E13

rm -f sol/vort1*
echo "Running VORT1 160x80"
$EXEC  ${COPT}M160N80T0.2 sol/vort1_80 1> sol/vort1_80.norm 2>> sol/vort1.log

echo "Running VORT1 - 240x120"
$EXEC  ${COPT}M240N120T0.15 sol/vort1_120 1> sol/vort1_120.norm 2>> sol/vort1.log

echo "Running VORT1 - 360x180"
$EXEC  ${COPT}M360N180T0.1 sol/vort1_180 1> sol/vort1_180.norm 2>> sol/vort1.log


rm -f sol/vort1.vort sol/vort1_str.vort

./pp_vort sol/vort1_80 10607 4 1>> sol/vort1.vort 2> x;  
./pp_vort sol/vort1_80 5001 5 1>> sol/vort1.vort 2> x; 
./pp_vort sol/vort1_80 317 5 1>> sol/vort1.vort 2> x; 
./pp_vort sol/vort1_80 242 5 1>> sol/vort1.vort 2> x; 
./pp_vort2 sol/vort1_80 1>> sol/vort1_str.vort 2> x; 
rm x

echo "" >> sol/vort1.vort; echo "" >> sol/vort1_str.vort; 
echo "" >> sol/vort1.vort; echo "" >> sol/vort1_str.vort;  
echo "" >> sol/vort1.vort; echo "" >> sol/vort1_str.vort; 


./pp_vort sol/vort1_120 23952 5 1>> sol/vort1.vort 2> x;  
./pp_vort sol/vort1_120 11221 5 1>> sol/vort1.vort 2> x; 
./pp_vort sol/vort1_120 595 5 1>> sol/vort1.vort 2> x; 
./pp_vort sol/vort1_120 484 5 1>> sol/vort1.vort 2> x; 
./pp_vort2 sol/vort1_120 1>> sol/vort1_str.vort 2> x;
rm x

echo "" >> sol/vort1.vort; echo "" >> sol/vort1_str.vort; 
echo "" >> sol/vort1.vort; echo "" >> sol/vort1_str.vort;  
echo "" >> sol/vort1.vort; echo "" >> sol/vort1_str.vort; 


./pp_vort sol/vort1_180 53747 4 1>> sol/vort1.vort 2> x;  
./pp_vort sol/vort1_180 25291 4 1>> sol/vort1.vort 2> x; 
./pp_vort sol/vort1_180 1433 5 1>> sol/vort1.vort 2> x; 
./pp_vort sol/vort1_180 1086 5 1>> sol/vort1.vort 2> x; 
./pp_vort2 sol/vort1_180 1>> sol/vort1_str.vort 2> x;
rm x

