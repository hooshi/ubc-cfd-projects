#!/bin/bash 

## -------------------------------------------------------------------
## Basic solution
## -------------------------------------------------------------------

#$EXEC  ${COPT}M240Y2 sol/vort2_200_120  2>> sol/vort2.log | tee sol/vort2.log ;
#$EXEC  ${COPT}M360Y3 sol/vort2_300_120  2>> sol/vort2.log | tee sol/vort2.log ;
#$EXEC  ${COPT}M300Y2.5 sol/vort2_250_120  2>> sol/vort2.log | tee sol/vort2.log ;
#$EXEC  ${COPT}M324Y2.7 sol/vort2_270_120  2>> sol/vort2.log | tee sol/vort2.log ;
#$EXEC  ${COPT}M330Y2.75 sol/vort2_275_120  2>> sol/vort2.log | tee sol/vort2.log ;
#$EXEC  ${COPT}M342Y2.85 sol/vort2_285_120  2>> sol/vort2.log | tee sol/vort2.log ;
#$EXEC  ${COPT}M336Y2.80 sol/vort2_280_120  2>> sol/vort2.log | tee sol/vort2.log ;
#$EXEC  ${COPT}M334Y2.78 sol/vort2_278_120  2>> sol/vort2.log | tee sol/vort2.log ;
#$EXEC  ${COPT}M335Y2.79 sol/vort2_279_120  2>> sol/vort2.log | tee sol/vort2.log ;


#COPT=-m32A0.6O1.3U1R200X1E13T0.10N180
#$EXEC  ${COPT}M504Y2.80 sol/vort2_280_180  2>> sol/vort2.log | tee sol/vort2.log ;

rm -f sol/vort2*
EXEC=./ns_test

## 120 cell per unit length
COPT=-m32A0.6O1.3U1R200X1E13T0.15N120
$EXEC  ${COPT}M334Y2.78 sol/vort2_278_120  2>> sol/vort2.log | tee sol/vort2.log ;
$EXEC  ${COPT}M335Y2.79 sol/vort2_279_120  2>> sol/vort2.log | tee sol/vort2.log ;

## 180 cell per unit length
COPT=-m32A0.6O1.3U1R200X1E13T0.10N180
$EXEC  ${COPT}M501Y2.78 sol/vort2_278_180  2>> sol/vort2.log | tee sol/vort2.log ;
$EXEC  ${COPT}M502Y2.79 sol/vort2_279_180  2>> sol/vort2.log | tee sol/vort2.log ;

./pp_vort2 sol/vort2_278_120 1> x 2> x ;
./pp_vort2 sol/vort2_279_120 1> x 2> x ;
./pp_vort2 sol/vort2_278_180 1> x 2> x ;
./pp_vort2 sol/vort2_279_180 1> x 2> x ;
rm x;
