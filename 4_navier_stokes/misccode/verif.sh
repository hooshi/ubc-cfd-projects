#!/bin/bash 

## -------------------------------------------------------------------
## Stability ( convergence to zero )
## -------------------------------------------------------------------

EXEC=./ns_test
DT=
E=

IOM="0 1 2 3 4 5"
N=(20 40 80 160 320 640)

rm -f sol/verif.norm

for ii in $IOM
do
	echo "Running flux verif N=${N[ii]}"
	$EXEC -m11N${N[ii]}M${N[ii]}} 1>> sol/verif.norm 2>> sol/verif.log
done

echo "Running Jacobian Test"
$EXEC -m12 1>> sol/verif.norm 2>> sol/verif.log
