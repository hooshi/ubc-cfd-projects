## ----------------------------------------------------------------------------
##
## vort1.m 
## 
##---------------------------------------------------------------------------

## load vortex centres
sol = load("-ascii", "../sol/vort1.vort");
X = [ ...
	  sol(1,2) sol(5,2) sol(9,2) ; ...
	  sol(2,2) sol(6,2) sol(10,2) ; ...
	  sol(3,2) sol(7,2) sol(11,2) ; ...
	  sol(4,2) sol(8,2) sol(12,2) ; ...	  
	]

Y = [ ...
	  sol(1,3) sol(5,3) sol(9,3) ; ...
	  sol(2,3) sol(6,3) sol(10,3) ; ...
	  sol(3,3) sol(7,3) sol(11,3) ; ...
	  sol(4,3) sol(8,3) sol(12,3) ; ...	  
	]

sol = load("-ascii", "../sol/vort1_str.vort");
S = abs([ ...
	  sol(1,4) sol(5,4) sol(9,4) ;...
	  sol(2,4) sol(6,4) sol(10,4) ; ...
	  sol(3,4) sol(7,4) sol(11,4) ;...
	  sol(4,4) sol(8,4) sol(12,4) ;...
	])
clear sol;

for i=1:4
  px = log( abs(X(i,1) - X(i,2)) / abs(X(i,2) - X(i,3)) ) / log(1.5);
  py = log( abs(Y(i,1) - Y(i,2)) / abs(Y(i,2) - Y(i,3)) ) / log(1.5);
  ps = log( abs(S(i,1) - S(i,2)) / abs(S(i,2) - S(i,3)) ) / log(1.5);

  Xe = (1.5 ^ px * X(i,3) - X(i,2) ) / (1.5 ^ px - 1);
  Ye = (1.5 ^ py * Y(i,3) - Y(i,2) ) / (1.5 ^ py - 1);
  Se = (1.5 ^ ps * S(i,3) - S(i,2) ) / (1.5 ^ ps - 1);


  Exe = abs(Xe - X(i,3))/(Xe) * 100;
  Eye = abs(Ye - Y(i,3))/(Ye) * 100;
  Ese = abs(Se - S(i,3))/(Se) * 100;


  GCIx = 1.25 * Exe / (1.5 ^ px - 1);
  GCIy = 1.25 * Eye / (1.5 ^ py - 1);
  GCIs = 1.25 * Ese / (1.5 ^ ps - 1);


  printf("p for vort %d: %f %f %f\n X: %f Y: %f S:%e\n", ...
		 i, px, py, ps,  Xe, Ye, Se);
  printf("Ex:%f Ey:%f Es:%f \n", Exe, Eye, Ese);
  printf("GCIx: %f GCIy:%f GCIs:%f\n\n", GCIx, GCIy, GCIs);
endfor
