## ----------------------------------------------------------------------------
##
## A.m 
## 
## Effect of A and B.
## ---------------------------------------------------------------------------
c = { 'b', 'r', 'g' };

A = [0 0.1 0.6 1.0];
NA = [2657 1148 1147 1139]; 

B = 0.1:0.1:2;
NB = [ 	1039 977 958 950 945 943 946 1001 1079 1147 1233 1268 ...
			 1321 1382 1431 1478 1510 1533 1595 1635 ];

clf, hold on;
plot(A, NA, "k-s", "linewidth", 2);

##axis label and range
hx=xlabel("A");
hy=ylabel("Number of iterations");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
axis([0 1]);# 1e-5 1]);
title("Effect of A in convergence");

##save the plot
print("../plot/A-iter.eps","-deps","-FArial", "-color");


clf, hold on;
plot(B, NB, "k-s", "linewidth", 2);

##axis label and range
hx=xlabel("B");
hy=ylabel("Number of iterations");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
axis([0 2]);# 1e-5 1]);
title("Effect of B in convergence");

##save the plot
print("../plot/B-iter.eps","-deps","-FArial", "-color");
