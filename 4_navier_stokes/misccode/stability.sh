#!/bin/bash 

## -------------------------------------------------------------------
## Stability ( convergence to zero )
## -------------------------------------------------------------------

EXEC=./ns_test
DT=T0.05
E=E7

IOM="0 1 2 3"
OMEGA=(1.0 1.2 1.4 1.6)
NOM=(10 12 14 16)

rm -f sol/stabil*

for im in $IOM
do
	echo "Running Stability 20x20 A=0 Omega=${OMEGA[im]}"
	$EXEC -m31${E}O${OMEGA[im]}U0${DT} sol/stabil_o${NOM[im]} \
		1> sol/stabil_o${NOM[im]}.norm 2>> sol/stabil.log
done
