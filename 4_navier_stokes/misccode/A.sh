#!/bin/bash

rm -f sol/A.log sol/A.norm

./ns_test -m32M40N40O1T0.05E13U1A0.0 sol/A0 2>> sol/A.log 1>> sol/A.norm 

./ns_test -m32M40N40O1T0.05E13U1A0.1 sol/A1 2>> sol/A.log 1>> sol/A.norm

./ns_test -m32M40N40O1T0.05E13U1A0.6 sol/A6 2>> sol/A.log 1>> sol/A.norm

./ns_test -m32M40N40O1T0.05E13U1A1.0 sol/A10 2>> sol/A.log 1>> sol/A.norm

rm -f sol/B.log sol/B.norm
 
./ns_test -m32M40N40O1T0.05E13U1A0.6B0.1 sol/B01 2>> sol/B.log 1>> sol/B.norm 

./ns_test -m32M40N40O1T0.05E13U1A0.6B0.2 sol/B05 2>> sol/B.log 1>> sol/B.norm

./ns_test -m32M40N40O1T0.05E13U1A0.6B0.3 sol/B05 2>> sol/B.log 1>> sol/B.norm

./ns_test -m32M40N40O1T0.05E13U1A0.6B0.4 sol/B05 2>> sol/B.log 1>> sol/B.norm

./ns_test -m32M40N40O1T0.05E13U1A0.6B0.5 sol/B05 2>> sol/B.log 1>> sol/B.norm

./ns_test -m32M40N40O1T0.05E13U1A0.6B0.6 sol/B05 2>> sol/B.log 1>> sol/B.norm

./ns_test -m32M40N40O1T0.05E13U1A0.6B0.7 sol/B05 2>> sol/B.log 1>> sol/B.norm

./ns_test -m32M40N40O1T0.05E13U1A0.6B0.8 sol/B05 2>> sol/B.log 1>> sol/B.norm
./ns_test -m32M40N40O1T0.05E13U1A0.6B0.9 sol/B05 2>> sol/B.log 1>> sol/B.norm
./ns_test -m32M40N40O1T0.05E13U1A0.6B01 sol/B05 2>> sol/B.log 1>> sol/B.norm
./ns_test -m32M40N40O1T0.05E13U1A0.6B01.1 sol/B05 2>> sol/B.log 1>> sol/B.norm
./ns_test -m32M40N40O1T0.05E13U1A0.6B01.2 sol/B05 2>> sol/B.log 1>> sol/B.norm
./ns_test -m32M40N40O1T0.05E13U1A0.6B01.3 sol/B05 2>> sol/B.log 1>> sol/B.norm
./ns_test -m32M40N40O1T0.05E13U1A0.6B01.4 sol/B05 2>> sol/B.log 1>> sol/B.norm
./ns_test -m32M40N40O1T0.05E13U1A0.6B01.5 sol/B05 2>> sol/B.log 1>> sol/B.norm
./ns_test -m32M40N40O1T0.05E13U1A0.6B01.6 sol/B05 2>> sol/B.log 1>> sol/B.norm
./ns_test -m32M40N40O1T0.05E13U1A0.6B01.7 sol/B05 2>> sol/B.log 1>> sol/B.norm
./ns_test -m32M40N40O1T0.05E13U1A0.6B01.8 sol/B05 2>> sol/B.log 1>> sol/B.norm
./ns_test -m32M40N40O1T0.05E13U1A0.6B01.9 sol/B05 2>> sol/B.log 1>> sol/B.norm
./ns_test -m32M40N40O1T0.05E13U1A0.6B02.0 sol/B05 2>> sol/B.log 1>> sol/B.norm
