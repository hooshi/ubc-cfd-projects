## ----------------------------------------------------------------------------
##
## verif.m 
## 
## Residual Verification.
## ---------------------------------------------------------------------------
c = { 'b', 'r', 'g' };

# load the data
sol = load("-ascii", "../sol/verif.norm");
N = sol(:,1);
l1 = sol(:,2);
l2 = sol(:,3);
l3 = sol(:,4);
lp = sol(:,5);
lu = sol(:,6);
lv = sol(:,7);
clear sol;

## slopes
p1 = polyfit (log10(N),  log10(l1),1);
p2 = polyfit (log10(N),  log10(l2),1);
p3 = polyfit (log10(N), log10(l3),1);
pp = polyfit (log10(N), log10(lp),1);
pu = polyfit (log10(N), log10(lu),1);
pv = polyfit (log10(N), log10(lv),1);

##-------------------------------------------------
## ALL
##------------------------------------------------
clf, hold on;
loglog(N, l1, ["k-o;L1, slope =" sprintf("%.2f", -p1(1)) ";"], "linewidth", 2);
loglog(N, l2, ["k--^;L2, slope =" sprintf("%.2f", -p2(1)) ";"], "linewidth", 2);
loglog(N, l3, ["k-.s;Linf, slope =" sprintf("%.2f", -p3(1)) ";"], "linewidth", 2);


##axis label and range
hx=xlabel("N");
hy=ylabel("Flux Error Norms");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);

##axis label
xt = get(gca,"XTick");
set(gca,"XTickLabel", sprintf("%.0f|",xt) );
axis([10 1000 1e-5 1]);
title("Error in all flux components");

##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "northeast", "linewidth",2);

##save the plot
print("../plot/verif-flux1.eps","-deps","-FArial", "-color");


##-------------------------------------------------
## ALL
##------------------------------------------------
clf, hold on;
loglog(N, lp, ["k-;P, slope =" sprintf("%.2f", -pp(1)) ";"], "linewidth", 2);
loglog(N, lu, ["k--;U, slope =" sprintf("%.2f", -pu(1)) ";"], "linewidth", 2);
loglog(N, lv, ["ks;V, slope =" sprintf("%.2f", -pv(1)) ";"], "linewidth", 2);


##axis label and range
hx=xlabel("N");
hy=ylabel("Flux Error L2 Norm");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);

##axis label
xt = get(gca,"XTick");
set(gca,"XTickLabel", sprintf("%.0f|",xt) );
axis([10 1000]);
title("Error in seperate flux components");


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "northeast", "linewidth",2);

##save the plot
print("../plot/verif-flux2.eps","-deps","-FArial", "-color");
