## ----------------------------------------------------------------------------
##
## basic2.m 
## 
## Stability case.
## ---------------------------------------------------------------------------
c = { 'g', 'm', 'r', 'b', 'k'};

#-----------------------------------------------------------------------------
# U along symmetry line
#-----------------------------------------------------------------------------
clf, hold on;

#-----------------------------------------------------------------------------
# 10x10
#-----------------------------------------------------------------------------
clear Y U;

# load the data
sol = load("-ascii", "../sol/basic_10.vline");
j = 1;
for i=1:length(sol(:,1))
  if ( rem(i,1) == 0 )
	Y(j) = sol(i,1);
	U(j) = sol(i,2);
	j=j+1;
  endif 
endfor
clear sol;

## plot the stuff
plot(U, Y , "o;10x10;","linewidth", 2, "color", c{1});

#-----------------------------------------------------------------------------
# 20x20
#-----------------------------------------------------------------------------
clear Y U;

# load the data
sol = load("-ascii", "../sol/basic_20.vline");
j = 1;
for i=1:length(sol(:,1))
  if ( rem(i,1)==0 )
	Y(j) = sol(i,1);
	U(j) = sol(i,2);
	j=j+1;
  endif 
endfor
clear sol;

## plot the stuff
plot(U, Y , "s;20x20;","linewidth", 2, "color", c{2});

#-----------------------------------------------------------------------------
# 40x40
#-----------------------------------------------------------------------------
clear Y U;

# load the data
sol = load("-ascii", "../sol/basic_40.vline");
j = 1;
for i=1:length(sol(:,1))
  if ( rem(i,1)==0 )
	Y(j) = sol(i,1);
	U(j) = sol(i,2);
	j=j+1;
  endif 
endfor
clear sol;

## plot the stuff
plot(U, Y , "^;40x40;","linewidth", 2, "color", c{3});

#-----------------------------------------------------------------------------
# 80x80
#-----------------------------------------------------------------------------
clear Y U;

# load the data
sol = load("-ascii", "../sol/basic_80.vline");
j = 1;
for i=1:length(sol(:,1))
  if ( rem(i,1)==0 )
	Y(j) = sol(i,1);
	U(j) = sol(i,2);
	j=j+1;
  endif 
endfor
clear sol;

## plot the stuff
plot(U, Y , "--;80x80;","linewidth", 2, "color", c{4});

#-----------------------------------------------------------------------------
# 160x160
#-----------------------------------------------------------------------------
## clear Y U;

## # load the data
## sol = load("-ascii", "../sol/basic_160.vline");
## j = 1;
## for i=1:length(sol(:,1))
##   if ( rem(i,1)==0 )
## 	Y(j) = sol(i,1);
## 	U(j) = sol(i,2);
## 	j=j+1;
##   endif 
## endfor
## clear sol;

## ## plot the stuff
## plot(U, Y , ":;160x160;","linewidth", 3, "color", c{5});




#-----------------------------------------------------------------------------
# plotting
#-----------------------------------------------------------------------------

##axis label and range
hx=xlabel("Velocity U");
hy=ylabel("Y");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
title("Velocity U along vertical line of symmetry");
grid on;

##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southeast", "linewidth",2);


##save the plot
print("../plot/basic-conv-vline.eps","-deps","-FArial", "-color");


#-----------------------------------------------------------------------------
# Error Norms
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# Using the whole solution
#-----------------------------------------------------------------------------
clear N L1 L2 L3;

# load the data
sol = load("-ascii", "../sol/basic_conv.norm");
N = sol(:,1);
L1 = sol(:,2);
L2 = sol(:,3);
L3 = sol(:,4); 
clear sol;

## plot the stuff
clf, hold on;
loglog(N, L1 , "-o;L1;","linewidth", 2, "color", "b");
loglog(N, L2 , "-s;L2;","linewidth", 2, "color", "r");
loglog(N, L3 , "-^;Linf;","linewidth", 2, "color", "g");

##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "northeast", "linewidth",2);

xt = get(gca,"XTick");
set(gca,"XTickLabel", sprintf("%.0f|",xt) );

##axis label and range
hx=xlabel("N");
hy=ylabel("L2 norm");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
title("L2 norm of change in u(x=0.5,y) with respect to coarser mesh");

##save the plot
print("../plot/basic-conv-norm.eps","-deps","-FArial", "-color");

printf("Order of convergence:\n last: %f %f %f \n  nonlast:%f %f %f \n", 
	   log2(L1(end-1)/L1(end)),  log2(L2(end-1)/L2(end)), ...
	   log2(L3(end-1)/L3(end)),  log2(L1(end-2)/L1(end-1)), ...
	   log2(L2(end-2)/L2(end-1)),  log2(L3(end-2)/L3(end-1))  );
