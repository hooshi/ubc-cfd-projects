#!/bin/bash 

## -------------------------------------------------------------------
## Basic solution
## -------------------------------------------------------------------

EXEC=./ns_test
COPT=-m32A0.6O1.3T0.27
rm -f sol/basic*


##=============================================================================
ERR=E6

echo "Running Basic 20x20 - symmetry"
$EXEC  ${COPT}${ERR}U1u1p1v1 sol/basic_20_symright 1> sol/basic_20_symright.norm 2>> sol/basic.log

echo "Running Basic 20x20 - symmetry"
$EXEC  ${COPT}${ERR}U-1u1p1v1 sol/basic_20_symleft 1> sol/basic_20_symleft.norm 2>> sol/basic.log

echo "Finding the difference in U for 20x20"
./pp_sym sol/basic_20_symleft sol/basic_20_symright 2>> sol/basic.log

##=============================================================================
ERR=E13

echo "Running Basic 10x10"
$EXEC  ${COPT}${ERR}U1M10N10 sol/basic_10 1> sol/basic_10.norm 2>> sol/basic.log

echo "Running Basic 20x20"
$EXEC  ${COPT}${ERR}U1 sol/basic_20 1> sol/basic_20.norm 2>> sol/basic.log

echo "Running Basic 40x40"
$EXEC  ${COPT}${ERR}U1M40N40 sol/basic_40 1> sol/basic_40.norm 2>> sol/basic.log

COPT=-m32A0.6O1.1T0.07
echo "Running Basic 80x80"
$EXEC  ${COPT}${ERR}U1M80N80 sol/basic_80 1> sol/basic_80.norm 2>> sol/basic.log

# 160x160 is converging fine but the change in solution is terrible (0.4th order)!
# COPT=-m32A0.6O1.1T0.07
# echo "Running Basic 160x160"
# $EXEC  ${COPT}${ERR}U1M160N160 sol/basic_160 1> sol/basic_160.norm 2>> sol/basic.log

./pp_conv sol/basic_10 sol/basic_20 2>> sol/basic.log 1>> sol/basic_conv.norm
./pp_conv sol/basic_20 sol/basic_40 2>> sol/basic.log 1>> sol/basic_conv.norm
./pp_conv sol/basic_40 sol/basic_80 2>> sol/basic.log 1>> sol/basic_conv.norm
# ./pp_conv sol/basic_80 sol/basic_160 2>> sol/basic.log 1>> sol/basic_conv.norm
