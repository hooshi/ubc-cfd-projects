## ----------------------------------------------------------------------------
##
## stability.m 
## 
## Stability case.
## ---------------------------------------------------------------------------
c = { 'b', 'r', 'g', 'k'};
NITER = int32(200);
# load the data

## omega = 1
sol10 = load("-ascii", "../sol/stabil_o10.norm");
N10 = sol10(:,1);
L10c = [ transpose(sol10(:,5)) ; transpose(sol10(:,6)) ; ...
	     transpose(sol10(:,7)) ; transpose(sol10(:,8))];
clear sol10;

## omega = 1.2
sol12 = load("-ascii", "../sol/stabil_o12.norm");
N12 = sol12(:,1);
L12c = [ transpose(sol12(:,5)) ; transpose(sol12(:,6)) ; ...
		 transpose(sol12(:,7)) ; transpose(sol12(:,8))];
clear sol12;

## omega = 1.4
sol14 = load("-ascii", "../sol/stabil_o14.norm");
N14 = sol14(:,1);
L14c = [ transpose(sol14(:,5)) ; transpose(sol14(:,6)) ; ...
		 transpose(sol14(:,7)) ; transpose(sol14(:,8)) ];
clear sol14;

## omega = 1.6
## sol16 = load("-ascii", "../sol/stabil_o16.norm");
## j = 1;
## for i=1:sol16(end,1)
## if ( rem(i,20) == 1 )
##   N16(j) = sol16(i,1);
##   L16c(:,j) = [ sol16(i,5) ; sol16(i,6) ; sol16(i,7) ; sol16(i,8) ];
##   j=j+1;
## endif 
## endfor
## clear sol16;

## ---------------------------------------------------------------------------
## Total
## ---------------------------------------------------------------------------
clf, hold on;
semilogy(N10, L10c(4,:) , ";omega=1.0;", "linewidth", 2, "color", c{1});
semilogy(N12, L12c(4,:) , ";omega=1.2;", "linewidth", 2, "color", c{2});
semilogy(N14, L14c(4,:) , ";omega=1.4;", "linewidth", 2, "color", c{3});
## semilogy(N16, L16c(4,:) , "^;omega=1.6;", "linewidth", 2, ...
## 		 "color", c{4}, "markersize", 6);

##axis label and range
hx=xlabel("N");
hy=ylabel("L2 norm of change per iteration");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
title("All Solution Variables");
axis([0 200 1e-6 1]);



##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "northeast", "linewidth",2);

##save the plot
print("../plot/stabil-tot.eps","-deps","-FArial", "-color");

## ---------------------------------------------------------------------------
## Pressure
## ---------------------------------------------------------------------------
clf, hold on;
semilogy(N10, L10c(1,:) , ";omega=1.0;", "linewidth", 2, "color", c{1});
semilogy(N12, L12c(1,:) , ";omega=1.2;", "linewidth", 1, "color", c{2});
semilogy(N14, L14c(1,:) , ";omega=1.4;", "linewidth", 1, "color", c{3});
## semilogy(N16, L16c(1,:) , "^;omega=1.6;", "linewidth", 2, ...
## 		 "color", c{4}, "markersize", 6);

##axis label and range
hx=xlabel("N");
hy=ylabel("L2 norm of change per iteration");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
title("Pressure");
axis([0 200 1e-6 1]);



##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "northeast", "linewidth",2);

##save the plot
print("../plot/stabil-p.eps","-deps","-FArial", "-color");

## ---------------------------------------------------------------------------
## U
## ---------------------------------------------------------------------------
clf, hold on;
semilogy(N10, L10c(2,:) , ";omega=1.0;", "linewidth", 2, "color", c{1});
semilogy(N12, L12c(2,:) , ";omega=1.2;", "linewidth", 1, "color", c{2});
semilogy(N14, L14c(2,:) , ";omega=1.4;", "linewidth", 1, "color", c{3});
## semilogy(N16, L16c(2,:) , "^;omega=1.6;", "linewidth", 2, ...
## 		 "color", c{4}, "markersize", 6);

##axis label and range
hx=xlabel("N");
hy=ylabel("L2 norm of change per iteration");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
title("Velocity U");
axis([0 200 1e-6 1]);



##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "northeast", "linewidth",2);

##save the plot
print("../plot/stabil-u.eps","-deps","-FArial", "-color");

## ---------------------------------------------------------------------------
## V
## ---------------------------------------------------------------------------
clf, hold on;
semilogy(N10, L10c(3,:) , ";omega=1.0;", "linewidth", 2, "color", c{1});
semilogy(N12, L12c(3,:) , ";omega=1.2;", "linewidth", 1, "color", c{2});
semilogy(N14, L14c(3,:) , ";omega=1.4;", "linewidth", 1, "color", c{3});
## semilogy(N16, L16c(3,:) , "^;omega=1.6;", "linewidth", 2, ...
## 		 "color", c{4}, "markersize", 6);

##axis label and range
hx=xlabel("N");
hy=ylabel("L2 norm of change per iteration");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
title("Velocity V");
axis([0 200 1e-6 1]);



##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "northeast", "linewidth",2);

##save the plot
print("../plot/stabil-v.eps","-deps","-FArial", "-color");
