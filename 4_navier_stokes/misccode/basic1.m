## ----------------------------------------------------------------------------
##
## basic1.m 
## 
## Stability case.
## ---------------------------------------------------------------------------
c = { 'b', 'r', 'g', 'm'};

#-----------------------------------------------------------------------------
# convergence history
#-----------------------------------------------------------------------------

# load the data
sol = load("-ascii", "../sol/basic_20.norm");
j = 1;
for i=1:sol(end,1)
  if ( rem(i,1) == 0 )
	N(j) = sol(i,1);
	Lc(:,j) = [ sol(i,2) ; sol(i,3) ; sol(i,4) ; sol(i,5) ];
	j=j+1;
  endif 
endfor
clear sol;

## plot the stuff
##figure;
clf, hold on;
semilogy(N, Lc(1,:) , ";Pressure;", "linewidth", 2, "color", c{1});
semilogy(N, Lc(2,:) , ";U;"       , "linewidth", 2, "color", c{2});
semilogy(N, Lc(3,:) , ";V;"       , "linewidth", 2, "color", c{3});
semilogy(N, Lc(4,:) , ";Total;"   , "linewidth", 2, "color", c{4});
axis([0 80 1e-6 1e-1]);

##axis label and range
hx=xlabel("N");
hy=ylabel("Norm");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
title("L2 norm of change per iteration");


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "northeast", "linewidth",2);

##save the plot
print("../plot/basic-20-conv.eps","-deps","-FArial", "-color");

clear N  Lc;
#-----------------------------------------------------------------------------
# U along symmetry line
#-----------------------------------------------------------------------------
clear Y U;

# load the data
sol = load("-ascii", "../sol/basic_20.vline");
j = 1;
for i=1:length(sol(:,1))
  if ( rem(i,1) == 0 )
	Y(j) = sol(i,1);
	U(j) = sol(i,2);
	j=j+1;
  endif 
endfor
clear sol;

## plot the stuff
clf, hold on;
plot(U, Y , "s-","linewidth", 2, "color", c{1});
grid on; 

##axis label and range
hx=xlabel("Velocity U");
hy=ylabel("Y");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
title("Velocity U along vertical line of symmetry");


##save the plot
print("../plot/basic-20-vline.eps","-deps","-FArial", "-color");
