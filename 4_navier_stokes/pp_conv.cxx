/*
 * pp_conv.cxx (Post Processor Convergence)
 *
 * an executable that reads the solution for a coarse mesh
 * and a fine mesh and returns the norm of the difference in the
 * solution using interpolation
 *
 * to run it use: pp_conv coarse_solution_address fine_solution_address
 *
 */

#include <cstdio>
#include "ns.hxx"

#define OFFSET 1
int main(int argc, char *argv[]){
	assert(argc == 3);

	Options optC, optF;
	sprintf(optC.strFName, "%s.dat", argv[1]);
	sprintf(optF.strFName, "%s.dat", argv[2]);


	VEC *avsolC, *avsolF;
    VEC *avdiff;

	vDatRead(&optC, &avsolC);
	vDatRead(&optF, &avsolF);

	/* make sure everything is ok */
	assert(optF.M == 2 * optC.M);
	assert(optF.N == 2 * optC.N);
	assert(GABS(optF.X - optC.X) < 1e-6);
	assert(GABS(optF.Y - optC.Y) < 1e-6);

	/* create the grids */
	Grid grF(&optF); int Mf=grF.iNCY(), Nf=grF.iNCX();
	Grid grC(&optC); int Mc=grC.iNCY(), Nc=grC.iNCX();

	/* create data and find the diff */
	avdiff = new VEC[Mc*Nc];
	Lnorm lnorm[4];
	Lnorm lvline;

	for (int ii=0 ; ii < 4 ; ii++) lnorm[ii].vInit();
	for(int jC = OFFSET ; jC < Mc-OFFSET ; jC++){
		for(int iC = OFFSET ; iC < Nc-OFFSET ; iC++){

			int iF1, iF2, jF1, jF2;
			VEC vCoarse, vFine;
			VEC vZero = {0,0,0};
			
			iF1 = iC*2 - 1; iF2 = iC*2;
			jF1 = jC*2 - 1; jF2 = jC*2;

			//coarse
		    CopyVec(avsolC[jC*Nc + iC], vCoarse);

			//fine
			SetVec(vFine, 0);
			AddVec(vFine, avsolF[jF1*Nf + iF1], 1, vFine);
			AddVec(vFine, avsolF[jF1*Nf + iF2], 1, vFine);
			AddVec(vFine, avsolF[jF2*Nf + iF1], 1, vFine);
			AddVec(vFine, avsolF[jF2*Nf + iF2], 1, vFine);
			AddVec(vZero, vFine, 0.25, vFine);

			//find the difference
			AddVec(vFine, vCoarse, -1, avdiff[jC*Nc + iC]);

			lnorm[3].vAddData(avdiff[jC*Nc + iC][P]);
			lnorm[3].vAddData(avdiff[jC*Nc + iC][U]);
			lnorm[3].vAddData(avdiff[jC*Nc + iC][V]);
			lnorm[P].vAddData(avdiff[jC*Nc + iC][P]);
			lnorm[U].vAddData(avdiff[jC*Nc + iC][U]);
			lnorm[V].vAddData(avdiff[jC*Nc + iC][V]);
		}
	}
	for (int ii=0 ; ii < 4 ; ii++) lnorm[ii].vCompute();

	/* find change in the solution only for the vertical mid-line */
	assert ( Nc % 2 == 0 );
	lvline.vInit();
	for (int jC = 1 ; jC < Mc - 1 ; jC++){

		int iC0, iC1, iF0, iF1, jF0, jF1;
		double duC, duF;
			
		iC0 = Nc / 2;
		iC1 = iC0 + 1;
		iF0 = Nf / 2;
		iF1 = iF0 + 1;
		jF0 = jC*2 - 1;
		jF1 = jC*2;
		
		duC = (avsolC[jC*Nc+iC0][U] + avsolC[jC*Nc+iC1][U])/2.;
		duF =   (avsolF[jF0*Nf+iF0][U] + avsolF[jF0*Nf+iF1][U]
			   + avsolF[jF1*Nf+iF0][U] + avsolF[jF1*Nf+iF1][U] )/4.;
		lvline.vAddData(duC - duF);

	}
	lvline.vCompute();
	
	
	/* write the data */
	{
		char tmp[300];
		sprintf(tmp, "%s_conv.vtk", argv[2]);
		
	    //grC.vWriteVtk(avdiff, NULL, tmp);

		printf("%.0lf %e %e %e\n", sqrt((Nf-2)*(Mf-2)),
			   lvline.dL1(), lvline.dL2(), lvline.dLInf() );
	}

	delete[] avdiff;
	delete[] avsolC;
	delete[] avsolF;
	
	return 0;
}
