/*
 * pp_vort2.cxx (Post Processor Vortex 2)
 *
 * Reads the solution And finds the vorticity and stream function.
 * Also calculates the strength of the vortices.
 * The integration part is hard coded so be carefull using it!!!
 *
 * to run it use: pp_vort2 path_to_solution
 */
#include <cstdio>
#include "ns.hxx"

enum EnumVort{ePsi=1, eOmega=0};

int main(int argc, char *argv[]){
 	assert(argc == 2);

 	Options opt;
 	vOptionsDef(&opt);
 	sprintf(opt.strFName, "%s.dat", argv[1]);

	VEC *avsol;
	double *advort[2];

	vDatRead(&opt, &avsol);

	/* create the grids */
	Physics ph(&opt);
	Grid gr(&opt); int m=gr.iNCY(), n=gr.iNCX();
	int M=m-2, N=n-2;
	vSetGhosts(&ph, &gr, avsol);
	
	/* create data and find the diff */
	advort[ePsi] = new double[M*N];
	advort[eOmega] = new double[M*N];

	/* Find the vorticity */
	for(int jj = 1 ; jj < m-1 ; jj++){
		for(int ii = 1 ; ii < n-1 ; ii++){
			
			int II = ii-1;
			int JJ = jj-1;
			
			advort[eOmega][JJ*N + II] =
				(avsol[jj*n+ii+1][V] - avsol[jj*n+ii-1][V]) / 2. / gr.dDx();
			
			advort[eOmega][JJ*N + II] -=
				(avsol[(jj+1)*n+ii][U] - avsol[(jj-1)*n+ii][U]) / 2. /gr.dDy();

		}
	}

	/* FInd the stream function*/
	double dMIN = 1e12;
	for(int jj = 1 ; jj < m-1 ; jj++){
		int JJ = jj-1;
		
		//first column
		advort[ePsi][JJ*N+0] = -gr.dDx() * avsol[jj*n+1][V] / 2.;
		dMIN = GMIN(advort[ePsi][JJ*N+0], dMIN);
		
		// other columns
		for(int ii = 2 ; ii < n-1 ; ii++){			
			int II = ii-1;

			advort[ePsi][JJ*N + II] = advort[ePsi][JJ*N + II - 1] - gr.dDx() *
				(avsol[jj*n+ii][V] + avsol[jj*n+ii-1][V] )/ 2.;

			dMIN = GMIN(advort[ePsi][JJ*N+II], dMIN);
		}
	}

	/* integrate vorticity strengths */
	if(gr.dY() < 2.1){
		int nmem[4];
		double dstrength[4];
		double dc[2];

		memset(nmem, 0, 4*sizeof(int));
		memset(dstrength, 0, 4*sizeof(double));
	    
		for(int jj = 1 ; jj < m-1 ; jj++){
			for(int ii = 1 ; ii < n-1 ; ii++){
			
				int II = ii-1;
				int JJ = jj-1;
			
			    gr.vCellCent(ii, jj, dc);

				/* add the corresponding data to where needed */
				if( advort[ePsi][JJ*N + II] > 0 ){
					nmem[1] ++;
					dstrength[1] += advort[eOmega][JJ*N + II];
				} else if( dc[eY] > 0.8 ){
					nmem[0] ++;
					dstrength[0] += advort[eOmega][JJ*N + II];
				} else if( dc[eX] > 0.5 ){
					nmem[2] ++;
					dstrength[2] += advort[eOmega][JJ*N + II];
				} else{
					nmem[3] ++;
					dstrength[3] += advort[eOmega][JJ*N + II];
				}
			}
					
		}

		printf("## num nmem str ave\n");
		for (int ii=0; ii < 4 ; ii++){
			double ave;
			ave = dstrength[ii] / nmem[ii];
			dstrength[ii] *= (gr.dDx() * gr.dDy());
			printf("%d %d %.10e %.10e\n", ii, nmem[ii], dstrength[ii], ave);
		}

	}
	
	
	
	/* write the data */
	{
		const char *c1 = "OMEGA";
		const char *c2 = "PSI";
		char const *cc[] = {c1, c2};
		char tmp[300];
		sprintf(tmp, "%s_vort.vtk", argv[1]);
		
		gr.vWriteVtk(advort, 2, cc, tmp);

	}

	delete[] avsol;
	delete[] advort[ePsi];
	delete[] advort[eOmega]; 
	
	return 0;
}
