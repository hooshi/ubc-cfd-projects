function fw2(coef,colorr)
##
x = 0:.01:2*pi;
f = @(x) 4*cos(x)-cos(2*x) - 3 + i*(-4*sin(x)+sin(2*x));
plot(f(x)/2*coef,colorr,"linewidth",2);


endfunction
