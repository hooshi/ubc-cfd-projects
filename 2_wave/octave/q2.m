clf, hold on;

## ------------------------------------------------------------
## plot border case
## -----------------------------------------------------------
[x,num,err,exa] = q2_1(504);
plot(x,num,".;CFL=0.504;","markersize",7,"color","r","linewidth", 1);

## ------------------------------------------------------------
## plot stable case
## figure 2 is on the same graph
## -----------------------------------------------------------
[x,num,err,exa] = q2_1(502);
plot(x,num,"--;CFL=502;","markersize",5,"color","k", "linewidth", 1.5);

## ------------------------------------------------------------
## plot unstable case
## -----------------------------------------------------------
[x,num,err,exa] = q2_1(506);
plot(x,num,"^;CFL=0.506;","markersize",5,"color","m","linewidth", 1);
plot(x,exa,"--;Exact;","linewidth",1,"color","b");

## beautiful plot stuff
figure(1);
hx=xlabel("X");
hy=ylabel("T(X)");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 12);
axis([15 20])

##legend
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 15 , "location" , "southeast", "linewidth",2);

#print
print("plot/q2.eps","-deps","-FArial" , "-color");
