## returns the value of solution for a problem solved for q1
function [x,num,err,exa] =  q1_1(str_n_mesh)

sol = load("-ascii",["sol/q1_" str_n_mesh ".sol"]);

x = [];
num = [];
err = [];
exa = [];
dx2 = (sol(2,1) - sol(1,1))/2; 

for i = 1:length(sol(:,1))
  x = [x sol(i,1)-dx2 sol(i,1)+dx2];
  num = [num sol(i,2) sol(i,2)];
  err = [err sol(i,3) sol(i,3)];
  exa = [exa sol(i,4) sol(i,4)]; 
endfor
clear sol

## figure(1), clf, hold on;
## plot(x,num,"-","linewidth",2,"color","k")
## plot(x,exa,"--","linewidth",2,"color","g")

endfunction
