## plot the stuff required for question 1


## ----------------------------------------------------------------------------
##
## Absolute error for n=20 and n=40
##
## ---------------------------------------------------------------------------

clf, hold on;
[x,num,err,exa] = q1_1("20");
plot(x,err , "b;N=20;", "linewidth", 2);
[x,num,err,exa] = q1_1("40");
plot(x,err , "r;N=40;", "linewidth", 2);

##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);

##axis label and range
hx=xlabel("X");
hy=ylabel("Solution Error");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
#axis([-1.5 .5 -1 1]), axis square, grid on


##save the plot
print("plot/q1-abs.eps","-deps","-FArial" , "-color");

## ----------------------------------------------------------------------------
##
## L norms 
##
## ---------------------------------------------------------------------------

clf, hold on;
l1 = zeros(1,7);
l2 = zeros(1,7);
linf = zeros(1,7);
size = [20 40 80 160 320 640 1280];
[l1(1),l2(1),linf(1),t] = q1_2("20");
[l1(2),l2(2),linf(2),t] = q1_2("40");
[l1(3),l2(3),linf(3),t] = q1_2("80");
[l1(4),l2(4),linf(4),t] = q1_2("160");
[l1(5),l2(5),linf(5),t] = q1_2("320");
[l1(6),l2(6),linf(6),t] = q1_2("640");
[l1(7),l2(7),linf(7),t] = q1_2("1280");

clf, hold on;
loglog(size, l1 ,  "k-;L1;", "linewidth", 2);
loglog(size, l2 ,  "k--;L2;", "linewidth", 2);
loglog(size, linf , "k-.;Linf;", "linewidth", 2);

##axis label and range
hx=xlabel("N");
hy=ylabel("Error Norm");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
##axis label
xt = get(gca,"XTick");
set(gca,"XTickLabel", sprintf("%.0f|",xt) );
axis([10 2000 ])


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);

##save the plot
print("plot/q1-l.eps","-deps","-FArial" , "-color");

## the slopes
p1 = polyfit (log10(size),  log10(l1),1)
p2 = polyfit (log10(size),  log10(l2),1)
pinf = polyfit (log10(size), log10(linf),1)

## save the slopes
save plot/q1-lslopes.txt p1 p2 pinf
