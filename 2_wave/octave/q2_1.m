## returns the value of solution for a problem solved for q2
function [x,num,err,exa] =  q2_1(cfl)

sol = load("-ascii",["sol/q2_" num2str(cfl) ".sol"]);

x = [];
num = [];
err = [];
exa = [];
dx2 = (sol(2,1) - sol(1,1))/2; 

for i = 1:length(sol(:,1))
  x = [x sol(i,1)];
  num = [num sol(i,2) ];
  err = [err sol(i,3) ];
  exa = [exa sol(i,4) ]; 
endfor

clear sol;

 ## figure(1), clf, hold on;
 ## plot(x,num,"-","linewidth",2,"color","k");
 ## plot(x,exa,"--","linewidth",1,"color","b");
 
endfunction
