## plot the required data for problem 1.
## data has to be copied here by hand .
function [l1,l2,linf,tend] =  q1_2(str_n_mesh)

sol = load("-ascii",["sol/q1_" str_n_mesh ".l"]);

tend = sol(end,1);
l1 =  sol(end,2);
l2 =  sol(end,3);
linf =  sol(end,4);

clear sol;

endfunction
