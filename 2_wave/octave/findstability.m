##taken from
##http://www.mathworks.com/matlabcentral/fileexchange/23972-chebfun/content/chebfun/examples/ode/html/Regions.html

C = 'color'; c = {'b','r','g','m','y','c'};
x = [0 0]; y = [-8 8]; K = 'k'; LW = 'linewidth'; FS = 'fontsize';

plot(y,x,K,LW,1), hold on, plot(x,y,K)
t = 0:.01:2*pi;
z = exp(1i*t); r = z-1;

## ----------------------------------------------------------------------------
##
## ADAMS - BASHFORTH 2 stability region and CFL(MAX)
##
## ---------------------------------------------------------------------------

## plot adams bashforth stability region
##s = 1; plot(r./s,C,c{1},LW,2)                      % order 1
s = (3-1./z)/2; plot(r./s, ";AB2 stability region;" ,C,c{1},LW,2)          
##s = (23-16./z+5./z.^2)/12; plot(r./s,C,c{3},LW,2)  % order 3

## plot the eigenvalues
fw2(.3 ,   "r;0.3L.dx/u;");
fw2(.25   ,"g;0.25L.dx/u;");
fw2(.2   , "m;0.2L.dx/u;");

##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);

##title
title("Finding CFL(MAX), AB2","fontsize",15);

##axis label and range
hx=xlabel("Re");
hy=ylabel("Im");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
axis([-1.5 .5 -1 1]), axis square, grid on


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);

##save the plot
print("plot/ab2.eps","-deps","-FArial" , "-color");


## ----------------------------------------------------------------------------
##
## RK 2 stability region and CFL(MAX)
##
## ---------------------------------------------------------------------------

##plot stability region for rk2
clf, plot(y,x,K,LW,1), hold on, plot(x,y,K)
w = z-1;
##plot(w,C,c{1},LW,2)                                % order 1
for i = 1:3
  w = w-(1+w+.5*w.^2-z.^2)./(1+w);
end
plot(w,";RK2 stability region;",C,c{1},LW,2)               % order 2
##for i = 1:4
##  w = w-(1+w+.5*w.^2+w.^3/6-z.^3)./(1+w+w.^2/2);
##end
##plot(w,C,c{3},LW,2)                                % order 3
##for i = 1:4
##  w = w-(1+w+.5*w.^2+w.^3/6+w.^4/24-z.^4)...
##      ./(1+w+w.^2/2+w.^3/6);
##end
##plot(w,C,c{4},LW,2)                                % order 4
##axis([-5 2 -3.5 3.5]), axis square, grid on

## plot eigenvalues of second order upwind
fw2(0.6 ,   "r;0.6L.dx/u;");
fw2(0.5   ,  "g;0.5L.dx/u;");
fw2(0.4   , "m;0.4L.dx/u;");

##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);

##title
title("Finding CFL(MAX), RK2","fontsize",15);

##axis label and range
hx=xlabel("Re");
hy=ylabel("Im");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
axis([-2.5 0.5 -1.75 1.75]), axis square, grid on


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);


##save the plot
print("plot/rk2.eps","-deps","-FArial" , "-color");

## ----------------------------------------------------------------------------
##
## euler stability region and CFL(MAX)
##
## ---------------------------------------------------------------------------
clf, plot(y,x,K,LW,1), hold on, plot(x,y,K)
w = z-1;

## plot eigenvalues of second order upwind
fw2(0.45   ,  [c{2} ";0.45L.dx/u;"]);
fw2(0.35   ,  [c{3} ";0.35L.dx/u;"]);
fw2(0.25   ,  [c{4} ";0.25L.dx/u;"]);
fw2(0.15   ,  [c{6} ";0.15L.dx/u;"]);

##plot stability region for explicit euler
plot(w,";Explicit Euler Stability Region;",C,c{1},LW,3)                                % order 1

##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 20 , "location" , "southwest", "linewidth",2);

##title
title("Finding CFL(MAX), Explicit Euler","fontsize",15);

##axis label and range
hx=xlabel("Re");
hy=ylabel("Im");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
axis([-2.2 0.5 -1.35 1.35]), axis square, grid on


##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 15 , "location" , "southwest", "linewidth",2);


##save the plot
print("plot/eu1.eps","-deps","-FArial" , "-color");
axis([-0.02 0.005 0.075 0.1]);
print("plot/eu1_close.eps","-deps","-FArial" , "-color");
