## output the norms for a scheme through time
function [l1,l2,linf,t] =  q3_2(fname)

sol = load("-ascii",["sol/" fname]);

t = sol(end,1);
l1 =  sol(end,2);
l2 =  sol(end,3);
linf =  sol(end,4);

clear sol;

endfunction
