## output the norms for a scheme through time
function [l1,l2,linf,t] =  q3_1(fname)

sol = load("-ascii",["sol/" fname]);

t = sol(:,1);
l1 =  sol(:,2);
l2 =  sol(:,3);
linf =  sol(:,4);

clear sol;

endfunction
