## plot the stuff required for question three

nm1 = ["1" ; "2" ; "3"]; 
nm2 = ["1" ; "2" ; "inf"]; 

## ----------------------------------------------------------------------------
##
## Error versus time
##
## ---------------------------------------------------------------------------

figure(1), clf, hold on;
figure(2), clf, hold on;
figure(3), clf, hold on;

[l1,l2,linf,t] = q3_1("q1_80.l");
figure(1), plot(t,l1, "b;RK2;", "linewidth", 2);
figure(2), plot(t,l2, "b;RK2;", "linewidth", 2);
figure(3), plot(t,linf, "b;RK2;", "linewidth", 2);

[l1,l2,linf,t] = q3_1("q3_a_80.l");
figure(1), plot(t,l1, "r;AB2+EXACT;", "linewidth", 2);
figure(2), plot(t,l2, "r;AB2+EXACT;", "linewidth", 2);
figure(3), plot(t,linf, "r;AB2+EXACT;", "linewidth", 2);

[l1,l2,linf,t] = q3_1("q3_b_80.l");
figure(1), plot(t,l1, "g;AB2+EULER;", "linewidth", 2);
figure(2), plot(t,l2, "g;AB2+EULER;", "linewidth", 2);
figure(3), plot(t,linf, "g;AB2+EULER;", "linewidth", 2);

[l1,l2,linf,t] = q3_1("q3_c_80.l");
figure(1), plot(t,l1, "m;AB2+RK2;", "linewidth", 2);
figure(2), plot(t,l2, "m;AB2+RK2;", "linewidth", 2);
figure(3), plot(t,linf, "m;AB2+RK2;", "linewidth", 2);


##legend font size
for i=1:3
figure(i);
 copied_legend = findobj(gcf(),"type","axes","Tag","legend");
 set(copied_legend, "FontSize", 15 , "location" , "southeast", ...
	 "linewidth",2);
endfor

##axis label and range
for i=1:3
  figure(i);
  hx=xlabel("t");
  hy=ylabel(["L_" nm2(i,:) " norm"]);
  set(hx, "fontsize", 15, "linewidth", 2);
  set(hy, "fontsize", 15, "linewidth", 2);
  set(gca,  "fontsize", 12);
  #axis([-1.5 .5 -1 1]), axis square, grid on
endfor

## ##save the plot
for i=1:3
  figure(i);
  print(["plot/q3-t-l" nm1(i,:) "-large.eps"],"-deps","-FArial" , "-color");
endfor
for i=1:3
  figure(i);
 axis([0 0.015 0 0.0003]);
  print(["plot/q3-t-l" nm1(i,:) "-zoom.eps"],"-deps","-FArial" , "-color");
endfor

## ----------------------------------------------------------------------------
##
## L norms versus mesh refinement
##
## ---------------------------------------------------------------------------

clf, hold on;

l1 = zeros(4,7);
l2 = zeros(4,7);
linf = zeros(4,7);

size = [20 40 80 160 320 640 1280];
fnames1 = {"q1_", "q3_a_", "q3_b_" , "q3_c_"};
fnames2 = {"RK2" , "AB2+EXACT" , "AB2+EULER" , "AB2+RK2"};
c = {"r" , "m", "b", "g"};

for i = 1:4
  [l1(i,1),l2(i,1),linf(i,1),t] = q3_2([deblank(fnames1{i}) "20.l"]);
  [l1(i,2),l2(i,2),linf(i,2),t] = q3_2([deblank(fnames1{i}) "40.l"]);
  [l1(i,3),l2(i,3),linf(i,3),t] = q3_2([deblank(fnames1{i}) "80.l"]);
  [l1(i,4),l2(i,4),linf(i,4),t] = q3_2([deblank(fnames1{i}) "160.l"]);
  [l1(i,5),l2(i,5),linf(i,5),t] = q3_2([deblank(fnames1{i}) "320.l"]);
  [l1(i,6),l2(i,6),linf(i,6),t] = q3_2([deblank(fnames1{i}) "640.l"]);
  [l1(i,7),l2(i,7),linf(i,7),t] = q3_2([deblank(fnames1{i}) ...
											   "1280.l"]);

  loglog(size, l1(i,:) ,  [c{i} "-;" deblank(fnames2{i}) " l1;"], ...
		 "linewidth", 2);
  loglog(size, l2(i,:) ,  [c{i} "--;" deblank(fnames2{i}) " l2;"], ...
		 "linewidth", 2);
  loglog(size, linf(i,:) ,  [c{i} "-.;" deblank(fnames2{i}) " linf;"], ...
		 "linewidth", 2);

endfor


##axis label and range
hx=xlabel("N");
hy=ylabel("Error Norm");
set(hx, "fontsize", 15, "linewidth", 2);
set(hy, "fontsize", 15, "linewidth", 2);
set(gca,  "fontsize", 15);
##axis label
xt = get(gca,"XTick");
set(gca,"XTickLabel", sprintf("%.0f|",xt) );
## axis([10 2000 ])


## ##legend font size
copied_legend = findobj(gcf(),"type","axes","Tag","legend");
set(copied_legend, "FontSize", 13 , "location" , "northeast", "linewidth",2);

##save the plot
print("plot/q3-x.eps","-deps","-FArial" , "-color");

