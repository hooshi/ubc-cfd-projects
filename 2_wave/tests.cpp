#include "wave.hpp"
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cstring>

class PhysicsRec:public Physics{
public:
	PhysicsRec(const double dU_):Physics(dU_){}
	double dF(const double x) const {
		return ( ((x<2/3.) && (x>1/3.)) ? 1 : 0);
	}
};

int main(int argc, char *argv[]){

	int iCase=-1;

	//read the case number
	if (argc < 2){
		printf("Test number not assigned\n");
		return 0;
	}
	else {
		sscanf(argv[1],"%d",&iCase);
	}

	/*
	  Create and test a Grid
	*/
	if(iCase==1){
		printf("Testing a grid\n");
		printf("Creating a 10 cell grind with X=1.5\n");
		Grid gr(10,2);

		printf("CELLS:\n");
		for (int ii=0 ; ii < gr.iNCell() ; ii++)
		{
			printf("F_l: %d, C:%d(%lf), F_r:%d \n",
				   gr.iFLeft(ii), ii, gr.dXCell(ii), gr.iFRight(ii));
		}

		printf("\nFACES:\n");
		for (int ii=0 ; ii < gr.iNFace() ; ii++)
		{
			printf("C_l: %d, F:%d(%lf), C_r:%d \n",
				   gr.iCLeft(ii), ii, gr.dXFace(ii), gr.iCRight(ii));
		}

		printf("Everything done, exiting...\n");
	}

	/*
	  Test the physics
	*/
	else if(iCase==2){
		Grid gr(400,4);
		double adTimes[] = {0, 1, 1.5};
		int iNTimes = 3;
		PhysicsRec ph(1);
			
		printf("## Testing Physics\n");				
		printf("## %-10s %-10s %-10s %-10s \n",
			   "X", "t=0", "t=1", "t=1.5");
		
		for (int ii=0 ; ii < gr.iNCell() ; ii++){
			printf("%-10.6lf", gr.dXCell(ii));
			for (int jj=0 ; jj < iNTimes ; jj++){
				printf("%-10.6lf", ph.dExactSol(gr.dXCell(ii), adTimes[jj]) );
			}
			printf("\n");
		}
                
		printf("## Finished. Exiting...\n");
	}

	/*
	  Testing flux integrator
	*/
	else if(iCase==3){
	    printf("Testing Flux integrator\n");
		Grid gr(5,7.5);
		double adT[] = {2, 7, 4, .5, 3};
		double adF[6];
	    Physics ph(2);

		vFInteg(gr, ph, adT, .4 /*t=.4*/ , adF);
		printf("G(0.4)=%lf\ndx=%lf\nU=%lf\n",ph.dG(.4),gr.dVCell(0),ph.dU);
		for (int ii=0 ; ii < gr.iNCell(); ii++){
			printf("%-20.5lf\n", adF[ii]);
		}
		printf("Finished. Exiting...\n");
	}

	/*
	  Testing time integration, RK2
	*/
	else if(iCase==4){
		int iNCell = 160, iNDT;
		double dXMax = 4, dCFL = 0.2, dDT, dTend=1, dU = 1;
		Grid gr(iNCell,dXMax);
		Physics ph(dU);
		double *adTrk = new double[gr.iNCell()];
		double *adTeu = new double[gr.iNCell()];
		double *adTexact = new double[gr.iNCell()];
		double *adTb1 = new double[gr.iNCell()];
		double *adTb2 = new double[gr.iNCell()];

		//find the rounded number dDT
		dDT = dCFL * gr.dVCell(0) / ph.dU;
		//find the number of time steps
		iNDT = (int)round(dTend/dDT);
		//find the new dDT
		dDT = dTend / iNDT;
		//find the new CFL
		dCFL = ph.dU * dDT / gr.dVCell(0);
			
		printf("## Testing RK2\n");
		printf("## CFL = %lf \n", dCFL);
		printf("## dt = %lf \n", dDT);
		printf("## n_dt = %d \n", iNDT);
		printf("## t_end will be %lf \n", iNDT * dDT);
		fprintf(stderr,"Testing RK2\n");
		fprintf(stderr,"CFL = %lf \n", dCFL);
		fprintf(stderr,"dt = %lf \n", dDT);
		fprintf(stderr,"n_dt = %d \n", iNDT);
		fprintf(stderr,"t_end will be %lf \n", iNDT * dDT);

		//set initial conditions
		//find exact solution
		for(int ii=0 ; ii < gr.iNCell() ; ii++){
			adTb2[ii] = adTb1[ii] = adTeu[ii] = adTrk[ii] =  ph.dF(gr.dXCell(ii)) ;
		    //adTb2[ii] = ph.dExactSol(gr.dXCell(ii), dDT);
			adTexact[ii] = ph.dExactSol(gr.dXCell(ii), dTend);
		}
						   
		//find RK2 solution
		//vTimeAdvanceRK2(gr, ph, adTb2, dDT, 1);
		vTimeAdvanceExplicitEuler(gr, ph, adTb2, dDT, 1);
		vTimeAdvanceExplicitEuler(gr, ph, adTeu, dDT, iNDT);
		vTimeAdvanceRK2(gr, ph, adTrk, dDT, iNDT);
		vTimeAdvanceBashforth2(gr, ph, adTb1, adTb2, dDT, iNDT);

		//print the solutions
		printf("## %-10s%-10s%-10s%-10s%-10s  \n",
			   "X", "RK2", "EULER" , "AB" ,"Exact");
		for(int ii=0 ; ii < gr.iNCell() ; ii++){
			printf("%-10.6lf",  gr.dXCell(ii)  );
			printf("%-10.6lf",  adTrk[ii]        );
			printf("%-10.6lf",  adTeu[ii]        );
			printf("%-10.6lf", adTb2[ii]  );
			printf("%-10.6lf\n", adTexact[ii]  );

	    }
				
		printf("## Finished. Exiting...\n");

		delete[] adTeu;
		delete[] adTrk;
		delete[] adTexact;
		delete[] adTb1;
		delete[] adTb2;
	}
	
	/*
	  No match.
	*/
	else{
		printf("## %d did not match any test.\n", iCase);
	}

	return 0;
}
