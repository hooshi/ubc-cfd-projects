/* wave.cpp
 *
 *   definition of all function in MECH510_wave.
 */

#include <cstdio>
#include <cassert>
#include "wave.hpp"

/****************************************************************************
 *                                    Grid                                  *
 ****************************************************************************/
Grid::Grid(const int iNCell__, const double dXmax__) :
        iNCell_(iNCell__), dXmax_(dXmax__){
        dDx_ = dXmax_/iNCell_;
        iNFace_ = iNCell_ + 1;
}
int Grid::iNFace() const{
        return iNFace_;
}
int Grid::iNCell() const{
        return iNCell_;
}
int Grid::iFRight(const int iC) const{
        return iC+1 ;
}
int Grid::iFLeft(const int iC) const{
        return iC ;
}
int Grid::iCLeft(const int iF) const{
        return iF-1;
}
int Grid::iCRight(const int iF) const{
        return iF;
}
double Grid::dXCell(const int iC) const{
        return (iC + 0.5) * dDx_;
}
double Grid::dXFace(const int iF) const{
        return (iF) * dDx_;
}
double Grid::dVCell(const int iC) const{
        return dDx_;
}



/****************************************************************************
 *                                    FluxInteg                             *
 ****************************************************************************/
void vFInteg(const Grid& gr,const  Physics& ph, const double* adT, const double dTime, double* adFlux){

	/*
	  set all fluxes to zero
	*/
	for (int ii=0 ; ii<gr.iNCell() ; ii++)
		adFlux[ii] = 0;
	
	/*
	  Loop over all faces. At each face calculate the flux:
	  F_{face} = - u * T_{face};
      
	  Then update the cell net fluxes:
	  F_{RightCell} += F_{face};
	  F_{LeftCell} -= F_{face};
	*/
	int iFace = 0;
	double dFluxFace;
	
	/*
	  First face: T is found using boundary conditions.
	  T_{face} = dG(t)
	*/
	dFluxFace = -ph.dU *  ph.dG(dTime);
	adFlux[gr.iCRight(iFace)] -= dFluxFace;
	iFace++;
	// fprintf(stderr,"F[%d]=%lf\n",iFace,dFluxFace); 
	
	/*
	  Second face:
	  Special interpolation: T_{face} = 2*T_{LeftCell} - dG(t)
	*/
	dFluxFace= -ph.dU * ( 2 * adT[gr.iCLeft(iFace)] - ph.dG(dTime) );
	adFlux[gr.iCRight(iFace)] -= dFluxFace;
	adFlux[gr.iCLeft(iFace)] += dFluxFace;
	iFace++;
	// fprintf(stderr,"F[%d]=%lf\n",iFace,dFluxFace);
	
   /*	
	Other faces:
	2nd order upwind: T_{face} = 3/2*T_{LeftCell} - 1/2*T_{LeftLeftCell}
   */
	for (; iFace < gr.iNFace()-1 ; iFace++){
		dFluxFace= -ph.dU * ( 1.5 * adT[gr.iCLeft(iFace)] - 0.5 * adT[gr.iCLeft(iFace)-1]);
		adFlux[gr.iCRight(iFace)] -= dFluxFace;
		adFlux[gr.iCLeft(iFace)] += dFluxFace;
		// fprintf(stderr,"F[%d]=%lf\n",iFace,dFluxFace);
	}
	// last face has no right cell
	dFluxFace= -ph.dU * ( 1.5 * adT[gr.iCLeft(iFace)] - 0.5 * adT[gr.iCLeft(iFace)-1]);
	adFlux[gr.iCLeft(iFace)] += dFluxFace;
	iFace++;	
	// fprintf(stderr,"F[%d]=%lf\n",iFace,dFluxFace);

	/*
	  all fluxes must be devided by dx becuase we are
	  actually looking for \Delta F / V_{Cell}.
	*/
	for (int ii=0 ; ii<gr.iNCell() ; ii++)
		adFlux[ii] /= gr.dVCell(ii);	
}

/****************************************************************************
 *                                    TimeAdvance                           *
 ****************************************************************************/
	
static void vLNorm(const Grid &gr ,const  Physics& ph, double* adT, const double dTime){
	double err , l1, l2, linf;
	l1 = l2 = linf = 0;
	for (int ii = 0 ; ii < gr.iNCell() ; ii++){
		err = ABS( adT[ii] - ph.dExactSol( gr.dXCell(ii), dTime) );
		l1 += err;
		l2 += err * err;
		linf = MAX(linf, err);
	}
	l1 /= gr.iNCell();
	l2 /= gr.iNCell(); l2 = sqrt(l2);

	printf("%-15.10lf %e %e %e\n",dTime, l1, l2, linf);
}

void vTimeAdvanceRK2
(const Grid &gr ,const  Physics& ph, double* adT, const double dDT, const int iNDT){

	double *adT_1 = new double [gr.iNCell()];
	double *adFlux = new double [gr.iNFace()];
	int iDT=0;

	while (iDT < iNDT){
		
		/*
		  step1: U_1 = U + dt/2 * F(U,t)
		*/
		vFInteg(gr, ph, adT, iDT*dDT, adFlux);
		for (int ii=0 ; ii<gr.iNCell(); ii++)
			adT_1[ii] = adT[ii] + dDT/2. * adFlux[ii];

		/*
		  step2: U = U + dt * F(U_1,t+dt/2)
		*/
		vFInteg(gr, ph, adT_1, iDT*dDT + dDT/2, adFlux);
		for (int ii=0 ; ii<gr.iNCell(); ii++)
			adT[ii] = adT[ii] + dDT * adFlux[ii];

		//time has advanced
		iDT++;
		//fprintf(stderr,"Time Advanced once with RK2.\n");
		vLNorm(gr, ph, adT, dDT * iDT);
	}

	fprintf(stderr, "RK2 was executed %d times\n", iDT);
	delete[] adT_1;
	delete[] adFlux;
}

void vTimeAdvanceBashforth2
(const Grid& gr, const Physics& ph, double*& adT0, double*& adT1, const double dDT, const int iNDT){
	double *adFlux0 = new double [gr.iNFace()];
	double *adFlux1 = new double [gr.iNFace()];
	double *pTmp;
	int iDT=1;

	//find flux of time iDT-1
	vFInteg(gr, ph, adT0, 0, adFlux0);

	while (iDT < iNDT){
		
		//find the flux of time iDT
		vFInteg(gr, ph, adT1, iDT*dDT, adFlux1);
		
		// U(n+1) = U(n) + dt/2 ( 3F(n)-F(n-1) ) 
		for (int ii=0 ; ii<gr.iNCell(); ii++){
			// find the new solution
			// it will be written on adT0
			adT0[ii] = adT1[ii] + dDT/2. * ( 3*adFlux1[ii] - adFlux0[ii]);
			
		}

		// the namings are not correct
		// instead of copying data, just change pointers 
		pTmp = adT1; adT1 = adT0; adT0 = pTmp;			
		pTmp = adFlux1; adFlux1 = adFlux0; adFlux0 = pTmp;
		//time has advanced
		iDT++;
		//fprintf(stderr,"Time Advanced once with AB2.\n");
		vLNorm(gr, ph, adT1, dDT * iDT);
	}

	fprintf(stderr, "AB2 was executed %d times\n", iDT-1);
	delete[] adFlux1;
	delete[] adFlux0;
}

void vTimeAdvanceExplicitEuler
(const Grid& gr, const Physics& ph, double* adT, const double dDT, const int iNDT){
	double *adFlux = new double [gr.iNFace()];
	int iDT=0;

	while (iDT < iNDT){
		
		/*
		  only step: U = U + dt * F(U,t)
		*/
		vFInteg(gr, ph, adT, iDT*dDT, adFlux);
		for (int ii=0 ; ii<gr.iNCell(); ii++)
			adT[ii] = adT[ii] + dDT * adFlux[ii];

		//time has advanced
		iDT++;
		//fprintf(stderr,"Time Advanced once with EulerExp.\n");
		vLNorm(gr, ph, adT, dDT * iDT);
	}

	fprintf(stderr, "EulerExp was executed %d times\n", iDT);
	delete[] adFlux;
}
