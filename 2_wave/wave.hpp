/* wave.hpp
 *
 * header file for functions in wave project.
 *
 */

#ifndef WAVE_HPP
#define WAVE_HPP

#include <cmath>
#define PI 3.141592653589793238462643383
#define ABS(x) ( ((x)>0) ? (x) : (-(x)) )
#define MAX(x,y) ( ((x)>(y)) ? (x) : (y) )

/* Grid

   Saves a one-dimensional grid.
*/
class Grid{
private:
	int iNCell_, //number of cells
		iNFace_; //number of faces = iNCell_+1
	double dXmax_, //length of domain  
		dDx_;      //length of each cell
	Grid();        //prohibited constructor

public:
	//constructor
	Grid(const int iNCell__, const double dXmax__);
	
	//return number of faces
	int iNFace() const;
	
	//return number of cells
	int iNCell() const;
	
	//param: number of a cell
	//return the number of the face on the right of the cell.
	int iFRight(const int) const;

	//param: number of a cell
	//return the number of the face on the left of cell
	int iFLeft(const int) const;

	//param: number of a face
	//return the number of the cell on the left of face
	int iCLeft(const int) const;

	//param: number of a face
	//return the number of the cell on the right of face
	int iCRight(const int) const;

	//param: number of a cell
	//retun location of the midpoint of cell
	double dXCell(const int) const;

	//param: number of a face
	//retun location of the face
	double dXFace(const int) const;

	//param: number of a cell
	//retun length of the cell
	double dVCell(const int) const;
};


/* Physics
 *
 * Stores necessary input for a linear wave problem.
 * The initial condition and boundary condition are defined as virtual
 * so that other initial conditions and boundary conditions can be used
 * by using polymorphism.
 */
class Physics{
private:
	Physics();
public:
	//speed of the wave
	double dU;

	//constructor
	Physics(const double dU_):dU(dU_){}

	//destructor
	virtual ~Physics(){}

	//boundary condition
	virtual double dG(const double dTime) const {return sin(4*PI*dTime);}

	//initial condition
	virtual double dF(const double dX) const {return -sin(2*PI*dX);}

	//exact solution
	double dExactSol(const double dX, const double dTime) const
		{return (dX > dTime*dU ? dF(dX-dTime*dU) : dG(dTime-dX/dU) );}

};

/* Flux integration
 *
 * Integrates the flux at a certain time step for a certain data.
 *
 * input:
 *       Grid
 *       Physics
 *       adT: the value of solution
 *       dTime: time of integraion
 * output:
 *       adFlux: flux integral
 */
void vFInteg(const Grid&, const Physics&, const double* adT, const double dTime, double* adFlux);

/* Time integration functions
 *
 * They take the value of solution at t=0 ( AB2 also takes the value at t=dt)
 * and return the value of solution at the desired time. They also print the
 * error norms on the screen at each time step.
 *
 * Input:
 *      Grid
 *      Physics
 *      adT or adT0: Value of solution at t=0
 *      adT1:        Value of solution at t=dt
 *      iNDT: Number of time steps to advance
 *
 * Output:
 *      adT or adT1: the solution at t=iNDT*dDT
 */
void vTimeAdvanceRK2
(const Grid&, const Physics&, double* adT, const double dDT, const int iNDT);
void vTimeAdvanceBashforth2
(const Grid&, const Physics&, double*& adT0, double*& adT1, const double dDT, const int iNDT);
void vTimeAdvanceExplicitEuler
(const Grid&, const Physics&, double* adT, const double dDT, const int iNDT);

#endif
