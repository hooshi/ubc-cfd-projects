#!/bin/bash 

## -------------------------------------------------------------------
## Run the examples for question 3
## -------------------------------------------------------------------

# create sol if it does not exist
if [ -d "sol/" ] ; then
    echo "sol/ found";
else
	mkdir sol;
fi

echo "Running question 3"
echo "x = 1, u = 2, NX=20-40-80-160-320-640-1280"
echo "CFL = 0.2 ,t_end=1, m=abc, ph=sine,  filename=sol/q3_a_size"
echo "========================================================="
echo ""

SIZE="20 40 80 160 320 640 1280"

for ii in $SIZE
do
	echo "Running N="$ii
	./driver.exe  1 2 $ii 0.2 1 a a sol/q3_a_$ii.sol > sol/q3_a_$ii.l
	
	echo "Running N="$ii
	./driver.exe  1 2 $ii 0.2 1 b a sol/q3_b_$ii.sol > sol/q3_b_$ii.l

	echo "Running N="$ii	
	./driver.exe  1 2 $ii 0.2 1 c a sol/q3_c_$ii.sol > sol/q3_c_$ii.l

	echo "================================================"
	
done
