#!/bin/bash 

## -------------------------------------------------------------------
## Run the examples for question 2
## -------------------------------------------------------------------

# create sol if it does not exist
if [ -d "sol/" ] ; then
    echo "sol/ found";
else
	mkdir sol;
fi

CFL="$1"

if [ "$CFL" == "" ]; then
    echo "Running question 2"
	echo "x = 20, u = 2, NX=500"
	echo "CFL = 0.XXX ,t_end=8, m=r, ph=ramp,  filename=sol/q2_XXX"
	echo "========================================================="

	CFL=502; echo ""; echo "CFL=$CFL, stable"
    ./driver.exe  20 2 500 0.$CFL 8 r b sol/q2_$CFL.sol > sol/q2_$CFL.l
	head -n 9 sol/q2_$CFL.l
	CFL=504; echo ""; echo "CFL=$CFL, middle"
	./driver.exe  20 2 500 0.$CFL 8 r b sol/q2_$CFL.sol > sol/q2_$CFL.l
	head -n 9 sol/q2_$CFL.l
	CFL=506; echo ""; echo "CFL=$CFL, slightly unstable"    
	./driver.exe  20 2 500 0.$CFL 8 r b sol/q2_$CFL.sol > sol/q2_$CFL.l
	head -n 9 sol/q2_$CFL.l
else
	echo "Running question 2"
	echo "x = 20, u = 2, NX=500"
	echo "CFL = 0.$CFL ,t_end=8, m=r, ph=ramp,  filename=sol/q2_$CFL"
	echo "========================================================="
	echo ""
	
	./driver.exe  20 2 500 0.$CFL 8 r b sol/q2_$CFL.sol > sol/q2_$CFL.l
	head -n 9 sol/q2_$CFL.l
fi
