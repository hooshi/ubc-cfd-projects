#!/bin/bash 

## -------------------------------------------------------------------
## Run the examples for question 1
## -------------------------------------------------------------------

# create sol if it does not exist
if [ -d "sol/" ] ; then
    echo "sol/ found";
else
	mkdir sol;
fi


SIZE="20 40 80 160 320 640 1280"

echo "Running question 1"
echo "x = 1, u = 2, NX=20-40-80-160-320-640-1280"
echo "CFL = 0.4 ,t_end=1, m=rk, ph=sine,  filename=sol/q1_size"
echo "========================================================="
echo ""

for ii in $SIZE
do
 echo "Running N="$ii
 ./driver.exe  1 2 $ii 0.4 1 r a sol/q1_$ii.sol > sol/q1_$ii.l
done
