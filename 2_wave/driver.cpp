// driver.cpp
//
// This file contains the main function for the solver written for the
// second assignment of MECH510.
//
// Shayan Hoshyari
// November 2015

#include "wave.hpp"
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cstring>

/* PhysicsStab:

   This class replaces the initial condition of the physics
   class to match problem 2 of assignment.
*/
class PhysicsStab:public Physics{
public:
	/* Constructor: Must always be present 	*/
	PhysicsStab(const double dU_):Physics(dU_){}
	/* The new initial condition */	
	double dF(const double x) const {
		return ( ((x<1+1e-9) && (x>-1e-9)) ? -x : 0);
	}
	~PhysicsStab(){}
};


/* prints how to use the executable */
void vHelp(){
	fprintf(stderr, "To run the program please use the following syntax\n");
	fprintf(stderr, "./driver x_{max} u N_c CFL t_e method physic outname\n");
	fprintf(stderr, "x_{max}: maximum length of domain.\n");
	fprintf(stderr, "u: wave speed.\n");
	fprintf(stderr, "N_c: number of cells.\n");
	fprintf(stderr, "t_e: final solution time.\n");
	fprintf(stderr, "method: a(AB2+EXACT) , b(AB2+EULER), c(AB2+RK2) r(RK2).\n");
	fprintf(stderr, "physic: a(sine) b(ramp) initial conditions.\n");
	fprintf(stderr, "outname: name of the output file for solution at t_e\n");
	
}

int main(int argc, char *argv[]){

	/*
	  Variable definitions - part 1
	*/
	int iNCell,            /* Number of CV's       */
		iNDT;              /* Number of time steps */
	double dXMax,          /* X_{max} */
		dCFL ,             /* Desired CFL number */
		dDT,               /* Time step */
		dTEnd,             /* Final time for solution */
		dU;                /* Wave speed */
	/* Name of output files*/
	char   cFNameSol[100];
	/* Time advance method - type of initial condition */
	char cMet, cPhys;
	/* How many euler steps to perform before switching to AB2 */
	int iNEuler; 
	/* File to write the solution error in */
	FILE *fSol;
	/* Physics */
	Physics *ph = (Physics*) NULL;

	/*
	  Read the input:
	  
	  dXMax, dU, iNCell, dCFL, dTEnd, cMet,
	  and iNEuler ( if cMet == b )
	*/
	if (argc != 9){
		vHelp();
		return 0;
	}
	else{
		int ii;
		
		//read dXMax
		ii = sscanf(argv[1], "%lf", &dXMax);
		if (!ii){
			fprintf(stderr, "Bad x_{max}\n");
			return 0;
		}
		//read dU
		ii = sscanf(argv[2], "%lf", &dU);
		if (!ii){
		    fprintf(stderr, "Bad U\n");
			return 0;
		}
		//read iNCell
		ii = sscanf(argv[3], "%d", &iNCell);
		if (!ii){
			fprintf(stderr, "Bad NX\n");
			return 0;
		}
		//read dCFL
		ii = sscanf(argv[4], "%lf", &dCFL);
		if (!ii){
		    fprintf(stderr, "Bad CFL\n");
			return 0;
		}
		//read dTEnd
		ii = sscanf(argv[5], "%lf", &dTEnd);
		if (!ii){
			fprintf(stderr, "Bad t_{end}\n");
			return 0;
		}
		//read cMethod and iNEuler
		ii = sscanf(argv[6], "%c", &cMet);
		if (!ii){
		    fprintf(stderr, "Bad Method\n");
			return 0;
		}
		if ( cMet == 'b' ){
			ii = sscanf(argv[6], "%c%d", &cMet, &iNEuler);
			if (ii != 2){
			    fprintf(stderr, "Adams Bashford with Euler chosen. ");
			    fprintf(stderr, "But iNEuler is not given, setting it to 1.\n");
				cMet = 'b';
				iNEuler = 1;
			} 
		}
		//read initial condition type
		ii = sscanf(argv[7], "%c", &cPhys);
		if (!ii){
		    fprintf(stderr, "Bad Type of initial condition\n");
			return 0;
		}
		//read output file name
		ii = sscanf(argv[8], "%s", cFNameSol);
		if (!ii){
			fprintf(stderr, "Bad solution file name\n");
			return 0;
		}
		
	}

	/*
	  print the input
	*/
	printf("## Given Input: x_{max}:%lf, U:%lf\n", dXMax, dU);
	printf("## Given Input: NX:%d, CFL:%lf, t_{end}:%lf \n", iNCell, dCFL, dTEnd);
	printf("## Method: ");
	switch (cMet){
	case 'r':
		printf("## RK2\n");
		break;
	case 'e':
		printf("## Explicit Euler \n");
		break;
	case 'a':
		printf("## AB2 + exact \n");
		break;
	case 'b':
		printf("## AB2e + Euler with %d times smaller steps \n", iNEuler);
		break;
	case 'c':
		printf("## AB2 + RK2 \n");
		break;
	default:
		printf("## Bad input %c!\n", cMet);
		return 0;
		break;
	}
	//create the physics
	printf("## Physics Type (initial condition): ");
	switch(cPhys){
	case 'a':
		printf("## Sine\n");
		ph = new Physics(dU);
		break;
	case 'b':
		printf("## Ramp(stability)\n");
		ph = new PhysicsStab(dU);
		break;
	default:
		printf("## %c not supported!\n", cPhys);
		return 0;		
	}
	printf("## Solution error file name: %s\n", cFNameSol);
	printf("## =================================\n\n");
	

    /*
	  further pre processing
	*/
	
	//create the grid
	Grid gr(iNCell,dXMax);
			
	// arrays to hold the solution 
	double *adT = new double[gr.iNCell()]; //solution
	double *adTB = new double[gr.iNCell()]; //adams bashforth backup solution
	double *adTErr = NULL;                 //solution error
	
	//calculate the CFL number
	dDT = dCFL * gr.dVCell(0) / ph->dU; //find the approximate number dDT	
	iNDT = (int)round(dTEnd/dDT); //find the number of time steps
	dDT = dTEnd / iNDT; //find the new dDT	
	dCFL = ph->dU * dDT / gr.dVCell(0); //find the new CFL
	printf("## Final CFL: %lf, Final dt: %lf, NT: %d \n", dCFL, dDT, iNDT);
	printf("## =================================\n\n");
	

	/*
	 * Solve the problem
	 */
	
	//initial solution
	for(int ii=0 ; ii < gr.iNCell() ; ii++){
		adTB[ii] = adT[ii] = ph->dF(gr.dXCell(ii)) ;
	}

	//solve the problem for t = t_end 
	switch (cMet){
	case 'r':
		//use runge-kutta
		vTimeAdvanceRK2(gr, *ph, adT, dDT, iNDT);
		break;
	case 'e':
		//use explicit euler
		vTimeAdvanceExplicitEuler(gr, *ph, adT, dDT, iNDT);
		break;
	case 'a':
		//use exact solution for time step n=1
		for(int ii=0 ; ii < gr.iNCell() ; ii++)
			adT[ii] = ph->dExactSol(gr.dXCell(ii), dDT);
		//use adams bashforth
		vTimeAdvanceBashforth2(gr, *ph, adTB, adT, dDT, iNDT);
		break;
	case 'b':
		//use explicit euler with iNEULER steps for n=1
		vTimeAdvanceExplicitEuler(gr, *ph, adT, dDT/iNEuler, iNEuler);
		//use adams bashforth
		vTimeAdvanceBashforth2(gr, *ph, adTB, adT, dDT, iNDT);
		break;
	case 'c':
		//use runge - kutta for n = 1
		vTimeAdvanceRK2(gr, *ph, adT, dDT, 1);
		//use adams bashforth
		vTimeAdvanceBashforth2(gr, *ph, adTB, adT, dDT, iNDT);
	    break;
    }

	
	/*
	 * calculate and print error(x) in output file.
	 * While lnorms/t are automatically printed on screen.
	 */
	fSol = fopen(cFNameSol, "w");
	if (!fSol){
		fprintf(stderr, "can not open %s \n", cFNameSol);
		return 0 ;
	}
	//print the solutions
	fprintf(fSol, "## %-10s%-10s%-10s%-10s  \n\n",
			"X", "SOL", "ERR" , "EXACT");
	for(int ii=0 ; ii < gr.iNCell() ; ii++){
		fprintf(fSol,"%-15.6lf ",  gr.dXCell(ii)  );
		fprintf(fSol,"%-15.6lf ",  adT[ii]        );
		fprintf(fSol,"%-15.6lf ",  ph->dExactSol(gr.dXCell(ii), dTEnd) - adT[ii] );
		fprintf(fSol,"%-15.6lf \n", ph->dExactSol(gr.dXCell(ii), dTEnd)  );

	}
	//close the file
	fclose(fSol);
					

	/*
	  delete arrays
	*/
	delete[] adT;
	if (adTB) delete[] adTB;
	if (adTErr) delete[] adTErr;
	if (ph) delete ph;
	
	return 0;
}
