/** pressure.cpp
 *  contains the main file that solves the pressure problem.
 *  Shayan Hoshyari
 *  October 2015
 */

#include <poisson.hpp>
#include <cmath>
#include <cstdio>
#include <ctime>
#include <cstring>

//bnd condition at down and left
double fzero(const double x){ return 0;}
//boundary condition at right and top
double fbnd(const double z){ return 5-.5*pow((1+z*z),3);}
//source term
double fsource(const double x, const double y){return -18 * pow(x*x+y*y,2);}

int main(int argc, char *argv[]){

	Problem p; //the problem to be solved
	double d_final, //final change in solution
	epsilon,		//desired final change in solution
	omega;			//SOR parameter
	int n_it,	//number of iteration
	NI_VAL,  	//Cells in x dir
	NJ_VAL ;	//Cells in y dir
	bool success;	//have we converged?

	//read the input
	if (argc != 6) {
		printf("PLEASE INPUT: NI NJ epsilon omega outputname \n");
		return 0;
	}
	sscanf(argv[1],"%d",&NI_VAL);
	sscanf(argv[2],"%d",&NJ_VAL);
	sscanf(argv[3],"%lf",&epsilon);
	sscanf(argv[4],"%lf",&omega);

	//initialize the problem
	problem_setup_c(p, NI_VAL, NJ_VAL,
			BNDNeu, BNDDirich, BNDDirich, BNDNeu,
			fzero, fbnd, fbnd, fzero,
			fsource,omega,epsilon);

	//solve the equation
	success = solve(p,d_final,n_it,1);

	//print the values of pressure at Cells around .5,.5
	//to do so the mesh should be (2N*2N)
	//also write a vtk
	if(success){
		printf("%% SUCCESS :) \n");
		write_vtk(p.NI,p.NJ,p.T,NULL,argv[5]);
		if ( (p.NI==p.NJ) && (p.NI%2 == 0) ){
			printf("Values around 0.5, 0.5: \n");
			printf("%20.15f %20.15f \n", p.T[p.NI/2][p.NI/2+1], p.T[p.NI/2+1][p.NI/2+1]);
			printf("%20.15f %20.15f \n", p.T[p.NI/2][p.NI/2], p.T[p.NI/2+1][p.NI/2]);
		}
	}
	else printf("%% FAILURE :( \n");
	printf("%% Number of iterations: %d\n", n_it);
	printf("%% Max change in last iteration: %e\n", d_final);

	//destroy the problem
	problem_destroy(p);

}
