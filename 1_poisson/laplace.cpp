/** laplace.cpp
 *  contains the main file that solves the laplace problem.
 *  Shayan Hoshyari
 *  October 2015
 */

#include <poisson.hpp>
#include <cmath>
#include <cstdio>
#include <ctime>
#include <cstring>

//boundary conditions
double fzero(const double x){ return 0;} //left down and right
double fcospx(const double y){ return cos(PI*y);} //top
//analytical solution
double fanalytic(const double x,const double y) {return cos(PI*x)*sinh(PI*y)/sinh(PI);}
//source term
double fsource(const double x, const double y){return 0;}

int main(int argc, char *argv[])
{

  Problem p; //problem to be solved
  double d_final, //final maximum change in solution
    **err, 			//the error in each CV
    **ana ,			//analytical solution at each cv
    l2,				// l norms
    l1,
    linf,
    epsilon,
    omega;
  int n_it, //final number of iterations
    NI_VAL,  //Cells in x dir
    NJ_VAL ; //Cells in y dir
  bool success; //have we converged?

  //read the input
  if (argc != 6)
    {
      printf("PLEASE INPUT: NI NJ epsilon omega outputname \n");
      return 0;
    }
  sscanf(argv[1],"%d",&NI_VAL);
  sscanf(argv[2],"%d",&NJ_VAL);
  sscanf(argv[3],"%lf",&epsilon);
  sscanf(argv[4],"%lf",&omega);

  //initialize the problem
  problem_setup_c(p, NI_VAL, NJ_VAL,
                  BNDDirich, BNDNeu, BNDDirich, BNDNeu,
                  fzero, fzero, fcospx, fzero,
                  fsource,omega,epsilon);

  //solve the equation
  success = solve(p,d_final,n_it,1);

  //calculate the norms
  if(success)
    {
      printf("%% SUCCESS :) \n");
      //find the error and l2 norm
      array_create_2d(err,NI_VAL+2, NJ_VAL+2, 0.);
      array_create_2d(ana,NI_VAL+2, NJ_VAL+2, 0.);
      average_values(ana,NI_VAL,NJ_VAL,fanalytic);
      l2 = l1 = linf = 0;
      for (int j = 1 ; j <= NJ_VAL ; j++)
        {
          for (int i = 1 ; i <= NI_VAL ; i++)
            {
              err[i][j] = ana[i][j] - p.T[i][j];
              l2 += err[i][j]*err[i][j];
              l1 += ABS(err[i][j]);
              linf = MAX(err[i][j],linf);
            }
        }
      l1 /= NI_VAL * NJ_VAL;
      l2 /= NI_VAL * NJ_VAL;
      l2 = sqrt(l2);
      //print the norms
      printf("%% L1:   %e\n", l1);
      printf("%% L2:   %e\n", l2);
      printf("%% Linf: %e\n", linf);
      //print the vtk
      write_vtk(p.NI,p.NJ,p.T,err,argv[5]);
      //destroy the arrays
      array_destroy_2d(err,NI_VAL+2);
      array_destroy_2d(ana, NI_VAL+2);
    }
  else
    {
      printf("%% FAILURE :( \n");
    }
  printf("%% Number of iterations: %d\n", n_it);
  printf("%% Max change in last iteration: %e\n", d_final);

  //destroy the problem
  problem_destroy(p);

}

/*
  Local Variables:
  mode:c++
  c-file-style:"GNU"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
