/** poisson.hpp
 *  Here lie the declaration for the functions used in MECH510_poisson.
 *  Shayan Hoshyari
 *  October 2015
 */

#ifndef POISSON_HPP
#define POISSON_HPP

#define ABS(x) ((x) > 0 ? (x) : -(x) )
#define MAX(x,y) ((x) > (y) ? (x) : (y) )
#define BINARY_VTK 0							//Binary vtk?
#define IT_MAX 1000000							//Maximum allowed number of SOR iteration
#define PI  3.141592653589793238462643383		//What a lovely number!!!
#define WX3A1 0.326794919243112270647255365		//Gauss Quadrature Parameter
#define WX3A2 0.673205080756887729352744634		//Gauss Quadrature Parameter
#define WX1A1 0.673205080756887729352744634		//Gauss Quadrature Parameter
#define WX1A2 0.326794919243112270647255365		//Gauss Quadrature Parameter
#define W1    0.555555555555555555555555555		//Gauss Quadrature Parameter
#define W0    0.888888888888888888888888888		//Gauss Quadrature Parameter
#define N_LOG_WRITE  1000                               //Number of maximum solution changes to write at once

/*********************************************************************
 * Structs, Typedefs, ...
 *********************************************************************/

//Indicate the type of boundary
enum BoundaryType
  {
  BNDDirich /*Dirichlet boundary condition*/,
  BNDNeu /*Neumann Boundary condition*/
  };

//Function for the value of dT/dz or T on boundary
typedef double (*BoundaryFunc)(const double);

//Function for the source term
typedef double (*SourceFunc)(const double, const double);

//Store a boundary condition
struct BoundaryEdge
{
  BoundaryFunc f;
  BoundaryType t;
};

//Store the main data for the problem
struct Problem
{
  //dimensions of the mes
  int NI, NJ;
  //boundary conditions
  BoundaryEdge bnd1, bnd2, bnd3, bnd4;
  //source term
  SourceFunc src1;
  //average value of solution and source term at each CV
  double **T, **S;
  //center coordinate of each CV
  double *x, *y;
  double dx, 	//obvious
    dy,			//obvious
    D, 			//(dy/dx)^2
    omega, 		//SOR parameter
    e,			//desired maximum change in solution
    t_beg;      //the real time (using cpu clock) the simulation has started
};

/*********************************************************************
 * Functions
 *********************************************************************/
// Defines a boundary condition
// bnd: the boundary condition to be set
// t_: type of boundary condition (Neumann, Dirichlet)
// f_: a function pointer for the value of T or dT/dz
void boundary_read_c(BoundaryEdge &bnd, BoundaryType t_, BoundaryFunc f_);

// Prints a boundary for inspection
void boundary_print(BoundaryEdge &bnd);

// Discretize an axis
// z: an already memory allocated pointer which is either x or y
// dz: dx or dy.
// N: NI or NJ, i.e., number of CV's in that direction, not counting ghost cells.
void disc_axis(double *z, double &dz, const int N);

// Sets up a Problem.
// Problem: the problem to be setup.
// ni:      NI, number of CV in x, not counting ghost cells.
// nj:      NJ, number of CV in y, not counting ghost cells.
// b1, ..., b4: boundary type of boundaries.
// f1, ..., f4: function pointer to the value of T or dT/dz at each boundary.
// s1: function pointer to the source term
// omega: SOR parameter
// e: desired maximum change in solution
// NOTE: boundaries are numbered in this order:
//	1: down 2:right 3:up 4:left
void problem_setup_c(Problem&,
                     const int ni,
                     const int nj,
                     const BoundaryType b1,
                     const BoundaryType b2,
                     const BoundaryType b3,
                     const BoundaryType b4,
                     const BoundaryFunc f1,
                     const BoundaryFunc f2,
                     const BoundaryFunc f3,
                     const BoundaryFunc f4,
                     const SourceFunc s1,
                     const double omega,
                     const double e);

//Destroy a problem, i.e., delete[] dynamically allocated memory
void problem_destroy(Problem&);

//print a problem for troubleshooting
void problem_print(Problem&);

//print an array
void array_print_2d(double **x, const int n, const int m);
void array_print_1d(double *x, const int n);

//allocate and free memory to an array
template<typename T> void array_create_2d(T **&x, const int n, const int m, const T def)
{
  x = new T*[n];
  for (int i = 0 ; i < n ; i++)
    {
      x[i] = new T [m];
      for (int j = 0 ; j < m ; j++)
        x[i][j] = def;
    }
}
template<typename T> void array_destroy_2d(T **&x, const int n)
{
  for (int i = 0 ; i < n ; i++)
    delete[] x[i];
  delete[] x;

}
template<typename T> void array_create_1d(T *&x, const int n)
{
  x = new T[n];
}
template<typename T> void array_destroy_1d(T *&x, const int n)
{
  delete[] x;
}

//evaluate value of one ghost cell
// orient: should be 1 for left and top boundary and -1 for others
//	bnd: the boundary condition
//  Ta: the value of solution at the adjacent CV to the ghost cell
// dw: dw of the mesh. w is the axis perpendicular to the boundary
// z:  z of the center point of the ghost cell. z is the axis parallel to the boundary.
double ghost_eval(const double orient,
                  const BoundaryEdge &bnd,
                  const double Ta,
                  const double dw,
                  const double z);

//evaluate value of all ghost cells
void ghost_eval_all(Problem&);

//do one single SOR iteration
double solve_iterate(Problem&);

// Solve a problem
// input-Problem: the Poisson problem to be solved
// output-d_final: the final maximum change in solution
// output-n_it: the final maximum change in solution
// const int - 0 : should the maximum value of solution at each iteration be printed??
bool solve(Problem&, double &d_final, int &n_it, const int = 0);

//integrate a function in a rectangle [a1 a2]*[b1 b2] using GAUSE QUADRATURE
// inputs are in order
// 1 - the function to be integrated
// 2 - a1,a2,b1,b2
double gauss_quadrature(SourceFunc, const double, const double, const double, const double);
//find the average value of a function in [a1 a2]*[b1 b2] using GAUSE QUADRATURE
// inputs are in order
// 1 - the function to be averaged
// 2 - a1,a2,b1,b2
double gauss_mean(SourceFunc, const double, const double, const double, const double);

// calculate the average value of a function in a structured mesh using GAUSS QUADRATURE
// Tave: the average values to be calculated, must be [NI+2][NJ+2]
// NI: the number of cells in x direction, not counting ghost cells
// NJ: the number of cells in y direction, not counting ghost cells
// SourceFunc: the function to be averaged
void average_values(double** Tave,const int NI, const int NJ, SourceFunc);

//write the data to a vtk file
// NI and NJ must be number of cells in each direction not counting the ghost cells.
// num: the numerical value of solution
// err: the solution error (can be set to NULL so nothing is written to file )
// namevtk: name of the vtk file.
void write_vtk(const int ni, const int nj , double**num, double**err, const char *namevtk);


#endif /*POISSON_HPP*/



/*
  Local Variables:
  mode:c++
  c-file-style:"GNU"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/


