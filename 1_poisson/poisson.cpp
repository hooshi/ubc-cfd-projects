/** poisson.cpp
 *  Here lie the definition for the functions used in MECH510_poisson.
 *  As most of the details are explained in poisson.hpp this file is not commented that much.
 *  Shayan Hoshyari
 *  October 2015
 */

#include <poisson.hpp>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <visit_writer.h>

//small project so why not? :)
using namespace std;

void boundary_read_c(BoundaryEdge &bnd, BoundaryType t_, BoundaryFunc f_)
{
  bnd.t = t_;
  bnd.f = f_;
}

void boundary_print(BoundaryEdge &bnd)
{

  //print type
  cout << "boundary edge: type: " ;
  if (bnd.t == BNDNeu) cout << "Neumann ";
  else if(bnd.t == BNDDirich) cout<<"Dirichlet ";
  else cout << "You are doomed! ";

  //print the value of f
  cout << "\n f: ";
  for (int i=0 ; i < 5 ; i++)	printf("%.1f:%.4f\t",i*.1,bnd.f(i*.1));
  cout << "\n";
  for (int i=5 ; i < 10 ; i++)	printf("%.1f:%.4f\t",i*.1,bnd.f(i*.1));
  cout << "\n";
}

void disc_axis(double *z, double &dz, const int N)
{

  dz = 1. / N;
  z[0] = -dz/2.;
  for (int i=1 ; i < N + 2 ; i++) z[i] = z[i-1]+dz;
}

void problem_setup_c(Problem &pbm,
                     const int ni,
                     const int nj,
		     const BoundaryType b1,
                     const BoundaryType b2,
                     const BoundaryType b3,
                     const BoundaryType b4,
		     const BoundaryFunc f1,
                     const BoundaryFunc f2,
                     const BoundaryFunc f3,
                     const BoundaryFunc f4,
		     const SourceFunc s1,
		     const double omega,
                     const double e)
{

  //dimensions
  pbm.NI = ni;
  pbm.NJ = nj;
  //boundary conditions
  boundary_read_c(pbm.bnd1,b1,f1);
  boundary_read_c(pbm.bnd2,b2,f2);
  boundary_read_c(pbm.bnd3,b3,f3);
  boundary_read_c(pbm.bnd4,b4,f4);
  //source term function -> currently NULL
  pbm.src1 =  s1;
  //x and y axis
  pbm.x = new double[pbm.NI+2];
  pbm.y = new double[pbm.NJ+2];
  disc_axis(pbm.x,pbm.dx,pbm.NI);
  disc_axis(pbm.y,pbm.dy,pbm.NJ);
  //array for T
  array_create_2d(pbm.T,pbm.NI+2,pbm.NJ+2,0.);
  //array for s
  array_create_2d(pbm.S,pbm.NI+2,pbm.NJ+2,0.);
  for (int i = 1 ; i < pbm.NI + 1 ; i++)
    {
      pbm.S[i][0] = pbm.S[i][pbm.NJ+1] = 0;
      for (int j = 1 ; j < pbm.NJ + 1 ; j++)
        {
          pbm.S[i][j] = pbm.src1(pbm.x[i],pbm.y[j]);
	}
    }
  for (int j = 0 ; j < pbm.NJ + 2 ; j++)
    {
      pbm.S[0][j] = pbm.S[pbm.NI+1][j] = 0;
    }
  //D,e omega and t_beg
  pbm.D = pbm.dy/pbm.dx * pbm.dy / pbm.dx;
  pbm.e = e;
  pbm.omega = omega;
  pbm.t_beg = clock();
}

void problem_destroy(Problem &pbm)
{
  delete[] pbm.x;
  delete[] pbm.y;
  array_destroy_2d(pbm.T,pbm.NI+2);
  array_destroy_2d(pbm.S,pbm.NI+2);
}

void problem_print(Problem& p)
{
  //dims
  printf("dims: %d, %d\n", p.NI, p.NJ);
  //x y T and S
  printf("X points:\n");
  array_print_1d(p.x,p.NI+2);
  printf("Y points:\n");
  array_print_1d(p.y,p.NJ+2);
  printf("T values: \n");
  array_print_2d(p.T,p.NI+2,p.NJ+2);
  printf("S values: \n");
  array_print_2d(p.S,p.NI+2,p.NJ+2);
  //boundaries
  boundary_print(p.bnd1);
  boundary_print(p.bnd2);
  boundary_print(p.bnd3);
  boundary_print(p.bnd4);
  //dx and dy
  printf("dx: %.6f, dy: %.6f\n", p.dx, p.dy);
  //omega and e
  printf("omega: %.6f, e: %f\n", p.omega, p.e);
  //time
  printf("elapsed time: %f \n", (clock() - p.t_beg) / CLOCKS_PER_SEC);
}

void array_print_1d(double *x, const int n)
{
  for (int i = 0 ; i < n ; i++) printf("%-9.4f",x[i]);
  printf("\n");
}

void array_print_2d(double **x, const int n, const int m)
{
  for (int j = m-1 ; j >= 0 ; j--)
    {
      for (int i = 0 ; i < n ; i++)
        printf("%-9.4f",x[i][j]);
      printf("\n");
    }
  printf("\n");
}


double ghost_eval(const double orient,
                  const BoundaryEdge &bnd,
		  const double Ta,
                  const double dw,
                  const double z)
{
  if (bnd.t == BNDDirich)
    {
      return 2*bnd.f(z) - Ta;
    }
  else if (bnd.t == BNDNeu)
    {
      return Ta + orient*bnd.f(z)*dw;
    }
  else
    {
      printf("impossibru!\n");
      return 0;
    }
}

void ghost_eval_all(Problem &p)
{
  for (int i = 1 ; i < p.NI + 1 ; i++)
    {
      p.T[i][0     ] = ghost_eval(-1,p.bnd1,p.T[i][1   ],p.dy,p.x[i]);
      p.T[i][p.NJ+1] = ghost_eval( 1,p.bnd3,p.T[i][p.NJ],p.dy,p.x[i]);
    }
  for (int j = 1 ; j < p.NJ + 1 ; j++)
    {
      p.T[p.NI+1][j] = ghost_eval( 1,p.bnd2,p.T[p.NI][j],p.dx,p.y[j]);
      p.T[0     ][j] = ghost_eval(-1,p.bnd4,p.T[1   ][j],p.dx,p.y[j]);
    }
}

double solve_iterate(Problem& p)
{
  double d, d_max = 0, Tnew;
  for (int i = 1 ; i <= p.NI ; i++)
    {
      for (int j = 1; j <= p.NJ ; j++)
        {
          Tnew = (
                  p.D * (p.T[i-1][j] + p.T[i+1][j]) + p.T[i][j+1] + p.T[i][j-1] - p.S[i][j] * p.dy * p.dy
                  )/ 2 / (p.D+1);
          d = Tnew - p.T[i][j];
          d_max = MAX(ABS(d), d_max);
          p.T[i][j] += p.omega * d ;
	}
    }
  return d_max*p.omega;
}

double gauss_quadrature(SourceFunc f , const double a1, const double a2, const double b1, const double b2)
{
  return (a2-a1)*(b2-b1)*gauss_mean(f,a1,a2,b1,b2);
}
double gauss_mean(SourceFunc f , const double a1, const double a2, const double b1, const double b2)
{
  double xr,xl,x0,yr,yl,y0;
  xl = WX1A1 * a1 + WX1A2 * a2;
  x0 = (a1 + a2) / 2. ;
  xr = WX3A1 * a1 + WX3A2 * a2;

  yl = WX1A1 * b1 + WX1A2 * b2;
  y0 = (b1 + b2) / 2. ;
  yr = WX3A1 * b1 + WX3A2 * b2;

  return  (
           W0*W0*  f(x0,y0)
           +W0*W1* ( f(x0,yr) + f(x0,yl) + f(xr,y0) + f(xl,y0) )
           +W1*W1* ( f(xl,yl) + f(xl,yr) + f(xr,yl) + f(xr,yr) )
           ) / 4.;
}

bool solve(Problem& p, double &d_final, int &n_it, const int log_do)
{

  int log_count = 1, log_pre_count=1;
  double log_d[N_LOG_WRITE];

  for (n_it = 1 ; n_it <= IT_MAX ; n_it++)
    {

      //do an iteration
      ghost_eval_all(p);
      d_final=solve_iterate(p);

      //store the error for logging
      if (log_do)
        {
          log_d[log_count-1] = d_final;
          if ( log_count == N_LOG_WRITE  )
            {
              for (int i = 0 ; i < log_count; i++) printf("%-4d %-15e\n",log_pre_count+i,log_d[i]);
              log_pre_count += log_count;
              log_count = 0;
	    }
          log_count++;
	}

      //return if converged
      if(d_final < p.e)
        {
          if (log_do) for (int i = 0 ; i < log_count - 1; i++) printf("%-4d %-15e\n",log_pre_count+i,log_d[i]);
          return true;
	}
    }
  return false;
}

void average_values(double** Tave,const int NI, const int NJ, SourceFunc Texa)
{
  for (int i = 1 ; i <= NI ; i++)
    for (int j = 1 ; j <=NJ; j++ )
      Tave[i][j]=
        gauss_mean(Texa, (double)(i-1)/NI, (double)i/NI, (double)(j-1)/NJ, (double)j/NJ );
}

void write_vtk(const int ni, const int nj,double**num, double**err, const char *namevtk)
{
  int dims[] = {ni+1, nj+1, 1}, k=0;
  float *x, *y, z[] = {0};
  int nvars = (err ? 2 : 1);
  int vardim[] = {1,1};
  int centering[] = {0,0};
  float *vars[2];
  const char varname1[] = "Numerical";
  const char varname2[] = "Error";
  const char *varnames[] = {varname1, varname2};

  //allocate memory
  x = new float[ni+1];
  y = new float[nj+1];
  vars[0] = new float[ni*nj];
  if (err) vars[1] = new float[ni*nj];

  //copy data
  for (int i = 0 ; i < ni+1 ; i++)
    x[i] = (float) i / ni;
  for (int j = 0 ; j < nj+1 ; j++)
    y[j] = (float) j / nj;

  for (int j = 1 ; j <= nj ; j++)
    for (int i = 1 ; i <= ni ; i++)
      {
        vars[0][k] = (float)num[i][j];
        if (err) vars[1][k] = (float)err[i][j];
        k++;
      }

  //write to a vtk file
  write_rectilinear_mesh(namevtk, BINARY_VTK, dims, x, y, z, nvars, vardim, centering, varnames, vars);

  //free memory
  delete[] x;
  delete[] y;
  delete[] vars[0];
  if (err) delete[] vars[1];
}


/*
  Local Variables:
  mode:c++
  c-file-style:"GNU"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
