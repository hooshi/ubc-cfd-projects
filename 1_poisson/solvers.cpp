#include <cstdio>
#include <cassert>

#include "solvers.hpp"


double iterate_jacobi(Problem& p)
{
  double d, d_max = 0, Tnew;
  for (int i = 1 ; i <= p.NI ; i++)
    {
      for (int j = 1; j <= p.NJ ; j++)
        {
          Tnew = (
                  p.D * (p.T[i-1][j] + p.T[i+1][j]) + p.T[i][j+1] + p.T[i][j-1] - p.S[i][j] * p.dy * p.dy
                  )/ 2 / (p.D+1);
          d = Tnew - p.T[i][j];
          d_max = MAX(ABS(d), d_max);
          p.T[i][j] += p.omega * d ;
	}
    }
  return d_max*p.omega;
}

double iterate_ADI(Problem&)
{
    assert(0 && "Not implemented");
}

bool solve(Problem& p, double &d_final, int &n_it, const int log_do)
{

  int log_count = 1, log_pre_count=1;
  double log_d[N_LOG_WRITE];

  for (n_it = 1 ; n_it <= IT_MAX ; n_it++)
    {

      //do an iteration
      ghost_eval_all(p);
      d_final=iterate_jacobi(p);

      //store the error for logging
      if (log_do)
        {
          log_d[log_count-1] = d_final;
          if ( log_count == N_LOG_WRITE  )
            {
              for (int i = 0 ; i < log_count; i++) printf("%-4d %-15e\n",log_pre_count+i,log_d[i]);
              log_pre_count += log_count;
              log_count = 0;
	    }
          log_count++;
	}

      //return if converged
      if(d_final < p.e)
        {
          if (log_do) for (int i = 0 ; i < log_count - 1; i++) printf("%-4d %-15e\n",log_pre_count+i,log_d[i]);
          return true;
	}
    }
  return false;
}
