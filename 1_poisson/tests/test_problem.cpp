#include <poisson.hpp>
#include <cmath>
#include <cstdio>
#include <ctime>

double f1(const double x){ return 1-x;}
double f2(const double y){ return y*y;}
double f3(const double x){ return cos(3.14*x);}
double f4(const double y){ return -y;}
double fs(const double x, const double y) {return x*x - y*y;}

int main(int argc, char *argv[]){

	Problem p;
	//initialize the problem
	problem_setup_c(p, 5, 10,
			BNDDirich, BNDNeu, BNDDirich, BNDNeu,
			f1, f2, f3, f4,
			fs,0.2,1e-7);

	//Print the problem
	//dims
	printf("dims: %d, %d", p.NI, p.NJ);
	//x y and T
	printf("X points:\n");
	array_print_1d(p.x,p.NI+2);
	printf("Y points:\n");
	array_print_1d(p.y,p.NJ+2);
	printf("T values: \n");
	array_print_2d(p.T,p.NI+2,p.NJ+2);
	//boundaries
	boundary_print(p.bnd1);
	boundary_print(p.bnd2);
	boundary_print(p.bnd3);
	boundary_print(p.bnd4);
	//dx and dy
	printf("dx: %.6f, dy: %.6f", p.dx, p.dy);
	//omega and e
	printf("omega: %.6f, e: %f", p.omega, p.e);
	//time
	printf("elapsed time: %f \n", (clock() - p.t_beg) / CLOCKS_PER_SEC);

	//Print using the print function
	problem_print(p);


	//destroy the problem
	problem_destroy(p);
}
