#include <poisson.hpp>
#include <cmath>
#include <cstdio>
#include <ctime>


int main(int argc, char *argv[]){

	double num[6][5];
	double exa[6][5];
	double *num_[6] = {&num[0][0],&num[1][0],&num[2][0],&num[3][0],&num[4][0],&num[5][0]};
	double *exa_[6] = {&exa[0][0],&exa[1][0],&exa[2][0],&exa[3][0],&exa[4][0],&exa[5][0]};

	for (int i = 0 ; i < 6 ; i++)
		for (int j = 0 ; j < 5 ; j++){
			num[i][j] = i+j;
			exa[i][j] = i-j;
		}

	printf("NUM:\n");
	array_print_2d(num_,6,5);
	printf("EXAC:\n");
	array_print_2d(exa_,6,5);

	write_vtk(4,3,num_,exa_,"vtk_test_both");
	write_vtk(4,3,num_,NULL,"vtk_test_num");

}
