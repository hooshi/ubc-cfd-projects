#include <poisson.hpp>
#include <cmath>
#include <cstdio>
#include <ctime>

double fzero(const double x){ return 0;}
double fcospx(const double y){ return cos(PI*y);}
double fanalytic(const double x,const double y) {return cos(PI*x)*sinh(PI*y)/sinh(PI);}
#define NI_VAL 20
#define NJ_VAL 20

int main(int argc, char *argv[]){

	Problem p;
	double d_final, **err, **ana , l2, l1, linf;
	int n_it;
	bool success;

	//initialize the problem
	problem_setup_c(p, NI_VAL, NJ_VAL,
			BNDDirich, BNDNeu, BNDDirich, BNDNeu,
			fzero, fzero, fcospx, fzero,
			NULL,1.5,1e-10);

	//solve the equation
	success = solve(p,d_final,n_it,1);

	//calculate the norms
	if(success){
		printf("SUCCESS :) \n");
		//find the error and l2 norm
		array_create_2d(err,NI_VAL+2, NJ_VAL+2, 0.);
		array_create_2d(ana,NI_VAL+2, NJ_VAL+2, 0.);
		average_values(ana,NI_VAL,NJ_VAL,fanalytic);
		l2 = l1 = linf = 0;
		for (int j = 1 ; j <= NJ_VAL ; j++){
			for (int i = 1 ; i <= NI_VAL ; i++){
					err[i][j] = ana[i][j] - p.T[i][j];
					l2 += err[i][j]*err[i][j];
					l1 += ABS(err[i][j]);
					linf = MAX(err[i][j],linf);
				}
			}
		l1 /= NI_VAL * NJ_VAL;
		l2 /= NI_VAL * NJ_VAL;
		l2 = sqrt(l2);
		//print the norms
		printf("L1:   %e\n", l1);
		printf("L2:   %e\n", l2);
		printf("Linf: %e\n", linf);
		//print the vtk
		write_vtk(p.NI,p.NJ,p.T,err,"solveall");
		//destroy the arrays
		array_destroy_2d(err,NI_VAL+2);
		array_destroy_2d(ana, NI_VAL+2);
	}
	else printf("FAILURE :( \n");
	printf("Number of iterations: %d\n", n_it);
	printf("Max change in last iteration: %e\n", d_final);

	//destroy the problem
	problem_destroy(p);

}
