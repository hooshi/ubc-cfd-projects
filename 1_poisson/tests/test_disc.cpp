#include <poisson.hpp>
#include <cmath>
#include <cstdio>

int main(int argc, char *argv[]){

	double x[100],dx;
	int N[] = {5,2,20};
	const int length = 3;
	for (int i = 0 ; i < length ; i++){
		disc_axis(x,dx,N[i]);
		printf("N:%d, dz:%f \n", N[i], dx);
		for (int j=0 ; j < N[i]+2 ; j++) printf("%.3f  ",x[j]);
		printf("\n");
	}
}
