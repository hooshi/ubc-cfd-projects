#include <poisson.hpp>
#include <cmath>
#include <cstdio>

double f1(const double x, const double y){
	return -y*cos(x);
}
double I1(const double a1, const double a2,
		const double b1, const double b2){
	return ( sin(a1) - sin(a2) )*(b2*b2 - b1 * b1) /2.;
}
double f2(const double x, const double y){
	return 1+x*x*y*y;
}
double I2(const double a1, const double a2,
		const double b1, const double b2){
	return (a2-a1)*(b2-b1)+(a2*a2*a2 - a1*a1*a1)*(b2*b2*b2 - b1*b1*b1)/9.;
}

typedef double (*IFunc)(const double,const double,const double,const double);

int main(int argc, char *argv[]){

	//set the functions
	int n_f=2, n_p=6;
	double anal, num;
	IFunc integ[]={I1,I2};
	SourceFunc src[] = {f1,f2};
	double x[][4] = {{1,1.1,2,2.1},{1,1.01,2,2.01},{1,1.001,2,2.001},
	{1,1.1,2,2.05},{1,1.01,2,2.005},{1,1.001,2,2.0005}};

	//integrate the stuff
	for (int i = 0 ; i < n_f ; i++){
		printf("INTEGRATING %dst function:\n",i+1);
		for (int j = 0 ; j < n_p ; j++){
			anal = integ[i](x[j][0],x[j][1],x[j][2],x[j][3]);
			num = gauss_quadrature(src[i], x[j][0],x[j][1],x[j][2],x[j][3]);
			printf("\t anal: %-18.10f num: %-18.10f, error: %-18e\n", anal, num, ABS(anal-num));
		}
	}

	return 0;
}
