#include <poisson.hpp>
#include <cmath>
#include <cstdio>
#include <ctime>

double f(const double x,const double y) {return cos(PI*x)*sinh(PI*y)/sinh(PI);}
#define NI_VAL 20
#define NJ_VAL 20

int main(int argc, char *argv[]){

	double **gauss, **center;
	double l2 = 0;
	array_create_2d(center, NI_VAL+2, NJ_VAL+2, 0.);
	array_create_2d(gauss, NI_VAL+2, NJ_VAL+2, 0.);


	average_values(gauss, NI_VAL, NJ_VAL, f);

	for (int j = 1 ; j <= NJ_VAL ; j++){
		for (int i = 1 ; i <= NI_VAL ; i++){
				center[i][j] = gauss[i][j] - f((i-.5)/NI_VAL,(j-.5)/NJ_VAL);
				l2 += center[i][j]*center[i][j];
			}
		}
	l2 /= NI_VAL * NJ_VAL;
	l2 = sqrt(l2);

	write_vtk(NI_VAL, NJ_VAL,gauss, center, "midvsgauss");
	printf("L2 = %e\n", l2);

	array_destroy_2d(center, NI_VAL+2);
	array_destroy_2d(gauss, NI_VAL+2);
}
