#include <poisson.hpp>
#include <cmath>
#include <cstdio>
#include <ctime>

double f1(const double x){ return 1-x;}
double f2(const double y){ return 1-y;}
double f3(const double x){ return 2-2*x;}
double f4(const double y){ return 2-2*y;}
double fsource(const double x, const double y){return 0;}

int main(int argc, char *argv[]){

	Problem p;
	double d_max;

	//initialize the problem
	problem_setup_c(p, 5, 4,
			BNDNeu, BNDNeu, BNDDirich, BNDDirich,
			f1, f2, f3, f4,
			fsource,1,1e-7);

	//Print using the print function
	problem_print(p);

	//set the ghost values
	ghost_eval_all(p);
	//print the new T
	printf("VALUES AFTER GHOST CELL EVALUATION:\n");
	array_print_2d(p.T,p.NI+2,p.NJ+2);
	printf("\n");

	//first iteration
	printf("VALUES AFTER FIRST ITERATION\n");
	d_max = solve_iterate(p);
	array_print_2d(p.T,p.NI+2,p.NJ+2);
	printf("d_max: %f\n", d_max);

	//destroy the problem
	problem_destroy(p);
}
