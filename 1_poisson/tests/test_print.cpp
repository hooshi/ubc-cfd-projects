#include <poisson.hpp>
#include <cmath>
#include <cstdio>

int main(int argc, char *argv[]){

	double *x[6];
	x[0] = new double [8];
	x[1] = new double [8];
	x[2] = new double [8];
	x[3] = new double [8];
	x[4] = new double [8];
	x[5] = new double [8];

	for (int i = 0 ; i < 6 ; i++)
		for (int j = 0 ; j < 8 ; j++)
			x[i][j] = i-j;


	array_print_2d(x, 6, 8);
	array_print_1d(x[1], 8);


	delete[] x[0] ;
	delete[] x[1] ;
	delete[] x[2] ;
	delete[] x[3] ;
	delete[] x[4] ;
	delete[] x[5] ;

	return 0;
}
