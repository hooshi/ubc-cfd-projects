#include <poisson.hpp>
#include <cmath>
#include <cstdio>

double f1(const double x){return cos(3.14*x); }
double f2(const double x){return x; }

int main(int argc, char *argv[]){

	//create boundaries
	BoundaryEdge bnd1, bnd2;
	//set values to them
	boundary_read_c(bnd1, BNDNeu, f1);
	boundary_read_c(bnd2, BNDDirich, f2);
	//print to see everything is ok
	boundary_print(bnd1);
	boundary_print(bnd2);
	//exit
	return 0;
}
