#!/bin/bash
set -x #echo on
echo "Running the laplace equation assignments"


## First assignment
## 10x10 with no over-relaxation
../laplace 10 10 1e-7 1 10x10w10 > 10x10w10.log

## Second assignment
## 10x10 with over-relaxation
../laplace 10 10 1e-7 1.5 10x10w15 > 10x10w15.log

## Third assignment
## comparison of convergance behaviour for different values of \omega
../laplace 20 20 1e-7 1 20x20w10 > 20x20w10.log
../laplace 20 20 1e-7 1.3 20x20w13 > 20x20w13.log
../laplace 20 20 1e-7 1.5 20x20w15 > 20x20w15.log


## Forth assignment
## L2-norm against mesh size
../laplace 10 10 1e-9 1.5 10x10e9 > 10x10e9.log
../laplace 20 20 1e-9 1.5 20x20e9 > 20x20e9.log
../laplace 40 40 1e-9 1.5 40x40e9 > 40x40e9.log
../laplace 80 80 1e-9 1.5 80x80e9 > 80x80e9.log
../laplace 160 160 1e-9 1.5 160x160e9 > 160x160e9.log

## Fifth assignment
## Pressure problem
../pressure 40 40 1e-10 1.5 p40 > p40.log
../pressure 80 80 1e-10 1.5 p80 > p80.log
../pressure 160 160 1e-10 1.5 p160 > p160.log
#../pressure 320 320 1e-8 1.5 p320 > p320.log

