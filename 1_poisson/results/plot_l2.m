## This file plots the L norms as a function of mesh size and
## also reports the order of accuracy.

n_mesh = 5;
L1 = [1.2497e-3 3.2040e-4 8.0698e-5 2.0218e-5 5.4808e-6 ];
L2 = [2.4570e-3 6.3861e-4 1.6122e-4 4.0404e-5 1.0180e-5];
Linf = [9.0957e-3 2.6780e-3 7.2048e-4 1.8646e-4 4.7389e-5];
NI = [10 20 40 80 160]

##plot the stuff
hold on;
loglog(NI,L1,"k-","linewidth", 2);
loglog(NI,L2,"k--","linewidth", 2);
loglog(NI,Linf,"k-.","linewidth", 2);
hold off;

## add some details
hx=xlabel("N cells");
hy=ylabel("Error norm");
htitle=title("Effect of dx in convergence");
hlegend=legend("L1","L2", "Linf" ,"location","northeast");
axis([10 10^2.5 10^-5.5 1e-2]);

##set the fonts right
set(gca,  "fontsize", 15);
set(hx, "fontsize", 20, "linewidth", 2);
set(hy, "fontsize", 20, "linewidth", 2);
set(htitle,"fontsize", 20);
set(hlegend,"fontsize",15);

## save the plot
print("handin/n-performance.eps","-deps","-FArial","-tight");

##print the slope of the curves
p1 = polyfit (log10(NI), log10(L1),1);
p2 = polyfit (log10(NI), log10(L2),1);
pinf = polyfit (log10(NI), log10(Linf),1);
printf("Slopes are L1=%.4f, L2=%.4f, Linf=%.4f\n", p1(1), p2(1), pinf(1));
