## This file plots the diagram for effect of w in convergence

1;

## plot for w = 1
  x = load("-ascii","20x20w10.log");
semilogy(x(:,1),x(:,2),"k-","linewidth", 2);
hold on;

## plot for w = 1.3
x = load("-ascii","20x20w13.log");
semilogy(x(:,1),x(:,2),"k--","linewidth", 2);

## plot for w = 1.5
x = load("-ascii","20x20w15.log");
semilogy(x(:,1),x(:,2),"k-.","linewidth", 2);
hold off;

## add some details
hx=xlabel("Iteration number");
hy=ylabel("Maximum change in solution");
htitle=title("Effect of w in convergence");
hlegend=legend("w=1","w=1.3", "w=1.5" ,"location","northeast");
axis([0 700 1e-7 1]);

##set the fonts right
set(gca,  "fontsize", 15);
set(hx, "fontsize", 20, "linewidth", 2);
set(hy, "fontsize", 20, "linewidth", 2);
set(htitle,"fontsize", 20);
set(hlegend,"fontsize",25);

## save the plot
print("handin/w-performance.eps","-deps","-FArial","-tight");
