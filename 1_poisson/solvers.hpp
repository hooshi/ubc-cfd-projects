/** solvers.hpp
 *  Here lie the declaration of solvers.
 *  Shayan Hoshyari
 *  October 2015
 */

#ifndef SOLVERS_HPP
#define SOLVERS_HPP

#include "poisson.hpp"

enum SmootherType
{
    SMOOTHER_ADI,
    SMOOTHER_JACOBI,
    SMOOTHER_GAUSS_SEIDEL
};

enum CycleType
{
    CYCLE_TYPE_V,
    CYCLE_TYPE_W,
    CYCLE_TYPE_SMOOTHER_ONLY
};

struct MultiGridSolver
{
    SmootherType smoother_type;
    CycleType    cycle_type;
    int          n_cycles;

};
    
//do one single SOR iteration
double iterate_jacobi(Problem&);

//do one single ADI iteration
double iterate_ADI(Problem&);

// Solve a problem
// input-Problem: the Poisson problem to be solved
// output-d_final: the final maximum change in solution
// output-n_it: the final maximum change in solution
// const int - 0 : should the maximum value of solution at each iteration be printed??
bool solve(Problem&, double &d_final, int &n_it, const int = 0);

#endif /*SOLVERS_HPP*/



/*
  Local Variables:
  mode:c++
  c-file-style:"GNU"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/


